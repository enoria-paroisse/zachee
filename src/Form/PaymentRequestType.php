<?php

namespace App\Form;

use App\Controller\Traits\UserableTrait;
use App\Entity\Organization;
use App\Entity\PaymentRequest;
use App\Entity\Person;
use App\Repository\PersonRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @extends AbstractType<mixed>
 */
class PaymentRequestType extends AbstractType
{
    use UserableTrait;

    private ?UserInterface $user;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->user = $tokenStorage->getToken()?->getUser();
    }

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('datetime', DateType::class, [
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control',
                ],
                'label' => 'Date',
            ])
            ->add('description')
            ->add('amount', MoneyType::class, [
                'help' => 'A negative amount indicates that it is the association which must pay it',
            ])
            ->add('organization', EntityType::class, [
                'class' => Organization::class,
                'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('o')
                    ->select(['o', 'd'])
                    ->innerJoin('o.diocese', 'd')
                    ->orderBy('d.name', 'ASC')
                    ->addOrderBy('o.name', 'ASC'),
                'choice_label' => fn (Organization $organization) => $organization->getDiocese()?->getDeptCode().' '.$organization->getDiocese().' : '.$organization,
                'required' => false,
            ])
            ->add('person', EntityType::class, [
                'class' => Person::class,
                'query_builder' => fn (PersonRepository $personRepository) => $personRepository->createQueryBuilder('p')
                    ->select(['p', 'u'])
                    ->leftJoin('p.user', 'u')
                    ->orderBy('p.lastname', 'ASC')
                    ->addOrderBy('p.firstname', 'ASC'),
                'required' => false,
            ])
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
                /** @var PaymentRequest $paymentRequest */
                $paymentRequest = $event->getData();
                if (null !== $paymentRequest->getLastReminder()) {
                    $event->getForm()->add('lastReminder', DateType::class, [
                        'widget' => 'single_text',
                        'attr' => [
                            'class' => 'form-control',
                        ],
                        'required' => false,
                    ]);
                }
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => PaymentRequest::class,
            'translation_domain' => 'forms',
        ]);
    }
}
