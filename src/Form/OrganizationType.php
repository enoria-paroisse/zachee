<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\Diocese;
use App\Entity\Organization;
use App\Repository\DioceseRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Intl\Countries;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @extends AbstractType<mixed>
 */
class OrganizationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('address', AddressType::class)
            ->add('type', null, [
                'attr' => ['class' => 'js-choice'],
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
            $organization = $event->getData();
            if ($organization instanceof Organization && ((null !== $organization->getDiocese() && $organization !== $organization->getDiocese()->getAssociatedOrganization()) || null === $organization->getDiocese())) {
                $event->getForm()->add('diocese', EntityType::class, [
                    'class' => Diocese::class,
                    'query_builder' => fn (DioceseRepository $dioceseRepository) => $dioceseRepository->createQueryBuilder('d')
                        ->orderBy('d.name', 'ASC'),
                    'choice_label' => fn (Diocese $diocese) => $diocese->getName()
                        .($diocese->getDeptCode() ? ' ('.$diocese->getDeptCode().')' : '')
                        .('FR' !== $diocese->getCountry() ? ' ('.Countries::getName($diocese->getCountry()).')' : ''),
                    'attr' => ['class' => 'js-choice'],
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Organization::class,
            'translation_domain' => 'forms',
        ]);
    }
}
