<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @extends AbstractType<mixed>
 */
class PublicLoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('lastname', null, [
                'label' => 'Family name',
            ])
            ->add('email', EmailType::class, [
                'required' => false,
            ])
            ->add('phone', PhoneNumberType::class, [
                'required' => false,
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event): void {
            $data = $event->getData();
            $data['phone'] = $data['phone_full'];
            $event->setData($data);
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'translation_domain' => 'forms',
            'allow_extra_fields' => 'phone_full',
        ]);
    }
}
