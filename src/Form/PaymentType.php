<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\Organization;
use App\Entity\Payment;
use App\Entity\Person;
use App\Repository\PersonRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @extends AbstractType<mixed>
 */
class PaymentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('datetime', DateType::class, [
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control',
                ],
                'label' => 'Date',
            ])
            ->add('type', ChoiceType::class, [
                'choices' => [
                    Payment::TYPE_TRANSFER,
                    Payment::TYPE_CHECK,
                    Payment::TYPE_CASH,
                    Payment::TYPE_CB,
                ],
                'choice_name' => fn ($choice) => $choice,
                'choice_label' => fn ($choice) => 'payment.type.'.$choice,
                'attr' => ['class' => 'js-choice'],
            ])
            ->add('checkNumber')
            ->add('checkIssuer')
            ->add('checkBank')
            ->add('amount', MoneyType::class)
            ->add('person', EntityType::class, [
                'class' => Person::class,
                'query_builder' => fn (PersonRepository $personRepository) => $personRepository->createQueryBuilder('p')
                    ->select(['p', 'u'])
                    ->leftJoin('p.user', 'u')
                    ->orderBy('p.lastname', 'ASC')
                    ->addOrderBy('p.firstname', 'ASC'),
                'required' => false,
            ])
            ->add('organization', EntityType::class, [
                'class' => Organization::class,
                'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('o')
                    ->select(['o', 'd'])
                    ->innerJoin('o.diocese', 'd')
                    ->orderBy('d.name', 'ASC')
                    ->addOrderBy('o.name', 'ASC'),
                'choice_label' => fn (Organization $organization) => $organization->getDiocese()?->getDeptCode().' '.$organization->getDiocese().' : '.$organization,
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Payment::class,
            'translation_domain' => 'forms',
        ]);
    }
}
