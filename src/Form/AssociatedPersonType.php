<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\AssociatedPerson;
use App\Entity\Person;
use App\Entity\TypeAssociatedPerson;
use App\Repository\PersonRepository;
use App\Repository\TypeAssociatedPersonRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @extends AbstractType<mixed>
 */
class AssociatedPersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', EntityType::class, [
                'class' => TypeAssociatedPerson::class,
                'query_builder' => fn (TypeAssociatedPersonRepository $typeRepository) => $typeRepository->createQueryBuilder('t')
                    ->select(['t'])
                    ->orderBy('t.name', 'ASC'),
                'label' => 'Role',
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
            $associatedPerson = $event->getData();
            if (!$associatedPerson || !($associatedPerson instanceof AssociatedPerson) || null === $associatedPerson->getId()) {
                $event->getForm()->add('person', EntityType::class, [
                    'class' => Person::class,
                    'query_builder' => fn (PersonRepository $personRepository) => $personRepository->createQueryBuilder('p')
                        ->select(['p', 'u'])
                        ->leftJoin('p.user', 'u')
                        ->orderBy('p.lastname', 'ASC')
                        ->addOrderBy('p.firstname', 'ASC'),
                    'attr' => ['class' => 'js-choice'],
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AssociatedPerson::class,
            'translation_domain' => 'forms',
        ]);
    }
}
