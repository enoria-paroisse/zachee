<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\Group;
use App\Entity\Person;
use App\Repository\PersonRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @extends AbstractType<mixed>
 */
class GroupAddType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('members', EntityType::class, [
                'class' => Person::class,
                'query_builder' => fn (PersonRepository $personRepository) => $personRepository->createQueryBuilder('p')
                    ->select(['p', 'u'])
                    ->leftJoin('p.user', 'u')
                    ->leftJoin('p.memberGroups', 'g')
                    ->where('g.id != :group')
                    ->orWhere('g.id IS NULL')
                    ->orderBy('p.lastname', 'ASC')
                    ->addOrderBy('p.firstname', 'ASC')
                    ->setParameter('group', $options['group']->getId()),
                'attr' => ['class' => 'js-choice'],
                'required' => true,
                'multiple' => true,
                'help' => 'Show only no members',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'translation_domain' => 'forms',
            'group' => new Group(),
        ]);
    }
}
