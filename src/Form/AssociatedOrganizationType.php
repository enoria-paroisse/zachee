<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\AssociatedPerson;
use App\Entity\Organization;
use App\Entity\TypeAssociatedPerson;
use App\Repository\OrganizationRepository;
use App\Repository\TypeAssociatedPersonRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @extends AbstractType<mixed>
 */
class AssociatedOrganizationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', EntityType::class, [
                'class' => TypeAssociatedPerson::class,
                'query_builder' => fn (TypeAssociatedPersonRepository $typeRepository) => $typeRepository->createQueryBuilder('t')
                    ->select(['t'])
                    ->orderBy('t.name', 'ASC'),
                'label' => 'Role',
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
            $associatedPerson = $event->getData();
            if (!$associatedPerson || !($associatedPerson instanceof AssociatedPerson) || null === $associatedPerson->getId()) {
                $event->getForm()->add('organization', EntityType::class, [
                    'class' => Organization::class,
                    'query_builder' => fn (OrganizationRepository $organizationRepository) => $organizationRepository->createQueryBuilder('o')
                        ->select(['o', 'd'])
                        ->innerJoin('o.diocese', 'd')
                        ->orderBy('d.name', 'ASC')
                        ->addOrderBy('o.name', 'ASC'),
                    'choice_label' => fn (Organization $organization) => $organization->getDiocese().' : '.$organization,
                    'attr' => ['class' => 'js-choice'],
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AssociatedPerson::class,
            'translation_domain' => 'forms',
        ]);
    }
}
