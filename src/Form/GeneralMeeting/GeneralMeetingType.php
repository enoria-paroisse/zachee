<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form\GeneralMeeting;

use App\Entity\Doc;
use App\Entity\GeneralMeeting;
use App\Repository\DocRepository;
use App\Repository\FolderRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @extends AbstractType<mixed>
 */
class GeneralMeetingType extends AbstractType
{
    public function __construct(
        private readonly DocRepository $docRepository,
        private readonly FolderRepository $folderRepository,
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => [
                    GeneralMeeting::TYPE_ORDINARY => GeneralMeeting::TYPE_ORDINARY,
                    GeneralMeeting::TYPE_EXTRAORDINARY => GeneralMeeting::TYPE_EXTRAORDINARY,
                ],
            ])
            ->add('datetime', DateTimeType::class, [
                'date_widget' => 'single_text',
                'time_widget' => 'single_text',
                'label' => 'Date',
            ])
            ->add('meetingLink')
            ->add('meetingId')
            ->add('meetingPasscode')
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
                $generalMeeting = $event->getData();
                $docs = $this->findDocs();
                $docsSummons = array_filter($docs, static function (Doc $doc) use ($generalMeeting) {
                    if (in_array('ag-multi', $doc->getTags(), true)) {
                        return true;
                    }
                    if (0 === $doc->getGeneralMeetingsSummons()->count() && 0 === $doc->getGeneralMeetingsReport()->count()) {
                        return true;
                    }

                    return $doc->getGeneralMeetingsSummons()->contains($generalMeeting);
                });
                $docsReport = array_filter($docs, static function (Doc $doc) use ($generalMeeting) {
                    if (in_array('ag-multi', $doc->getTags(), true)) {
                        return true;
                    }
                    if (0 === $doc->getGeneralMeetingsSummons()->count() && 0 === $doc->getGeneralMeetingsReport()->count()) {
                        return true;
                    }

                    return $doc->getGeneralMeetingsReport()->contains($generalMeeting);
                });

                $event->getForm()->add('summonsDocs', EntityType::class, [
                    'class' => Doc::class,
                    'attr' => ['class' => 'js-choice'],
                    'choices' => $docsSummons,
                    'multiple' => true,
                    'required' => false,
                ])
                    ->add('reportDocs', EntityType::class, [
                        'class' => Doc::class,
                        'attr' => ['class' => 'js-choice'],
                        'choices' => $docsReport,
                        'multiple' => true,
                        'required' => false,
                    ])
                ;
            })
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event): void {
                foreach ($event->getData()->getSummonsDocs() as $summonsDoc) {
                    $summonsDoc->addGeneralMeetingSummons($event->getData());
                }
                foreach ($event->getData()->getReportDocs() as $reportDoc) {
                    $reportDoc->addGeneralMeetingReport($event->getData());
                }
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => GeneralMeeting::class,
            'translation_domain' => 'forms',
        ]);
    }

    /**
     * @return Doc[]
     */
    private function findDocs(): array
    {
        $docs = $this->docRepository->findByTags(['ag', 'ag-multi'], false);
        $subDocs = [$docs];
        foreach ($this->folderRepository->findByTags(['ag', 'ag-multi'], false) as $folder) {
            $subDocs[] = $folder->getSubDocs();
        }
        $docs = \call_user_func_array('array_merge', $subDocs);

        return array_unique((array) $docs);
    }
}
