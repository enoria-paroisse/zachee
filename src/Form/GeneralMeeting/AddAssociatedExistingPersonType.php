<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form\GeneralMeeting;

use App\Entity\AssociatedPerson;
use App\Entity\Person;
use App\Repository\PersonRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @extends AbstractType<mixed>
 */
class AddAssociatedExistingPersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (null === $options['types']) {
            $builder
                ->add('type', null, [
                    'label' => 'Role',
                ])
            ;
        } else {
            $builder
                ->add('type', ChoiceType::class, [
                    'label' => 'Role',
                    'choices' => $options['types'],
                    'choice_label' => function ($choice) {
                        return $choice->getName();
                    },
                    'choice_translation_domain' => false,
                ])
            ;
        }

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
            $associatedPerson = $event->getData();
            if ($associatedPerson instanceof AssociatedPerson) {
                $event->getForm()->add('person', EntityType::class, [
                    'class' => Person::class,
                    'query_builder' => fn (PersonRepository $personRepository) => $personRepository->createQueryBuilder('p')
                        ->select(['p', 'ao', 'o'])
                        ->leftJoin('p.associatedOrganizations', 'ao')
                        ->leftJoin('ao.organization', 'o')
                        ->where('o.id = :organization')
                        ->setParameter('organization', $associatedPerson->getOrganization()),
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AssociatedPerson::class,
            'translation_domain' => 'forms',
            'types' => null,
        ]);
    }
}
