<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form\GeneralMeeting;

use App\Entity\PaymentRequest;
use App\Entity\Summons;
use App\Repository\SummonsRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @extends AbstractType<mixed>
 */
class MandateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
            $summons = $event->getData();
            if ($summons instanceof Summons) {
                $event->getForm()->add('mandate', EntityType::class, [
                    'class' => Summons::class,
                    'query_builder' => fn (SummonsRepository $summonsRepository) => $summonsRepository->createQueryBuilder('s')
                        ->select(['s', 'm', 'pr', 'prf', 'prdms', 'o', 'p', 'u', 'mandate'])
                        ->leftJoin('s.membership', 'm')
                        ->leftJoin('m.paymentRequest', 'pr')
                        ->leftJoin('pr.file', 'prf')
                        ->leftJoin('pr.dioceseMembershipSupplement', 'prdms')
                        ->leftJoin('m.organization', 'o')
                        ->leftJoin('m.person', 'p')
                        ->leftJoin('p.user', 'u')
                        ->leftJoin('s.mandates', 'mandate')
                        ->where('s.generalMeeting = :generalMeeting')
                        ->setParameter('generalMeeting', $summons->getGeneralMeeting()),
                    'choice_label' => function (Summons $choice) {
                        if ($choice->getMembership()?->getOrganization()) {
                            return '🏢 '.$choice->getMembership()->getOrganization()->getName();
                        }

                        if ('F' === $choice->getMembership()?->getPerson()?->getSex()) {
                            return '👩 '.$choice->getMembership()->getPerson()->getfullname();
                        }

                        return '👨 '.$choice->getMembership()?->getPerson()?->getfullname();
                    },
                    'choice_attr' => function (Summons $choice) {
                        if (2 <= count($choice->getMandates())) {
                            return ['disabled' => 'disabled'];
                        }

                        if (null === $choice->getMembership() || null === $choice->getMembership()->getPaymentRequest()
                            || PaymentRequest::STATUS_COMPLETED !== $choice->getMembership()->getPaymentRequest()->getStatus()) {
                            return ['disabled' => 'disabled'];
                        }

                        return [];
                    },
                    'required' => false,
                    'attr' => ['class' => 'js-choice'],
                    'help' => 'mandate.help',
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Summons::class,
            'translation_domain' => 'forms',
        ]);
    }
}
