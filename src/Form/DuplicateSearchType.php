<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContext;

/**
 * @extends AbstractType<mixed>
 */
class DuplicateSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', CheckboxType::class, [
                'label' => 'Email',
                'attr' => [
                    'class' => 'selectable-check',
                ],
                'label_attr' => [
                    'class' => 'checkbox-switch',
                ],
                'required' => false,
            ])
            ->add('phone', CheckboxType::class, [
                'label' => 'Phone',
                'attr' => [
                    'class' => 'selectable-check',
                ],
                'label_attr' => [
                    'class' => 'checkbox-switch',
                ],
                'required' => false,
            ])
            ->add('lastname', CheckboxType::class, [
                'label' => 'Lastname',
                'attr' => [
                    'class' => 'selectable-check',
                ],
                'label_attr' => [
                    'class' => 'checkbox-switch',
                ],
                'required' => false,
            ])
            ->add('firstname', CheckboxType::class, [
                'label' => 'Firstname',
                'attr' => [
                    'class' => 'selectable-check',
                ],
                'label_attr' => [
                    'class' => 'checkbox-switch',
                ],
                'required' => false,
            ])
            ->add('sex', CheckboxType::class, [
                'label' => 'Sex',
                'attr' => [
                    'class' => 'selectable-check',
                ],
                'label_attr' => [
                    'class' => 'checkbox-switch',
                ],
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'constraints' => [
                new Callback(function (array $data, ExecutionContext $context): void {
                    if (empty(array_filter($data))) {
                        $context->buildViolation('Veuillez choisir au moins une valeur')->addViolation();
                    }
                }),
            ],
            'translation_domain' => 'forms',
        ]);
    }
}
