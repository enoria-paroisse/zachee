<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\Aggregate;
use App\Entity\Membership;
use App\Entity\Newsletter;
use App\Repository\NewsletterRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @extends AbstractType<mixed>
 */
class AggregateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('newsletters', EntityType::class, [
                'class' => Newsletter::class,
                'query_builder' => fn (NewsletterRepository $newsletterRepository) => $newsletterRepository->createQueryBuilder('n')
                    ->select(['n', 'f'])
                    ->leftJoin('n.folder', 'f')
                    ->where('n.exist = 1')
                    ->orderBy('f.name', 'ASC')
                    ->addOrderBy('n.name', 'ASC'),
                'choice_label' => fn (Newsletter $newsletter) => ($newsletter->getFolder() ? $newsletter->getFolder()->getName().' : ' : ' ').$newsletter->getName(),
                'attr' => ['class' => 'js-choice'],
                'multiple' => true,
                'required' => false,
            ])
            ->add('personOrOrganization', ChoiceType::class, [
                'choices' => [
                    Aggregate::PERSON => Aggregate::PERSON,
                    Aggregate::ORGANIZATION => Aggregate::ORGANIZATION,
                ],
                'attr' => ['class' => 'js-choice'],
            ])
            ->add('membership', ChoiceType::class, [
                'choices' => [
                    Membership::STATUS_VALIDATED => Membership::STATUS_VALIDATED,
                    Membership::STATUS_IN_PROGRESS => Membership::STATUS_IN_PROGRESS,
                    Membership::STATUS_CANCELED => Membership::STATUS_CANCELED,
                    'is_first' => 'is_first',
                    'not_first' => 'not_first',
                    'non_member' => 'non_member',
                ],
                'multiple' => true,
                'required' => false,
                'attr' => ['class' => 'js-choice'],
                'translation_domain' => 'membership',
            ])
            ->add('organizationTypes', null, [
                'attr' => ['class' => 'js-choice'],
            ])
            ->add('associatedPersonTypes', null, [
                'attr' => ['class' => 'js-choice'],
                'label' => 'Roles',
            ])
            ->add('personGroups', null, [
                'attr' => ['class' => 'js-choice'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Aggregate::class,
            'translation_domain' => 'forms',
        ]);
    }
}
