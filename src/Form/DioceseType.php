<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\Diocese;
use App\Entity\Organization;
use App\Entity\SubOrganizationMembershipFee;
use App\Repository\OrganizationRepository;
use App\Repository\SubOrganizationMembershipFeeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @extends AbstractType<mixed>
 */
class DioceseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('wasselynck', CollectionType::class, [
                'entry_type' => WasselynckType::class,
            ])
            ->add('deptCode')
            ->add('country', CountryType::class, [
                'attr' => ['class' => 'js-choice'],
                'required' => false,
                'preferred_choices' => ['FR'],
            ])
            ->add('subOrganizationMembershipFee', EntityType::class, [
                'class' => SubOrganizationMembershipFee::class,
                'attr' => ['class' => 'js-choice'],
                'query_builder' => function (SubOrganizationMembershipFeeRepository $repository) {
                    return $repository->createQueryBuilder('subOrganizationMembershipFee')->orderBy('subOrganizationMembershipFee.name', 'ASC');
                },
            ])
            // Si le pays n'a pas été précisé, ce sera la France
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event): void {
                $diocese = (array) $event->getData();
                if ($diocese && '' === $diocese['country']) {
                    $diocese['country'] = 'FR';
                    $event->setData($diocese);
                }
            })
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
            $diocese = $event->getData();
            if ($diocese instanceof Diocese && null !== $diocese->getId()) {
                $event->getForm()->add('associatedOrganization', EntityType::class, [
                    'class' => Organization::class,
                    'query_builder' => fn (OrganizationRepository $organizationRepository) => $organizationRepository->createQueryBuilder('o')
                        ->where('o.diocese = :diocese')
                        ->orderBy('o.name', 'ASC')
                        ->setParameter('diocese', $diocese->getId()),
                    'required' => false,
                    'attr' => ['class' => 'select2'],
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Diocese::class,
            'translation_domain' => 'forms',
        ]);
    }
}
