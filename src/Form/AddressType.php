<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @extends AbstractType<mixed>
 */
class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('addressLineA', null, [
                'empty_data' => '',
            ])
            ->add('addressLineB', null, [
                'empty_data' => '',
            ])
            ->add('city', null, [
                'empty_data' => '',
            ])
            ->add('postalCode', null, [
                'empty_data' => '',
            ])
            ->add('country', CountryType::class, [
                'attr' => ['class' => 'js-choice'],
                'required' => false,
                'preferred_choices' => ['FR'],
            ])
            // Si le pays n'a pas été précisé, ce sera la France
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event): void {
                $address = (array) $event->getData();
                if ($address && '' === $address['country'] && ('' !== $address['addressLineA'] || '' !== $address['addressLineB'] || '' !== $address['postalCode'] || '' !== $address['city'])) {
                    $address['country'] = 'FR';
                    $event->setData($address);
                }
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
            'translation_domain' => 'forms',
        ]);
    }
}
