<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\CaMember;
use App\Entity\Person;
use App\Repository\PersonRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @extends AbstractType<mixed>
 */
class CaMemberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('role', null, [
                'required' => false,
            ])
            ->add('person', EntityType::class, [
                'class' => Person::class,
                'query_builder' => fn (PersonRepository $er) => $er->createQueryBuilder('p')
                    ->select(['p', 'u'])
                    ->leftJoin('p.user', 'u')
                    ->where('p.email IS NOT NULL')
                    ->orderBy('p.lastname', 'ASC')
                    ->addOrderBy('p.firstname', 'ASC'),
                'attr' => ['class' => 'js-choice'],
            ])
            ->add('begin', DateType::class, [
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control',
                ],
                'label' => 'Ca member begin',
                'required' => true,
            ])
            ->add('birthDate', DateType::class, [
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control',
                ],
                'required' => false,
            ])
            ->add('birthCity', null, [
                'required' => false,
            ])
            ->add('birthDepartment', null, [
                'help' => '99 for abroad',
                'required' => false,
            ])
            ->add('birthCountry', CountryType::class, [
                'attr' => ['class' => 'js-choice'],
                'required' => false,
                'preferred_choices' => ['FR'],
            ])
            ->add('nationality', null, [
                'required' => false,
            ])
            ->add('profession', null, [
                'required' => false,
            ])
        ;

        if ($options['edit']) {
            $builder
                ->add('end', DateType::class, [
                    'widget' => 'single_text',
                    'attr' => [
                        'class' => 'form-control',
                    ],
                    'label' => 'Ca member end',
                    'required' => false,
                ])
                ->add('removeInformations', CheckboxType::class, [
                    'label_attr' => ['class' => 'checkbox-switch'],
                    'mapped' => false,
                    'required' => false,
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CaMember::class,
            'translation_domain' => 'forms',
            'edit' => false,
        ]);
    }
}
