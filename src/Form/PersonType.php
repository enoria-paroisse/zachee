<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\Person;
use App\Entity\Title;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;

/**
 * @extends AbstractType<mixed>
 */
class PersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstname', null, [
                'disabled' => $options['secure_name'],
            ])
            ->add('lastname', null, [
                'attr' => ['class' => 'smallcaps'],
                'disabled' => $options['secure_name'],
            ])
            ->add('sex', ChoiceType::class, [
                'choices' => [
                    'Male' => 'M',
                    'Female' => 'F',
                ],
                'attr' => ['class' => 'sex-switch'],
            ])
            ->add('email', EmailType::class, [
                'required' => false,
            ])
            ->add('phone', PhoneNumberType::class, [
                'required' => false,
            ])
            ->add('title', EntityType::class, [
                'class' => Title::class,
                'choice_label' => fn (Title $title) => $title->getName(),
                'choice_attr' => fn (Title $title) => ['data-sex' => $title->getSex()],
            ])
            ->add('address', AddressType::class, [
                'constraints' => new Valid(),
                'required' => false,
            ])
            ->add('phone_full', HiddenType::class, [
                'mapped' => false,
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event): void {
            $data = $event->getData();
            $data['phone'] = $data['phone_full'];
            $event->setData($data);

            if ((array_key_exists('deleteAddress', $data) && $data['deleteAddress'] && $event->getForm()->getConfig()->getOption('delete_address', false)) || !array_key_exists('address', $data)) {
                $event->getForm()->remove('address');
                unset($data['address']);
                $event->setData($data);
            }
        })
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
                if ($event->getForm()->getConfig()->getOption('delete_address', false)) {
                    $event->getForm()
                        ->add('deleteAddress', CheckboxType::class, [
                            'required' => false,
                            'label_attr' => ['class' => 'checkbox-switch'],
                            'mapped' => false,
                        ])
                    ;
                }
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Person::class,
            'translation_domain' => 'forms',
            'secure_name' => false,
            'delete_address' => false,
        ]);
    }
}
