<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\PaymentRequest;
use App\Entity\SmsBilling;
use App\Repository\PaymentRequestRepository;
use App\Repository\PeriodRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @extends AbstractType<mixed>
 */
class SmsBillingPaymentRequestType extends AbstractType
{
    public function __construct(private PeriodRepository $periodRepository)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('paymentRequest', EntityType::class, [
                'class' => PaymentRequest::class,
                'query_builder' => fn (PaymentRequestRepository $pr) => $pr->getQueryByPeriod($options['period'])
                    ->andWhere('p.membership IS NULL')
                    ->andWhere('dms IS NULL')
                    ->andWhere('sms IS NULL'),
                'choice_label' => fn (PaymentRequest $paymentRequest) => $paymentRequest->getDatetime()?->format('d/m/Y').' : '.$paymentRequest->getDescription().' ('.$paymentRequest->getAmount().' €)',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SmsBilling::class,
            'translation_domain' => 'forms',
            'period' => $this->periodRepository->findCurrentPeriod(),
        ]);
    }
}
