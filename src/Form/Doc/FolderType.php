<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form\Doc;

use App\Entity\Folder;
use App\Repository\FolderRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @extends AbstractType<mixed>
 */
class FolderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('ref')
            ->add('name')
            ->add('tags', TagType::class, [
                'required' => false,
                'inherit_data' => true,
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
            $folder = $event->getData();
            if ($folder instanceof Folder && null !== $folder->getId()) {
                $event->getForm()->add('parent', EntityType::class, [
                    'class' => Folder::class,
                    'query_builder' => fn (FolderRepository $folderRepository) => $folderRepository->createQueryBuilder('f')
                        ->where('f.id != :id')
                        ->orderBy('f.ref', 'ASC')
                        ->setParameter('id', $folder->getId()),
                    'choice_label' => fn (Folder $folder) => ($folder->getRef() ? '['.$folder->getRef().'] ' : '').$folder,
                    'attr' => ['class' => 'js-choice'],
                    'required' => false,
                ]);
            } else {
                $event->getForm()->add('parent', EntityType::class, [
                    'class' => Folder::class,
                    'query_builder' => fn (FolderRepository $folderRepository) => $folderRepository->createQueryBuilder('f')
                        ->orderBy('f.ref', 'ASC'),
                    'choice_label' => fn (Folder $folder) => ($folder->getRef() ? '['.$folder->getRef().'] ' : '').$folder,
                    'attr' => ['class' => 'js-choice'],
                    'required' => false,
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Folder::class,
            'translation_domain' => 'doc',
        ]);
    }
}
