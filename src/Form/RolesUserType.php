<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @extends AbstractType<mixed>
 */
class RolesUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if ($options['can_set_admin']) {
            $builder->add('user_type', ChoiceType::class, [
                'choices' => [
                    'Normal' => 'normal',
                    'Admin' => 'admin',
                ],
            ]);
        } else {
            $builder->add('user_type', ChoiceType::class, [
                'choices' => [
                    'Normal' => 'normal',
                ],
            ]);
        }

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
            $this->preSetData($event);
        });

        $builder->add('permissions', ChoiceType::class, [
            'choices' => $this->getRoles(),
            'choice_name' => fn ($choice) => $choice,
            'choice_label' => fn ($choice) => 'roles.description.'.$choice,
            'expanded' => true,
            'multiple' => true,
            'label_attr' => ['class' => 'checkbox-switch'],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'translation_domain' => 'security',
            'can_set_admin' => false,
        ]);
    }

    /**
     * @param array<string, mixed> $data
     *
     * @return string[]
     */
    public static function transformData(array $data): array
    {
        $roles = [];
        if ('admin' === $data['user_type']) {
            $roles[] = 'ROLE_ADMIN';
        } else {
            if (in_array('user_show', $data['permissions'], true) && in_array('user_edit', $data['permissions'], true)) {
                array_splice($data['permissions'], array_search('user_show', $data['permissions'], true), 1); // @phpstan-ignore-line
            }
            if (in_array('membership_edit', $data['permissions'], true) && in_array('membership_request_show', $data['permissions'], true)) {
                array_splice($data['permissions'], array_search('membership_request_show', $data['permissions'], true), 1); // @phpstan-ignore-line
            }
            if (in_array('general_meeting_edit', $data['permissions'], true) && in_array('general_meeting_show', $data['permissions'], true)) {
                array_splice($data['permissions'], array_search('general_meeting_show', $data['permissions'], true), 1); // @phpstan-ignore-line
            }
            if (in_array('application_edit', $data['permissions'], true) && in_array('application_show', $data['permissions'], true)) {
                array_splice($data['permissions'], array_search('application_show', $data['permissions'], true), 1); // @phpstan-ignore-line
            }
            if (in_array('basic_show', $data['permissions'], true)
                && (
                    in_array('diocese_edit', $data['permissions'], true) || in_array('person_edit', $data['permissions'], true) || in_array('organization_edit', $data['permissions'], true)
                    || in_array('ca_edit', $data['permissions'], true) || in_array('membership_request_show', $data['permissions'], true) || in_array('membership_request_edit', $data['permissions'], true)
                    || in_array('accounting', $data['permissions'], true)
                    || in_array('general_meeting_show', $data['permissions'], true) || in_array('general_meeting_edit', $data['permissions'], true)
                    || in_array('application_show', $data['permissions'], true) || in_array('application_edit', $data['permissions'], true)
                    || in_array('user_show', $data['permissions'], true) || in_array('user_edit', $data['permissions'], true)
                )
            ) {
                array_splice($data['permissions'], array_search('basic_show', $data['permissions'], true), 1); // @phpstan-ignore-line
            }
            $roles = array_map(static fn ($permission) => 'ROLE_'.strtoupper((string) $permission), $data['permissions']);
        }

        return $roles;
    }

    /**
     * @return string[]
     */
    protected function getRoles(): array
    {
        return [
            'basic_show',
            'diocese_edit',
            'person_edit',
            'organization_edit',
            'volunteer_edit',
            'ca_edit',
            'membership_request_show',
            'membership_edit',
            'aggregate_edit',
            'doc_edit',
            'stats_show',
            'user_show',
            'user_edit',
            'accounting',
            'general_meeting_show',
            'general_meeting_edit',
            'application_show',
            'application_edit',
        ];
    }

    protected function preSetData(FormEvent $event): void
    {
        $roles = (array) $event->getData();
        // Ne s'exécute pas lors d'une soumission de formulaire
        if (in_array('user_type', $roles, true)) {
            return;
        }

        $userType = 'normal';
        $permissions = [];
        if (in_array('ROLE_ADMIN', $roles, true)) {
            $userType = 'admin';
            $permissions = $this->getRoles();
        } else {
            $roles = array_map(fn ($role) => strtolower(str_replace('ROLE_', '', (string) $role)), $roles);
            foreach ($roles as $permission) {
                $permissions[] = $permission;
            }
        }

        $event->setData([
            'user_type' => $userType,
            'permissions' => $permissions,
        ]);
    }
}
