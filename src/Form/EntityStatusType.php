<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\Entity;
use App\Entity\Instance;
use App\Repository\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @extends AbstractType<mixed>
 */
class EntityStatusType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if ($options['instance'] instanceof Instance) {
            $builder
                ->add('entity', EntityType::class, [
                    'class' => Entity::class,
                    'query_builder' => fn (EntityRepository $entityRepository) => $entityRepository->createQueryBuilder('e')
                        ->select(['e'])
                        ->where('e.instance = :instance')
                        ->orderBy('e.name', 'ASC')
                        ->setParameter('instance', $options['instance']),
                    'choice_label' => fn (Entity $entity) => $entity->getName(),
                    'attr' => ['class' => 'js-choice'],
                    'multiple' => false,
                ])
            ;
        } else {
            $builder
                ->add('entity', EntityType::class, [
                    'class' => Entity::class,
                    'query_builder' => fn (EntityRepository $entityRepository) => $entityRepository->createQueryBuilder('e')
                        ->select(['e', 'i'])
                        ->leftJoin('e.instance', 'i')
                        ->where('i.syncUpEnabled = TRUE')
                        ->orderBy('i.name', 'ASC')
                        ->addOrderBy('e.name', 'ASC'),
                    'choice_label' => fn (Entity $entity) => $entity->getInstance()->getName().' : '.$entity->getName(),
                    'attr' => ['class' => 'js-choice'],
                    'multiple' => false,
                ])
            ;
        }
        $builder->add('status', ChoiceType::class, [
            'choices' => [
                'Actif' => 'A',
                'Désactivé' => 'F',
                'Attente de paiement' => 'AP',
                'Retard de paiement' => 'RP',
                'Attente infos' => 'AI',
            ],
            'choice_translation_domain' => false,
            'attr' => ['class' => 'js-choice'],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'translation_domain' => 'forms',
            'instance' => null,
        ]);
    }
}
