<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form\Membership;

use App\Entity\Membership;
use App\Entity\Organization;
use App\Entity\Person;
use App\Repository\PersonRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @extends AbstractType<mixed>
 */
class MembershipRequestAddType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => [
                    Membership::TYPE_MEMBER => Membership::TYPE_MEMBER,
                    Membership::TYPE_FOUNDING => Membership::TYPE_FOUNDING,
                    Membership::TYPE_HONORARY => Membership::TYPE_HONORARY,
                    Membership::TYPE_EX_OFFICIO => Membership::TYPE_EX_OFFICIO,
                ],
            ])
            ->add('hosting', ChoiceType::class, [
                'choices' => [
                    Membership::HOSTING_ENORIA => Membership::HOSTING_ENORIA,
                    Membership::HOSTING_SELF => Membership::HOSTING_SELF,
                ],
            ])
            ->add('notes', TextareaType::class, [
                'attr' => ['rows' => 12],
                'required' => false,
            ])
            ->add('organization', EntityType::class, [
                'class' => Organization::class,
                'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('o')
                    ->select(['o', 'd'])
                    ->innerJoin('o.diocese', 'd')
                    ->orderBy('d.name', 'ASC')
                    ->addOrderBy('o.name', 'ASC'),
                'choice_label' => fn (Organization $organization) => $organization->getDiocese().' : '.$organization,
                'attr' => ['class' => 'js-choice'],
                'required' => false,
            ])
            ->add('person', EntityType::class, [
                'class' => Person::class,
                'query_builder' => fn (PersonRepository $personRepository) => $personRepository->createQueryBuilder('p')
                    ->select(['p', 'u'])
                    ->leftJoin('p.user', 'u')
                    ->orderBy('p.lastname', 'ASC')
                    ->addOrderBy('p.firstname', 'ASC'),
                'attr' => ['class' => 'js-choice'],
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Membership::class,
            'translation_domain' => 'membership',
        ]);
    }
}
