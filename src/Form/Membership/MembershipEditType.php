<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form\Membership;

use App\Entity\Entity;
use App\Entity\Membership;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

/**
 * @extends AbstractType<mixed>
 */
class MembershipEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('membershipFee', MoneyType::class)
            ->add('notes', TextareaType::class, [
                'attr' => ['rows' => 12],
                'required' => false,
            ])
            ->add('sheet', FileType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '2048k',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage' => 'invalid_pdf_file',
                    ]),
                ],
                'attr' => [
                    'placeholder' => 'Choisir un PDF',
                ],
            ])
            ->add('sheet_actions', ChoiceType::class, [
                'mapped' => false,
                'required' => false,
                'choices' => [
                    'remove',
                    'default',
                ],
                'choice_name' => fn ($choice) => $choice,
                'choice_label' => fn ($choice) => 'sheet.description.'.$choice,
                'expanded' => true,
                'multiple' => true,
                'label_attr' => ['class' => 'checkbox-switch'],
            ])
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
                /** @var Membership $membership */
                $membership = $event->getData();
                if ($membership->getOrganization()) {
                    $event->getForm()->add('hosting', ChoiceType::class, [
                        'choices' => [
                            Membership::HOSTING_ENORIA => Membership::HOSTING_ENORIA,
                            Membership::HOSTING_SELF => Membership::HOSTING_SELF,
                        ],
                    ])->add('entity', EntityType::class, [
                        'class' => Entity::class,
                        'choice_label' => fn (Entity $entity) => $entity->getInstance().': '.$entity.' ('.$entity->getIdInInstance().')',
                        'attr' => ['class' => 'js-choice'],
                        'required' => false,
                    ]);
                }
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Membership::class,
            'translation_domain' => 'membership',
        ]);
    }
}
