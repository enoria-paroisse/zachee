<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form\Membership;

use App\Entity\Entity;
use App\Entity\Membership;
use App\Entity\Period;
use App\Repository\PeriodRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\PositiveOrZero;

/**
 * @extends AbstractType<mixed>
 */
class MembershipRequestEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('period', EntityType::class, [
                'class' => Period::class,
                'query_builder' => fn (PeriodRepository $periodRepository) => $periodRepository->createQueryBuilder('p')
                    ->orderBy('p.name', 'ASC'),
                'attr' => ['class' => 'js-choice'],
            ])
            ->add('type', ChoiceType::class, [
                'choices' => [
                    Membership::TYPE_MEMBER => Membership::TYPE_MEMBER,
                    Membership::TYPE_FOUNDING => Membership::TYPE_FOUNDING,
                    Membership::TYPE_HONORARY => Membership::TYPE_HONORARY,
                    Membership::TYPE_EX_OFFICIO => Membership::TYPE_EX_OFFICIO,
                ],
            ])
            ->add('membershipFee', MoneyType::class, [
                'constraints' => [
                    new PositiveOrZero(),
                ],
            ])
            ->add('notes', TextareaType::class, [
                'attr' => ['rows' => 12],
                'required' => false,
            ])
            ->add('sheet', FileType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '2048k',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage' => 'invalid_pdf_file',
                    ]),
                ],
                'attr' => [
                    'placeholder' => 'Choisir un PDF',
                ],
            ])
            ->add('sheet_actions', ChoiceType::class, [
                'mapped' => false,
                'required' => false,
                'choices' => [
                    'remove',
                    'default',
                ],
                'choice_name' => fn ($choice) => $choice,
                'choice_label' => fn ($choice) => 'sheet.description.'.$choice,
                'expanded' => true,
                'multiple' => true,
                'label_attr' => ['class' => 'checkbox-switch'],
            ])
            ->add('helpRequest', null, [
                'label' => 'help.request-change',
                'label_attr' => ['class' => 'checkbox-switch'],
            ])
            ->add('lastReminder', DateType::class, [
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control',
                ],
                'required' => false,
            ])
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
                /** @var Membership $membership */
                $membership = $event->getData();
                if ($membership->getOrganization()) {
                    $event->getForm()->add('hosting', ChoiceType::class, [
                        'choices' => [
                            Membership::HOSTING_ENORIA => Membership::HOSTING_ENORIA,
                            Membership::HOSTING_SELF => Membership::HOSTING_SELF,
                        ],
                    ])->add('entity', EntityType::class, [
                        'class' => Entity::class,
                        'choice_label' => fn (Entity $entity) => $entity->getInstance().': '.$entity.' ('.$entity->getIdInInstance().')',
                        'attr' => ['class' => 'js-choice'],
                        'required' => false,
                    ]);
                }

                if ($membership->getInformationSubmitted()) {
                    $event->getForm()->add('informationSubmitted', null, [
                        'label' => 'info.resubmit',
                        'label_attr' => ['class' => 'checkbox-switch'],
                    ])
                        ->add('informationValidated', null, [
                            'label' => 'info.validate',
                            'label_attr' => ['class' => 'checkbox-switch'],
                        ])
                    ;
                }

                if ($membership->getHelpRequest()) {
                    $event->getForm()->add('helpStatus', ChoiceType::class, [
                        'choices' => [
                            'help.status.untreated' => Membership::HELP_UNTREATED,
                            'help.status.await_info' => Membership::HELP_AWAIT_INFO,
                            'help.status.await_ca' => Membership::HELP_AWAIT_CA,
                            'help.status.accepted' => Membership::HELP_ACCEPTED,
                            'help.status.refused' => Membership::HELP_REFUSED,
                        ],
                        'label' => 'help.status.message',
                    ]);
                }

                if ($membership->getSheet()) {
                    $event->getForm()->add('sheetValidated', null, [
                        'label' => 'sheet.validate',
                        'label_attr' => ['class' => 'checkbox-switch'],
                    ]);
                }
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Membership::class,
            'translation_domain' => 'membership',
        ]);
    }
}
