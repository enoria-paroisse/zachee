<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form\Membership;

use App\Entity\AssociatedPerson;
use App\Entity\TypeAssociatedPerson;
use App\Form\PersonType;
use App\Repository\TypeAssociatedPersonRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;

/**
 * @extends AbstractType<mixed>
 */
class MembershipPublicInfoEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (null === $options['types']) {
            $builder
                ->add('type', EntityType::class, [
                    'class' => TypeAssociatedPerson::class,
                    'query_builder' => fn (TypeAssociatedPersonRepository $typeRepository) => $typeRepository->createQueryBuilder('t')
                        ->select(['t'])
                        ->orderBy('t.name', 'ASC'),
                    'label' => 'Role',
                ])
            ;
        } else {
            $builder
                ->add('type', ChoiceType::class, [
                    'label' => 'Role',
                    'choices' => $options['types'],
                    'choice_label' => function ($choice) {
                        return $choice->getName();
                    },
                    'choice_translation_domain' => false,
                ])
            ;
        }
        $builder
            ->add('person', PersonType::class, [
                'secure_name' => $options['secure_name'],
                'delete_address' => $options['delete_address'],
                'constraints' => new Valid(),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AssociatedPerson::class,
            'translation_domain' => 'forms',
            'secure_name' => false,
            'delete_address' => false,
            'types' => null,
        ]);
    }
}
