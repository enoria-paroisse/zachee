<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\PersonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use libphonenumber\PhoneNumber;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [new Get(), new GetCollection()],
    normalizationContext: ['groups' => ['person']],
    paginationClientEnabled: true
)]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['id' => 'exact'])]
#[ORM\Entity(repositoryClass: PersonRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Person
{
    #[Groups(['associated-person', 'person', 'payment:get'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[Groups(['associated-person', 'person'])]
    #[ORM\Column(type: 'string', length: 255)]
    private string $firstname;

    #[Groups(['associated-person', 'person'])]
    #[ORM\Column(type: 'string', length: 255)]
    private string $lastname;

    #[Groups(['associated-person', 'person'])]
    #[ORM\ManyToOne(targetEntity: Title::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Title $title;

    #[ORM\Column(type: 'string', length: 1)]
    private string $sex;

    #[Assert\Email]
    #[Groups(['associated-person', 'person'])]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $email;

    #[AssertPhoneNumber]
    #[ORM\Column(type: 'phone_number', nullable: true)]
    private ?PhoneNumber $phone;

    #[Assert\Valid]
    #[ORM\OneToOne(targetEntity: Address::class, cascade: ['persist', 'remove'], orphanRemoval: true)]
    #[ORM\JoinColumn(nullable: true)]
    private ?Address $address;

    /**
     * @var Collection<int, AssociatedPerson>
     */
    #[ORM\OneToMany(mappedBy: 'person', targetEntity: AssociatedPerson::class, cascade: ['remove'], orphanRemoval: true)]
    private Collection $associatedOrganizations;

    /**
     * @var Collection<int, Membership>
     */
    #[ORM\OneToMany(mappedBy: 'person', targetEntity: Membership::class)]
    private Collection $memberships;

    #[ORM\Column(type: 'datetime')]
    private \DateTimeInterface $updatedAt;

    #[ORM\ManyToOne(targetEntity: User::class)]
    private ?User $updatedBy = null;

    #[ORM\OneToOne(mappedBy: 'person', targetEntity: User::class)]
    private ?User $user;

    /**
     * @var Collection<int, PaymentRequest>
     */
    #[ORM\OneToMany(mappedBy: 'person', targetEntity: PaymentRequest::class)]
    private Collection $paymentRequests;

    /**
     * @var Collection<int, Payment>
     */
    #[ORM\OneToMany(mappedBy: 'person', targetEntity: Payment::class)]
    private Collection $payments;

    /**
     * @var Collection<int, Group>
     */
    #[ORM\ManyToMany(targetEntity: Group::class, mappedBy: 'people')]
    private Collection $memberGroups;

    /**
     * @var Collection<int, NewsletterSubscription>
     */
    #[ORM\OneToMany(mappedBy: 'person', targetEntity: NewsletterSubscription::class, orphanRemoval: true)]
    private Collection $newsletters;

    /**
     * @var Collection<int, GeneralMeeting>
     */
    #[ORM\ManyToMany(targetEntity: GeneralMeeting::class, mappedBy: 'arePresent')]
    private Collection $generalMeetings;

    /**
     * @var Collection<int, ApplicationUser>
     */
    #[ORM\OneToMany(targetEntity: ApplicationUser::class, mappedBy: 'person', orphanRemoval: true)]
    private Collection $applicationUsers;

    public function __construct()
    {
        $this->associatedOrganizations = new ArrayCollection();
        $this->memberships = new ArrayCollection();
        $this->paymentRequests = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->memberGroups = new ArrayCollection();
        $this->newsletters = new ArrayCollection();
        $this->generalMeetings = new ArrayCollection();
        $this->applicationUsers = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getFullname();
    }

    #[ORM\PrePersist]
    public function setCreated(): void
    {
        $this->setUpdatedAt(new \DateTime());
    }

    #[ORM\PreUpdate]
    public function setUpdated(): void
    {
        $this->setUpdatedAt(new \DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getTitle(): ?Title
    {
        return $this->title;
    }

    public function setTitle(Title $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSex(): ?string
    {
        return $this->sex;
    }

    public function setSex(string $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(?string $country = null): ?PhoneNumber
    {
        return $this->phone;
    }

    public function getPhoneNumber(): ?PhoneNumber
    {
        return $this->phone;
    }

    public function setPhone(?PhoneNumber $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getFullname(bool $title = false, bool $format = false): string
    {
        return ($title ? $this->getTitle() : '').' '.$this->getFirstname().' '.($format ? '<span class="smallcaps">'.$this->getLastname().'</span>' : $this->getLastname());
    }

    /**
     * @return Collection<int, AssociatedPerson>
     */
    public function getAssociatedOrganizations(): Collection
    {
        return $this->associatedOrganizations;
    }

    /**
     * @return array<''|int, array{organization: Organization, types: array<int, null|TypeAssociatedPerson>}>
     */
    public function getUniqueAssociatedOrganizations(): array
    {
        $list = [];
        foreach ($this->getAssociatedOrganizations() as $associate) {
            if (null !== $associate->getOrganization()->getId() && array_key_exists($associate->getOrganization()->getId(), $list)) {
                $list[$associate->getOrganization()->getId()]['types'][] = $associate->getType();
            } else {
                $list[$associate->getOrganization()->getId()] = ['organization' => $associate->getOrganization(), 'types' => [$associate->getType()]];
            }
        }

        return $list;
    }

    /**
     * @return Collection<int, Membership>
     */
    public function getMemberships(): Collection
    {
        return $this->memberships;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?User $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, PaymentRequest>
     */
    public function getPaymentRequests(): Collection
    {
        return $this->paymentRequests;
    }

    public function addPaymentRequest(PaymentRequest $paymentRequest): self
    {
        if (!$this->paymentRequests->contains($paymentRequest)) {
            $this->paymentRequests->add($paymentRequest);
            $paymentRequest->setPerson($this);
        }

        return $this;
    }

    public function removePaymentRequest(PaymentRequest $paymentRequest): self
    {
        if ($this->paymentRequests->removeElement($paymentRequest)) {
            // set the owning side to null (unless already changed)
            if ($paymentRequest->getPerson() === $this) {
                $paymentRequest->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Payment>
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(Payment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments->add($payment);
            $payment->setPerson($this);
        }

        return $this;
    }

    public function removePayment(Payment $payment): self
    {
        if ($this->payments->removeElement($payment)) {
            // set the owning side to null (unless already changed)
            if ($payment->getPerson() === $this) {
                $payment->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Group>
     */
    public function getMemberGroups(): Collection
    {
        return $this->memberGroups;
    }

    public function addMemberGroup(Group $memberGroup): self
    {
        if (!$this->memberGroups->contains($memberGroup)) {
            $this->memberGroups->add($memberGroup);
            $memberGroup->addPerson($this);
        }

        return $this;
    }

    public function removeMemberGroup(Group $memberGroup): self
    {
        if ($this->memberGroups->removeElement($memberGroup)) {
            $memberGroup->removePerson($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, NewsletterSubscription>
     */
    public function getNewsletters(): Collection
    {
        return $this->newsletters;
    }

    public function addNewsletter(NewsletterSubscription $newsletter): self
    {
        if (!$this->newsletters->contains($newsletter)) {
            $this->newsletters->add($newsletter);
            $newsletter->setPerson($this);
        }

        return $this;
    }

    public function removeNewsletter(NewsletterSubscription $newsletter): self
    {
        if ($this->newsletters->removeElement($newsletter)) {
            // set the owning side to null (unless already changed)
            if ($newsletter->getPerson() === $this) {
                $newsletter->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, GeneralMeeting>
     */
    public function getGeneralMeetings(): Collection
    {
        return $this->generalMeetings;
    }

    public function addGeneralMeeting(GeneralMeeting $generalMeeting): static
    {
        if (!$this->generalMeetings->contains($generalMeeting)) {
            $this->generalMeetings->add($generalMeeting);
            $generalMeeting->addArePresent($this);
        }

        return $this;
    }

    public function removeGeneralMeeting(GeneralMeeting $generalMeeting): static
    {
        if ($this->generalMeetings->removeElement($generalMeeting)) {
            $generalMeeting->removeArePresent($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, ApplicationUser>
     */
    public function getApplicationUsers(): Collection
    {
        return $this->applicationUsers;
    }
}
