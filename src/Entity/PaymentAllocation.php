<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Repository\PaymentAllocationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(operations: [
    new Post(),
    new Put(extraProperties: ['standard_put' => false]),
    new Delete(),
], security: 'is_granted("ROLE_ACCOUNTING")')]
#[ApiFilter(SearchFilter::class, properties: ['payment' => 'exact'])]
#[ORM\Entity(repositoryClass: PaymentAllocationRepository::class)]
class PaymentAllocation
{
    #[Groups(['payment:get'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'paymentAllocations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Payment $payment = null;

    #[Groups(['payment:get'])]
    #[ORM\ManyToOne(inversedBy: 'paymentAllocations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?PaymentRequest $paymentRequest = null;

    #[Groups(['payment:get'])]
    #[Assert\Positive]
    #[Assert\Expression(
        '(this.getAmount() <= this.getPayment().getAmount())',
        message: 'less_or_equal_payment_amount'
    )]
    #[Assert\Expression(
        '(this.getAmount() <= this.getPaymentRequest().getAbsAmount())',
        message: 'less_or_equal_payment_request_amount'
    )]
    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2)]
    private string $amount = '0.00';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPayment(): ?Payment
    {
        return $this->payment;
    }

    public function setPayment(?Payment $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getPaymentRequest(): ?PaymentRequest
    {
        return $this->paymentRequest;
    }

    public function setPaymentRequest(?PaymentRequest $paymentRequest): self
    {
        $this->paymentRequest = $paymentRequest;

        return $this;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}
