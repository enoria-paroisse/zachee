<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Repository\DocRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DocRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Doc
{
    public const array TAGS = [
        'public',
        'role_user',
        'role_basic_show',
        'role_diocese_edit',
        'role_person_edit',
        'role_organization_edit',
        'role_volunteer_edit',
        'role_ca_edit',
        'role_membership_request_show',
        'role_membership_edit',
        'role_aggregate_edit',
        'role_doc_edit',
        'role_user_show',
        'role_user_edit',
        'role_accounting',
        'role_general_meeting_show',
        'role_general_meeting_edit',
        'role_admin',
        'help',
        'membership_public_legal',
        'membership_public_natural',
        'ag',
        'ag-multi',
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $ref;

    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    #[ORM\ManyToOne(targetEntity: Folder::class, inversedBy: 'docs')]
    #[ORM\JoinColumn(nullable: false)]
    private Folder $folder;

    /**
     * @var string[]
     */
    #[ORM\Column(type: 'simple_array', nullable: true)]
    private array $tags = [];

    #[ORM\Column(type: 'string', length: 255)]
    private string $file;

    #[ORM\Column(type: 'datetime')]
    private \DateTimeInterface $updatedAt;

    /**
     * @var Collection<int, GeneralMeeting>
     */
    #[ORM\ManyToMany(targetEntity: GeneralMeeting::class, mappedBy: 'summonsDocs')]
    private Collection $generalMeetingsSummons;

    /**
     * @var Collection<int, GeneralMeeting>
     */
    #[ORM\ManyToMany(targetEntity: GeneralMeeting::class, mappedBy: 'reportDocs')]
    private Collection $generalMeetingsReport;

    public function __construct()
    {
        $this->generalMeetingsSummons = new ArrayCollection();
        $this->generalMeetingsReport = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    #[ORM\PrePersist]
    public function setCreated(): void
    {
        $this->setUpdatedAt(new \DateTime());
    }

    #[ORM\PreUpdate]
    public function setUpdated(): void
    {
        $this->setUpdatedAt(new \DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function setRef(?string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFolder(): Folder
    {
        return $this->folder;
    }

    public function setFolder(Folder $folder): self
    {
        $this->folder = $folder;

        return $this;
    }

    /**
     * @return Folder[]
     */
    public function getFolders(): array
    {
        $folders = $this->getFolder()->getParents();
        $folders[] = $this->getFolder();

        return $folders;
    }

    /**
     * @return string[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param string[] $tags
     *
     * @return $this
     */
    public function setTags(array $tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    public function getFile(): string
    {
        return $this->file;
    }

    public function setFile(string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection<int, GeneralMeeting>
     */
    public function getGeneralMeetingsSummons(): Collection
    {
        return $this->generalMeetingsSummons;
    }

    public function addGeneralMeetingSummons(GeneralMeeting $generalMeetingSummons): static
    {
        if (!$this->generalMeetingsSummons->contains($generalMeetingSummons)) {
            $this->generalMeetingsSummons->add($generalMeetingSummons);
            $generalMeetingSummons->addSummonsDoc($this);
        }

        return $this;
    }

    public function removeGeneralMeetingSummons(GeneralMeeting $generalMeetingSummons): static
    {
        if ($this->generalMeetingsSummons->removeElement($generalMeetingSummons)) {
            $generalMeetingSummons->removeSummonsDoc($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, GeneralMeeting>
     */
    public function getGeneralMeetingsReport(): Collection
    {
        return $this->generalMeetingsReport;
    }

    public function addGeneralMeetingReport(GeneralMeeting $generalMeetingReport): static
    {
        if (!$this->generalMeetingsReport->contains($generalMeetingReport)) {
            $this->generalMeetingsReport->add($generalMeetingReport);
            $generalMeetingReport->addReportDoc($this);
        }

        return $this;
    }

    public function removeGeneralMeetingReport(GeneralMeeting $generalMeetingReport): static
    {
        if ($this->generalMeetingsReport->removeElement($generalMeetingReport)) {
            $generalMeetingReport->removeReportDoc($this);
        }

        return $this;
    }
}
