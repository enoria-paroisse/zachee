<?php

/**
 * @copyright Copyright (c) 2025 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Repository\ApplicationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ApplicationRepository::class)]
class Application
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private string $name;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $icon = null;

    #[Assert\Url]
    #[ORM\Column(length: 255)]
    private ?string $url = null;

    /**
     * @var Collection<int, ApplicationUser>
     */
    #[ORM\OneToMany(targetEntity: ApplicationUser::class, mappedBy: 'application', orphanRemoval: true)]
    private Collection $users;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $api = null;

    /**
     * @var null|mixed[]
     */
    #[ORM\Column(nullable: true)]
    private ?array $apiConfig = null;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): static
    {
        $this->icon = $icon;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): static
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return Collection<int, ApplicationUser>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(ApplicationUser $user): static
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setApplication($this);
        }

        return $this;
    }

    public function getApi(): ?string
    {
        return $this->api;
    }

    public function setApi(?string $api): static
    {
        $this->api = $api;

        return $this;
    }

    /**
     * @return null|mixed[]
     */
    public function getApiConfig(): ?array
    {
        return $this->apiConfig;
    }

    /**
     * @param null|mixed[] $apiConfig
     *
     * @return $this
     */
    public function setApiConfig(?array $apiConfig): static
    {
        $this->apiConfig = $apiConfig;

        return $this;
    }
}
