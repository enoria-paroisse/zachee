<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Repository\AggregateRepository;
use App\Validator\AggregateMembership;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AggregateRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Aggregate
{
    public const PERSON = 'person';
    public const ORGANIZATION = 'organization';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 15)]
    private ?string $personOrOrganization = null;

    /**
     * @var Collection<int, Newsletter>
     */
    #[ORM\ManyToMany(targetEntity: Newsletter::class, inversedBy: 'aggregates')]
    private Collection $newsletters;

    /**
     * @var string[]
     */
    #[AggregateMembership]
    #[ORM\Column(type: Types::JSON)]
    private array $membership = [];

    /**
     * @var Collection<int, OrganizationType>
     */
    #[ORM\ManyToMany(targetEntity: OrganizationType::class, inversedBy: 'aggregates')]
    private Collection $organizationTypes;

    /**
     * @var Collection<int, TypeAssociatedPerson>
     */
    #[ORM\ManyToMany(targetEntity: TypeAssociatedPerson::class)]
    private Collection $associatedPersonTypes;

    /**
     * @var Collection<int, Group>
     */
    #[ORM\ManyToMany(targetEntity: Group::class, inversedBy: 'aggregates')]
    private Collection $personGroups;

    public function __construct()
    {
        $this->newsletters = new ArrayCollection();
        $this->organizationTypes = new ArrayCollection();
        $this->associatedPersonTypes = new ArrayCollection();
        $this->personGroups = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getName().'';
    }

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function cleanEntity(): void
    {
        if (self::PERSON === $this->getPersonOrOrganization()) {
            foreach ($this->getOrganizationTypes() as $type) {
                $this->removeOrganizationType($type);
            }
            foreach ($this->getAssociatedPersonTypes() as $type) {
                $this->removeAssociatedPersonType($type);
            }
        } else {
            foreach ($this->getPersonGroups() as $group) {
                $this->removePersonGroup($group);
            }
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPersonOrOrganization(): ?string
    {
        return $this->personOrOrganization;
    }

    public function setPersonOrOrganization(string $personOrOrganization): self
    {
        if (!in_array($personOrOrganization, [self::PERSON, self::ORGANIZATION], true)) {
            throw new \InvalidArgumentException('Invalid personOrOrganization value');
        }

        $this->personOrOrganization = $personOrOrganization;

        return $this;
    }

    /**
     * @return Collection<int, Newsletter>
     */
    public function getNewsletters(): Collection
    {
        return $this->newsletters;
    }

    public function addNewsletter(Newsletter $newsletter): self
    {
        if (!$this->newsletters->contains($newsletter)) {
            $this->newsletters->add($newsletter);
        }

        return $this;
    }

    public function removeNewsletter(Newsletter $newsletter): self
    {
        $this->newsletters->removeElement($newsletter);

        return $this;
    }

    /**
     * @return string[]
     */
    public function getMembership(): array
    {
        return $this->membership;
    }

    /**
     * @param string[] $membership
     *
     * @return $this
     */
    public function setMembership(array $membership): self
    {
        $this->membership = $membership;

        return $this;
    }

    /**
     * @return Collection<int, OrganizationType>
     */
    public function getOrganizationTypes(): Collection
    {
        return $this->organizationTypes;
    }

    public function addOrganizationType(OrganizationType $organizationType): self
    {
        if (!$this->organizationTypes->contains($organizationType)) {
            $this->organizationTypes->add($organizationType);
        }

        return $this;
    }

    public function removeOrganizationType(OrganizationType $organizationType): self
    {
        $this->organizationTypes->removeElement($organizationType);

        return $this;
    }

    /**
     * @return Collection<int, TypeAssociatedPerson>
     */
    public function getAssociatedPersonTypes(): Collection
    {
        return $this->associatedPersonTypes;
    }

    public function addAssociatedPersonType(TypeAssociatedPerson $associatedPersonType): self
    {
        if (!$this->associatedPersonTypes->contains($associatedPersonType)) {
            $this->associatedPersonTypes->add($associatedPersonType);
        }

        return $this;
    }

    public function removeAssociatedPersonType(TypeAssociatedPerson $associatedPersonType): self
    {
        $this->associatedPersonTypes->removeElement($associatedPersonType);

        return $this;
    }

    /**
     * @return Collection<int, Group>
     */
    public function getPersonGroups(): Collection
    {
        return $this->personGroups;
    }

    public function addPersonGroup(Group $group): self
    {
        if (!$this->personGroups->contains($group)) {
            $this->personGroups->add($group);
        }

        return $this;
    }

    public function removePersonGroup(Group $group): self
    {
        $this->personGroups->removeElement($group);

        return $this;
    }
}
