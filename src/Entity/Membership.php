<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Repository\MembershipRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Mapping\ClassMetadata;

#[ORM\Entity(repositoryClass: MembershipRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[UniqueEntity(
    fields: ['token'],
)]
class Membership
{
    public const STATUS_IN_PROGRESS = 'in_progress';
    public const STATUS_VALIDATED = 'validated';
    public const STATUS_CANCELED = 'canceled';

    public const TYPE_FOUNDING = 'founding';
    public const TYPE_EX_OFFICIO = 'ex_officio';
    public const TYPE_HONORARY = 'honorary';
    public const TYPE_MEMBER = 'member';

    public const HOSTING_SELF = 'self';
    public const HOSTING_ENORIA = 'enoria';

    public const HELP_UNTREATED = 'untreated';
    public const HELP_AWAIT_INFO = 'await_info';
    public const HELP_AWAIT_CA = 'await_ca';
    public const HELP_ACCEPTED = 'accepted';
    public const HELP_REFUSED = 'refused';

    #[Groups(['payment_request:get'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Period::class)]
    #[ORM\JoinColumn(nullable: false)]
    private ?Period $period;

    #[ORM\Column(type: 'string', length: 20)]
    private string $status;

    #[ORM\Column(type: 'string', length: 20)]
    private string $type;

    #[ORM\ManyToOne(targetEntity: Organization::class, inversedBy: 'memberships')]
    private ?Organization $organization = null;

    #[ORM\ManyToOne(targetEntity: Person::class, inversedBy: 'memberships')]
    private ?Person $person = null;

    #[ORM\Column(type: 'string', length: 20, nullable: true)]
    private ?string $hosting;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $notes;

    #[ORM\ManyToOne(targetEntity: Entity::class, inversedBy: 'memberships')]
    private ?Entity $entity = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $sheet;

    #[ORM\Column(type: 'boolean')]
    private bool $sheetValidated = false;

    #[ORM\Column(type: 'boolean')]
    private bool $informationSubmitted;

    #[ORM\Column(type: 'boolean')]
    private bool $informationValidated;

    #[ORM\Column(type: 'boolean')]
    private bool $helpRequest;

    #[ORM\Column(type: 'string', length: 20, nullable: false)]
    private string $helpStatus = self::HELP_UNTREATED;

    #[ORM\Column(type: 'boolean')]
    private bool $first;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $token;

    #[ORM\Column(type: 'datetime')]
    private \DateTimeInterface $createdAt;

    #[ORM\Column(type: 'datetime')]
    private \DateTimeInterface $updatedAt;

    #[ORM\OneToOne(mappedBy: 'membership', cascade: ['persist', 'remove'])]
    private ?PaymentRequest $paymentRequest = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $lastReminder = null;

    /**
     * @var Collection<int, Summons>
     */
    #[ORM\OneToMany(mappedBy: 'membership', targetEntity: Summons::class, orphanRemoval: true)]
    private Collection $summons;

    #[ORM\Column]
    private bool $reminderEnabled = true;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $cancellationDate = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $accessDeactivationDate = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $requestDataRestitutionDate = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dataRestitutionDate = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $deletionDataDate = null;

    public function __construct()
    {
        $this->summons = new ArrayCollection();
    }

    public function __toString(): string
    {
        if (null !== $this->getPerson()) {
            return 'Adhésion '.(null !== $this->getPeriod() ? $this->getPeriod()->getName() : '').' '.$this->getPerson()->__toString();
        }

        if (null !== $this->getOrganization()) {
            return 'Adhésion '.(null !== $this->getPeriod() ? $this->getPeriod()->getName() : '').' '.$this->getOrganization()->__toString();
        }

        return 'Adhésion '.(null !== $this->getPeriod() ? $this->getPeriod()->getName() : '');
    }

    #[ORM\PrePersist]
    public function setCreated(): void
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    #[ORM\PreUpdate]
    public function setUpdated(): void
    {
        $this->setUpdatedAt(new \DateTime());
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata): void
    {
        $metadata->addConstraint(new UniqueEntity([
            'fields' => ['period', 'person'],
            'message' => 'already_member_person',
        ]));
        $metadata->addConstraint(new UniqueEntity([
            'fields' => ['period', 'organization'],
            'message' => 'already_member_organization',
        ]));
        $metadata->addConstraint(new Assert\Callback(static function (self $object, ExecutionContextInterface $context): void {
            if ($object->getPerson() && $object->getOrganization()) {
                $context->buildViolation('person_or_organization')
                    ->atPath('organization')
                    ->addViolation()
                ;
            }
        }));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPeriod(): ?Period
    {
        return $this->period;
    }

    public function setPeriod(?Period $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        if (!in_array($status, [self::STATUS_IN_PROGRESS, self::STATUS_VALIDATED, self::STATUS_CANCELED], true)) {
            throw new \InvalidArgumentException('Invalid status');
        }

        if (self::STATUS_CANCELED === $status) {
            $this->getPaymentRequest()?->setStatus(PaymentRequest::STATUS_CANCELED);
        } elseif (PaymentRequest::STATUS_CANCELED === $this->getPaymentRequest()?->getStatus()) {
            $this->getPaymentRequest()->setStatus(PaymentRequest::STATUS_IN_PROGRESS);
            $this->getPaymentRequest()->autoStatus();
        }
        $this->status = $status;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        if (!in_array($type, [self::TYPE_FOUNDING, self::TYPE_EX_OFFICIO, self::TYPE_HONORARY, self::TYPE_MEMBER], true)) {
            throw new \InvalidArgumentException('Invalid status');
        }

        $this->type = $type;

        return $this;
    }

    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    public function setOrganization(?Organization $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getHosting(): ?string
    {
        return $this->hosting;
    }

    public function setHosting(?string $hosting): self
    {
        if (!in_array($hosting, [self::HOSTING_SELF, self::HOSTING_ENORIA], true)) {
            throw new \InvalidArgumentException('Invalid status');
        }

        $this->hosting = $hosting;

        return $this;
    }

    public function getMembershipFee(): ?string
    {
        return $this->paymentRequest?->getAmount() ?? '0.00';
    }

    public function setMembershipFee(float|string $membershipFee): self
    {
        $this->paymentRequest?->setAmount(''.$membershipFee);

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getEntity(): ?Entity
    {
        return $this->entity;
    }

    public function setEntity(?Entity $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getSheet(): ?string
    {
        return $this->sheet;
    }

    public function setSheet(?string $sheet, bool $invalidate = false): self
    {
        $this->sheet = $sheet;

        if ($invalidate) {
            $this->sheetValidated = false;
        }

        return $this;
    }

    public function getSheetValidated(): bool
    {
        return $this->sheetValidated;
    }

    public function setSheetValidated(bool $sheetValidated): self
    {
        $this->sheetValidated = $sheetValidated;

        return $this;
    }

    public function getInformationSubmitted(): ?bool
    {
        return $this->informationSubmitted;
    }

    public function setInformationSubmitted(bool $informationSubmitted): self
    {
        $this->informationSubmitted = $informationSubmitted;

        return $this;
    }

    public function getInformationValidated(): ?bool
    {
        return $this->informationValidated;
    }

    public function setInformationValidated(bool $informationValidated): self
    {
        $this->informationValidated = $informationValidated;

        return $this;
    }

    public function getHelpRequest(): ?bool
    {
        return $this->helpRequest;
    }

    public function setHelpRequest(bool $helpRequest): self
    {
        $this->helpRequest = $helpRequest;

        return $this;
    }

    public function getHelpStatus(): string
    {
        return $this->helpStatus;
    }

    public function setHelpStatus(string $helpStatus): self
    {
        if (!in_array($helpStatus, [self::HELP_UNTREATED, self::HELP_AWAIT_INFO, self::HELP_AWAIT_CA, self::HELP_ACCEPTED, self::HELP_REFUSED], true)) {
            throw new \InvalidArgumentException('Invalid status');
        }

        $this->helpStatus = $helpStatus;

        return $this;
    }

    public function getFirst(): ?bool
    {
        return $this->first;
    }

    public function setFirst(bool $first): self
    {
        $this->first = $first;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function isLegalPerson(): bool
    {
        return null !== $this->getOrganization();
    }

    public function isNaturalPerson(): bool
    {
        return null !== $this->getPerson();
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getPaymentRequest(): ?PaymentRequest
    {
        return $this->paymentRequest;
    }

    public function setPaymentRequest(?PaymentRequest $paymentRequest): self
    {
        // unset the owning side of the relation if necessary
        if (null === $paymentRequest && null !== $this->paymentRequest) {
            $this->paymentRequest->setMembership(null);
        }

        // set the owning side of the relation if necessary
        if (null !== $paymentRequest && $paymentRequest->getMembership() !== $this) {
            $paymentRequest->setMembership($this);
        }

        $this->paymentRequest = $paymentRequest;

        return $this;
    }

    public function getLastReminder(): ?\DateTimeInterface
    {
        return $this->lastReminder;
    }

    public function setLastReminder(?\DateTimeInterface $lastReminder): self
    {
        $this->lastReminder = $lastReminder;

        return $this;
    }

    public function touchLastReminder(): self
    {
        $this->lastReminder = new \DateTime();

        return $this;
    }

    /**
     * @return Collection<int, Summons>
     */
    public function getSummons(): Collection
    {
        return $this->summons;
    }

    public function addSummon(Summons $summon): static
    {
        if (!$this->summons->contains($summon)) {
            $this->summons->add($summon);
            $summon->setMembership($this);
        }

        return $this;
    }

    public function removeSummon(Summons $summon): static
    {
        if ($this->summons->removeElement($summon)) {
            // set the owning side to null (unless already changed)
            if ($summon->getMembership() === $this) {
                $summon->setMembership(null);
            }
        }

        return $this;
    }

    public function isReminderEnabled(): bool
    {
        return $this->reminderEnabled;
    }

    public function setReminderEnabled(bool $reminderEnabled): static
    {
        $this->reminderEnabled = $reminderEnabled;

        return $this;
    }

    public function getCancellationDate(): ?\DateTimeInterface
    {
        return $this->cancellationDate;
    }

    public function setCancellationDate(?\DateTimeInterface $cancellationDate): static
    {
        $this->cancellationDate = $cancellationDate;

        return $this;
    }

    public function getAccessDeactivationDate(): ?\DateTimeInterface
    {
        return $this->accessDeactivationDate;
    }

    public function setAccessDeactivationDate(?\DateTimeInterface $accessDeactivationDate): static
    {
        $this->accessDeactivationDate = $accessDeactivationDate;

        return $this;
    }

    public function getRequestDataRestitutionDate(): ?\DateTimeInterface
    {
        return $this->requestDataRestitutionDate;
    }

    public function setRequestDataRestitutionDate(?\DateTimeInterface $requestDataRestitutionDate): static
    {
        $this->requestDataRestitutionDate = $requestDataRestitutionDate;

        return $this;
    }

    public function getDataRestitutionDate(): ?\DateTimeInterface
    {
        return $this->dataRestitutionDate;
    }

    public function setDataRestitutionDate(?\DateTimeInterface $dataRestitutionDate): static
    {
        $this->dataRestitutionDate = $dataRestitutionDate;

        return $this;
    }

    public function getDeletionDataDate(): ?\DateTimeInterface
    {
        return $this->deletionDataDate;
    }

    public function setDeletionDataDate(?\DateTimeInterface $deletionDataDate): static
    {
        $this->deletionDataDate = $deletionDataDate;

        return $this;
    }

    public function canValidate(): bool
    {
        return Membership::STATUS_IN_PROGRESS === $this->getStatus()
            && $this->getInformationSubmitted()
            && $this->getInformationValidated()
            && null !== $this->getSheet()
            && $this->getSheetValidated()
            && $this->getPaymentRequest()
            && PaymentRequest::STATUS_COMPLETED === $this->getPaymentRequest()->getStatus()
            && (!$this->getHelpRequest() || (Membership::HELP_ACCEPTED === $this->getHelpStatus() || Membership::HELP_REFUSED === $this->getHelpStatus()))
            && ($this->getPerson() || Membership::HOSTING_SELF === $this->getHosting() || (Membership::HOSTING_ENORIA === $this->getHosting() && null !== $this->getEntity()));
    }
}
