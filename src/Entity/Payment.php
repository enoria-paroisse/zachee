<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use App\Repository\PaymentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(operations: [
    new Get(normalizationContext: ['groups' => 'payment:get']),
], security: 'is_granted("accounting_show", object)')]
#[ORM\Entity(repositoryClass: PaymentRepository::class)]
class Payment
{
    public const TYPE_CHECK = 'check';
    public const TYPE_TRANSFER = 'transfer';
    public const TYPE_CASH = 'cash';
    public const TYPE_CB = 'cb';

    public const STATUS_REGISTERED = 'registered';
    public const STATUS_ALLOCATED = 'allocated';
    public const STATUS_VALIDATED = 'validated';
    public const STATUS_CANCELED = 'canceled';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Groups(['payment:get'])]
    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $datetime = null;

    #[ORM\Column(length: 10)]
    private string $type = self::TYPE_TRANSFER;

    #[ORM\Column(length: 50, nullable: true)]
    #[Assert\Expression(
        '(this.getCheckNumber() == null and this.getType() != "check") or (this.getCheckNumber() != null and this.getType() == "check")',
        message: 'only_required_for_check_type'
    )]
    private ?string $checkNumber = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\Expression(
        '(this.getCheckIssuer() == null and this.getType() != "check") or this.getType() == "check"',
        message: 'only_required_for_check_type'
    )]
    private ?string $checkIssuer = null;

    #[ORM\Column(length: 50, nullable: true)]
    #[Assert\Expression(
        '(this.getCheckBank() == null and this.getType() != "check") or (this.getCheckBank() != null and this.getType() == "check")',
        message: 'only_required_for_check_type'
    )]
    private ?string $checkBank = null;

    #[Groups(['payment:get'])]
    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2)]
    #[Assert\Positive]
    private string $amount = '0.00';

    #[Groups(['payment:get'])]
    #[ORM\ManyToOne(inversedBy: 'payments')]
    #[Assert\Expression(
        '(this.getOrganization() == null and this.getPerson() != null) or (this.getOrganization() != null and this.getPerson() == null)',
        message: 'organization_xor_person'
    )]
    private ?Person $person = null;

    #[Groups(['payment:get'])]
    #[ORM\ManyToOne(inversedBy: 'payments')]
    #[Assert\Expression(
        '(this.getOrganization() == null and this.getPerson() != null) or (this.getOrganization() != null and this.getPerson() == null)',
        message: 'organization_xor_person'
    )]
    private ?Organization $organization = null;

    #[Groups(['payment:get'])]
    #[ORM\Column(length: 20)]
    private string $status = self::STATUS_REGISTERED;

    /**
     * @var Collection<int, PaymentAllocation>
     */
    #[Groups(['payment:get'])]
    #[ORM\OneToMany(mappedBy: 'payment', targetEntity: PaymentAllocation::class, orphanRemoval: true)]
    private Collection $paymentAllocations;

    #[Groups(['payment:get'])]
    #[ORM\OneToOne(mappedBy: 'payment', cascade: ['persist', 'remove'])]
    private ?Receipt $receipt = null;

    public function __construct()
    {
        $this->paymentAllocations = new ArrayCollection();
    }

    #[Groups(['payment:get'])]
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        if (!in_array($type, [self::TYPE_CHECK, self::TYPE_TRANSFER, self::TYPE_CASH, self::TYPE_CB], true)) {
            throw new \InvalidArgumentException('Invalid type');
        }

        $this->type = $type;

        return $this;
    }

    public function getCheckNumber(): ?string
    {
        return $this->checkNumber;
    }

    public function setCheckNumber(?string $checkNumber): self
    {
        $this->checkNumber = $checkNumber;

        return $this;
    }

    public function getCheckIssuer(): ?string
    {
        return $this->checkIssuer;
    }

    public function setCheckIssuer(?string $checkIssuer): self
    {
        $this->checkIssuer = $checkIssuer;

        return $this;
    }

    public function getCheckBank(): ?string
    {
        return $this->checkBank;
    }

    public function setCheckBank(?string $checkBank): self
    {
        $this->checkBank = $checkBank;

        return $this;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    #[Groups(['payment:get'])]
    public function getAmountAllocated(): string
    {
        $result = 0;
        foreach ($this->getPaymentAllocations() as $allocation) {
            $result += (float) $allocation->getAmount();
        }

        return $result.'';
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    public function setOrganization(?Organization $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        if (!in_array($status, [self::STATUS_CANCELED, self::STATUS_ALLOCATED, self::STATUS_REGISTERED, self::STATUS_VALIDATED], true)) {
            throw new \InvalidArgumentException('Invalid status');
        }

        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection<int, PaymentAllocation>
     */
    public function getPaymentAllocations(): Collection
    {
        return $this->paymentAllocations;
    }

    public function addPaymentAllocation(PaymentAllocation $paymentAllocation): self
    {
        if (!$this->paymentAllocations->contains($paymentAllocation)) {
            $this->paymentAllocations->add($paymentAllocation);
            $paymentAllocation->setPayment($this);
        }

        return $this;
    }

    public function removePaymentAllocation(PaymentAllocation $paymentAllocation): self
    {
        if ($this->paymentAllocations->removeElement($paymentAllocation)) {
            // set the owning side to null (unless already changed)
            if ($paymentAllocation->getPayment() === $this) {
                $paymentAllocation->setPayment(null);
            }
        }

        return $this;
    }

    public function getReceipt(): ?Receipt
    {
        return $this->receipt;
    }

    public function setReceipt(Receipt $receipt): self
    {
        // set the owning side of the relation if necessary
        if ($receipt->getPayment() !== $this) {
            $receipt->setPayment($this);
        }

        $this->receipt = $receipt;

        return $this;
    }
}
