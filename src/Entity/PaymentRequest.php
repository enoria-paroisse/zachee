<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\PaymentRequestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(operations: [
    new Get(normalizationContext: ['groups' => 'payment_request:get'], security: 'is_granted("accounting_show", object)'),
    new GetCollection(normalizationContext: ['groups' => 'payment_request:list'], security: 'is_granted("ROLE_ACCOUNTING")'),
])]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'organization' => 'exact',
        'person' => 'exact',
        'status' => 'exact',
        'membership.organization' => 'exact',
        'membership.person' => 'exact',
        'dioceseMembershipSupplement.diocese.associatedOrganization' => 'exact',
    ]
)]
#[ORM\Entity(repositoryClass: PaymentRequestRepository::class)]
class PaymentRequest
{
    public const STATUS_COMPLETED = 'completed';
    public const STATUS_IN_PROGRESS = 'in_progress';
    public const STATUS_CANCELED = 'canceled';

    #[Groups(['payment_request:get', 'payment_request:list', 'payment:get'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Groups(['payment_request:get'])]
    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $datetime = null;

    #[Groups(['payment_request:list', 'payment:get'])]
    #[ORM\Column(length: 255)]
    private ?string $description = null;

    #[Groups(['payment_request:get', 'payment_request:list', 'payment:get'])]
    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2)]
    private ?string $amount = null;

    #[ORM\ManyToOne(inversedBy: 'paymentRequests')]
    #[Assert\Expression(
        '(this.getOrganization() == null and this.getPerson() != null) or (this.getOrganization() != null and this.getPerson() == null) or ((this.getMembership() != null or this.getDioceseMembershipSupplement() != null) and this.getOrganization() == null and this.getPerson() == null)',
        message: 'organization_xor_person'
    )]
    #[Assert\Expression(
        '(this.getMembership() != null and this.getOrganization() == null) or this.getMembership() == null',
        message: 'no_organization_if_membership'
    )]
    private ?Organization $organization = null;

    #[ORM\ManyToOne(inversedBy: 'paymentRequests')]
    #[Assert\Expression(
        '(this.getOrganization() == null and this.getPerson() != null) or (this.getOrganization() != null and this.getPerson() == null) or ((this.getMembership() != null or this.getDioceseMembershipSupplement() != null) and this.getOrganization() == null and this.getPerson() == null)',
        message: 'organization_xor_person'
    )]
    #[Assert\Expression(
        '(this.getMembership() != null and this.getPerson() == null) or this.getMembership() == null',
        message: 'no_person_if_membership'
    )]
    private ?Person $person = null;

    #[Groups(['payment_request:get', 'payment_request:list', 'payment:get'])]
    #[ORM\Column(length: 20)]
    private ?string $status = null;

    #[Groups(['payment_request:get'])]
    #[ORM\OneToOne(inversedBy: 'paymentRequest', cascade: ['persist', 'remove'])]
    private ?Membership $membership = null;

    #[Groups(['payment_request:get'])]
    #[ORM\OneToOne(mappedBy: 'paymentRequest', cascade: ['persist', 'remove'])]
    private ?DioceseMembershipSupplement $dioceseMembershipSupplement = null;

    /**
     * @var Collection<int, PaymentAllocation>
     */
    #[ORM\OneToMany(mappedBy: 'paymentRequest', targetEntity: PaymentAllocation::class)]
    private Collection $paymentAllocations;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $lastReminder = null;

    #[Groups(['payment_request:get'])]
    #[ORM\OneToOne(mappedBy: 'paymentRequest', cascade: ['persist', 'remove'])]
    private ?PaymentRequestFile $file = null;

    #[Groups(['payment_request:get'])]
    #[ORM\OneToOne(mappedBy: 'paymentRequest', cascade: ['persist', 'remove'])]
    private ?SmsBilling $smsBilling = null;

    public function __construct()
    {
        $this->paymentAllocations = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->description ?? '';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function getAbsAmount(): float
    {
        return abs((float) $this->amount);
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;
        $this->autoStatus();

        return $this;
    }

    #[Groups(['payment_request:get'])]
    public function getAmountPaid(): string
    {
        $result = 0;
        foreach ($this->getPaymentAllocations() as $allocation) {
            if ($allocation->getPayment() && Payment::STATUS_CANCELED !== $allocation->getPayment()->getStatus()) {
                $result += (float) $allocation->getAmount();
            }
        }

        return $result.'';
    }

    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    public function setOrganization(?Organization $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        if (!in_array($status, [self::STATUS_IN_PROGRESS, self::STATUS_COMPLETED, self::STATUS_CANCELED], true)) {
            throw new \InvalidArgumentException('Invalid status');
        }

        $this->status = $status;

        return $this;
    }

    public function autoStatus(): string
    {
        if (self::STATUS_CANCELED === $this->getStatus()) {
            return self::STATUS_CANCELED;
        }

        if (abs((float) $this->getAmount()) > (float) $this->getAmountPaid()) {
            $this->setStatus(self::STATUS_IN_PROGRESS);
        } else {
            $this->setStatus(self::STATUS_COMPLETED);
        }

        return $this->getStatus().'';
    }

    public function getMembership(): ?Membership
    {
        return $this->membership;
    }

    public function setMembership(?Membership $membership): self
    {
        if ($membership) {
            $this->setDescription('Cotisation annuelle '.$membership->getPeriod()?->getName().' '.$membership->getOrganization()?->getName().$membership->getPerson()?->getFullname());
            $this->setOrganization(null);
            $this->setPerson(null);
        } elseif ($this->membership?->getOrganization()) {
            $this->setOrganization($this->membership->getOrganization());
        } elseif ($this->membership?->getPerson()) {
            $this->setPerson($this->membership->getPerson());
        }

        $this->membership = $membership;

        return $this;
    }

    public function getDioceseMembershipSupplement(): ?DioceseMembershipSupplement
    {
        return $this->dioceseMembershipSupplement;
    }

    public function setDioceseMembershipSupplement(?DioceseMembershipSupplement $dioceseMembershipSupplement): self
    {
        $this->dioceseMembershipSupplement = $dioceseMembershipSupplement;

        return $this;
    }

    /**
     * @return Collection<int, PaymentAllocation>
     */
    public function getPaymentAllocations(): Collection
    {
        return $this->paymentAllocations;
    }

    public function addPaymentAllocation(PaymentAllocation $paymentAllocation): self
    {
        if (!$this->paymentAllocations->contains($paymentAllocation)) {
            $this->paymentAllocations->add($paymentAllocation);
            $paymentAllocation->setPaymentRequest($this);
        }

        return $this;
    }

    public function removePaymentAllocation(PaymentAllocation $paymentAllocation): self
    {
        if ($this->paymentAllocations->removeElement($paymentAllocation)) {
            // set the owning side to null (unless already changed)
            if ($paymentAllocation->getPaymentRequest() === $this) {
                $paymentAllocation->setPaymentRequest(null);
            }
        }

        return $this;
    }

    public function getLastReminder(): ?\DateTimeInterface
    {
        return $this->lastReminder;
    }

    public function setLastReminder(?\DateTimeInterface $lastReminder): static
    {
        $this->lastReminder = $lastReminder;

        return $this;
    }

    public function getFile(): ?PaymentRequestFile
    {
        return $this->file;
    }

    public function setFile(PaymentRequestFile $file): static
    {
        // set the owning side of the relation if necessary
        if ($file->getPaymentRequest() !== $this) {
            $file->setPaymentRequest($this);
        }

        $this->file = $file;

        return $this;
    }

    public function getSmsBilling(): ?SmsBilling
    {
        return $this->smsBilling;
    }

    public function setSmsBilling(?SmsBilling $smsBilling): static
    {
        // unset the owning side of the relation if necessary
        if (null === $smsBilling && null !== $this->smsBilling) {
            $this->smsBilling->setPaymentRequest(null);
        }

        // set the owning side of the relation if necessary
        if (null !== $smsBilling && $smsBilling->getPaymentRequest() !== $this) {
            $smsBilling->setPaymentRequest($this);
        }

        $this->smsBilling = $smsBilling;

        return $this;
    }
}
