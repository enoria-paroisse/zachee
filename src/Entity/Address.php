<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Repository\AddressRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: AddressRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Address
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[Assert\NotBlank]
    #[ORM\Column(type: 'string', length: 255)]
    private ?string $addressLineA = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $addressLineB = null;

    #[Assert\NotBlank]
    #[ORM\Column(type: 'string', length: 255)]
    private ?string $city = null;

    #[Assert\NotBlank]
    #[ORM\Column(type: 'string', length: 20)]
    private ?string $postalCode = null;

    #[Assert\Country]
    #[ORM\Column(type: 'string', length: 2, nullable: false)]
    private string $country = 'FR';

    #[ORM\Column(type: 'datetime')]
    private \DateTimeInterface $updatedAt;

    #[ORM\ManyToOne(targetEntity: User::class)]
    private ?User $updatedBy = null;

    public function __toString(): string
    {
        return $this->getFormatted();
    }

    #[ORM\PrePersist]
    public function setCreated(): void
    {
        $this->setUpdatedAt(new \DateTime());
    }

    #[ORM\PreUpdate]
    public function setUpdated(): void
    {
        $this->setUpdatedAt(new \DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddressLineA(): ?string
    {
        return $this->addressLineA;
    }

    public function setAddressLineA(string $addressLineA): self
    {
        $this->addressLineA = $addressLineA;

        return $this;
    }

    public function getAddressLineB(): ?string
    {
        return $this->addressLineB;
    }

    public function setAddressLineB(?string $addressLineB): self
    {
        $this->addressLineB = $addressLineB;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getFormatted(bool $country = false): string
    {
        return $this->addressLineA.($this->addressLineB ? ' - '.$this->addressLineB : '')
            .' '.$this->postalCode.' '.$this->city.($country ? ' ('.$this->country.')' : '');
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?User $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }
}
