<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Repository\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: EntityRepository::class)]
#[UniqueEntity(
    fields: ['instance', 'idInInstance'],
)]
class Entity
{
    public const STATUS_ACTIVE = 'A';
    public const STATUS_INACTIVE = 'F';
    public const STATUS_WAITING_PAYMENT = 'AP';
    public const STATUS_LATE_PAYMENT = 'RP';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[Assert\NotBlank]
    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    #[Groups(['payment_request:get'])]
    #[ORM\ManyToOne(targetEntity: Instance::class, inversedBy: 'entities')]
    #[ORM\JoinColumn(nullable: false)]
    private Instance $instance;

    #[Assert\NotBlank]
    #[Assert\Positive]
    #[ORM\Column(type: 'integer')]
    private int $idInInstance;

    #[ORM\OneToMany(mappedBy: 'entity', targetEntity: Membership::class)]
    private $memberships;

    #[ORM\Column]
    private bool $enable = false;

    #[ORM\Column(length: 5)]
    private string $status = self::STATUS_ACTIVE;

    public function __construct()
    {
        $this->memberships = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getInstance(): Instance
    {
        return $this->instance;
    }

    public function setInstance(Instance $instance): self
    {
        $this->instance = $instance;

        return $this;
    }

    public function getIdInInstance(): ?int
    {
        return $this->idInInstance;
    }

    public function setIdInInstance(int $idInInstance): self
    {
        $this->idInInstance = $idInInstance;

        return $this;
    }

    /**
     * @return Collection<int, Membership>
     */
    public function getMemberships(): Collection
    {
        return $this->memberships;
    }

    public function isEnable(): bool
    {
        return $this->enable;
    }

    public function setEnable(bool $enable): static
    {
        $this->enable = $enable;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): static
    {
        $this->status = $status;

        return $this;
    }
}
