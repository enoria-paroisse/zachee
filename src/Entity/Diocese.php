<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Repository\DioceseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[UniqueEntity('name')]
#[ORM\Entity(repositoryClass: DioceseRepository::class)]
class Diocese
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[Groups(['organization'])]
    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    /**
     * @var Collection<int, Organization>
     */
    #[ORM\OneToMany(mappedBy: 'diocese', targetEntity: Organization::class)]
    private Collection $organizations;

    #[ORM\OneToOne(targetEntity: Organization::class, cascade: ['persist', 'remove'])]
    private ?Organization $associatedOrganization;

    /**
     * @var Collection<int, Wasselynck>
     */
    #[ORM\OneToMany(mappedBy: 'diocese', targetEntity: Wasselynck::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $wasselynck;

    /**
     * @var Collection<int, DioceseMembershipSupplement>
     */
    #[ORM\OneToMany(mappedBy: 'diocese', targetEntity: DioceseMembershipSupplement::class)]
    private Collection $dioceseMembershipSupplement;

    #[Assert\Country]
    #[ORM\Column(type: 'string', length: 2, nullable: false)]
    private string $country = 'FR';

    #[Groups(['organization'])]
    #[ORM\Column(type: 'string', length: 20, nullable: true)]
    private ?string $deptCode;

    #[ORM\ManyToOne(inversedBy: 'dioceses')]
    #[ORM\JoinColumn(nullable: false, options: ['default' => 1])]
    private ?SubOrganizationMembershipFee $subOrganizationMembershipFee = null;

    public function __construct()
    {
        $this->organizations = new ArrayCollection();
        $this->wasselynck = new ArrayCollection();
        $this->dioceseMembershipSupplement = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Organization>
     */
    public function getOrganizations(): Collection
    {
        return $this->organizations;
    }

    public function addOrganisation(Organization $organization): self
    {
        if (!$this->organizations->contains($organization)) {
            $this->organizations[] = $organization;
            $organization->setDiocese($this);
        }

        return $this;
    }

    public function removeOrganization(Organization $organization): self
    {
        if ($this->organizations->removeElement($organization)) {
            if ($organization->getDiocese() === $this) {
                $organization->setDiocese(null);
            }
            if ($this->getAssociatedOrganization() === $organization) {
                $this->setAssociatedOrganization(null);
            }
        }

        return $this;
    }

    public function getAssociatedOrganization(): ?Organization
    {
        return $this->associatedOrganization;
    }

    public function setAssociatedOrganization(?Organization $associatedOrganization): self
    {
        $this->associatedOrganization = $associatedOrganization;

        return $this;
    }

    /**
     * @return Collection<int, Wasselynck>
     */
    public function getWasselynck(): Collection
    {
        return $this->wasselynck;
    }

    public function addWasselynck(Wasselynck $wasselynck): self
    {
        if (!$this->wasselynck->contains($wasselynck)) {
            $this->wasselynck[] = $wasselynck;
            $wasselynck->setDiocese($this);
        }

        return $this;
    }

    public function removeWasselynck(Wasselynck $wasselynck): self
    {
        if ($this->wasselynck->removeElement($wasselynck)) {
            // set the owning side to null (unless already changed)
            if ($wasselynck->getDiocese() === $this) {
                $wasselynck->setDiocese(null);
            }
        }

        return $this;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getDeptCode(): ?string
    {
        return $this->deptCode;
    }

    public function setDeptCode(?string $deptCode): self
    {
        $this->deptCode = $deptCode;

        return $this;
    }

    public function getSubOrganizationMembershipFee(): ?SubOrganizationMembershipFee
    {
        return $this->subOrganizationMembershipFee;
    }

    public function setSubOrganizationMembershipFee(?SubOrganizationMembershipFee $subOrganizationMembershipFee): self
    {
        $this->subOrganizationMembershipFee = $subOrganizationMembershipFee;

        return $this;
    }

    /**
     * @return Collection<int, DioceseMembershipSupplement>
     */
    public function getDioceseMembershipSupplement(): Collection
    {
        return $this->dioceseMembershipSupplement;
    }
}
