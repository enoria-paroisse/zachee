<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\OrganizationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ApiResource(
    operations: [new Get(), new GetCollection()],
    normalizationContext: ['groups' => ['organization']],
    paginationClientEnabled: true
)]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['id' => 'exact'])]
#[ORM\Entity(repositoryClass: OrganizationRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Organization
{
    #[Groups(['associated-person', 'organization', 'payment:get'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[Groups(['associated-person', 'organization'])]
    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    #[Groups(['organization'])]
    #[ORM\ManyToOne(targetEntity: Diocese::class, inversedBy: 'organizations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Diocese $diocese = null;

    #[ORM\OneToOne(targetEntity: Address::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private Address $address;

    /**
     * @var Collection<int, AssociatedPerson>
     */
    #[ORM\OneToMany(mappedBy: 'organization', targetEntity: AssociatedPerson::class, orphanRemoval: true)]
    private Collection $associatedPeople;

    /**
     * @var Collection<int, Membership>
     */
    #[ORM\OneToMany(mappedBy: 'organization', targetEntity: Membership::class)]
    private Collection $memberships;

    #[ORM\Column(type: 'datetime')]
    private \DateTimeInterface $updatedAt;

    #[ORM\ManyToOne(targetEntity: User::class)]
    private ?User $updatedBy = null;

    /**
     * @var Collection<int, PaymentRequest>
     */
    #[ORM\OneToMany(mappedBy: 'organization', targetEntity: PaymentRequest::class)]
    private Collection $paymentRequests;

    /**
     * @var Collection<int, Payment>
     */
    #[ORM\OneToMany(mappedBy: 'organization', targetEntity: Payment::class)]
    private Collection $payments;

    #[ORM\ManyToOne(inversedBy: 'organizations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?OrganizationType $type = null;

    public function __construct()
    {
        $this->associatedPeople = new ArrayCollection();
        $this->memberships = new ArrayCollection();
        $this->paymentRequests = new ArrayCollection();
        $this->payments = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name;
    }

    #[ORM\PrePersist]
    public function setCreated(): void
    {
        $this->setUpdatedAt(new \DateTime());
    }

    #[ORM\PreUpdate]
    public function setUpdated(): void
    {
        $this->setUpdatedAt(new \DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDiocese(): ?Diocese
    {
        return $this->diocese;
    }

    public function setDiocese(?Diocese $diocese): self
    {
        $this->diocese = $diocese;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection<int, AssociatedPerson>
     */
    public function getAssociatedPeople(): Collection
    {
        return $this->associatedPeople;
    }

    /**
     * @return array<''|int, array{person: Person, types: array<int, null|TypeAssociatedPerson>}>
     */
    public function getUniqueAssociatedPeople(): array
    {
        $list = [];
        foreach ($this->getAssociatedPeople() as $associate) {
            if (null !== $associate->getPerson()->getId() && array_key_exists($associate->getPerson()->getId(), $list)) {
                $list[$associate->getPerson()->getId()]['types'][] = $associate->getType();
            } else {
                $list[$associate->getPerson()->getId()] = ['person' => $associate->getPerson(), 'types' => [$associate->getType()]];
            }
        }

        return $list;
    }

    /**
     * @return Collection<int, Membership>
     */
    public function getMemberships(): Collection
    {
        return $this->memberships;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?User $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * @return Collection<int, PaymentRequest>
     */
    public function getPaymentRequests(): Collection
    {
        return $this->paymentRequests;
    }

    public function addPaymentRequest(PaymentRequest $paymentRequest): self
    {
        if (!$this->paymentRequests->contains($paymentRequest)) {
            $this->paymentRequests->add($paymentRequest);
            $paymentRequest->setOrganization($this);
        }

        return $this;
    }

    public function removePaymentRequest(PaymentRequest $paymentRequest): self
    {
        if ($this->paymentRequests->removeElement($paymentRequest)) {
            // set the owning side to null (unless already changed)
            if ($paymentRequest->getOrganization() === $this) {
                $paymentRequest->setOrganization(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Payment>
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(Payment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments->add($payment);
            $payment->setOrganization($this);
        }

        return $this;
    }

    public function removePayment(Payment $payment): self
    {
        if ($this->payments->removeElement($payment)) {
            // set the owning side to null (unless already changed)
            if ($payment->getOrganization() === $this) {
                $payment->setOrganization(null);
            }
        }

        return $this;
    }

    public function getType(): ?OrganizationType
    {
        return $this->type;
    }

    public function setType(?OrganizationType $type): self
    {
        $this->type = $type;

        return $this;
    }
}
