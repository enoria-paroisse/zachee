<?php

/**
 * @copyright Copyright (c) 2022 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use App\Repository\ReceiptRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ApiResource(operations: [new Get(normalizationContext: ['groups' => 'receipt:get'])], security: 'is_granted("accounting_show", object)')]
#[ORM\Entity(repositoryClass: ReceiptRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Receipt
{
    #[Groups(['receipt:get'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[Groups(['receipt:get'])]
    #[ORM\Column(type: 'string', length: 25, unique: true)]
    private ?string $ref;

    #[Groups(['receipt:get'])]
    #[ORM\Column(type: 'integer')]
    private ?int $refPrefix;

    #[Groups(['receipt:get'])]
    #[ORM\Column(type: 'integer')]
    private ?int $refNumber;

    #[Groups(['receipt:get'])]
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $file = null;

    #[ORM\OneToOne(inversedBy: 'receipt', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Payment $payment = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function setRef(?string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }

    public function getRefPrefix(): ?int
    {
        return $this->refPrefix;
    }

    public function setRefPrefix(?int $refPrefix): self
    {
        $this->refPrefix = $refPrefix;

        return $this;
    }

    public function getRefNumber(): ?int
    {
        return $this->refNumber;
    }

    public function setRefNumber(?int $refNumber): self
    {
        $this->refNumber = $refNumber;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getPayment(): ?Payment
    {
        return $this->payment;
    }

    public function setPayment(Payment $payment): self
    {
        $this->payment = $payment;

        return $this;
    }
}
