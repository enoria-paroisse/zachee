<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Repository\GroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GroupRepository::class)]
#[ORM\Table(name: '`group`')]
class Group
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    /**
     * @var Collection<int, Person>
     */
    #[ORM\ManyToMany(targetEntity: Person::class, inversedBy: 'memberGroups')]
    private Collection $people;

    /**
     * @var Collection<int, Aggregate>
     */
    #[ORM\ManyToMany(targetEntity: Aggregate::class, mappedBy: 'personGroups')]
    private Collection $aggregates;

    #[ORM\ManyToOne]
    private ?Aggregate $aggregate = null;

    #[ORM\Column]
    private bool $viewable = false;

    public function __construct()
    {
        $this->people = new ArrayCollection();
        $this->aggregates = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getName() ?? '';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Person>
     */
    public function getPeople(): Collection
    {
        return $this->people;
    }

    public function addPerson(Person $person): self
    {
        if (!$this->people->contains($person)) {
            $this->people->add($person);
        }

        return $this;
    }

    public function removePerson(Person $person): self
    {
        $this->people->removeElement($person);

        return $this;
    }

    /**
     * @return Collection<int, Aggregate>
     */
    public function getAggregates(): Collection
    {
        return $this->aggregates;
    }

    public function addAggregate(Aggregate $aggregate): self
    {
        if (!$this->aggregates->contains($aggregate)) {
            $this->aggregates->add($aggregate);
            $aggregate->addPersonGroup($this);
        }

        return $this;
    }

    public function removeAggregate(Aggregate $aggregate): self
    {
        if ($this->aggregates->removeElement($aggregate)) {
            $aggregate->removePersonGroup($this);
        }

        return $this;
    }

    public function getAggregate(): ?Aggregate
    {
        return $this->aggregate;
    }

    public function setAggregate(?Aggregate $aggregate): static
    {
        $this->aggregate = $aggregate;

        return $this;
    }

    public function isViewable(): bool
    {
        return $this->viewable;
    }

    public function setViewable(bool $viewable): static
    {
        $this->viewable = $viewable;

        return $this;
    }
}
