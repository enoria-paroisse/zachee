<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Repository\InstanceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: InstanceRepository::class)]
#[UniqueEntity(
    fields: ['url'],
)]
class Instance
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[Groups(['payment_request:get'])]
    #[Assert\NotBlank]
    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    #[Assert\NotBlank]
    #[Assert\Url]
    #[ORM\Column(type: 'string', length: 255)]
    private string $url;

    /**
     * @var Collection<int, Entity>
     */
    #[ORM\OneToMany(mappedBy: 'instance', targetEntity: Entity::class)]
    private Collection $entities;

    #[ORM\Column(type: 'string', length: 128, nullable: true)]
    private ?string $token;

    #[ORM\Column]
    private bool $smsBilling = false;

    #[ORM\Column]
    private bool $syncUpEnabled = true;

    public function __construct()
    {
        $this->entities = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return Collection<int, Entity>
     */
    public function getEntities(): Collection
    {
        return $this->entities;
    }

    public function addEntity(Entity $entity): self
    {
        if (!$this->entities->contains($entity)) {
            $this->entities[] = $entity;
            $entity->setInstance($this);
        }

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function isSmsBilling(): bool
    {
        return $this->smsBilling;
    }

    public function setSmsBilling(bool $smsBilling): static
    {
        $this->smsBilling = $smsBilling;

        return $this;
    }

    public function isSyncUpEnabled(): bool
    {
        return $this->syncUpEnabled;
    }

    public function setSyncUpEnabled(bool $syncUpEnabled): static
    {
        $this->syncUpEnabled = $syncUpEnabled;

        return $this;
    }
}
