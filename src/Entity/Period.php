<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\PeriodRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(
    operations: [new GetCollection()],
    normalizationContext: ['groups' => ['period']],
    paginationClientEnabled: true
)]
#[ORM\Entity(repositoryClass: PeriodRepository::class)]
class Period
{
    #[Groups(['period'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[Groups(['period'])]
    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    #[ORM\Column(type: 'date')]
    private \DateTimeInterface $start;

    #[Assert\GreaterThan(propertyPath: 'start', message: 'La date de fin doit être postérieure à la date de début')]
    #[ORM\Column(type: 'date')]
    private \DateTimeInterface $end;

    /**
     * @var Collection<int, Wasselynck>
     */
    #[ORM\OneToMany(mappedBy: 'period', targetEntity: Wasselynck::class, orphanRemoval: true)]
    private Collection $wasselynck;

    #[Assert\PositiveOrZero]
    #[ORM\Column(type: 'decimal', precision: 10, scale: 2, options: ['default' => '14000.00'])]
    private string $dioceseBaseMaxMembershipfee;

    #[Assert\PositiveOrZero]
    #[ORM\Column(type: 'integer', options: ['default' => 37350])]
    private int $dioceseBaseWasselynck;

    #[Assert\PositiveOrZero]
    #[ORM\Column(type: 'decimal', precision: 10, scale: 2, options: ['default' => '10.00'])]
    private string $membershipfeeNatural;

    #[Assert\PositiveOrZero]
    #[ORM\Column(type: 'decimal', precision: 10, scale: 2, options: ['default' => '600.00'])]
    private string $membershipfeeLegal;

    /**
     * @var Collection<int, SubOrganizationMembershipFeePeriod>
     */
    #[ORM\OneToMany(mappedBy: 'period', targetEntity: SubOrganizationMembershipFeePeriod::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $subOrganizationMembershipFeePeriods;

    public function __construct()
    {
        $this->wasselynck = new ArrayCollection();
        $this->subOrganizationMembershipFeePeriods = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStart(): ?\DateTimeInterface
    {
        return $this->start;
    }

    public function setStart(\DateTimeInterface $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(\DateTimeInterface $end): self
    {
        $this->end = $end;

        return $this;
    }

    /**
     * @return Collection<int, Wasselynck>
     */
    public function getWasselynck(): Collection
    {
        return $this->wasselynck;
    }

    public function addWasselynck(Wasselynck $wasselynck): self
    {
        if (!$this->wasselynck->contains($wasselynck)) {
            $this->wasselynck[] = $wasselynck;
            $wasselynck->setPeriod($this);
        }

        return $this;
    }

    public function removeWasselynck(Wasselynck $wasselynck): self
    {
        if ($this->wasselynck->removeElement($wasselynck)) {
            // set the owning side to null (unless already changed)
            if ($wasselynck->getPeriod() === $this) {
                $wasselynck->setPeriod(null);
            }
        }

        return $this;
    }

    public function getDioceseBaseMaxMembershipfee(): string
    {
        return $this->dioceseBaseMaxMembershipfee;
    }

    public function setDioceseBaseMaxMembershipfee(string $dioceseBaseMaxMembershipfee): self
    {
        $this->dioceseBaseMaxMembershipfee = $dioceseBaseMaxMembershipfee;

        return $this;
    }

    public function getDioceseBaseWasselynck(): int
    {
        return $this->dioceseBaseWasselynck;
    }

    public function setDioceseBaseWasselynck(int $dioceseBaseWasselynck): self
    {
        $this->dioceseBaseWasselynck = $dioceseBaseWasselynck;

        return $this;
    }

    public function getMembershipfeeNatural(): string
    {
        return $this->membershipfeeNatural;
    }

    public function setMembershipfeeNatural(string $membershipfeeNatural): self
    {
        $this->membershipfeeNatural = $membershipfeeNatural;

        return $this;
    }

    public function getMembershipfeeLegal(): string
    {
        return $this->membershipfeeLegal;
    }

    public function setMembershipfeeLegal(string $membershipfeeLegal): self
    {
        $this->membershipfeeLegal = $membershipfeeLegal;

        return $this;
    }

    /**
     * @return Collection<int, SubOrganizationMembershipFeePeriod>
     */
    public function getSubOrganizationMembershipFeePeriods(): Collection
    {
        return $this->subOrganizationMembershipFeePeriods;
    }

    public function addSubOrganizationMembershipFeePeriod(SubOrganizationMembershipFeePeriod $subOrganizationMembershipFeePeriod): self
    {
        if (!$this->subOrganizationMembershipFeePeriods->contains($subOrganizationMembershipFeePeriod)) {
            $this->subOrganizationMembershipFeePeriods->add($subOrganizationMembershipFeePeriod);
            $subOrganizationMembershipFeePeriod->setPeriod($this);
        }

        return $this;
    }

    public function removeSubOrganizationMembershipFeePeriod(SubOrganizationMembershipFeePeriod $subOrganizationMembershipFeePeriod): self
    {
        if ($this->subOrganizationMembershipFeePeriods->removeElement($subOrganizationMembershipFeePeriod)) {
            // set the owning side to null (unless already changed)
            if ($subOrganizationMembershipFeePeriod->getPeriod() === $this) {
                $subOrganizationMembershipFeePeriod->setPeriod(null);
            }
        }

        return $this;
    }
}
