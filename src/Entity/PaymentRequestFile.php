<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use App\Repository\PaymentRequestFileRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ApiResource(
    operations: [new Get(normalizationContext: ['groups' => 'payment_request_file:get'])],
    security: 'is_granted("accounting_show", object)'
)]
#[ORM\Entity(repositoryClass: PaymentRequestFileRepository::class)]
class PaymentRequestFile
{
    #[Groups(['payment_request_file:get'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Groups(['payment_request_file:get'])]
    #[ORM\Column(length: 35, unique: true)]
    private ?string $ref = null;

    #[Groups(['payment_request_file:get'])]
    #[ORM\Column(length: 25)]
    private ?string $refPrefix = null;

    #[Groups(['payment_request_file:get'])]
    #[ORM\Column]
    private ?int $refNumber = null;

    #[Groups(['payment_request_file:get'])]
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $file = null;

    #[ORM\OneToOne(inversedBy: 'file', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?PaymentRequest $paymentRequest = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function setRef(string $ref): static
    {
        $this->ref = $ref;

        return $this;
    }

    public function getRefPrefix(): ?string
    {
        return $this->refPrefix;
    }

    public function setRefPrefix(string $refPrefix): static
    {
        $this->refPrefix = $refPrefix;

        return $this;
    }

    public function getRefNumber(): ?int
    {
        return $this->refNumber;
    }

    public function setRefNumber(int $refNumber): static
    {
        $this->refNumber = $refNumber;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): static
    {
        $this->file = $file;

        return $this;
    }

    public function getPaymentRequest(): ?PaymentRequest
    {
        return $this->paymentRequest;
    }

    public function setPaymentRequest(PaymentRequest $paymentRequest): static
    {
        $this->paymentRequest = $paymentRequest;

        return $this;
    }
}
