<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Repository\SubOrganizationMembershipFeePeriodRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: SubOrganizationMembershipFeePeriodRepository::class)]
class SubOrganizationMembershipFeePeriod
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'subOrganizationMembershipFeePeriods')]
    #[ORM\JoinColumn(nullable: false)]
    private ?SubOrganizationMembershipFee $subOrganizationMembershipFee;

    #[Assert\PositiveOrZero]
    #[ORM\Column(type: 'decimal', precision: 10, scale: 2, options: ['default' => '100.00'])]
    private string $membershipFee;

    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'subOrganizationMembershipFeePeriods')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Period $period;

    public function __toString(): string
    {
        return 'test';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubOrganizationMembershipFee(): ?SubOrganizationMembershipFee
    {
        return $this->subOrganizationMembershipFee;
    }

    public function setSubOrganizationMembershipFee(?SubOrganizationMembershipFee $subOrganizationMembershipFee): self
    {
        $this->subOrganizationMembershipFee = $subOrganizationMembershipFee;

        return $this;
    }

    public function getMembershipFee(): string
    {
        return $this->membershipFee;
    }

    public function setMembershipFee(string $membershipFee): self
    {
        $this->membershipFee = $membershipFee;

        return $this;
    }

    public function getPeriod(): ?Period
    {
        return $this->period;
    }

    public function setPeriod(?Period $period): self
    {
        $this->period = $period;

        return $this;
    }
}
