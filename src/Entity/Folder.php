<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Repository\FolderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: FolderRepository::class)]
class Folder
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $ref;

    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    /**
     * @var string[]
     */
    #[ORM\Column(type: 'simple_array', nullable: true)]
    private array $tags = [];

    #[Assert\Expression(
        '!this.getParent() || this.getId() !== this.getParent().getId()',
        message: 'no_self_parent_folder'
    )]
    #[ORM\ManyToOne(targetEntity: Folder::class, inversedBy: 'folders')]
    private ?Folder $parent = null;

    /**
     * @var Collection<int, self>
     */
    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: Folder::class)]
    private Collection $folders;

    /**
     * @var Collection<int, Doc>
     */
    #[ORM\OneToMany(mappedBy: 'folder', targetEntity: Doc::class)]
    private Collection $docs;

    public function __construct()
    {
        $this->folders = new ArrayCollection();
        $this->docs = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function setRef(?string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param string[] $tags
     *
     * @return $this
     */
    public function setTags(array $tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @param self[] $parentsHistory
     *
     * @return self[]
     */
    public function getParents(array $parentsHistory = []): array
    {
        $parent = $this->getParent();
        if (in_array($parent, $parentsHistory, true)) {
            throw new \DomainException('Recursive parent');
        }
        if ($parent) {
            $parentsHistory[] = $parent;

            return array_merge($parent->getParents($parentsHistory), [$parent]);
        }

        return [];
    }

    /**
     * @return Collection<int, self>
     */
    public function getFolders(): Collection
    {
        return $this->folders;
    }

    public function addFolder(self $folder): self
    {
        if (!$this->folders->contains($folder)) {
            $this->folders[] = $folder;
            $folder->setParent($this);
        }

        return $this;
    }

    public function removeFolder(self $folder): self
    {
        if ($this->folders->removeElement($folder)) {
            // set the owning side to null (unless already changed)
            if ($folder->getParent() === $this) {
                $folder->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Doc>
     */
    public function getDocs(): Collection
    {
        return $this->docs;
    }

    /**
     * @return Doc[]
     */
    public function getSubDocs(): array
    {
        $docs = [$this->getDocs()->toArray()];
        foreach ($this->getFolders() as $folder) {
            $docs[] = $folder->getSubDocs();
        }

        return (array) \call_user_func_array('array_merge', $docs);
    }
}
