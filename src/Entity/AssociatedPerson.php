<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\AssociatedPersonRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(operations: [new Get(), new GetCollection()], normalizationContext: ['groups' => ['associated-person']])]
#[ApiFilter(filterClass: SearchFilter::class, properties: ['organization' => 'exact', 'type' => 'exact'])]
#[ORM\Entity(repositoryClass: AssociatedPersonRepository::class)]
class AssociatedPerson
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[Groups(['associated-person'])]
    #[ORM\ManyToOne(targetEntity: Organization::class, inversedBy: 'associatedPeople')]
    #[ORM\JoinColumn(nullable: false)]
    private Organization $organization;

    #[Groups(['associated-person'])]
    #[Assert\Valid]
    #[ORM\ManyToOne(targetEntity: Person::class, inversedBy: 'associatedOrganizations')]
    #[ORM\JoinColumn(nullable: false)]
    private Person $person;

    #[ORM\ManyToOne(targetEntity: TypeAssociatedPerson::class)]
    #[ORM\JoinColumn(nullable: false)]
    private ?TypeAssociatedPerson $type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrganization(): Organization
    {
        return $this->organization;
    }

    public function setOrganization(Organization $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function setPerson(Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getType(): ?TypeAssociatedPerson
    {
        return $this->type;
    }

    public function setType(?TypeAssociatedPerson $type): self
    {
        $this->type = $type;

        return $this;
    }
}
