<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Repository\OrganizationTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[UniqueEntity('name')]
#[ORM\Entity(repositoryClass: OrganizationTypeRepository::class)]
class OrganizationType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    /**
     * @var Collection<int, Organization>
     */
    #[ORM\OneToMany(mappedBy: 'type', targetEntity: Organization::class)]
    private Collection $organizations;

    /**
     * @var Collection<int, Aggregate>
     */
    #[ORM\ManyToMany(targetEntity: Aggregate::class, mappedBy: 'organizationTypes')]
    private Collection $aggregates;

    public function __construct()
    {
        $this->organizations = new ArrayCollection();
        $this->aggregates = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name ?? '';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Organization>
     */
    public function getOrganizations(): Collection
    {
        return $this->organizations;
    }

    public function addOrganization(Organization $organization): self
    {
        if (!$this->organizations->contains($organization)) {
            $this->organizations->add($organization);
            $organization->setType($this);
        }

        return $this;
    }

    public function removeOrganization(Organization $organization): self
    {
        if ($this->organizations->removeElement($organization)) {
            // set the owning side to null (unless already changed)
            if ($organization->getType() === $this) {
                $organization->setType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Aggregate>
     */
    public function getAggregates(): Collection
    {
        return $this->aggregates;
    }

    public function addAggregate(Aggregate $aggregate): self
    {
        if (!$this->aggregates->contains($aggregate)) {
            $this->aggregates->add($aggregate);
            $aggregate->addOrganizationType($this);
        }

        return $this;
    }

    public function removeAggregate(Aggregate $aggregate): self
    {
        if ($this->aggregates->removeElement($aggregate)) {
            $aggregate->removeOrganizationType($this);
        }

        return $this;
    }
}
