<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Repository\GeneralMeetingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: GeneralMeetingRepository::class)]
#[UniqueEntity(
    fields: ['token'],
)]
class GeneralMeeting
{
    public const TYPE_ORDINARY = 'ordinary';

    public const TYPE_EXTRAORDINARY = 'extraordinary';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 15)]
    private ?string $type = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $datetime = null;

    /**
     * @var Collection<int, Summons>
     */
    #[ORM\OneToMany(mappedBy: 'generalMeeting', targetEntity: Summons::class, orphanRemoval: true)]
    private Collection $summons;

    /**
     * @var Collection<int, Doc>
     */
    #[ORM\ManyToMany(targetEntity: Doc::class, inversedBy: 'generalMeetingsSummons')]
    #[ORM\JoinTable(name: 'general_meeting_summons_doc')]
    private Collection $summonsDocs;

    /**
     * @var Collection<int, Doc>
     */
    #[ORM\ManyToMany(targetEntity: Doc::class, inversedBy: 'generalMeetingsReport')]
    #[ORM\JoinTable(name: 'general_meeting_report_doc')]
    private Collection $reportDocs;

    #[ORM\Column(length: 255)]
    private ?string $token = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $meetingLink = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $meetingId = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $meetingPasscode = null;

    #[ORM\Column]
    private bool $locked = false;

    /**
     * @var Collection<int, Person>
     */
    #[ORM\ManyToMany(targetEntity: Person::class, inversedBy: 'generalMeetings')]
    private Collection $arePresent;

    public function __construct()
    {
        $this->summons = new ArrayCollection();
        $this->summonsDocs = new ArrayCollection();
        $this->reportDocs = new ArrayCollection();
        $this->arePresent = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        if (!in_array($type, [self::TYPE_ORDINARY, self::TYPE_EXTRAORDINARY], true)) {
            throw new \InvalidArgumentException('Invalid status');
        }

        $this->type = $type;

        return $this;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): static
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * @return Collection<int, Summons>
     */
    public function getSummons(): Collection
    {
        return $this->summons;
    }

    public function addSummon(Summons $summon): static
    {
        if (!$this->summons->contains($summon)) {
            $this->summons->add($summon);
            $summon->setGeneralMeeting($this);
        }

        return $this;
    }

    public function removeSummon(Summons $summon): static
    {
        if ($this->summons->removeElement($summon)) {
            // set the owning side to null (unless already changed)
            if ($summon->getGeneralMeeting() === $this) {
                $summon->setGeneralMeeting(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Doc>
     */
    public function getSummonsDocs(): Collection
    {
        return $this->summonsDocs;
    }

    public function addSummonsDoc(Doc $summonsDoc): static
    {
        if (!$this->summonsDocs->contains($summonsDoc)) {
            $this->summonsDocs->add($summonsDoc);
        }

        return $this;
    }

    public function removeSummonsDoc(Doc $summonsDoc): static
    {
        $this->summonsDocs->removeElement($summonsDoc);

        return $this;
    }

    /**
     * @return Collection<int, Doc>
     */
    public function getReportDocs(): Collection
    {
        return $this->reportDocs;
    }

    public function addReportDoc(Doc $reportDoc): static
    {
        if (!$this->reportDocs->contains($reportDoc)) {
            $this->reportDocs->add($reportDoc);
        }

        return $this;
    }

    public function removeReportDoc(Doc $reportDoc): static
    {
        $this->reportDocs->removeElement($reportDoc);

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): static
    {
        $this->token = $token;

        return $this;
    }

    public function getMeetingLink(): ?string
    {
        return $this->meetingLink;
    }

    public function setMeetingLink(?string $meetingLink): static
    {
        $this->meetingLink = $meetingLink;

        return $this;
    }

    public function getMeetingId(): ?string
    {
        return $this->meetingId;
    }

    public function setMeetingId(?string $meetingId): static
    {
        $this->meetingId = $meetingId;

        return $this;
    }

    public function getMeetingPasscode(): ?string
    {
        return $this->meetingPasscode;
    }

    public function setMeetingPasscode(?string $meetingPasscode): static
    {
        $this->meetingPasscode = $meetingPasscode;

        return $this;
    }

    public function isLocked(): bool
    {
        return $this->locked;
    }

    public function setLocked(bool $locked): static
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * @return Collection<int, Person>
     */
    public function getArePresent(): Collection
    {
        return $this->arePresent;
    }

    public function addArePresent(Person $arePresent): static
    {
        if (!$this->arePresent->contains($arePresent)) {
            $this->arePresent->add($arePresent);
        }

        return $this;
    }

    public function removeArePresent(Person $arePresent): static
    {
        $this->arePresent->removeElement($arePresent);

        return $this;
    }
}
