<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Repository\SubOrganizationMembershipFeeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[UniqueEntity('name')]
#[ORM\Entity(repositoryClass: SubOrganizationMembershipFeeRepository::class)]
class SubOrganizationMembershipFee
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private string $name;

    /**
     * @var Collection<int, Diocese>
     */
    #[ORM\OneToMany(mappedBy: 'subOrganizationMembershipFee', targetEntity: Diocese::class)]
    private Collection $dioceses;

    /**
     * @var Collection<int, SubOrganizationMembershipFeePeriod>
     */
    #[ORM\OneToMany(mappedBy: 'subOrganizationMembershipFee', targetEntity: SubOrganizationMembershipFeePeriod::class, orphanRemoval: true)]
    private Collection $subOrganizationMembershipFeePeriods;

    public function __construct()
    {
        $this->dioceses = new ArrayCollection();
        $this->subOrganizationMembershipFeePeriods = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Diocese>
     */
    public function getDioceses(): Collection
    {
        return $this->dioceses;
    }

    public function addDiocese(Diocese $diocese): self
    {
        if (!$this->dioceses->contains($diocese)) {
            $this->dioceses->add($diocese);
            $diocese->setSubOrganizationMembershipFee($this);
        }

        return $this;
    }

    public function removeDiocese(Diocese $diocese): self
    {
        if ($this->dioceses->removeElement($diocese)) {
            // set the owning side to null (unless already changed)
            if ($diocese->getSubOrganizationMembershipFee() === $this) {
                $diocese->setSubOrganizationMembershipFee(null);
            }
        }

        return $this;
    }

    public function getFee(Period $period): string
    {
        foreach ($period->getSubOrganizationMembershipFeePeriods() as $fee) {
            if ($fee->getSubOrganizationMembershipFee() === $this) {
                return $fee->getMembershipFee();
            }
        }

        return '0.00';
    }

    /**
     * @return Collection<int, SubOrganizationMembershipFeePeriod>
     */
    public function getSubOrganizationMembershipFeePeriods(): Collection
    {
        return $this->subOrganizationMembershipFeePeriods;
    }

    public function addSubOrganizationMembershipFeePeriod(SubOrganizationMembershipFeePeriod $subOrganizationMembershipFeePeriod): self
    {
        if (!$this->subOrganizationMembershipFeePeriods->contains($subOrganizationMembershipFeePeriod)) {
            $this->subOrganizationMembershipFeePeriods->add($subOrganizationMembershipFeePeriod);
            $subOrganizationMembershipFeePeriod->setSubOrganizationMembershipFee($this);
        }

        return $this;
    }

    public function removeSubOrganizationMembershipFeePeriod(SubOrganizationMembershipFeePeriod $subOrganizationMembershipFeePeriod): self
    {
        if ($this->subOrganizationMembershipFeePeriods->removeElement($subOrganizationMembershipFeePeriod)) {
            // set the owning side to null (unless already changed)
            if ($subOrganizationMembershipFeePeriod->getSubOrganizationMembershipFee() === $this) {
                $subOrganizationMembershipFeePeriod->setSubOrganizationMembershipFee(null);
            }
        }

        return $this;
    }
}
