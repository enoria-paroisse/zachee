<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Repository\NewsletterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: NewsletterRepository::class)]
#[UniqueEntity('brevoId')]
class Newsletter
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $brevoId = null;

    #[ORM\ManyToOne(inversedBy: 'lists')]
    #[ORM\JoinColumn(nullable: false)]
    private ?NewsletterFolder $folder = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    /**
     * @var Collection<int, Aggregate>
     */
    #[ORM\ManyToMany(targetEntity: Aggregate::class, mappedBy: 'newsletters')]
    private Collection $aggregates;

    /**
     * @var Collection<int, NewsletterSubscription>
     */
    #[ORM\OneToMany(mappedBy: 'newsletter', targetEntity: NewsletterSubscription::class, orphanRemoval: true)]
    private Collection $subscriptions;

    #[ORM\Column]
    private bool $exist = false;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $lastSync = null;

    #[ORM\Column]
    private bool $success = false;

    public function __construct()
    {
        $this->aggregates = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBrevoId(): ?int
    {
        return $this->brevoId;
    }

    public function setBrevoId(int $brevoId): self
    {
        $this->brevoId = $brevoId;

        return $this;
    }

    public function getFolder(): ?NewsletterFolder
    {
        return $this->folder;
    }

    public function setFolder(?NewsletterFolder $folder): self
    {
        $this->folder = $folder;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Aggregate>
     */
    public function getAggregates(): Collection
    {
        return $this->aggregates;
    }

    public function addAggregate(Aggregate $aggregate): self
    {
        if (!$this->aggregates->contains($aggregate)) {
            $this->aggregates->add($aggregate);
            $aggregate->addNewsletter($this);
        }

        return $this;
    }

    public function removeAggregate(Aggregate $aggregate): self
    {
        if ($this->aggregates->removeElement($aggregate)) {
            $aggregate->removeNewsletter($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, NewsletterSubscription>
     */
    public function getSubscriptions(): Collection
    {
        return $this->subscriptions;
    }

    public function addSubscription(NewsletterSubscription $subscription): self
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions->add($subscription);
            $subscription->setNewsletter($this);
        }

        return $this;
    }

    public function removeSubscription(NewsletterSubscription $subscription): self
    {
        if ($this->subscriptions->removeElement($subscription)) {
            // set the owning side to null (unless already changed)
            if ($subscription->getNewsletter() === $this) {
                $subscription->setNewsletter(null);
            }
        }

        return $this;
    }

    public function isExist(): bool
    {
        return $this->exist;
    }

    public function setExist(bool $exist): self
    {
        $this->exist = $exist;

        return $this;
    }

    public function getLastSync(): ?\DateTimeInterface
    {
        return $this->lastSync;
    }

    public function setLastSync(\DateTimeInterface $lastSync): self
    {
        $this->lastSync = $lastSync;

        return $this;
    }

    public function isSuccess(): bool
    {
        return $this->success;
    }

    public function setSuccess(bool $success): self
    {
        $this->success = $success;

        return $this;
    }
}
