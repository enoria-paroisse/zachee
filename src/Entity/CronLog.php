<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Repository\CronLogRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CronLogRepository::class)]
class CronLog
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $datetime = null;

    /**
     * @var array<string, string>
     */
    #[ORM\Column]
    private array $result = [];

    /**
     * @var Collection<int, CronTaskLog>
     */
    #[ORM\OneToMany(mappedBy: 'cron', targetEntity: CronTaskLog::class, orphanRemoval: true)]
    private Collection $cronTaskLogs;

    public function __construct()
    {
        $this->cronTaskLogs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * @return array<string, string>
     */
    public function getResult(): array
    {
        return $this->result;
    }

    /**
     * @param array<string, string> $result
     *
     * @return $this
     */
    public function setResult(array $result): self
    {
        $this->result = $result;

        return $this;
    }

    /**
     * @return Collection<int, CronTaskLog>
     */
    public function getCronTaskLogs(): Collection
    {
        return $this->cronTaskLogs;
    }

    public function addCronTaskLog(CronTaskLog $cronTaskLog): self
    {
        if (!$this->cronTaskLogs->contains($cronTaskLog)) {
            $this->cronTaskLogs->add($cronTaskLog);
            $cronTaskLog->setCron($this);
        }

        return $this;
    }

    public function removeCronTaskLog(CronTaskLog $cronTaskLog): self
    {
        if ($this->cronTaskLogs->removeElement($cronTaskLog)) {
            // set the owning side to null (unless already changed)
            if ($cronTaskLog->getCron() === $this) {
                $cronTaskLog->setCron(null);
            }
        }

        return $this;
    }
}
