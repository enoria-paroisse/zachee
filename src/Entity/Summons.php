<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Repository\SummonsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: SummonsRepository::class)]
#[UniqueEntity(
    fields: ['generalMeeting', 'membership'],
)]
class Summons
{
    public const MAX_MANDATES = 2;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'summons')]
    #[ORM\JoinColumn(nullable: false)]
    private ?GeneralMeeting $generalMeeting = null;

    #[ORM\ManyToOne(inversedBy: 'summons')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Membership $membership = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateVideo = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateReport = null;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'mandates')]
    private ?self $mandate = null;

    /**
     * @var Collection<int, self>
     */
    #[ORM\OneToMany(mappedBy: 'mandate', targetEntity: self::class)]
    private Collection $mandates;

    #[ORM\ManyToOne]
    private ?Person $whoGiveMandate = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $fileMandate = null;

    #[ORM\Column]
    private bool $isPresent = false;

    public function __construct()
    {
        $this->mandates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGeneralMeeting(): ?GeneralMeeting
    {
        return $this->generalMeeting;
    }

    public function setGeneralMeeting(?GeneralMeeting $generalMeeting): static
    {
        $this->generalMeeting = $generalMeeting;

        return $this;
    }

    public function getMembership(): ?Membership
    {
        return $this->membership;
    }

    public function setMembership(?Membership $membership): static
    {
        $this->membership = $membership;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): static
    {
        $this->date = $date;

        return $this;
    }

    public function getDateVideo(): ?\DateTimeInterface
    {
        return $this->dateVideo;
    }

    public function setDateVideo(?\DateTimeInterface $dateVideo): static
    {
        $this->dateVideo = $dateVideo;

        return $this;
    }

    public function getDateReport(): ?\DateTimeInterface
    {
        return $this->dateReport;
    }

    public function setDateReport(?\DateTimeInterface $dateReport): static
    {
        $this->dateReport = $dateReport;

        return $this;
    }

    public function getMandate(): ?self
    {
        return $this->mandate;
    }

    public function setMandate(?self $mandate): static
    {
        $this->mandate = $mandate;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getMandates(): Collection
    {
        return $this->mandates;
    }

    public function addMandate(self $mandate): static
    {
        if (!$this->mandates->contains($mandate)) {
            $this->mandates->add($mandate);
            $mandate->setMandate($this);
        }

        return $this;
    }

    public function removeMandate(self $mandate): static
    {
        if ($this->mandates->removeElement($mandate)) {
            // set the owning side to null (unless already changed)
            if ($mandate->getMandate() === $this) {
                $mandate->setMandate(null);
            }
        }

        return $this;
    }

    public function getWhoGiveMandate(): ?Person
    {
        return $this->whoGiveMandate;
    }

    public function setWhoGiveMandate(?Person $whoGiveMandate): static
    {
        $this->whoGiveMandate = $whoGiveMandate;

        return $this;
    }

    public function getFileMandate(): ?string
    {
        return $this->fileMandate;
    }

    public function setFileMandate(?string $fileMandate): static
    {
        $this->fileMandate = $fileMandate;

        return $this;
    }

    public function isIsPresent(): bool
    {
        return $this->isPresent;
    }

    public function setIsPresent(bool $isPresent): static
    {
        $this->isPresent = $isPresent;

        return $this;
    }
}
