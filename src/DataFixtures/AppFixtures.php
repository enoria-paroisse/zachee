<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\Diocese;
use App\Entity\OrganizationType;
use App\Entity\Period;
use App\Entity\Person;
use App\Entity\SubOrganizationMembershipFee;
use App\Entity\SubOrganizationMembershipFeePeriod;
use App\Entity\Title;
use App\Entity\TypeAssociatedPerson;
use App\Entity\User;
use App\Entity\Wasselynck;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    public function __construct(private readonly UserPasswordHasherInterface $encoder)
    {
    }

    public function load(ObjectManager $manager): void
    {
        // Cotisation d'une paroisse membre d'un diocèse membre
        $subOrganizationMembershipFee = new SubOrganizationMembershipFee();
        $subOrganizationMembershipFee->setName('Base');
        $manager->persist($subOrganizationMembershipFee);

        // Première période
        $period = new Period();
        $period->setName('2020')
            ->setStart(new \DateTime('2020-01-01'))
            ->setEnd(new \DateTime('2020-12-31'))
            ->setMembershipfeeNatural('10.00')
            ->setMembershipfeeLegal('600.00')
            ->setDioceseBaseWasselynck(37350)
            ->setDioceseBaseMaxMembershipfee('14000.00')
        ;
        $manager->persist($period);
        $subOrganizationMembershipFeePeriod = new SubOrganizationMembershipFeePeriod();
        $subOrganizationMembershipFeePeriod->setMembershipFee('100.00')
            ->setPeriod($period)
            ->setSubOrganizationMembershipFee($subOrganizationMembershipFee)
        ;
        $manager->persist($subOrganizationMembershipFeePeriod);

        // Les diocèses
        $dioceses = [
            ['name' => 'Paris', 'indice' => 115072],
            ['name' => 'Lyon', 'indice' => 37350],
            ['name' => 'Versailles', 'indice' => 29957],
            ['name' => 'Nanterre', 'indice' => 29465],
            ['name' => 'Lille', 'indice' => 27535],
            ['name' => 'Nantes', 'indice' => 23808],
            ['name' => 'Fréjus-Toulon', 'indice' => 19293],
            ['name' => 'Bordeaux et Bazas', 'indice' => 18815],
            ['name' => 'Rennes', 'indice' => 18050],
            ['name' => 'Arras', 'indice' => 17835],
            ['name' => 'Nice', 'indice' => 16390],
            ['name' => 'Créteil', 'indice' => 15677],
            ['name' => 'Vannes', 'indice' => 15261],
            ['name' => 'Quimper et Léon', 'indice' => 15124],
            ['name' => 'Grenoble (inc. ZV)', 'indice' => 14216],
            ['name' => 'Luçon', 'indice' => 14017],
            ['name' => 'Annecy', 'indice' => 13701],
            ['name' => 'Meaux', 'indice' => 13092],
            ['name' => 'Bayeux et Lisieux', 'indice' => 13086],
            ['name' => 'Toulouse', 'indice' => 12993],
            ['name' => 'Aix-en-Provence et Arles', 'indice' => 12905],
            ['name' => 'Cambrai', 'indice' => 12689],
            ['name' => 'Évry', 'indice' => 12590],
            ['name' => 'Marseille', 'indice' => 12546],
            ['name' => 'Angers', 'indice' => 12019],
            ['name' => 'Bayonne', 'indice' => 11944],
            ['name' => 'Montpellier', 'indice' => 11021],
            ['name' => 'Beauvais', 'indice' => 10925],
            ['name' => 'Dijon', 'indice' => 10888],
            ['name' => 'Nançy (et Toul)', 'indice' => 10841],
            ['name' => 'Rouen', 'indice' => 10690],
            ['name' => 'Saint-Brieuc (et Tréguier)', 'indice' => 10511],
            ['name' => 'Besançon', 'indice' => 10447],
            ['name' => 'Poitiers', 'indice' => 10105],
            ['name' => 'Saint-Étienne', 'indice' => 10038],
            ['name' => 'Fort de France (Martinique)', 'indice' => 9535],
            ['name' => 'Clermont', 'indice' => 9452],
            ['name' => 'Autun', 'indice' => 9328],
            ['name' => 'Coutances et Avranches', 'indice' => 9264],
            ['name' => 'Pontoise', 'indice' => 8264],
            ['name' => 'Orléans', 'indice' => 8974],
            ['name' => 'Le Mans', 'indice' => 8940],
            ['name' => 'Saint-Denis-en-France', 'indice' => 8616],
            ['name' => 'Belley-Ars', 'indice' => 8601],
            ['name' => 'La Rochelle (et Saintes)', 'indice' => 8525],
            ['name' => 'Rodez et Vabres', 'indice' => 7775],
            ['name' => 'Nîmes', 'indice' => 7739],
            ['name' => 'Séez', 'indice' => 7680],
            ['name' => 'Aire et Dax', 'indice' => 7670],
            ['name' => 'Bourges', 'indice' => 7581],
            ['name' => 'Viviers', 'indice' => 7124],
            ['name' => 'Saint-Dié', 'indice' => 7025],
            ['name' => 'Périgueux (et Sarlat)', 'indice' => 6975],
            ['name' => 'Avignon', 'indice' => 6791],
            ['name' => 'Tours', 'indice' => 6773],
            ['name' => 'Soissons', 'indice' => 6749],
            ['name' => 'Le Puy', 'indice' => 6734],
            ['name' => 'Reims', 'indice' => 6689],
            ['name' => 'Amiens', 'indice' => 6545],
            ['name' => 'Albi', 'indice' => 6530],
            ['name' => 'Valence', 'indice' => 6317],
            ['name' => 'Évreux', 'indice' => 6274],
            ['name' => 'Limoges', 'indice' => 6035],
            ['name' => 'Chambéry', 'indice' => 5886],
            ['name' => 'Chartres', 'indice' => 5866],
            ['name' => 'Laval', 'indice' => 5365],
            ['name' => 'Blois', 'indice' => 5166],
            ['name' => 'Basse Terre (Guadeloupe)', 'indice' => 5079],
            ['name' => 'Moulins', 'indice' => 4985],
            ['name' => 'Sens - Auxerre', 'indice' => 4947],
            ['name' => 'Saint Claude', 'indice' => 4618],
            ['name' => 'Chalons-en-Champagne', 'indice' => 4514],
            ['name' => 'Saint-Flour', 'indice' => 4309],
            ['name' => 'Perpignan-Elne', 'indice' => 4295],
            ['name' => 'Montauban', 'indice' => 4250],
            ['name' => 'Angoulême', 'indice' => 4219],
            ['name' => 'Le Havre', 'indice' => 4171],
            ['name' => 'Belfort', 'indice' => 4129],
            ['name' => 'Carcassonne', 'indice' => 4111],
            ['name' => 'Strasbourg', 'indice' => 4057],
            ['name' => 'Tarbes et Lourdes', 'indice' => 3952],
            ['name' => 'Nevers', 'indice' => 3838],
            ['name' => 'Agen', 'indice' => 3812],
            ['name' => 'Mende', 'indice' => 3512],
            ['name' => 'Auch', 'indice' => 3383],
            ['name' => 'Troyes', 'indice' => 3283],
            ['name' => 'Ajaccio', 'indice' => 2365],
            ['name' => 'Gap', 'indice' => 3262],
            ['name' => 'Verdun', 'indice' => 2948],
            ['name' => 'Cahors', 'indice' => 2891],
            ['name' => 'Tulle', 'indice' => 2674],
            ['name' => 'Saint-Denis-de La Réunion', 'indice' => 2365],
            ['name' => 'Digne', 'indice' => 2339],
            ['name' => 'Langres', 'indice' => 2333],
            ['name' => 'Metz', 'indice' => 2004],
            ['name' => 'Tarentaise', 'indice' => 1930],
            ['name' => 'Cayenne', 'indice' => 1605],
            ['name' => 'Pamiers', 'indice' => 1433],
            ['name' => 'Armées', 'indice' => 1398],
            ['name' => 'Maurienne', 'indice' => 881],
        ];

        array_walk($dioceses, static function ($dioceseData) use ($manager, $period, $subOrganizationMembershipFee): void {
            $indice = new Wasselynck();
            $diocese = new Diocese();
            $indice->setDiocese($diocese)
                ->setPeriod($period)
                ->setIndice($dioceseData['indice'])
            ;
            $diocese->setName($dioceseData['name'])
                ->addWasselynck($indice)
                ->setSubOrganizationMembershipFee($subOrganizationMembershipFee)
            ;
            $manager->persist($indice);
            $manager->persist($diocese);
        });

        // titres
        $monsieur = new Title();
        $monsieur->setSex('M')
            ->setName('Monsieur')
        ;
        $manager->persist($monsieur);

        $titles = [
            ['sex' => 'F', 'name' => 'Madame'],
            ['sex' => 'F', 'name' => 'Mademoiselle'],
            ['sex' => 'M', 'name' => 'Frère'],
            ['sex' => 'F', 'name' => 'Sœur'],
            ['sex' => 'M', 'name' => 'Père'],
            ['sex' => 'M', 'name' => 'M. l’abbé'],
            ['sex' => 'M', 'name' => 'Don'],
            ['sex' => 'M', 'name' => 'Monseigneur'],
        ];
        array_walk($titles, static function ($titleData, $key, $manager): void {
            $title = new Title();
            $title->setSex($titleData['sex'])
                ->setName($titleData['name'])
            ;
            $manager->persist($title);
        }, $manager);

        // Types d'organisations
        $typeOrganizations = ['Paroisse', 'Ensemble paroissial', 'Doyenné', 'Diocèse', 'Communauté religieuse'];
        array_walk($typeOrganizations, static function ($typeName, $key, $manager): void {
            $typeOrganization = new OrganizationType();
            $typeOrganization->setName($typeName);
            $manager->persist($typeOrganization);
        }, $manager);

        // Types d'association personne-organisation
        $types = [
            ['name' => 'Autre', 'description' => 'Contact ne faisant pas partie des autres types', 'icon' => 'user'],
            ['name' => 'Secrétaire', 'description' => 'Secrétaire paroissial(e)', 'icon' => 'pen-fancy'],
            ['name' => 'Responsable', 'description' => 'Responsable légal de l’organisation', 'icon' => 'crown'],
            ['name' => 'Comptable', 'description' => 'Contact comptabilité', 'icon' => 'money-check'],
            ['name' => 'Admin référent', 'description' => 'Responsable Enoria de la paroisse', 'icon' => 'cogs'],
            ['name' => 'Représentant', 'description' => 'Représentant de l’organisation aux AG (si différent du responsable légal)', 'icon' => 'users'],
        ];
        array_walk($types, static function ($typeData, $key, $manager): void {
            $type = new TypeAssociatedPerson();
            $type->setName($typeData['name'])
                ->setDescription($typeData['description'])
                ->setIcon($typeData['icon'])
            ;
            $manager->persist($type);
        }, $manager);

        // Premier utilisateur
        $person = new Person();
        $address = new Address();
        $person->setEmail('admin@enoria-paroisse.fr')
            ->setFirstname('Administrateur')
            ->setLastname('Enoria')
            ->setSex('M')
            ->setTitle($monsieur)
            ->setAddress($address)
        ;
        $address->setAddressLineA('49 rue du Général de Gaulle')
            ->setCity('Melun')
            ->setCountry('FR')
            ->setPostalCode('77000')
        ;
        $manager->persist($address);
        $manager->persist($person);

        // On enregistre le tout
        $manager->flush();

        // On enregistre l'user
        $user = new User();
        $user->setUsername('admin')
            ->setPassword($this->encoder->hashPassword($user, 'pass'))
            ->setPerson($person)
            ->setRoles(['ROLE_ADMIN'])
            ->setEnabled(true)
            ->setPeriod($period)
        ;
        $manager->persist($user);
        $manager->flush();
    }
}
