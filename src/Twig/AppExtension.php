<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Twig;

use App\Entity\Period;
use App\Entity\User;
use App\Services\GrantedService;
use App\Services\MembershipFeeService;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function __construct(private readonly GrantedService $grantedService, private readonly MembershipFeeService $membershipFeeService)
    {
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('auto_phone_number_format', $this->autoPhoneNumberFormat(...)),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('userIsGranted', $this->userIsGranted(...)),
            new TwigFunction('getDiocesePricing', $this->getDiocesePricing(...)),
        ];
    }

    public function userIsGranted(User $user, string $attribute, mixed $subject = null): bool
    {
        return $this->grantedService->isGranted($user, $attribute, $subject);
    }

    public function getDiocesePricing(int $indice, Period $period): float
    {
        return $this->membershipFeeService->getDiocesePricing($indice, $period);
    }

    public function autoPhoneNumberFormat(PhoneNumber $phone, string $country = 'FR'): string
    {
        if ($country && PhoneNumberUtil::getInstance()->getRegionCodeForNumber($phone) === $country) {
            return PhoneNumberUtil::getInstance()->format($phone, PhoneNumberFormat::NATIONAL);
        }

        return PhoneNumberUtil::getInstance()->format($phone, PhoneNumberFormat::INTERNATIONAL);
    }
}
