<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\Entity;
use App\Entity\Instance;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Entity>
 *
 * @method null|Entity find($id, $lockMode = null, $lockVersion = null)
 * @method null|Entity findOneBy(mixed[] $criteria, string[] $orderBy = null)
 * @method Entity[]    findAll()
 * @method Entity[]    findBy(mixed[] $criteria, string[] $orderBy = null, $limit = null, $offset = null)
 */
class EntityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Entity::class);
    }

    public function save(Entity $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Entity $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return Entity[]
     */
    public function findByInstance(Instance $instance): array
    {
        return $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.instance = :instance')
            ->setParameter('instance', $instance)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Entity[]
     */
    public function findByInstanceFullJoin(Instance $instance): array
    {
        return $this->createQueryBuilder('e')
            ->select('e', 'm', 'pr', 'dms', 'f', 'sms')
            ->leftJoin('e.memberships', 'm')
            ->leftJoin('m.paymentRequest', 'pr')
            ->leftJoin('pr.dioceseMembershipSupplement', 'dms')
            ->leftJoin('pr.file', 'f')
            ->leftJoin('pr.smsBilling', 'sms')
            ->where('e.instance = :instance')
            ->setParameter('instance', $instance)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findByInstanceAndId(Instance $instance, int $id): ?Entity
    {
        return $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.instance = :instance')
            ->andWhere('e.idInInstance = :id')
            ->setParameter('instance', $instance)
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
