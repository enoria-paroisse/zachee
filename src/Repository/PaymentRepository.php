<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\Payment;
use App\Entity\Period;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Payment>
 *
 * @method null|Payment find($id, $lockMode = null, $lockVersion = null)
 * @method null|Payment findOneBy(mixed[] $criteria, string[] $orderBy = null)
 * @method Payment[]    findAll()
 * @method Payment[]    findBy(mixed[] $criteria, string[] $orderBy = null, $limit = null, $offset = null)
 */
class PaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Payment::class);
    }

    public function save(Payment $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Payment $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return Payment[] Returns an array of PaymentRequest objects
     */
    public function findByPeriodAndStatus(Period $period, string $status): array
    {
        return $this->getQueryByPeriod($period)
            ->andWhere('p.status = :status')
            ->setParameter('status', $status)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Payment[] Returns an array of PaymentRequest objects
     */
    public function findByStatus(string $status): array
    {
        return $this->getBaseQuery()
            ->where('p.status = :status')
            ->setParameter('status', $status)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Payment[] Returns an array of PaymentRequest objects
     */
    public function findMissingReceipt(): array
    {
        return $this->getBaseQuery()
            ->andWhere('r IS NULL')
            ->andWhere('p.status = :status')
            ->setParameter('status', Payment::STATUS_VALIDATED)
            ->getQuery()
            ->getResult()
        ;
    }

    protected function getQueryByPeriod(Period $period): QueryBuilder
    {
        return $this->getBaseQuery()
            ->where('p.datetime >= :start')
            ->andWhere('p.datetime <= :end')
            ->setParameter('start', $period->getStart())
            ->setParameter('end', $period->getEnd())
        ;
    }

    protected function getBaseQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('p')
            ->select(['p', 'r', 'o', 'd', 'person', 'user', 'pa'])
            ->leftJoin('p.receipt', 'r')
            ->leftJoin('p.organization', 'o')
            ->leftJoin('o.diocese', 'd')
            ->leftJoin('p.person', 'person')
            ->leftJoin('person.user', 'user')
            ->leftJoin('p.paymentAllocations', 'pa')
        ;
    }
}
