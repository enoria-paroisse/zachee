<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\Diocese;
use App\Entity\Period;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Diocese>
 *
 * @method null|Diocese find($id, $lockMode = null, $lockVersion = null)
 * @method null|Diocese findOneBy(mixed[] $criteria, string[] $orderBy = null)
 * @method Diocese[]    findAll()
 * @method Diocese[]    findBy(mixed[] $criteria, string[] $orderBy = null, $limit = null, $offset = null)
 */
class DioceseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Diocese::class);
    }

    /**
     * @return Diocese[]
     */
    public function findAllWithOrganizationsByPeriod(Period $period): array
    {
        return $this->createQueryBuilder('d')
            ->select('d', 'o', 'w', 'somf', 'dms', 'dmspr', 'dmsprf', 'm', 'pr', 'prdms', 'prf', 'pa', 'p', 'r', 'sms', 'sms2')
            ->leftJoin('d.organizations', 'o')
            ->leftJoin('d.wasselynck', 'w', 'WITH', 'w.period = :period')
            ->leftJoin('d.subOrganizationMembershipFee', 'somf')
            ->leftJoin('d.dioceseMembershipSupplement', 'dms')
            ->leftJoin('dms.paymentRequest', 'dmspr')
            ->leftJoin('dmspr.file', 'dmsprf')
            ->leftJoin('o.memberships', 'm', 'WITH', 'm.period = :period')
            ->leftJoin('m.paymentRequest', 'pr')
            ->leftJoin('pr.dioceseMembershipSupplement', 'prdms')
            ->leftJoin('pr.file', 'prf')
            ->leftJoin('pr.paymentAllocations', 'pa')
            ->leftJoin('pa.payment', 'p')
            ->leftJoin('p.receipt', 'r')
            ->leftJoin('pr.smsBilling', 'sms')
            ->leftJoin('dmspr.smsBilling', 'sms2')
            ->setParameter('period', $period)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findOneWithCurrentPeriod(int $id, Period $period): ?Diocese
    {
        return $this->createQueryBuilder('d')
            ->select('d', 'o', 'w')
            ->leftJoin('d.organizations', 'o')
            ->leftJoin('d.wasselynck', 'w', 'WITH', 'w.period = :period')
            ->where('d.id = :id')
            ->setParameter('id', $id)
            ->setParameter('period', $period)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findByIdFullJoin(int $id): ?Diocese
    {
        return $this->createQueryBuilder('d')
            ->select('d', 'o', 'w', 'somf', 'dms', 'dmspr', 'dmsprf', 'm', 'e', 'i', 'pr', 'prf', 'prms', 'pa', 'p', 'r', 'sms', 'sms2')
            ->leftJoin('d.organizations', 'o')
            ->leftJoin('d.wasselynck', 'w')
            ->leftJoin('d.subOrganizationMembershipFee', 'somf')
            ->leftJoin('d.dioceseMembershipSupplement', 'dms')
            ->leftJoin('dms.paymentRequest', 'dmspr')
            ->leftJoin('dmspr.file', 'dmsprf')
            ->leftJoin('o.memberships', 'm')
            ->leftJoin('m.entity', 'e')
            ->leftJoin('e.instance', 'i')
            ->leftJoin('m.paymentRequest', 'pr')
            ->leftJoin('pr.file', 'prf')
            ->leftJoin('pr.dioceseMembershipSupplement', 'prms')
            ->leftJoin('pr.paymentAllocations', 'pa')
            ->leftJoin('pa.payment', 'p')
            ->leftJoin('p.receipt', 'r')
            ->leftJoin('pr.smsBilling', 'sms')
            ->leftJoin('dmspr.smsBilling', 'sms2')
            ->where('d.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @return Diocese[]
     */
    public function search(string $search): array
    {
        return $this
            ->createQueryBuilder('d')
            ->where('d.name LIKE :search')
            ->setParameter('search', '%'.$search.'%')
            ->getQuery()
            ->getResult()
        ;
    }
}
