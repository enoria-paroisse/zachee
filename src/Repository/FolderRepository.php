<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\Folder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Folder>
 *
 * @method null|Folder find($id, $lockMode = null, $lockVersion = null)
 * @method null|Folder findOneBy(mixed[] $criteria, string[] $orderBy = null)
 * @method Folder[]    findAll()
 * @method Folder[]    findBy(mixed[] $criteria, string[] $orderBy = null, $limit = null, $offset = null)
 */
class FolderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Folder::class);
    }

    public function save(Folder $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Folder $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return Folder[]
     */
    public function findAllRoot(): array
    {
        return $this->createQueryBuilder('f')
            ->select(['f', 'd'])
            ->leftJoin('f.docs', 'd')
            ->where('f.parent IS NULL')
            ->orderBy('f.ref', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param string[] $tags
     *
     * @return Folder[]
     */
    public function findByTags(array $tags, bool $and = true): array
    {
        $queryBuilder = $this->createQueryBuilder('f')
            ->select(['f', 'd'])
            ->leftJoin('f.docs', 'd')
            ->orderBy('f.ref', 'ASC')
        ;

        foreach ($tags as $tag) {
            $tagName = str_replace('-', '_', $tag);
            if ($and) {
                $queryBuilder->andWhere('f.tags LIKE :tag_'.$tagName);
            } else {
                $queryBuilder->orWhere('f.tags LIKE :tag_'.$tagName);
            }
            $queryBuilder->setParameter('tag_'.$tagName, '%'.$tag.'%');
        }

        return $queryBuilder
            ->getQuery()
            ->getResult()
        ;
    }
}
