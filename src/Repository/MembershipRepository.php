<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\Diocese;
use App\Entity\Membership;
use App\Entity\Organization;
use App\Entity\Period;
use App\Entity\Person;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Membership>
 *
 * @method null|Membership find($id, $lockMode = null, $lockVersion = null)
 * @method null|Membership findOneBy(mixed[] $criteria, string[] $orderBy = null)
 * @method Membership[]    findAll()
 * @method Membership[]    findBy(mixed[] $criteria, string[] $orderBy = null, $limit = null, $offset = null)
 */
class MembershipRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Membership::class);
    }

    public function save(Membership $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Membership $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return Membership[]
     */
    public function findAllFullJoin(): array
    {
        return $this->addJoin($this->createQueryBuilder('m'))
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Membership[]
     */
    public function findLegalPersonInProgressByPeriod(Period $period): array
    {
        return $this->addJoin($this->andIsLegalPerson($this->andWhereStatus($this->getQueryByPeriod($period), Membership::STATUS_IN_PROGRESS)))
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Membership[]
     */
    public function findNaturalPersonInProgressByPeriod(Period $period): array
    {
        return $this->addJoin($this->andIsNaturalPerson($this->andWhereStatus($this->getQueryByPeriod($period), Membership::STATUS_IN_PROGRESS)))
            ->leftJoin('p.memberGroups', 'g')
            ->addSelect(['g'])
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Membership[]
     */
    public function findLegalPersonCanceledByPeriod(Period $period): array
    {
        return $this->addJoin($this->andIsLegalPerson($this->andWhereStatus($this->getQueryByPeriod($period), Membership::STATUS_CANCELED)))
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Membership[]
     */
    public function findNaturalPersonCanceledByPeriod(Period $period): array
    {
        return $this->addJoin($this->andIsNaturalPerson($this->andWhereStatus($this->getQueryByPeriod($period), Membership::STATUS_CANCELED)))
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Membership[]
     */
    public function findLegalPersonValidatedByPeriod(Period $period): array
    {
        return $this->addJoin($this->andIsLegalPerson($this->andWhereStatus($this->getQueryByPeriod($period), Membership::STATUS_VALIDATED)))
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Membership[]
     */
    public function findNaturalPersonValidatedByPeriod(Period $period): array
    {
        return $this->addJoin($this->andIsNaturalPerson($this->andWhereStatus($this->getQueryByPeriod($period), Membership::STATUS_VALIDATED)))
            ->leftJoin('p.memberGroups', 'g')
            ->addSelect(['g'])
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Membership[]
     */
    public function findByPeriod(int|Period $period): array
    {
        return $this->createQueryBuilder('m')
            ->where('m.period = :period')
            ->setParameter('period', $period)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Membership[]
     */
    public function findByPeriodFullJoin(Period $period): array
    {
        return $this->createQueryBuilder('m')
            ->select(['m', 'mpr', 'p', 'pt', 'pu', 'mp', 'pao', 'pat', 'po', 'o', 'mo', 'd', 'mprf', 'mprs', 'mprdms'])
            ->leftJoin('m.paymentRequest', 'mpr')
            ->leftJoin('m.person', 'p')
            ->leftJoin('p.title', 'pt')
            ->leftJoin('p.user', 'pu')
            ->leftJoin('p.memberships', 'mp', 'WITH', 'mp.period = :period')
            ->leftJoin('p.associatedOrganizations', 'pao')
            ->leftJoin('pao.type', 'pat')
            ->leftJoin('pao.organization', 'po')
            ->leftJoin('m.organization', 'o')
            ->leftJoin('o.memberships', 'mo', 'WITH', 'mo.period = :period')
            ->leftJoin('o.diocese', 'd')
            ->leftJoin('mpr.file', 'mprf')
            ->leftJoin('mpr.smsBilling', 'mprs')
            ->leftJoin('mpr.dioceseMembershipSupplement', 'mprdms')
            ->where('m.period = :period')
            ->setParameter('period', $period)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findOneByPeriodOrganizationPersonFullJoin(Period $period, ?Organization $organization, ?Person $person): ?Membership
    {
        $queryBuilder = $this->createQueryBuilder('m')
            ->select(['m', 'mpr', 'p', 'pt', 'pu', 'mp', 'pao', 'pat', 'po', 'o', 'mo', 'd', 'mprf', 'mprs', 'mprdms'])
            ->leftJoin('m.paymentRequest', 'mpr')
            ->leftJoin('m.person', 'p')
            ->leftJoin('p.title', 'pt')
            ->leftJoin('p.user', 'pu')
            ->leftJoin('p.memberships', 'mp', 'WITH', 'mp.period = :period')
            ->leftJoin('p.associatedOrganizations', 'pao')
            ->leftJoin('pao.type', 'pat')
            ->leftJoin('pao.organization', 'po')
            ->leftJoin('m.organization', 'o')
            ->leftJoin('o.memberships', 'mo', 'WITH', 'mo.period = :period')
            ->leftJoin('o.diocese', 'd')
            ->leftJoin('mpr.file', 'mprf')
            ->leftJoin('mpr.smsBilling', 'mprs')
            ->leftJoin('mpr.dioceseMembershipSupplement', 'mprdms')
            ->where('m.period = :period')
            ->setParameter('period', $period)
        ;

        if ($organization) {
            $queryBuilder->andWhere('m.organization = :organization')
                ->setParameter('organization', $organization)
            ;
        } else {
            $queryBuilder->andWhere('m.organization IS NULL');
        }
        if ($person) {
            $queryBuilder->andWhere('m.person = :person')
                ->setParameter('person', $person)
            ;
        } else {
            $queryBuilder->andWhere('m.person IS NULL');
        }

        return $queryBuilder->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult()
        ;
    }

    /**
     * @return Membership[]
     */
    public function findOrganizationsByPeriodFullJoin(Period $period): array
    {
        return $this->createQueryBuilder('m')
            ->select(['m', 'o', 'd', 'a', 'pr', 'prdms', 'prf', 'ap', 'p', 'pt', 'mp', 'mppr', 'mpprdms', 'mpprf', 'apt', 'user', 'g', 'pao', 'paoo'])
            ->leftJoin('m.organization', 'o')
            ->leftJoin('o.associatedPeople', 'ap')
            ->leftJoin('o.diocese', 'd')
            ->leftJoin('o.address', 'a')
            ->leftJoin('m.paymentRequest', 'pr')
            ->leftJoin('pr.dioceseMembershipSupplement', 'prdms')
            ->leftJoin('pr.file', 'prf')
            ->leftJoin('ap.person', 'p')
            ->leftJoin('p.title', 'pt')
            ->leftJoin('p.memberships', 'mp', 'WITH', 'mp.period = :period')
            ->leftJoin('mp.paymentRequest', 'mppr')
            ->leftJoin('mppr.dioceseMembershipSupplement', 'mpprdms')
            ->leftJoin('mppr.file', 'mpprf')
            ->leftJoin('ap.type', 'apt')
            ->leftJoin('p.user', 'user')
            ->leftJoin('p.memberGroups', 'g')
            ->leftJoin('p.associatedOrganizations', 'pao')
            ->leftJoin('pao.organization', 'paoo')
            ->where('m.period = :period')
            ->andWhere('m.organization IS NOT NULL')
            ->setParameter('period', $period)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Membership[]
     */
    public function findPeopleByPeriodFullJoin(Period $period): array
    {
        return $this->createQueryBuilder('m')
            ->select(['m', 'mpr', 'mprdms', 'mprf', 'p', 'pt', 'pu', 'mp', 'g', 'pao', 'pat', 'po', 'o', 'mo'])
            ->leftJoin('m.paymentRequest', 'mpr')
            ->leftJoin('mpr.dioceseMembershipSupplement', 'mprdms')
            ->leftJoin('mpr.file', 'mprf')
            ->leftJoin('m.person', 'p')
            ->leftJoin('p.title', 'pt')
            ->leftJoin('p.user', 'pu')
            ->leftJoin('p.memberships', 'mp', 'WITH', 'mp.period = :period')
            ->leftJoin('p.memberGroups', 'g')
            ->leftJoin('p.associatedOrganizations', 'pao')
            ->leftJoin('pao.type', 'pat')
            ->leftJoin('pao.organization', 'po')
            ->leftJoin('m.organization', 'o')
            ->leftJoin('o.memberships', 'mo', 'WITH', 'mo.period = :period')
            ->andWhere('m.person IS NOT NULL')
            ->where('m.period = :period')
            ->setParameter('period', $period)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Membership[]
     */
    public function findByPerson(Person $person): array
    {
        return $this->createQueryBuilder('m')
            ->where('m.person = :person')
            ->setParameter('person', $person)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Membership[]
     */
    public function findByOrganization(Organization $organization): array
    {
        return $this->createQueryBuilder('m')
            ->where('m.organization = :organization')
            ->setParameter('organization', $organization)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param array<string, string> $status
     *
     * @return Membership[]
     */
    public function findByDioceseAndPeriod(Diocese $diocese, Period $period, array $status = []): array
    {
        $queryBuilder = $this->andIsLegalPerson($this->getQueryByPeriod($period))
            ->select(['m', 'o', 'd', 'pr'])
            ->innerJoin('m.organization', 'o')
            ->leftJoin('m.paymentRequest', 'pr')
            ->innerJoin('o.diocese', 'd', 'WITH', 'o.diocese = :diocese')
            ->setParameter('diocese', $diocese)
        ;

        $statusWhereQuery = '';
        foreach ($status as $key => $whereStatus) {
            $statusWhereQuery .= ('' !== $statusWhereQuery ? ' OR ' : '').'m.status = :status_'.$key;
            $queryBuilder->setParameter('status_'.$key, $whereStatus);
        }
        $queryBuilder->andWhere($statusWhereQuery);

        return $queryBuilder
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Membership[]
     */
    public function findForRemind(Period $period): array
    {
        return $this->createQueryBuilder('m')
            ->where('m.period = :period')
            ->andWhere('m.status = :status')
            ->andWhere('m.token IS NOT NULL')
            ->setParameter('period', $period)
            ->setParameter('status', Membership::STATUS_IN_PROGRESS)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Membership[]
     */
    public function findAvailableForGeneralMeeting(Period $period): array
    {
        return $this->createQueryBuilder('m')
            ->select(['m', 's', 'gm'])
            ->leftJoin('m.summons', 's')
            ->leftJoin('s.generalMeeting', 'gm')
            ->where('m.period = :period')
            ->andWhere('m.status = :statusValidated OR m.status = :statusInProgress')
            ->setParameter('period', $period)
            ->setParameter('statusValidated', Membership::STATUS_VALIDATED)
            ->setParameter('statusInProgress', Membership::STATUS_IN_PROGRESS)
            ->getQuery()
            ->getResult()
        ;
    }

    protected function getQueryByPeriod(Period $period): QueryBuilder
    {
        return $this->createQueryBuilder('m')
            ->where('m.period = :period')
            ->setParameter('period', $period)
        ;
    }

    protected function andWhereStatus(QueryBuilder $queryBuilder, string $status): QueryBuilder
    {
        return $queryBuilder->andWhere('m.status = :status')
            ->setParameter('status', $status)
        ;
    }

    protected function andIsLegalPerson(QueryBuilder $queryBuilder): QueryBuilder
    {
        return $queryBuilder->andWhere('m.person IS NULL');
    }

    protected function andIsNaturalPerson(QueryBuilder $queryBuilder): QueryBuilder
    {
        return $queryBuilder->andWhere('m.organization IS NULL');
    }

    private function addJoin(QueryBuilder $queryBuilder): QueryBuilder
    {
        return $queryBuilder->select(['m', 'e', 'pr', 'prdms', 'prf', 'pa', 'payment', 'receipt', 'p', 'o', 'user'])
            ->leftJoin('m.entity', 'e')
            ->leftJoin('m.paymentRequest', 'pr')
            ->leftJoin('pr.dioceseMembershipSupplement', 'prdms')
            ->leftJoin('pr.file', 'prf')
            ->leftJoin('pr.paymentAllocations', 'pa')
            ->leftJoin('pa.payment', 'payment')
            ->leftJoin('payment.receipt', 'receipt')
            ->leftJoin('m.person', 'p')
            ->leftJoin('m.organization', 'o')
            ->leftJoin('p.user', 'user')
        ;
    }
}
