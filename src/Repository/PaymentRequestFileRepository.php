<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\PaymentRequestFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PaymentRequestFile>
 *
 * @method null|PaymentRequestFile find($id, $lockMode = null, $lockVersion = null)
 * @method null|PaymentRequestFile findOneBy(mixed[] $criteria, string[] $orderBy = null)
 * @method PaymentRequestFile[]    findAll()
 * @method PaymentRequestFile[]    findBy(mixed[] $criteria, string[] $orderBy = null, $limit = null, $offset = null)
 */
class PaymentRequestFileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PaymentRequestFile::class);
    }

    public function save(PaymentRequestFile $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(PaymentRequestFile $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findMaxRefByPrefix(mixed $prefix): ?int
    {
        $qb = $this->createQueryBuilder('prf');

        $qb->where($qb->expr()->eq('prf.refPrefix', ':prefix'))
            ->setParameter('prefix', $prefix)
        ;

        $qb->select($qb->expr()->max('prf.refNumber'));

        $result = $qb->getQuery()->getOneOrNullResult();

        if (null !== $result) {
            return reset($result);
        }

        return 0;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findByIdFullJoin(int $id): ?PaymentRequestFile
    {
        return $this->createQueryBuilder('f')
            ->select(['f', 'pr', 'o', 'p', 'm', 'dms', 'prf', 'd', 'ao', 'do', 'dom', 'dompr', 'domprdms', 'domprf', 'domprpa'])
            ->leftJoin('f.paymentRequest', 'pr')
            ->leftJoin('pr.organization', 'o')
            ->leftJoin('pr.person', 'p')
            ->leftJoin('pr.membership', 'm')
            ->leftJoin('pr.dioceseMembershipSupplement', 'dms')
            ->leftJoin('pr.file', 'prf')
            ->leftJoin('dms.diocese', 'd')
            ->leftJoin('d.associatedOrganization', 'ao')
            ->leftJoin('d.organizations', 'do')
            ->leftJoin('do.memberships', 'dom')
            ->leftJoin('dom.paymentRequest', 'dompr')
            ->leftJoin('dompr.dioceseMembershipSupplement', 'domprdms')
            ->leftJoin('dompr.file', 'domprf')
            ->leftJoin('dompr.paymentAllocations', 'domprpa')
            ->where('f.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
