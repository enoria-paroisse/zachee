<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\AssociatedPerson;
use App\Entity\Diocese;
use App\Entity\Organization;
use App\Entity\Person;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AssociatedPerson>
 *
 * @method null|AssociatedPerson find($id, $lockMode = null, $lockVersion = null)
 * @method null|AssociatedPerson findOneBy(mixed[] $criteria, string[] $orderBy = null)
 * @method AssociatedPerson[]    findAll()
 * @method AssociatedPerson[]    findBy(mixed[] $criteria, string[] $orderBy = null, $limit = null, $offset = null)
 */
class AssociatedPersonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AssociatedPerson::class);
    }

    /**
     * Need to set the parameter "person".
     *
     * @param string[] $select You can use "a" for AssociatedPerson, "person", or "organization". ex: `['person.id']`
     */
    public function findRelatedByPersonQuery(array $select): QueryBuilder
    {
        $qb = $this->createQueryBuilder('a');

        return $qb
            ->select($select)
            ->where($qb->expr()->orX(
                $qb->expr()->in(
                    'a.organization',
                    $this->getEntityManager()->createQueryBuilder()
                        ->select(['o1.id'])
                        ->from(AssociatedPerson::class, 'a1')
                        ->where('a1.person = :person')
                        ->join('a1.organization', 'o1')
                        ->getDQL()
                ),
                $qb->expr()->in(
                    'a.organization',
                    $this->getEntityManager()->createQueryBuilder()
                        ->select(['o2.id'])
                        ->from(Organization::class, 'o2')
                        ->where($qb->expr()->in(
                            'o2.diocese',
                            $this->getEntityManager()->createQueryBuilder()
                                ->select(['d1.id'])
                                ->from(Diocese::class, 'd1')
                                ->where($qb->expr()->in(
                                    'd1.associatedOrganization',
                                    $this->getEntityManager()->createQueryBuilder()
                                        ->select(['o3.id'])
                                        ->from(AssociatedPerson::class, 'a2')
                                        ->where('a2.person = :person')
                                        ->join('a2.organization', 'o3')
                                        ->getDQL()
                                ))
                                ->getDQL()
                        ))
                        ->getDQL()
                )
            ))
            ->join('a.person', 'person')
            ->join('a.organization', 'organization')
        ;
    }

    public function getQueryBuilder(string $alias): QueryBuilder
    {
        return $this->createQueryBuilder($alias);
    }

    /**
     * @return AssociatedPerson[]
     */
    public function findByPerson(Person $person): array
    {
        return $this->createQueryBuilder('a')
            ->select(['a', 'p', 'o'])
            ->leftJoin('a.person', 'p')
            ->leftJoin('a.organization', 'o')
            ->where('a.person = :person')
            ->setParameter('person', $person)
            ->getQuery()
            ->getResult()
        ;
    }
}
