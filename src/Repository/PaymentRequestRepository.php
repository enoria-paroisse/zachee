<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\Diocese;
use App\Entity\PaymentRequest;
use App\Entity\Period;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PaymentRequest>
 *
 * @method null|PaymentRequest find($id, $lockMode = null, $lockVersion = null)
 * @method null|PaymentRequest findOneBy(mixed[] $criteria, string[] $orderBy = null)
 * @method PaymentRequest[]    findAll()
 * @method PaymentRequest[]    findBy(mixed[] $criteria, string[] $orderBy = null, $limit = null, $offset = null)
 */
class PaymentRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PaymentRequest::class);
    }

    public function save(PaymentRequest $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(PaymentRequest $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return PaymentRequest[] Returns an array of PaymentRequest objects
     */
    public function findByPeriodAndStatus(Period $period, string $status): array
    {
        return $this->getQueryByPeriod($period)
            ->andWhere('p.status = :status')
            ->setParameter('status', $status)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return PaymentRequest[] Returns an array of PaymentRequest objects
     */
    public function findByStatus(string $status): array
    {
        return $this->getBaseQuery()
            ->where('p.status = :status')
            ->setParameter('status', $status)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return PaymentRequest[]
     */
    public function findForRemind(): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.status = :status')
            ->setParameter('status', PaymentRequest::STATUS_IN_PROGRESS)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return PaymentRequest[]
     */
    public function findDioceseMembershipSupplement(Diocese $diocese): array
    {
        return $this->createQueryBuilder('p')
            ->select(['p', 'dms'])
            ->leftJoin('p.dioceseMembershipSupplement', 'dms')
            ->where('dms.diocese = :diocese')
            ->setParameter('diocese', $diocese)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getQueryByPeriod(Period $period): QueryBuilder
    {
        return $this->getBaseQuery()
            ->where('p.datetime >= :start')
            ->andWhere('p.datetime <= :end')
            ->setParameter('start', $period->getStart())
            ->setParameter('end', $period->getEnd())
        ;
    }

    protected function getBaseQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('p')
            ->select(['p', 'o', 'person', 'm', 'dms', 'f', 'd', 'dmo', 'pa', 'payment', 'receipt', 'mo', 'mp', 'user', 'sms'])
            ->leftJoin('p.organization', 'o')
            ->leftJoin('p.person', 'person')
            ->leftJoin('p.membership', 'm')
            ->leftJoin('p.dioceseMembershipSupplement', 'dms')
            ->leftJoin('p.file', 'f')
            ->leftJoin('m.organization', 'mo')
            ->leftJoin('m.person', 'mp')
            ->leftJoin('mp.user', 'user')
            ->leftJoin('o.diocese', 'd')
            ->leftJoin('mo.diocese', 'dmo')
            ->leftJoin('p.paymentAllocations', 'pa')
            ->leftJoin('pa.payment', 'payment')
            ->leftJoin('payment.receipt', 'receipt')
            ->leftJoin('p.smsBilling', 'sms')
        ;
    }
}
