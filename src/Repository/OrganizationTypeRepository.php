<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\OrganizationType;
use App\Entity\Period;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OrganizationType>
 *
 * @method null|OrganizationType find($id, $lockMode = null, $lockVersion = null)
 * @method null|OrganizationType findOneBy(mixed[] $criteria, string[] $orderBy = null)
 * @method OrganizationType[]    findAll()
 * @method OrganizationType[]    findBy(mixed[] $criteria, string[] $orderBy = null, $limit = null, $offset = null)
 */
class OrganizationTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrganizationType::class);
    }

    public function save(OrganizationType $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(OrganizationType $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByIdFullJoin(int $id): ?OrganizationType
    {
        return $this->getQueryFullJoin()
            ->where('t.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findByIdAndPeriod(int $id, Period $period): ?OrganizationType
    {
        return $this->getQueryFullJoin($period)
            ->where('t.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function getQueryFullJoin(?Period $period = null): QueryBuilder
    {
        $query = $this->createQueryBuilder('t')
            ->select(['t', 'o', 'm', 'e', 'i', 'd', 'a', 'pr', 'prdms', 'prf', 'pa', 'payment', 'r', 'ap', 'p', 'user', 'pt', 'apt', 'u'])
            ->leftJoin('t.organizations', 'o')
        ;

        if (null !== $period) {
            $query
                ->leftJoin('o.memberships', 'm', 'WITH', 'm.period = :period')
                ->setParameter('period', $period)
            ;
        } else {
            $query->leftJoin('o.memberships', 'm');
        }

        $query->leftJoin('m.entity', 'e')
            ->leftJoin('e.instance', 'i')
            ->leftJoin('o.associatedPeople', 'ap')
            ->leftJoin('o.diocese', 'd')
            ->leftJoin('o.address', 'a')
            ->leftJoin('m.paymentRequest', 'pr')
            ->leftJoin('pr.dioceseMembershipSupplement', 'prdms')
            ->leftJoin('pr.file', 'prf')
            ->leftJoin('pr.paymentAllocations', 'pa')
            ->leftJoin('pa.payment', 'payment')
            ->leftJoin('payment.receipt', 'r')
            ->leftJoin('ap.person', 'p')
            ->leftJoin('p.user', 'user')
            ->leftJoin('p.title', 'pt')
            ->leftJoin('ap.type', 'apt')
            ->leftJoin('o.updatedBy', 'u')
        ;

        return $query;
    }
}
