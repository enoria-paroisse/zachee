<?php

/**
 * @copyright Copyright (c) 2025 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\User;
use App\Entity\WebauthnCredential;
use Doctrine\Persistence\ManagerRegistry;
use Webauthn\Bundle\Repository\DoctrineCredentialSourceRepository;
use Webauthn\PublicKeyCredentialSource;

/**
 * @extends DoctrineCredentialSourceRepository<WebauthnCredential>
 */
final class WebauthnCredentialRepository extends DoctrineCredentialSourceRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WebauthnCredential::class);
    }

    public function saveCredentialSource(PublicKeyCredentialSource $publicKeyCredentialSource, ?User $user = null, ?string $keyName = null): void
    {
        if (!$publicKeyCredentialSource instanceof WebauthnCredential) {
            $publicKeyCredentialSource = new WebauthnCredential(
                $publicKeyCredentialSource->publicKeyCredentialId,
                $publicKeyCredentialSource->type,
                $publicKeyCredentialSource->transports,
                $publicKeyCredentialSource->attestationType,
                $publicKeyCredentialSource->trustPath,
                $publicKeyCredentialSource->aaguid,
                $publicKeyCredentialSource->credentialPublicKey,
                $publicKeyCredentialSource->userHandle,
                $publicKeyCredentialSource->counter,
                $keyName
            );
        }
        parent::saveCredentialSource($publicKeyCredentialSource);
    }

    /**
     * @return WebauthnCredential[]
     */
    public function findAllForUser(User $user): array
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->from($this->class, 'c')
            ->select('c')
            ->where('c.userHandle = :userHandle')
            ->setParameter(':userHandle', $user->getId())
            ->getQuery()
            ->getArrayResult()
        ;
    }

    public function removeCredentialForUser(User $user, string $credentialId): void
    {
        $this->getEntityManager()
            ->createQueryBuilder()
            ->delete($this->class, 'c')
            ->where('c.userHandle = :userHandle')
            ->andWhere('c.id = :credentialId')
            ->setParameter(':userHandle', $user->getId())
            ->setParameter(':credentialId', $credentialId)
            ->getQuery()
            ->execute()
        ;
    }

    public function renameCredentialForUser(User $user, string $credentialId, string $keyName): void
    {
        $this->getEntityManager()
            ->createQueryBuilder()
            ->update($this->class, 'c')
            ->set('c.keyName', ':keyName')
            ->where('c.userHandle = :userHandle')
            ->andWhere('c.id = :credentialId OR c.publicKeyCredentialId = :credentialIdB64')
            ->setParameter(':userHandle', $user->getId())
            ->setParameter(':credentialId', $credentialId)
            ->setParameter(':credentialIdB64', self::convertToBase64ID($credentialId))
            ->setParameter(':keyName', $keyName)
            ->getQuery()
            ->execute()
        ;
    }

    /**
     * Convert a base64url encoded string to a base64 encoded string.
     */
    private static function convertToBase64ID(string $string): string
    {
        // + can have been replaced by - and / by _ in the base64url encoding
        $string = str_replace(['-', '_'], ['+', '/'], $string);

        // Add = padding
        $mod = strlen($string) % 4;

        if (2 === $mod) {
            return $string.'==';
        }

        if (3 === $mod) {
            return $string.'=';
        }

        return $string;
    }
}
