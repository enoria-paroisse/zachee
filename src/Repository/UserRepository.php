<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\Period;
use App\Entity\Person;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method null|User find($id, $lockMode = null, $lockVersion = null)
 * @method null|User findOneBy(mixed[] $criteria, string[] $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(mixed[] $criteria, string[] $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function save(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function loadUserByUsername(string $username): ?User
    {
        $user = $this->createQueryBuilder('u')
            ->where('u.username = :username')
            ->setParameter('username', $username)
            ->getQuery()
            ->getOneOrNullResult()
        ;
        if ($user instanceof User) {
            return $user;
        }

        return null;
    }

    public function findByUsernameAndMail(string $username, string $email): ?User
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->leftJoin('u.person', 'p')
            ->where('p.email = :email')
            ->andWhere('u.username = :username')
            ->setParameter('email', $email)
            ->setParameter('username', $username)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findOneByPerson(Person $person): ?User
    {
        return $this->createQueryBuilder('u')
            ->where('u.person = :person')
            ->setParameter('person', $person)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @return User[]
     */
    public function findByPeriod(Period $period): array
    {
        return $this->createQueryBuilder('u')
            ->where('u.period = :period')
            ->setParameter('period', $period)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return User[]
     */
    public function findAllFullJoin(?bool $enabled = null): array
    {
        $queryBuilder = $this->createQueryBuilder('u')
            ->select(['u', 'p'])
            ->leftJoin('u.person', 'p')
        ;

        if (null !== $enabled) {
            $queryBuilder->where('u.enabled = :enabled')->setParameter('enabled', $enabled);
        }

        return $queryBuilder
            ->getQuery()
            ->getResult()
        ;
    }
}
