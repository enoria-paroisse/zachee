<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\Aggregate;
use App\Entity\Period;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Aggregate>
 *
 * @method null|Aggregate find($id, $lockMode = null, $lockVersion = null)
 * @method null|Aggregate findOneBy(mixed[] $criteria, string[] $orderBy = null)
 * @method Aggregate[]    findAll()
 * @method Aggregate[]    findBy(mixed[] $criteria, string[] $orderBy = null, $limit = null, $offset = null)
 */
class AggregateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Aggregate::class);
    }

    public function save(Aggregate $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Aggregate $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return Aggregate[]
     */
    public function findAllFullJoin(): array
    {
        return $this->createQueryBuilder('a')
            ->select(['a', 'n', 'ot', 'at', 'g'])
            ->leftJoin('a.newsletters', 'n')
            ->leftJoin('a.organizationTypes', 'ot')
            ->leftJoin('a.associatedPersonTypes', 'at')
            ->leftJoin('a.personGroups', 'g')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findByIdFullJoin(int $id, ?Period $period = null): ?Aggregate
    {
        $queryBuilder = $this->createQueryBuilder('a')
            ->select(['a', 'n', 'ot', 'at', 'g', 'p', 't', 'u', 'pg', 'ao', 'aoo', 'aot'])
            ->leftJoin('a.newsletters', 'n')
            ->leftJoin('a.organizationTypes', 'ot')
            ->leftJoin('a.associatedPersonTypes', 'at')
            ->leftJoin('a.personGroups', 'g')
            ->leftJoin('g.people', 'p')
            ->leftJoin('p.title', 't')
            ->leftJoin('p.user', 'u')
            ->leftJoin('p.memberGroups', 'pg')
            ->leftJoin('p.associatedOrganizations', 'ao')
            ->leftJoin('ao.organization', 'aoo')
            ->leftJoin('ao.type', 'aot')
            ->where('a.id = :id')
            ->setParameter('id', $id)
        ;

        if (null !== $period) {
            $queryBuilder = $queryBuilder
                ->addSelect(['pm', 'pmpr'])
                ->leftJoin('p.memberships', 'pm', 'WITH', 'pm.period = :period')
                ->leftJoin('pm.paymentRequest', 'pmpr')
                ->setParameter('period', $period)
            ;
        } else {
            $queryBuilder = $queryBuilder
                ->addSelect(['pm', 'pmpr'])
                ->leftJoin('p.memberships', 'pm')
                ->leftJoin('pm.paymentRequest', 'pmpr')
            ;
        }

        $queryBuilder
            ->addSelect(['pmprdms', 'pmprf'])
            ->leftJoin('pmpr.dioceseMembershipSupplement', 'pmprdms')
            ->leftJoin('pmpr.file', 'pmprf')
        ;

        return $queryBuilder
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @return Aggregate[]
     */
    public function search(string $search): array
    {
        return $this
            ->createQueryBuilder('a')
            ->where('a.name LIKE :search')
            ->setParameter('search', '%'.$search.'%')
            ->getQuery()
            ->getResult()
        ;
    }
}
