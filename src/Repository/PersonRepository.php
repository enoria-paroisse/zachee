<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\GeneralMeeting;
use App\Entity\Period;
use App\Entity\Person;
use App\Entity\TypeAssociatedPerson;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

/**
 * @extends ServiceEntityRepository<Person>
 *
 * @method null|Person find($id, $lockMode = null, $lockVersion = null)
 * @method null|Person findOneBy(mixed[] $criteria, string[] $orderBy = null)
 * @method Person[]    findAll()
 * @method Person[]    findBy(mixed[] $criteria, string[] $orderBy = null, $limit = null, $offset = null)
 */
class PersonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Person::class);
    }

    /**
     * @return Person[]
     */
    public function findAllWithMembershipAndOrganizationsByPeriod(Period $period): array
    {
        return $this->createQueryBuilder('p')
            ->select(['p', 't', 'm', 'pr', 'ao', 'at', 'o', 'user', 'g', 'prdms', 'prf'])
            ->leftJoin('p.title', 't')
            ->leftJoin('p.memberships', 'm', 'WITH', 'm.period = :period')
            ->leftJoin('m.paymentRequest', 'pr')
            ->leftJoin('p.associatedOrganizations', 'ao')
            ->leftJoin('ao.type', 'at')
            ->leftJoin('ao.organization', 'o')
            ->leftJoin('p.user', 'user')
            ->leftJoin('p.memberGroups', 'g')
            ->leftJoin('pr.dioceseMembershipSupplement', 'prdms')
            ->leftJoin('pr.file', 'prf')
            ->setParameter('period', $period)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findByIdFullJoin(int $id): ?Person
    {
        return $this->createQueryBuilder('p')
            ->select(['p', 't', 'a', 'm', 'pr', 'prdms', 'prf', 'prpa', 'ao', 'at', 'o', 'uBy', 'g', 'u', 'newsletters', 'n', 'payment', 'r', 'ppr', 'pprpa'])
            ->leftJoin('p.title', 't')
            ->leftJoin('p.address', 'a')
            ->leftJoin('p.memberships', 'm')
            ->leftJoin('m.paymentRequest', 'pr')
            ->leftJoin('pr.dioceseMembershipSupplement', 'prdms')
            ->leftJoin('pr.file', 'prf')
            ->leftJoin('pr.paymentAllocations', 'prpa')
            ->leftJoin('p.associatedOrganizations', 'ao')
            ->leftJoin('ao.type', 'at')
            ->leftJoin('ao.organization', 'o')
            ->leftJoin('p.updatedBy', 'uBy')
            ->leftJoin('p.memberGroups', 'g')
            ->leftJoin('p.user', 'u')
            ->leftJoin('p.newsletters', 'newsletters')
            ->leftJoin('newsletters.newsletter', 'n')
            ->leftJoin('p.payments', 'payment')
            ->leftJoin('payment.receipt', 'r')
            ->leftJoin('p.paymentRequests', 'ppr')
            ->leftJoin('ppr.paymentAllocations', 'pprpa')
            ->where('p.id = :personId')
            ->setParameter('personId', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @return Person[]
     */
    public function findAllVolunteers(): array
    {
        return $this->createQueryBuilder('p')
            ->select(['p', 'g', 'u'])
            ->leftJoin('p.memberGroups', 'g')
            ->leftJoin('p.user', 'u')
            ->where('p.memberGroups is not empty')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Person[]
     */
    public function findNonMemberByPeriod(Period $period): array
    {
        return $this->createQueryBuilder('p')
            ->select(['p', 't', 'm', 'g', 'u', 'ao', 'o', 'aot'])
            ->leftJoin('p.memberships', 'm', 'WITH', 'm.period = :period')
            ->leftJoin('p.title', 't')
            ->leftJoin('p.memberGroups', 'g')
            ->leftJoin('p.user', 'u')
            ->leftJoin('p.associatedOrganizations', 'ao')
            ->leftJoin('ao.organization', 'o')
            ->leftJoin('ao.type', 'aot')
            ->where('m.person IS NULL')
            ->setParameter('period', $period)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Person[]
     */
    public function findAllDeletable(): array
    {
        return $this->createQueryBuilder('p')
            ->select(['p', 't', 'u', 'ao', 'pr', 'pa', 'm'])
            ->leftJoin('p.title', 't')
            ->leftJoin('p.user', 'u')
            ->leftJoin('p.associatedOrganizations', 'ao')
            ->leftJoin('p.memberGroups', 'g')
            ->leftJoin('p.paymentRequests', 'pr')
            ->leftJoin('p.payments', 'pa')
            ->leftJoin('App\Entity\CaMember', 'ca', 'WITH', 'p.id = ca.person')
            ->leftJoin('p.memberships', 'm')
            ->where('u.id IS NULL')
            ->andWhere('ao.id IS NULL')
            ->andWhere('g.id IS NULL')
            ->andWhere('pr.id IS NULL')
            ->andWhere('pa.id IS NULL')
            ->andWhere('ca.id IS NULL')
            ->andWhere('m.id IS NULL')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param array<string, string> $params
     *
     * @return Person[]
     *
     * @throws Exception
     */
    public function findDuplicateByParams(
        array $params,
    ): array {
        $select = '';
        $join = [];
        $groupBy = [];

        if (!empty($params['email'])) {
            $select .= 'p2.email, ';
            $join[] = '(p.email = tmp.email AND tmp.email != "" ) ';
            $groupBy[] = 'p2.email';
        }

        if (!empty($params['phone'])) {
            $select .= 'p2.phone, ';
            $join[] = '(p.phone = tmp.phone AND tmp.phone != "") ';
            $groupBy[] = 'p2.phone';
        }

        if (!empty($params['lastname'])) {
            $select .= 'p2.lastname, ';
            $join[] = '(p.lastname = tmp.lastname AND tmp.lastname != "") ';
            $groupBy[] = 'p2.lastname';
        }

        if (!empty($params['firstname'])) {
            $select .= 'p2.firstname, ';
            $join[] = '(p.firstname = tmp.firstname AND tmp.firstname != "") ';
            $groupBy[] = 'p2.firstname';
        }

        if (!empty($params['sex'])) {
            $select .= 'p2.sex, ';
            $join[] = '(p.sex = tmp.sex AND tmp.sex != "") ';
            $groupBy[] = 'p2.sex';
        }

        $connection = $this->getEntityManager()->getConnection();
        $query = $connection->prepare(
            '
            SELECT p.id
            FROM person p
            INNER JOIN (
                SELECT '.$select.' COUNT(*) as qty
                FROM person p2
                GROUP BY '.implode(', ', $groupBy).'
                HAVING qty > 1
            ) as tmp ON '.implode(' AND ', $join)
        );
        if (!$results = $query->executeQuery()->fetchAllAssociative()) {
            return [];
        }

        $ids = array_column($results, 'id');

        return $this
            ->createQueryBuilder('p')
            ->where('p.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param string[] $idsDuplicated
     *
     * @return Person[]
     */
    public function findDuplicateByIds(array $idsDuplicated): array
    {
        return $this
            ->createQueryBuilder('p')
            ->select(['p', 't', 'a', 'u', 'ao', 'g', 'm', 'pr', 'payment', 'n', 'o', 'aot', 'period', 'mpr'])
            ->leftJoin('p.title', 't')
            ->leftJoin('p.address', 'a')
            ->leftJoin('p.user', 'u')
            ->leftJoin('p.associatedOrganizations', 'ao')
            ->leftJoin('p.memberGroups', 'g')
            ->leftJoin('p.memberships', 'm')
            ->leftJoin('p.paymentRequests', 'pr')
            ->leftJoin('p.payments', 'payment')
            ->leftJoin('p.newsletters', 'n')
            ->leftJoin('ao.organization', 'o')
            ->leftJoin('ao.type', 'aot')
            ->leftJoin('m.period', 'period')
            ->leftJoin('m.paymentRequest', 'mpr')
            ->where('p.id IN (:ids)')
            ->setParameter('ids', $idsDuplicated)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param TypeAssociatedPerson[] $types
     *
     * @throws NonUniqueResultException
     */
    public function findForPublicLogin(
        GeneralMeeting $generalMeeting,
        array $types,
        string $lastname,
        ?string $email = null,
        ?PhoneNumber $phone = null,
    ): ?Person {
        $queryBuilder = $this
            ->createQueryBuilder('p')
            ->select(['p'])
            ->leftJoin('p.memberships', 'm')
            ->leftJoin('m.summons', 's')
            ->leftJoin('p.associatedOrganizations', 'ao')
            ->leftJoin('ao.organization', 'o')
            ->leftJoin('o.memberships', 'o_m')
            ->leftJoin('o_m.summons', 'o_s')
            ->andWhere('p.lastname = :lastname')
            ->andWhere('s.generalMeeting = :generalMeeting or (o_s.generalMeeting = :generalMeeting and ao.type IN (:types))')
            ->setParameter('generalMeeting', $generalMeeting)
            ->setParameter('lastname', $lastname)
            ->setParameter('types', $types)
        ;

        if (null !== $email) {
            $queryBuilder
                ->andWhere('p.email = :email')
                ->setParameter('email', $email)
            ;
        }

        if (null !== $phone) {
            $queryBuilder
                ->andWhere('p.phone = :phone')
                ->setParameter('phone', PhoneNumberUtil::getInstance()->format($phone, PhoneNumberFormat::E164))
            ;
        }

        return $queryBuilder
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @param TypeAssociatedPerson[] $types
     *
     * @return Person[]
     */
    public function findByGeneralMeeting(GeneralMeeting $generalMeeting, array $types): array
    {
        return $this
            ->createQueryBuilder('p')
            ->select(['p', 'ao', 'o', 'u'])
            ->leftJoin('p.memberships', 'm')
            ->leftJoin('m.summons', 's')
            ->leftJoin('p.associatedOrganizations', 'ao')
            ->leftJoin('ao.organization', 'o')
            ->leftJoin('o.memberships', 'o_m')
            ->leftJoin('o_m.summons', 'o_s')
            ->leftJoin('p.user', 'u')
            ->andWhere('s.generalMeeting = :generalMeeting or (o_s.generalMeeting = :generalMeeting and ao.type IN (:types))')
            ->setParameter('generalMeeting', $generalMeeting)
            ->setParameter('types', $types)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Person[]
     */
    public function search(string $search): array
    {
        return $this
            ->createQueryBuilder('p')
            ->where('p.firstname LIKE :search')
            ->orwhere('p.lastname LIKE :search')
            ->orwhere('CONCAT(p.firstname, \' \', p.lastname) LIKE :search')
            ->orwhere('CONCAT(p.lastname, \' \', p.firstname) LIKE :search')
            ->orwhere('p.email LIKE :search')
            ->orwhere('p.phone LIKE :search_phone')
            ->setParameter('search', '%'.$search.'%')
            ->setParameter('search_phone', '%'.ltrim(str_replace(' ', '', $search), '0').'%')
            ->getQuery()
            ->getResult()
        ;
    }
}
