<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\Organization;
use App\Entity\Period;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Organization>
 *
 * @method null|Organization find($id, $lockMode = null, $lockVersion = null)
 * @method null|Organization findOneBy(mixed[] $criteria, string[] $orderBy = null)
 * @method Organization[]    findAll()
 * @method Organization[]    findBy(mixed[] $criteria, string[] $orderBy = null, $limit = null, $offset = null)
 */
class OrganizationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Organization::class);
    }

    /**
     * @return Organization[]
     */
    public function findAllWithMembershipAndPeopleByPeriod(Period $period): array
    {
        return $this->createQueryBuilder('o')
            ->select(['o', 'm', 'd', 'a', 'pr', 'ap', 'p', 'apt', 'user', 'prdms', 'prf'])
            ->leftJoin('o.memberships', 'm', 'WITH', 'm.period = :period')
            ->leftJoin('o.associatedPeople', 'ap')
            ->leftJoin('o.diocese', 'd')
            ->leftJoin('o.address', 'a')
            ->leftJoin('m.paymentRequest', 'pr')
            ->leftJoin('ap.person', 'p')
            ->leftJoin('ap.type', 'apt')
            ->leftJoin('p.user', 'user')
            ->leftJoin('pr.dioceseMembershipSupplement', 'prdms')
            ->leftJoin('pr.file', 'prf')
            ->setParameter('period', $period)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Organization[]
     */
    public function findAllForAggregate(Period $period): array
    {
        return $this->createQueryBuilder('o')
            ->select(['o', 'm', 'd', 'a', 'pr', 'prdms', 'prf', 'ap', 'p', 'pt', 'mp', 'mppr', 'mpprdms', 'mpprf', 'apt', 'user', 'pao', 'g'])
            ->leftJoin('o.memberships', 'm', 'WITH', 'm.period = :period')
            ->leftJoin('o.associatedPeople', 'ap')
            ->leftJoin('o.diocese', 'd')
            ->leftJoin('o.address', 'a')
            ->leftJoin('m.paymentRequest', 'pr')
            ->leftJoin('pr.dioceseMembershipSupplement', 'prdms')
            ->leftJoin('pr.file', 'prf')
            ->leftJoin('ap.person', 'p')
            ->leftJoin('p.title', 'pt')
            ->leftJoin('p.memberships', 'mp', 'WITH', 'mp.period = :period')
            ->leftJoin('mp.paymentRequest', 'mppr')
            ->leftJoin('mppr.dioceseMembershipSupplement', 'mpprdms')
            ->leftJoin('mppr.file', 'mpprf')
            ->leftJoin('ap.type', 'apt')
            ->leftJoin('p.user', 'user')
            ->leftJoin('p.associatedOrganizations', 'pao')
            ->leftJoin('p.memberGroups', 'g')
            ->setParameter('period', $period)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findByIdFullJoin(int $id): ?Organization
    {
        return $this->createQueryBuilder('o')
            ->select(['o', 'm', 'e', 'i', 'd', 'a', 'pr', 'dms', 'prpa', 'payment', 'paymentpa', 'r', 'ap', 'p', 'user', 'pt', 'apt', 'u'])
            ->leftJoin('o.memberships', 'm')
            ->leftJoin('m.entity', 'e')
            ->leftJoin('e.instance', 'i')
            ->leftJoin('o.associatedPeople', 'ap')
            ->leftJoin('o.diocese', 'd')
            ->leftJoin('o.address', 'a')
            ->leftJoin('m.paymentRequest', 'pr')
            ->leftJoin('pr.dioceseMembershipSupplement', 'dms')
            ->leftJoin('pr.paymentAllocations', 'prpa')
            ->leftJoin('prpa.payment', 'payment')
            ->leftJoin('payment.paymentAllocations', 'paymentpa')
            ->leftJoin('payment.receipt', 'r')
            ->leftJoin('ap.person', 'p')
            ->leftJoin('p.user', 'user')
            ->leftJoin('p.title', 'pt')
            ->leftJoin('ap.type', 'apt')
            ->leftJoin('o.updatedBy', 'u')
            ->where('o.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @return Organization[]
     */
    public function findNonMemberByPeriod(Period $period): array
    {
        return $this->createQueryBuilder('o')
            ->select(['o', 'ap', 'p', 'u', 'g', 'mp', 'po'])
            ->leftJoin('o.memberships', 'm', 'WITH', 'm.period = :period')
            ->leftJoin('o.associatedPeople', 'ap')
            ->leftJoin('ap.person', 'p')
            ->leftJoin('p.user', 'u')
            ->leftJoin('p.memberGroups', 'g')
            ->leftJoin('p.memberships', 'mp', 'WITH', 'mp.period = :period')
            ->leftJoin('p.associatedOrganizations', 'po')
            ->where('m.organization IS NULL')
            ->setParameter('period', $period)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Organization[]
     */
    public function search(string $search): array
    {
        return $this
            ->createQueryBuilder('o')
            ->where('o.name LIKE :search')
            ->setParameter('search', '%'.$search.'%')
            ->getQuery()
            ->getResult()
        ;
    }
}
