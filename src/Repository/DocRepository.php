<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\Doc;
use App\Entity\Folder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Doc>
 *
 * @method null|Doc find($id, $lockMode = null, $lockVersion = null)
 * @method null|Doc findOneBy(mixed[] $criteria, string[] $orderBy = null)
 * @method Doc[]    findAll()
 * @method Doc[]    findBy(mixed[] $criteria, string[] $orderBy = null, $limit = null, $offset = null)
 */
class DocRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Doc::class);
    }

    public function save(Doc $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Doc $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param string[] $tags
     *
     * @return Doc[]
     */
    public function findByTags(array $tags, bool $and = true): array
    {
        $queryBuilder = $this->createQueryBuilder('d')
            ->orderBy('d.ref', 'ASC')
        ;

        foreach ($tags as $tag) {
            $tagName = str_replace('-', '_', $tag);
            if ($and) {
                $queryBuilder->andWhere('d.tags LIKE :tag_'.$tagName);
            } else {
                $queryBuilder->orWhere('d.tags LIKE :tag_'.$tagName);
            }
            $queryBuilder->setParameter('tag_'.$tagName, '%'.$tag.'%');
        }

        return $queryBuilder
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param string[] $tags
     *
     * @return Doc[]
     */
    public function findDeepByTags(array $tags, bool $and = true): array
    {
        $docs = $this->findByTags($tags, $and);
        $folderRepository = $this->getEntityManager()->getRepository(Folder::class);
        // @phpstan-ignore-next-line instanceof.alwaysTrue
        if ($folderRepository instanceof FolderRepository) {
            $subDocs = [$docs];
            foreach ($folderRepository->findByTags($tags, $and) as $folder) {
                $subDocs[] = $folder->getSubDocs();
            }
            $docs = \call_user_func_array('array_merge', $subDocs);
        }

        return array_unique((array) $docs);
    }

    /**
     * @return Doc[]
     */
    public function search(string $search): array
    {
        return $this
            ->createQueryBuilder('d')
            ->where('d.name LIKE :search')
            ->setParameter('search', '%'.$search.'%')
            ->getQuery()
            ->getResult()
        ;
    }
}
