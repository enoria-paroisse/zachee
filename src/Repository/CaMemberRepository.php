<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\CaMember;
use App\Entity\Person;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CaMember>
 *
 * @method null|CaMember find($id, $lockMode = null, $lockVersion = null)
 * @method null|CaMember findOneBy(mixed[] $criteria, string[] $orderBy = null)
 * @method CaMember[]    findAll()
 * @method CaMember[]    findBy(mixed[] $criteria, string[] $orderBy = null, $limit = null, $offset = null)
 */
class CaMemberRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CaMember::class);
    }

    /**
     * @return CaMember[]
     */
    public function findByPerson(Person $person): array
    {
        return $this->createQueryBuilder('c')
            ->where('c.person = :person')
            ->setParameter('person', $person)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return CaMember[]
     */
    public function findAllFullJoin(?bool $active = null): array
    {
        $queryBuilder = $this->createQueryBuilder('c')
            ->select(['c', 'p', 't', 'u'])
            ->leftJoin('c.person', 'p')
            ->leftJoin('p.title', 't')
            ->leftJoin('p.user', 'u')
        ;

        if (null !== $active) {
            $queryBuilder->where('c.active = :activeTest')
                ->setParameter('activeTest', $active)
            ;
        }

        return $queryBuilder->getQuery()
            ->getResult()
        ;
    }
}
