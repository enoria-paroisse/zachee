<?php

/**
 * @copyright Copyright (c) 2022 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\Period;
use App\Entity\Receipt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Receipt>
 *
 * @method null|Receipt find($id, $lockMode = null, $lockVersion = null)
 * @method null|Receipt findOneBy(mixed[] $criteria, string[] $orderBy = null)
 * @method Receipt[]    findAll()
 * @method Receipt[]    findBy(mixed[] $criteria, string[] $orderBy = null, $limit = null, $offset = null)
 */
class ReceiptRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Receipt::class);
    }

    public function save(Receipt $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findMaxRefByPrefix(mixed $prefix): ?int
    {
        $qb = $this->createQueryBuilder('r');

        $qb->where($qb->expr()->eq('r.refPrefix', ':prefix'))
            ->setParameter('prefix', $prefix)
        ;

        $qb->select($qb->expr()->max('r.refNumber'));

        $result = $qb->getQuery()->getOneOrNullResult();

        if (null !== $result) {
            return reset($result);
        }

        return 0;
    }

    /**
     * @return Receipt[] Returns an array of PaymentRequest objects
     */
    public function findByPeriod(Period $period): array
    {
        return $this->getQueryByPeriod($period)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Receipt[] Returns an array of PaymentRequest objects
     */
    public function findMissingFile(): array
    {
        return $this->getBaseQuery()
            ->andWhere('r.file IS NULL')
            ->getQuery()
            ->getResult()
        ;
    }

    protected function getQueryByPeriod(Period $period): QueryBuilder
    {
        return $this->getBaseQuery()
            ->where('p.datetime >= :start')
            ->andWhere('p.datetime <= :end')
            ->setParameter('start', $period->getStart())
            ->setParameter('end', $period->getEnd())
        ;
    }

    protected function getBaseQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('r')
            ->select(['r', 'p', 'o', 'person', 'user', 'd', 'pa'])
            ->leftJoin('r.payment', 'p')
            ->leftJoin('p.organization', 'o')
            ->leftJoin('p.person', 'person')
            ->leftJoin('person.user', 'user')
            ->leftJoin('o.diocese', 'd')
            ->leftJoin('p.paymentAllocations', 'pa')
        ;
    }
}
