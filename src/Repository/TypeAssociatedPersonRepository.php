<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\TypeAssociatedPerson;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TypeAssociatedPerson>
 *
 * @method null|TypeAssociatedPerson find($id, $lockMode = null, $lockVersion = null)
 * @method null|TypeAssociatedPerson findOneBy(mixed[] $criteria, string[] $orderBy = null)
 * @method TypeAssociatedPerson[]    findAll()
 * @method TypeAssociatedPerson[]    findBy(mixed[] $criteria, string[] $orderBy = null, $limit = null, $offset = null)
 */
class TypeAssociatedPersonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeAssociatedPerson::class);
    }

    public function save(TypeAssociatedPerson $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(TypeAssociatedPerson $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param string[] $tags
     *
     * @return TypeAssociatedPerson[]
     */
    public function findByTags(array $tags, bool $and = true): array
    {
        $queryBuilder = $this->createQueryBuilder('t')
            ->orderBy('t.name', 'ASC')
        ;

        foreach ($tags as $tag) {
            $tagName = str_replace('-', '_', $tag);
            if ($and) {
                $queryBuilder->andWhere('t.tags LIKE :tag_'.$tagName);
            } else {
                $queryBuilder->orWhere('t.tags LIKE :tag_'.$tagName);
            }
            $queryBuilder->setParameter('tag_'.$tagName, '%'.$tag.'%');
        }

        return $queryBuilder
            ->getQuery()
            ->getResult()
        ;
    }
}
