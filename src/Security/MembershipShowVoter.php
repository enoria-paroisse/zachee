<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Security;

use App\Entity\Membership;
use App\Entity\Organization;
use App\Entity\User;
use App\Repository\AssociatedPersonRepository;
use App\Security\Traits\CanShowTrait;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @extends Voter<string, mixed>
 */
class MembershipShowVoter extends Voter
{
    use CanShowTrait;

    public function __construct(
        private readonly AccessDecisionManagerInterface $accessDecisionManager,
        AssociatedPersonRepository $associatedPersonRepository,
    ) {
        $this->associatedPersonRepository = $associatedPersonRepository;
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if ('membership_show' !== $attribute) {
            return false;
        }

        if (!$subject instanceof Membership && !$subject instanceof Organization) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        // The user must be logged
        if (!$user instanceof User) {
            return false;
        }

        if ($this->accessDecisionManager->decide($token, ['ROLE_MEMBERSHIP_REQUEST_SHOW'])) {
            return true;
        }

        if ($subject instanceof Membership) {
            if ($subject->getPerson() === $user->getPerson()) {
                return true;
            }

            $organization = $subject->getOrganization();
            if ($organization && $this->canShowOrganization($organization, $user)) {
                return true;
            }
        }

        if ($subject instanceof Organization && $this->canShowOrganization($subject, $user)) {
            return true;
        }

        return false;
    }
}
