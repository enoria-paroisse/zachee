<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Security;

use App\Entity\Doc;
use App\Entity\Folder;
use App\Services\DocService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @extends Voter<string, mixed>
 */
class ShowDocVoter extends Voter
{
    public function __construct(
        private readonly AccessDecisionManagerInterface $accessDecisionManager,
        private readonly DocService $docService,
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if ('show_doc' !== $attribute) {
            return false;
        }

        if (!$subject instanceof Folder && !$subject instanceof Doc) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        if ($this->accessDecisionManager->decide($token, ['ROLE_DOC_EDIT'])) {
            return true;
        }

        $tags = [];
        if ($subject instanceof Doc || $subject instanceof Folder) {
            $tags = $this->docService->getFullTagsList($subject);
        }

        if (in_array('public', $tags, true)
            || in_array('ag', $tags, true)
            || in_array('ag-multi', $tags, true)
        ) {
            return true;
        }

        return array_reduce($tags, function ($vote, $tag) use ($token): bool {
            if (str_starts_with($tag, 'role_')) {
                return $vote || $this->accessDecisionManager->decide($token, [strtoupper($tag)]);
            }
            if (str_starts_with($tag, 'membership_public_') || str_starts_with($tag, 'organization_')) {
                return true;
            }

            return $vote;
        }, false);
    }
}
