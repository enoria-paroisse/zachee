<?php

/**
 * @copyright Copyright (c) 2025 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Security;

use App\Services\OvhService;
use Erkens\Security\TwoFactorTextBundle\Model\TwoFactorTextInterface;
use Erkens\Security\TwoFactorTextBundle\TextSender\AuthCodeTextInterface;

class TextSenderAuthCode implements AuthCodeTextInterface
{
    private string $format;

    public function __construct(private readonly OvhService $ovhService)
    {
    }

    public function sendAuthCode(TwoFactorTextInterface $user, ?string $code = null): void
    {
        $this->ovhService->sendSmsMessage(
            sprintf($this->getMessageFormat(), $code ?? $user->getTextAuthCode()),
            [$user->getTextAuthRecipient()],
        );
    }

    public function setMessageFormat(string $format): void
    {
        $this->format = $format;
    }

    public function getMessageFormat(): string
    {
        return $this->format;
    }
}
