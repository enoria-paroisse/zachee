<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Security\Traits;

use App\Entity\AssociatedPerson;
use App\Entity\Diocese;
use App\Entity\Group;
use App\Entity\Organization;
use App\Entity\Payment;
use App\Entity\PaymentRequest;
use App\Entity\PaymentRequestFile;
use App\Entity\Person;
use App\Entity\Receipt;
use App\Entity\User;
use App\Repository\AssociatedPersonRepository;

trait CanShowTrait
{
    protected AssociatedPersonRepository $associatedPersonRepository;

    protected function canShowPerson(Person $person, User $user): bool
    {
        if ($person === $user->getPerson()) {
            return true;
        }

        $organizations = [];
        $associateLinks = $this->associatedPersonRepository->findBy([
            'person' => $person,
        ]);
        foreach ($associateLinks as $link) {
            $organizations[] = $link->getOrganization();
        }
        foreach ($organizations as $organization) {
            if ($this->canShowOrganization($organization, $user)) {
                return true;
            }
        }

        $personGroups = $user->getPerson()->getMemberGroups()->toArray();
        if (0 < count($personGroups)) {
            foreach ($personGroups as $group) {
                if ($this->canShowGroup($group, $user) && in_array($person, $group->getPeople()->toArray())) {
                    return true;
                }
            }
        }

        return false;
    }

    protected function canShowOrganization(Organization $organization, User $user): bool
    {
        if ($this->associatedPersonRepository->findOneBy([
            'person' => $user->getPerson(),
            'organization' => $organization,
        ])) {
            return true;
        }

        $diocese = $organization->getDiocese();

        return $diocese && $this->canShowDiocese($diocese, $user);
    }

    protected function canShowDiocese(Diocese $diocese, User $user): bool
    {
        $organizations = [];
        if (null !== $diocese->getAssociatedOrganization()) {
            /** @var AssociatedPerson $associatedOrganization */
            foreach ($user->getPerson()->getAssociatedOrganizations()->toArray() as $associatedOrganization) {
                $organizations[] = $associatedOrganization->getOrganization();
            }

            return in_array($diocese->getAssociatedOrganization(), $organizations, true);
        }

        return false;
    }

    protected function canShowGroup(Group $group, User $user): bool
    {
        return $group->isViewable() && in_array($group, $user->getPerson()->getMemberGroups()->toArray(), true);
    }

    protected function canShowPaymentRequest(PaymentRequest $paymentRequest, User $user): bool
    {
        if (null !== $paymentRequest->getOrganization()) {
            return $this->canShowOrganization($paymentRequest->getOrganization(), $user);
        }
        if ($paymentRequest->getPerson() === $user->getPerson()) {
            return true;
        }
        if (null !== $paymentRequest->getMembership()) {
            if (null !== $paymentRequest->getMembership()->getOrganization()) {
                return $this->canShowOrganization($paymentRequest->getMembership()->getOrganization(), $user);
            }
            if ($paymentRequest->getMembership()->getPerson() === $user->getPerson()) {
                return true;
            }
        }
        if (null !== $paymentRequest->getDioceseMembershipSupplement() && null !== $paymentRequest->getDioceseMembershipSupplement()->getDiocese()) {
            return $this->canShowDiocese($paymentRequest->getDioceseMembershipSupplement()->getDiocese(), $user);
        }

        return false;
    }

    protected function canShowPayment(Payment $payment, User $user): bool
    {
        if (null !== $payment->getOrganization()) {
            return $this->canShowOrganization($payment->getOrganization(), $user);
        }
        if ($payment->getPerson() === $user->getPerson()) {
            return true;
        }

        return false;
    }

    protected function canShowReceipt(Receipt $receipt, User $user): bool
    {
        if (null !== $receipt->getPayment()) {
            return $this->canShowPayment($receipt->getPayment(), $user);
        }

        return false;
    }

    protected function canShowPaymentRequestFile(PaymentRequestFile $file, User $user): bool
    {
        if (null !== $file->getPaymentRequest()) {
            return $this->canShowPaymentRequest($file->getPaymentRequest(), $user);
        }

        return false;
    }
}
