<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Security\UploadsVoter;

use App\Entity\Doc;
use App\Security\ShowDocVoter;

class DocUploadsVoter extends ShowDocVoter
{
    protected function supports(string $attribute, mixed $subject): bool
    {
        if ('show_uploaded_doc' !== $attribute) {
            return false;
        }

        if (!$subject instanceof Doc) {
            return false;
        }

        return true;
    }
}
