<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Command\Fonts;

use App\Services\PdfService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:fonts:list'
)]
class ListCommand extends Command
{
    public function __construct(protected PdfService $pdfService)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $fontMetrics = $this->pdfService->getFontMetrics();

        $io->title('Available font list for PDF generation');

        $io->section('Font config');
        $io->definitionList(
            'Path',
            ['chroot' => implode(', ', $fontMetrics->getOptions()->getChroot())],
            ['fontDir' => $fontMetrics->getOptions()->getFontDir()],
            ['fontCache' => $fontMetrics->getOptions()->getFontCache()],
            new TableSeparator(),
            'Default',
            ['Default Font' => $fontMetrics->getOptions()->getDefaultFont()],
        );

        $io->section('Font list');
        foreach ($fontMetrics->getFontFamilies() as $familyName => $fontTypes) {
            $io->text($familyName);
            $io->listing($fontTypes);
        }

        return Command::SUCCESS;
    }
}
