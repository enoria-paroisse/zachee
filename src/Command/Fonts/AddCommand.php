<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Command\Fonts;

use App\Services\PdfService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:fonts:add'
)]
class AddCommand extends Command
{
    public function __construct(protected PdfService $pdfService)
    {
        parent::__construct();
    }

    public function configure(): void
    {
        $this
            ->setDescription('Add a font for the PDF generator')
            ->setHelp('This function maps a font-family name to a font. It tries to locate the bold, italic, and bold italic versions of the font as well. Once the files are located, ttf versions of the font are copied to the fonts directory. Changes to the font lookup table are saved to the cache.')
            ->addArgument('fontname', InputArgument::REQUIRED, 'the font-family name')
            ->addArgument('normal', InputArgument::REQUIRED, 'the filename of the normal face font subtype')
            ->addArgument('bold', InputArgument::OPTIONAL, 'the filename of the bold face font subtype')
            ->addArgument('italic', InputArgument::OPTIONAL, 'the filename of the italic face font subtype')
            ->addArgument('boldItalic', InputArgument::OPTIONAL, 'the filename of the bold italic face font subtype')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $this->pdfService->installFontFamily(
                $input->getArgument('fontname'),
                $input->getArgument('normal'),
                $input->getArgument('bold'),
                $input->getArgument('italic'),
                $input->getArgument('boldItalic'),
            );
            $io->success($input->getArgument('fontname').' added');

            return 0;
        } catch (\RuntimeException $e) {
            $io->error($e->getMessage());

            return 1;
        }
    }
}
