<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Command;

use App\Command\Cron\AbstractTask;
use App\Command\Cron\EnoriaSyncTask;
use App\Command\Cron\EntityStatusUpdateTask;
use App\Command\Cron\GroupSyncTask;
use App\Command\Cron\MembershipRemindTask;
use App\Command\Cron\NewsletterSyncTask;
use App\Command\Cron\PaymentRequestRemindTask;
use App\Entity\CronLog;
use App\Entity\CronTaskLog;
use App\Repository\CronLogRepository;
use App\Repository\CronTaskLogRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:cron'
)]
class CronCommand extends Command
{
    public function __construct(
        private readonly CronLogRepository $cronLogRepository,
        private readonly CronTaskLogRepository $cronTaskLogRepository,
        private readonly MembershipRemindTask $membershipRemindTask,
        private readonly NewsletterSyncTask $newsletterSyncTask,
        private readonly PaymentRequestRemindTask $paymentRequestRemindTask,
        private readonly GroupSyncTask $groupSyncTask,
        private readonly EnoriaSyncTask $enoriaSyncTask,
        private readonly EntityStatusUpdateTask $entityStatusUpdateTask
    ) {
        parent::__construct();
    }

    public function configure(): void
    {
        $this
            ->setDescription('Execute a cron task')
            ->setHelp('Your cron system need to execute this command. You should run this command only once a day!')
            ->addArgument('task', InputArgument::OPTIONAL, 'The task to run')
            ->addOption('list', 'l', InputOption::VALUE_NONE, 'List the tasks')
            ->addOption('dry-run', 'd', InputOption::VALUE_NONE, 'Dry run')
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Force task execution')
        ;
    }

    /**
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $cronLog = new CronLog();
        $cronLog->setDatetime(new \DateTime())->setResult([]);

        $io = new SymfonyStyle($input, $output);

        try {
            if ($input->getOption('list')) {
                $table = new Table($output);
                $table
                    ->setStyle('box')
                    ->setHeaders(['name', 'when'])
                ;

                foreach ($this->getTasks() as $task) {
                    $table->addRow([$task->getName(), $task->getWhen()]);
                }
                $table->render();

                return Command::SUCCESS;
            }

            $this->cronLogRepository->save($cronLog, true);
            $this->runTasks($input, $output, $cronLog);
            $io->success('Cron success');

            $this->cronLogRepository->save($cronLog, true);

            return Command::SUCCESS;
        } catch (\Exception $e) {
            $io->error($e->getMessage());
            $this->cronLogRepository->save($cronLog, true);
            $io->error('Cron error');

            throw $e;
        }
    }

    /**
     * @return array<string, AbstractTask>
     */
    protected function getTasks(): array
    {
        return [
            $this->membershipRemindTask->getName() => $this->membershipRemindTask,
            $this->newsletterSyncTask->getName() => $this->newsletterSyncTask,
            $this->paymentRequestRemindTask->getName() => $this->paymentRequestRemindTask,
            $this->groupSyncTask->getName() => $this->groupSyncTask,
            $this->enoriaSyncTask->getName() => $this->enoriaSyncTask,
            $this->entityStatusUpdateTask->getName() => $this->entityStatusUpdateTask,
        ];
    }

    /**
     * @throws \Exception
     */
    protected function runTasks(InputInterface $input, OutputInterface $output, CronLog $cronLog): void
    {
        $tasks = $this->getTasks();
        $wantedTask = $input->getArgument('task');
        if ($wantedTask) {
            if (!array_key_exists($wantedTask, $tasks)) {
                $cronLog->setResult([$wantedTask => 'fail']);
                $log = new CronTaskLog();
                $log->setCron($cronLog)
                    ->setTask($wantedTask)
                    ->setObject('Exception')
                    ->setDescription('Task "'.$wantedTask.'" don\'t exist')
                ;
                $this->cronTaskLogRepository->save($log, true);

                return;
            }
            $tasks = [$tasks[$wantedTask]];
        }

        foreach ($tasks as $task) {
            $this->runOneTask($task, $input, $output, $cronLog);
        }
    }

    protected function runOneTask(AbstractTask $task, InputInterface $input, OutputInterface $output, CronLog $cronLog): void
    {
        $result = 'skip';

        if ($this->needToRun($task) || $input->getOption('force')) {
            try {
                $result = $task->execute($input, $output, $cronLog);
            } catch (\Exception $e) {
                $result = 'fail';
                $log = new CronTaskLog();
                $log->setCron($cronLog)
                    ->setTask($task->getName())
                    ->setObject('Exception')
                    ->setDescription($e->getMessage())
                ;
                $this->cronTaskLogRepository->save($log, true);
            }
        }

        $results = $cronLog->getResult();
        $results[$task->getName()] = $result;
        $cronLog->setResult($results);
    }

    protected function needToRun(AbstractTask $task): bool
    {
        $date = new \DateTime();
        $when = $task->getWhen();

        if ('daily' === $when) {
            return true;
        }
        if ('never' === $when) {
            return false;
        }

        $parts = explode(',', $when);
        $parts = array_map('trim', $parts);

        foreach ($parts as $part) {
            if ($part === $date->format('d') || $part === $date->format('j')) {
                return true;
            }
            if ('w'.$date->format('w') === $part) {
                return true;
            }
        }

        return false;
    }
}
