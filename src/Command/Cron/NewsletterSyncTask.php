<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Command\Cron;

use App\Entity\CronLog;
use App\Entity\Newsletter;
use App\Repository\CronTaskLogRepository;
use App\Services\NewsletterService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class NewsletterSyncTask extends AbstractTask
{
    public function __construct(
        private readonly CronTaskLogRepository $cronTaskLogRepository,
        private readonly NewsletterService $newsletterService,
    ) {
    }

    public function getName(): string
    {
        return 'newsletter_sync';
    }

    public function getWhen(): string
    {
        return 'never';
    }

    /**
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output, CronLog $cronLog): string
    {
        $io = new SymfonyStyle($input, $output);

        $log = $this->createCronTaskLog($cronLog);

        try {
            $this->newsletterService->sync();
            $log
                ->setObject(str_replace('App\Entity\\', '', Newsletter::class))
                ->setDescription('Synchronisation des newsletters')
            ;
            $this->cronTaskLogRepository->save($log, true);

            $io->success('Newsletters sync');

            return AbstractTask::SUCCESS;
        } catch (\Exception $e) {
            $log->setObject('Exception')->setDescription($e->getMessage());
            $this->cronTaskLogRepository->save($log, true);

            $io->error('Newsletters sync error: '.$e->getMessage());

            return AbstractTask::FAIL;
        }
    }
}
