<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Command\Cron;

use App\Entity\CronLog;
use App\Mailer\AccountingMailer;
use App\Repository\CronTaskLogRepository;
use App\Repository\PaymentRequestRepository;
use App\Services\PaymentRequestService;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

class PaymentRequestRemindTask extends AbstractTask
{
    public function __construct(
        private readonly CronTaskLogRepository $cronTaskLogRepository,
        private readonly PaymentRequestRepository $paymentRequestRepository,
        private readonly AccountingMailer $mailer,
        private readonly PaymentRequestService $paymentRequestService,
    ) {
    }

    public function getName(): string
    {
        return 'payment_request_remind';
    }

    public function getWhen(): string
    {
        return 'w2';
    }

    /**
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output, CronLog $cronLog): string
    {
        $io = new SymfonyStyle($input, $output);

        $log = $this->createCronTaskLog($cronLog);

        try {
            $paymentRequests = $this->paymentRequestRepository->findForRemind();
            $progressBar = new ProgressBar($output, count($paymentRequests));

            foreach ($paymentRequests as $paymentRequest) {
                if (null !== $paymentRequest->getLastReminder()) {
                    $interval = (new \DateTime())->diff($paymentRequest->getLastReminder());
                    if (((int) $interval->format('%a') > 13) && null === $paymentRequest->getMembership()) {
                        $log = $this->createCronTaskLog($cronLog);

                        if (!$input->getOption('dry-run') && $paymentRequest->getFile()) {
                            try {
                                $this->mailer->sendPaymentRequestRemind($paymentRequest->getFile(), $this->paymentRequestService);
                                $paymentRequest->setLastReminder(new \DateTime());
                                $log
                                    ->setObject(str_replace('App\Entity\\', '', $paymentRequest::class))
                                    ->setObjectId($paymentRequest->getId())
                                    ->setDescription(
                                        'Envoi d’un mail de rappel pour le demande de paiement '.$paymentRequest->getDescription()
                                    )
                                ;
                            } catch (TransportExceptionInterface $e) {
                                $log->setObject('Exception')->setDescription($e->getMessage());
                                $this->cronTaskLogRepository->save($log, true);

                                $io->error('Membership remind');

                                return AbstractTask::FAIL;
                            }
                            $this->cronTaskLogRepository->save($log, true);
                        }
                    }
                }
                $progressBar->advance();
            }

            $io->success('Payment request remind');

            return AbstractTask::SUCCESS;
        } catch (\Exception $e) {
            $log->setObject('Exception')->setDescription($e->getMessage());
            $this->cronTaskLogRepository->save($log, true);

            $io->error('Payment request remind error: '.$e->getMessage());

            return AbstractTask::FAIL;
        }
    }
}
