<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Command\Cron;

use App\Entity\CronLog;
use App\Entity\Entity;
use App\Entity\Membership;
use App\Repository\CronTaskLogRepository;
use App\Repository\MembershipRepository;
use App\Repository\PeriodRepository;
use App\Services\EnoriaException;
use App\Services\EnoriaService;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class EntityStatusUpdateTask extends AbstractTask
{
    public function __construct(
        private readonly CronTaskLogRepository $cronTaskLogRepository,
        private readonly MembershipRepository $membershipRepository,
        private readonly PeriodRepository $periodRepository,
        private readonly EnoriaService $enoriaService,
    ) {
    }

    public function getName(): string
    {
        return 'entity_status_update';
    }

    public function getWhen(): string
    {
        return 'daily';
    }

    /**
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output, CronLog $cronLog): string
    {
        $io = new SymfonyStyle($input, $output);

        $period = $this->periodRepository->findCurrentPeriod();
        if (null !== $period) {
            $memberships = $this->membershipRepository->findByPeriod($period);

            /** @var Membership[] $membershipsFinal */
            $membershipsFinal = [];
            foreach ($memberships as $membership) {
                if (Membership::STATUS_CANCELED !== $membership->getStatus() && null !== $membership->getEntity() && 'F' !== $membership->getEntity()->getStatus() && $membership->getEntity()->getInstance()->isSyncUpEnabled()) {
                    $membershipsFinal[] = $membership;
                }
            }

            $progressBar = new ProgressBar($output, count($membershipsFinal));
            foreach ($membershipsFinal as $membership) {
                $log = $this->createCronTaskLog($cronLog);

                if (!$input->getOption('dry-run')) {
                    /** @var Entity $entity */
                    $entity = $membership->getEntity();

                    try {
                        if ($this->enoriaService->autoUpdateEntityStatus($entity, $membership)) {
                            $log
                                ->setObject(str_replace('App\Entity\\', '', Entity::class))
                                ->setObjectId($entity->getId())
                                ->setDescription(
                                    'Mise à jour du statut de '.$entity->getName().' ('.$entity->getInstance()->getName().')'
                                )
                            ;
                            $cronLog->addCronTaskLog($log);
                            $this->cronTaskLogRepository->save($log, true);
                        }
                    } catch (EnoriaException $e) {
                        $log->setObject('Exception')->setDescription($e->getMessage());
                        $this->cronTaskLogRepository->save($log, true);

                        $io->error('Entity status update');
                    }
                }
                $progressBar->advance();
            }

            $io->success('Entity status update');

            return AbstractTask::SUCCESS;
        }

        throw new \Exception('Unable to find the current period');
    }
}
