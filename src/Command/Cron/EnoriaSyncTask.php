<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Command\Cron;

use App\Entity\CronLog;
use App\Entity\Entity;
use App\Entity\Instance;
use App\Entity\SmsBilling;
use App\Repository\CronTaskLogRepository;
use App\Repository\InstanceRepository;
use App\Services\EnoriaService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class EnoriaSyncTask extends AbstractTask
{
    public function __construct(
        private readonly CronTaskLogRepository $cronTaskLogRepository,
        private readonly InstanceRepository $instanceRepository,
        private readonly EnoriaService $enoria,
    ) {
    }

    public function getName(): string
    {
        return 'enoria_sync';
    }

    public function getWhen(): string
    {
        return 'daily';
    }

    /**
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output, CronLog $cronLog): string
    {
        $io = new SymfonyStyle($input, $output);

        $log = $this->createCronTaskLog($cronLog);

        try {
            $instances = $this->instanceRepository->findAll();

            /** @var Instance[] $hasError */
            $hasError = [];

            foreach ($instances as $instance) {
                $log = $this->createCronTaskLog($cronLog);

                try {
                    $this->enoria->syncInstance($instance);
                    $log
                        ->setObject(str_replace('App\Entity\\', '', Entity::class))
                        ->setDescription('Synchronisation des entités de l’instance '.$instance->getName())
                    ;
                    $cronLog->addCronTaskLog($log);
                } catch (\Exception $e) {
                    $log->setObject('Exception')->setDescription($e->getMessage());
                }
                $this->cronTaskLogRepository->save($log, true);

                $log = $this->createCronTaskLog($cronLog);

                try {
                    $this->enoria->syncSms($instance);
                    $log
                        ->setObject(str_replace('App\Entity\\', '', SmsBilling::class))
                        ->setDescription('Synchronisation des rapprochements SMS de l’instance '.$instance->getName())
                    ;
                    $cronLog->addCronTaskLog($log);
                } catch (\Exception $e) {
                    $log->setObject('Exception')->setDescription($e->getMessage());
                    $hasError[] = $instance;
                }
                $this->cronTaskLogRepository->save($log, true);
            }

            if (0 < count($hasError)) {
                $io->error('Enoria sync error: '.implode(', ', $hasError));

                return AbstractTask::FAIL;
            }

            $io->success('Enoria sync success');

            return AbstractTask::SUCCESS;
        } catch (\Exception $e) {
            $log->setObject('Exception')->setDescription($e->getMessage());
            $this->cronTaskLogRepository->save($log, true);

            $io->error('Enoria sync error: '.$e->getMessage());

            return AbstractTask::FAIL;
        }
    }
}
