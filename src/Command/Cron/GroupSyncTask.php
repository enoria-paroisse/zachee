<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Command\Cron;

use App\Entity\CronLog;
use App\Entity\Group;
use App\Repository\CronTaskLogRepository;
use App\Repository\GroupRepository;
use App\Services\GroupService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GroupSyncTask extends AbstractTask
{
    public function __construct(
        private readonly CronTaskLogRepository $cronTaskLogRepository,
        private readonly GroupRepository $groupRepository,
        private readonly GroupService $groupService,
    ) {
    }

    public function getName(): string
    {
        return 'group_sync';
    }

    public function getWhen(): string
    {
        return 'daily';
    }

    /**
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output, CronLog $cronLog): string
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $groups = $this->groupRepository->findAll();

            foreach ($groups as $group) {
                if (null !== $group->getAggregate()) {
                    $log = $this->createCronTaskLog($cronLog);
                    $this->groupService->sync($group);
                    $log
                        ->setObject(str_replace('App\Entity\\', '', Group::class))
                        ->setDescription('Synchronisation du groupe '.$group->getName().' avec l’agrégat '.$group->getAggregate()?->getName())
                    ;
                    $cronLog->addCronTaskLog($log);
                    $this->cronTaskLogRepository->save($log, true);
                }
            }

            $io->success('Group sync');

            return AbstractTask::SUCCESS;
        } catch (\Exception $e) {
            $log = $this->createCronTaskLog($cronLog);
            $log->setObject('Exception')->setDescription($e->getMessage());
            $this->cronTaskLogRepository->save($log, true);

            $io->error('Group sync error: '.$e->getMessage());

            return AbstractTask::FAIL;
        }
    }
}
