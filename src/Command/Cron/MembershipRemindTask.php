<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Command\Cron;

use App\Entity\CronLog;
use App\Entity\Membership;
use App\Entity\PaymentRequest;
use App\Mailer\MembershipMailer;
use App\Repository\CronTaskLogRepository;
use App\Repository\MembershipRepository;
use App\Repository\PeriodRepository;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

class MembershipRemindTask extends AbstractTask
{
    public function __construct(
        private readonly CronTaskLogRepository $cronTaskLogRepository,
        private readonly MembershipRepository $membershipRepository,
        private readonly PeriodRepository $periodRepository,
        private readonly MembershipMailer $mailer,
    ) {
    }

    public function getName(): string
    {
        return 'membership_remind';
    }

    public function getWhen(): string
    {
        return 'w2';
    }

    /**
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output, CronLog $cronLog): string
    {
        $io = new SymfonyStyle($input, $output);

        $period = $this->periodRepository->findCurrentPeriod();
        if (null !== $period) {
            $memberships = $this->membershipRepository->findForRemind($period);
            $progressBar = new ProgressBar($output, count($memberships));

            foreach ($memberships as $membership) {
                if (null !== $membership->getLastReminder() && $membership->isReminderEnabled()) {
                    $interval = (new \DateTime())->diff($membership->getLastReminder());
                    if (((int) $interval->format('%a') > 6
                            && (!$membership->getInformationSubmitted() || !$membership->getSheet() || Membership::HELP_AWAIT_INFO === $membership->getHelpStatus()))
                        || ((int) $interval->format('%a') > 13
                            && (!$membership->getHelpRequest() || Membership::HELP_REFUSED === $membership->getHelpStatus() || Membership::HELP_ACCEPTED === $membership->getHelpStatus())
                            && $membership->getPaymentRequest() && PaymentRequest::STATUS_COMPLETED !== $membership->getPaymentRequest()->getStatus())
                    ) {
                        $log = $this->createCronTaskLog($cronLog);

                        if (!$input->getOption('dry-run')) {
                            try {
                                $this->mailer->sendMembershipRemind($membership);
                                $membership->setLastReminder(new \DateTime());
                                $log
                                    ->setObject(str_replace('App\Entity\\', '', $membership::class))
                                    ->setObjectId($membership->getId())
                                    ->setDescription(
                                        'Envoi d’un mail de rappel pour '.($membership->getOrganization() ? $membership->getOrganization().' - '.$membership->getOrganization()->getDiocese() : $membership->getPerson()).' ('.$period.')'
                                    )
                                ;
                            } catch (TransportExceptionInterface $e) {
                                $log->setObject('Exception')->setDescription($e->getMessage());
                                $this->cronTaskLogRepository->save($log, true);

                                $io->error('Membership remind');

                                return AbstractTask::FAIL;
                            }
                            $this->cronTaskLogRepository->save($log, true);
                        }
                    }
                }
                $progressBar->advance();
            }

            $io->success('Membership remind');

            return AbstractTask::SUCCESS;
        }

        throw new \Exception('Unable to find the current period');
    }
}
