<?php

/**
 * @copyright Copyright (c) 2022 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Services;

use App\Entity\Receipt;
use App\Repository\ReceiptRepository;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Environment;

class ReceiptService
{
    public function __construct(private readonly ReceiptRepository $receiptRepository, private readonly PdfService $pdfService, private readonly Environment $environment, private readonly string $targetDirectory)
    {
    }

    public function generatePdf(Receipt $receipt): PdfService
    {
        return $this->pdfService->renderHtml($this->environment->render('pdf/receipt.pdf.twig', [
            'rootDirectory' => $this->pdfService->getRootDirectory(),
            'receipt' => $receipt,
        ]));
    }

    public function generatePdfAndSave(Receipt $receipt): void
    {
        $pdf = $this->generatePdf($receipt);
        $filename = uniqid('receipt_'.$receipt->getRefPrefix().sprintf('%04d', $receipt->getRefNumber()).'_', true).'.pdf';
        file_put_contents($this->targetDirectory.'/'.$filename, $pdf->output());

        if ($receipt->getFile()) {
            $fs = new Filesystem();
            $fs->remove($this->targetDirectory.'/'.$receipt->getFile());
        }
        $receipt->setFile($filename);
        $this->receiptRepository->save($receipt, true);
    }

    public function getTargetDirectory(): string
    {
        return $this->targetDirectory;
    }
}
