<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Services;

use App\Entity\Entity;
use App\Entity\Instance;
use App\Entity\Membership;
use App\Entity\PaymentRequest;
use App\Entity\SmsBilling;
use App\Repository\EntityRepository;
use App\Repository\SmsBillingRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class EnoriaService
{
    public function __construct(
        private readonly EntityRepository $entityRepository,
        private readonly SmsBillingRepository $smsBillingRepository,
    ) {
    }

    /**
     * @return array<string, mixed>
     *
     * @throws EnoriaException
     */
    public function getEntities(Instance $instance): array
    {
        if (null === $instance->getToken()) {
            throw new EnoriaException(sprintf('Instance %s don\'t have API key registered', $instance->getName()));
        }

        $result = [];

        try {
            $httpClient = HttpClient::create();
            $response = $httpClient->request('GET', $instance->getUrl().'/api/entites?pagination=false', ['auth_bearer' => $instance->getToken()]);
            if (200 !== $response->getStatusCode()) {
                throw new EnoriaException(
                    sprintf(
                        'Impossible de récupérer la liste des entités de l’instance %s (%s/api/entites) return code %s',
                        $instance->getName(),
                        $instance->getUrl(),
                        $response->getStatusCode(),
                    )
                );
            }

            $raw = $response->toArray();
            if (null !== $raw['hydra:member']) {
                $result = $raw['hydra:member'];
            }
        } catch (TransportExceptionInterface $e) {
            throw new EnoriaException(
                sprintf(
                    'Impossible de récupérer la liste des entités de l’instance %s (%s/api/entites) %s',
                    $instance->getName(),
                    $instance->getUrl(),
                    $e->getMessage(),
                )
            );
        }

        return $result;
    }

    /**
     * @return array<string, array<string, mixed>>
     *
     * @throws EnoriaException
     */
    public function getSms(Instance $instance): array
    {
        if (null === $instance->getToken()) {
            throw new EnoriaException(sprintf('Instance %s don\'t have API key registered', $instance->getName()));
        }

        $result = [];

        try {
            $httpClient = HttpClient::create();
            $response = $httpClient->request('GET', $instance->getUrl().'/api/sms_rapprochements?pagination=false', ['auth_bearer' => $instance->getToken()]);
            if (200 !== $response->getStatusCode()) {
                throw new EnoriaException(
                    sprintf(
                        'Impossible de récupérer la liste des rapprochements SMS de l’instance %s (%s/api/sms_rapprochements) return code %s',
                        $instance->getName(),
                        $instance->getUrl(),
                        $response->getStatusCode(),
                    )
                );
            }

            $raw = $response->toArray();
            if (null !== $raw['hydra:member']) {
                $result = $raw['hydra:member'];
            }
        } catch (TransportExceptionInterface $e) {
            throw new EnoriaException(
                sprintf(
                    'Impossible de récupérer la liste des rapprochements SMS de l’instance %s (%s/api/sms_rapprochements) %s',
                    $instance->getName(),
                    $instance->getUrl(),
                    $e->getMessage(),
                )
            );
        }

        return $result;
    }

    /**
     * @throws EnoriaException
     */
    public function updateSms(SmsBilling $smsBilling): void
    {
        $instance = $smsBilling->getEntity()?->getInstance();

        if ($instance && null === $smsBilling->getEntity()?->getInstance()->getToken()) {
            throw new EnoriaException(sprintf('Instance %s don\'t have API key registered', $instance->getName()));
        }

        try {
            $httpClient = HttpClient::create();
            $response = $httpClient->request(
                'PUT',
                $smsBilling->getEntity()?->getInstance()->getUrl().'/api/sms_rapprochements/'.$smsBilling->getIdInInstance(),
                [
                    'auth_bearer' => $smsBilling->getEntity()?->getInstance()->getToken(),
                    'headers' => ['content-type' => 'application/json'],
                    'json' => ['ref' => $smsBilling->getRef(), 'paye' => $smsBilling->isPaid()],
                ]
            );
            if (200 !== $response->getStatusCode()) {
                throw new EnoriaException(
                    sprintf(
                        'Impossible de mettre à jour le rapprochement SMS de l’instance %s (%s/api) return code %s',
                        $instance?->getName(),
                        $instance?->getUrl(),
                        $response->getStatusCode(),
                    )
                );
            }
        } catch (TransportExceptionInterface $e) {
            throw new EnoriaException(
                sprintf(
                    'Impossible de mettre à jour le rapprochement SMS de l’instance %s (%s/api) %s',
                    $instance?->getName(),
                    $instance?->getUrl(),
                    $e->getMessage(),
                )
            );
        }
    }

    /**
     * @throws EnoriaException
     */
    public function syncInstance(Instance $instance): void
    {
        $entities = $this->getEntities($instance);

        /** @var Entity[] $oldEntities */
        $oldEntities = [];
        foreach ($this->entityRepository->findByInstance($instance) as $entity) {
            $oldEntities[$entity->getIdInInstance()] = $entity;
        }

        foreach ($entities as $entity) {
            if (array_key_exists($entity['id'], $oldEntities)) {
                $oldEntities[$entity['id']]->setName($entity['nom'])
                    ->setEnable(true)
                    ->setStatus($entity['statut'])
                ;
                $this->entityRepository->save($oldEntities[$entity['id']], true);
                unset($oldEntities[$entity['id']]);
            } else {
                $newEntity = new Entity();
                $newEntity->setName($entity['nom'])
                    ->setInstance($instance)
                    ->setIdInInstance($entity['id'])
                    ->setEnable(true)
                    ->setStatus($entity['statut'])
                ;
                $this->entityRepository->save($newEntity, true);
            }
        }

        foreach ($oldEntities as $entity) {
            $entity->setEnable(false);
            $this->entityRepository->save($entity, true);
        }
    }

    /**
     * @throws EnoriaException
     * @throws NonUniqueResultException
     */
    public function syncSms(Instance $instance): void
    {
        /** @var SmsBilling[] $oldSmss */
        $oldSmss = [];
        foreach ($this->smsBillingRepository->findByInstance($instance) as $sms) {
            $oldSmss[$sms->getIdInInstance()] = $sms;
        }

        if ($instance->isSmsBilling()) {
            $smss = $this->getSms($instance);

            foreach ($smss as $sms) {
                $entity = $this->entityRepository->findByInstanceAndId($instance, $sms['entiteId']);
                if (null !== $entity) {
                    if (array_key_exists($sms['id'], $oldSmss)) {
                        $oldSmss[$sms['id']]
                            ->setExist(true)
                            ->setDate(new \DateTime($sms['dateRaw']))
                            ->setRef($sms['ref'])
                            ->setPaid($sms['paye'])
                            ->setTotal($sms['total'])
                            ->setEntity($entity)
                            ->setIdInInstance($sms['id'])
                        ;

                        // update
                        if ($oldSmss[$sms['id']]->getPaymentRequest()) {
                            if ($oldSmss[$sms['id']]->getPaymentRequest()->getFile()) {
                                $oldSmss[$sms['id']]->setRef($oldSmss[$sms['id']]->getPaymentRequest()->getDescription());
                            }
                            $oldSmss[$sms['id']]->setPaid(PaymentRequest::STATUS_COMPLETED === $oldSmss[$sms['id']]->getPaymentRequest()->getStatus());
                            $this->updateSms($oldSmss[$sms['id']]);
                        }

                        $this->smsBillingRepository->save($oldSmss[$sms['id']], true);
                        unset($oldSmss[$sms['id']]);
                    } else {
                        $newEntity = new SmsBilling();
                        $newEntity
                            ->setExist(true)
                            ->setDate(new \DateTime($sms['dateRaw']))
                            ->setRef($sms['ref'])
                            ->setPaid($sms['paye'])
                            ->setTotal($sms['total'])
                            ->setEntity($entity)
                            ->setIdInInstance($sms['id'])
                        ;
                        $this->smsBillingRepository->save($newEntity, true);
                    }
                }
            }
        }

        foreach ($oldSmss as $sms) {
            $sms->setExist(false);
            $this->smsBillingRepository->save($sms, true);
        }
    }

    /**
     * @throws EnoriaException
     */
    public function updateEntityStatus(Entity $entity, string $status, bool $save = true): void
    {
        if (!$entity->getInstance()->isSyncUpEnabled()) {
            throw new EnoriaException('La synchronisation vers Enoria est désactivée pour l’instance '.$entity->getInstance()->getName());
        }

        try {
            $httpClient = HttpClient::create();
            $response = $httpClient->request(
                'PUT',
                $entity->getInstance()->getUrl().'/api/entites/'.$entity->getIdInInstance(),
                [
                    'auth_bearer' => $entity->getInstance()->getToken(),
                    'headers' => ['content-type' => 'application/json'],
                    'json' => ['statut' => $status],
                ]
            );
            if (200 !== $response->getStatusCode()) {
                throw new EnoriaException(
                    sprintf(
                        'Impossible de mettre à jour le statut de  l’entité %s dans l’instance %s (%s/api) return code %s',
                        $entity->getName(),
                        $entity->getInstance()->getName(),
                        $entity->getInstance()->getUrl(),
                        $response->getStatusCode(),
                    )
                );
            }
        } catch (TransportExceptionInterface $e) {
            throw new EnoriaException(
                sprintf(
                    'Impossible de mettre à jour le statut de  l’entité %s dans l’instance %s (%s/api)',
                    $entity->getName(),
                    $entity->getInstance()->getName(),
                    $entity->getInstance()->getUrl(),
                )
            );
        }

        $entity->setStatus($status);
        $this->entityRepository->save($entity, $save);
    }

    /**
     * @return bool true if status updated
     *
     * @throws EnoriaException
     */
    public function autoUpdateEntityStatus(Entity $entity, Membership $membership, bool $save = true): bool
    {
        $old = $entity->getStatus();
        $new = self::getGoodEntityStatus($membership);

        if ($old !== $new) {
            $this->updateEntityStatus($entity, $new, $save);

            return true;
        }

        return false;
    }

    public static function getGoodEntityStatus(Membership $membership): string
    {
        if ($membership->getPaymentRequest() && PaymentRequest::STATUS_IN_PROGRESS === $membership->getPaymentRequest()->getStatus()
            && ($membership->getPaymentRequest()->getLastReminder() || $membership->getLastReminder())
        ) {
            if (null !== $membership->getPaymentRequest()->getDatetime() && 31 < $membership->getPaymentRequest()->getDatetime()->diff(new \DateTime())->days) {
                return 'RP';
            }

            return 'AP';
        }
        if (!$membership->getInformationValidated() || null === $membership->getSheet()) {
            return 'AI';
        }

        return 'A';
    }
}
