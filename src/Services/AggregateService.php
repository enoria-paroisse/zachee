<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Services;

use App\Entity\Aggregate;
use App\Entity\Organization;
use App\Entity\OrganizationType;
use App\Entity\Period;
use App\Entity\Person;
use App\Entity\TypeAssociatedPerson;
use App\Repository\MembershipRepository;
use App\Repository\OrganizationRepository;
use App\Repository\PeriodRepository;
use App\Repository\PersonRepository;

class AggregateService
{
    private ?Period $period = null;

    public function __construct(
        private readonly PeriodRepository $periodRepository,
        private readonly MembershipRepository $membershipRepository,
        private readonly PersonRepository $personRepository,
        private readonly OrganizationRepository $organizationRepository,
    ) {
    }

    public function setPeriod(?Period $period = null): void
    {
        $this->period = $period;
    }

    /**
     * @throws \Exception
     */
    public function getPeriod(): Period
    {
        if (null === $this->period) {
            $period = $this->periodRepository->findCurrentPeriod();
            if (null === $period) {
                throw new \Exception('Impossible de trouver la période courante');
            }
            $this->period = $period;
        }

        return $this->period;
    }

    /**
     * @return Person[]
     *
     * @throws \Exception
     */
    public function getPeople(Aggregate $aggregate): array
    {
        /** @var Person[] $people */
        $people = [];

        if (Aggregate::PERSON === $aggregate->getPersonOrOrganization()) {
            if (0 !== count($aggregate->getMembership())) {
                $memberships = $this->membershipRepository->findPeopleByPeriodFullJoin($this->getPeriod());

                foreach ($memberships as $membership) {
                    if (in_array('is_first', $aggregate->getMembership(), true) && !$membership->getFirst()) {
                        continue;
                    }
                    if (in_array('not_first', $aggregate->getMembership(), true) && $membership->getFirst()) {
                        continue;
                    }
                    if ($membership->getPerson() && in_array($membership->getStatus(), $aggregate->getMembership(), true)) {
                        $people[] = $membership->getPerson();
                    }
                }

                if (in_array('non_member', $aggregate->getMembership(), true)) {
                    $people = array_merge($people, $this->personRepository->findNonMemberByPeriod($this->getPeriod()));
                }

                if (0 !== count($aggregate->getPersonGroups())) {
                    $people = array_filter($people, static function ($person) use ($aggregate) {
                        return count(array_intersect($person->getMemberGroups()->toArray(), $aggregate->getPersonGroups()->toArray())) > 0;
                    });
                }
            } else {
                $groups = $aggregate->getPersonGroups();
                if (0 === count($groups)) {
                    return $this->personRepository->findAllWithMembershipAndOrganizationsByPeriod($this->getPeriod());
                }
                $groupsPeople = [];
                foreach ($groups as $group) {
                    $groupsPeople[] = $group->getPeople()->toArray();
                }
                $people = array_merge(...$groupsPeople);
            }
        } elseif (Aggregate::ORGANIZATION === $aggregate->getPersonOrOrganization()) {
            /** @var Organization[] $organizations */
            $organizations = [];
            if (0 !== count($aggregate->getMembership())) {
                $memberships = $this->membershipRepository->findOrganizationsByPeriodFullJoin($this->getPeriod());

                foreach ($memberships as $membership) {
                    if (in_array('is_first', $aggregate->getMembership(), true) && !$membership->getFirst()) {
                        continue;
                    }
                    if (in_array('not_first', $aggregate->getMembership(), true) && $membership->getFirst()) {
                        continue;
                    }
                    if (null !== $membership->getOrganization() && in_array($membership->getStatus(), $aggregate->getMembership(), true)) {
                        $organizations[] = $membership->getOrganization();
                    }
                }

                if (in_array('non_member', $aggregate->getMembership(), true)) {
                    $organizations = array_merge($organizations, $this->organizationRepository->findNonMemberByPeriod($this->getPeriod()));
                }
            } else {
                $organizations = $this->organizationRepository->findAllForAggregate($this->getPeriod());
            }
            $organizations = $this->filterOrganizationsByType($aggregate->getOrganizationTypes()->toArray(), $organizations);
            $people = $this->filterOrganizationsByPersonType($aggregate->getAssociatedPersonTypes()->toArray(), $organizations);
        }

        return array_unique($people);
    }

    /**
     * @param OrganizationType[] $types
     * @param Organization[]     $organizations
     *
     * @return Organization[]
     */
    private function filterOrganizationsByType(array $types, array $organizations): array
    {
        if (0 < count($types)) {
            $result = [];
            foreach ($organizations as $organization) {
                if (in_array($organization->getType(), $types, true)) {
                    $result[] = $organization;
                }
            }

            return $result;
        }

        return $organizations;
    }

    /**
     * @param TypeAssociatedPerson[] $types
     * @param Organization[]         $organizations
     *
     * @return Person[]
     */
    private function filterOrganizationsByPersonType(array $types, array $organizations): array
    {
        $result = [];

        if (0 < count($types)) {
            foreach ($organizations as $organization) {
                foreach ($organization->getAssociatedPeople() as $person) {
                    if (in_array($person->getType(), $types, true)) {
                        $result[] = $person->getPerson();
                    }
                }
            }

            return $result;
        }

        foreach ($organizations as $organization) {
            foreach ($organization->getAssociatedPeople() as $person) {
                $result[] = $person->getPerson();
            }
        }

        return $result;
    }
}
