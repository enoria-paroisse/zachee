<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Services;

use App\Entity\Diocese;
use App\Entity\DioceseMembershipSupplement;
use App\Entity\Membership;
use App\Entity\PaymentRequest;
use App\Entity\Period;
use App\Entity\Wasselynck;
use App\Repository\MembershipRepository;

class MembershipFeeService
{
    public function __construct(
        private readonly MembershipRepository $membershipRepository,
    ) {
    }

    public function calculateFee(Membership $membership): float
    {
        if (null !== $membership->getPeriod() && Membership::TYPE_MEMBER === $membership->getType()) {
            $organization = $membership->getOrganization();
            if ($membership->getPerson()) {
                return (float) $membership->getPeriod()->getMembershipfeeNatural();
            }
            if ($organization) {
                $fee = (float) $membership->getPeriod()->getMembershipfeeLegal();
                $diocese = $organization->getDiocese();
                if ($diocese) {
                    $wasselynck = null;
                    foreach ($diocese->getWasselynck() as $wasselynckObj) {
                        if ($wasselynckObj->getPeriod() === $membership->getPeriod()) {
                            $wasselynck = $wasselynckObj;
                        }
                    }
                    if (Membership::HOSTING_SELF === $membership->getHosting()) {
                        return round(
                            $this->getDiocesePricing(
                                $wasselynck ? $wasselynck->getIndice() : $membership->getPeriod()->getDioceseBaseWasselynck(),
                                $membership->getPeriod()
                            ) * 0.25,
                            2
                        );
                    }
                    $dioceseAssociatedOrganization = $diocese->getAssociatedOrganization();
                    if ($dioceseAssociatedOrganization) {
                        $dioceseParent = $this->membershipRepository->findOneBy([
                            'period' => $membership->getPeriod()->getId(),
                            'organization' => $dioceseAssociatedOrganization->getId(),
                        ]);
                        if ($dioceseParent && Membership::STATUS_CANCELED !== $dioceseParent->getStatus() && Membership::HOSTING_ENORIA === $dioceseParent->getHosting() && $diocese->getSubOrganizationMembershipFee()) {
                            return (float) $diocese->getSubOrganizationMembershipFee()->getFee($membership->getPeriod());
                        }
                    }
                }

                return $fee;
            }
        }

        return 0.0;
    }

    public function getDiocesePricing(int $indice, Period $period): float
    {
        return (float) $period->getDioceseBaseMaxMembershipfee() * sqrt((float) $indice) / sqrt($period->getDioceseBaseWasselynck());
    }

    /**
     * @return array{data: array<string, mixed>, results: Membership[], wasselynck: null|Wasselynck, membershipSupplement: null|DioceseMembershipSupplement}
     */
    public function getDioceseAssociatedMembershipData(Diocese $diocese, Period $period): array
    {
        $wasselynck = null;
        foreach ($diocese->getWasselynck() as $wasselynckObj) {
            if ($wasselynckObj->getPeriod() === $period) {
                $wasselynck = $wasselynckObj;
            }
        }
        $actualAssociatedMemberships = [
            'nbOrganizations' => 0,
            'totalMembershipFee' => 0,
            'finalMembershipFee' => 0,
            'isMaximum' => false,
            'total' => 0,
        ];

        /** @var Membership[] $actualAssociatedMembershipsResult */
        $actualAssociatedMembershipsResult = [];
        foreach ($diocese->getOrganizations() as $organization) {
            foreach ($organization->getMemberships() as $membership) {
                if ($membership->getPeriod() === $period && in_array($membership->getStatus(), [Membership::STATUS_VALIDATED, Membership::STATUS_IN_PROGRESS], true)) {
                    $actualAssociatedMembershipsResult[] = $membership;
                }
            }
        }
        $membershipSupplement = null;
        foreach ($diocese->getDioceseMembershipSupplement() as $dioceseMembershipSupplement) {
            if ($period === $dioceseMembershipSupplement->getPeriod()) {
                $membershipSupplement = $dioceseMembershipSupplement;
            }
        }

        $feePaid = 0;

        /** @var Membership[] $help */
        $help = [];
        foreach ($actualAssociatedMembershipsResult as $actualAssociatedMembership) {
            ++$actualAssociatedMemberships['nbOrganizations'];
            $actualAssociatedMemberships['totalMembershipFee'] += $actualAssociatedMembership->getMembershipFee();

            if (Membership::HELP_ACCEPTED === $actualAssociatedMembership->getHelpStatus()) {
                $help[] = $actualAssociatedMembership;
            }
        }
        $actualAssociatedMemberships['finalMembershipFee'] = $actualAssociatedMemberships['totalMembershipFee'];

        if ($wasselynck) {
            $actualAssociatedMemberships['finalMembershipFee'] = $actualAssociatedMemberships['nbOrganizations'] * ((float) $period->getMembershipfeeLegal());

            foreach ($help as $membership) {
                if ((float) $membership->getPaymentRequest()?->getAmount() < (float) $period->getMembershipfeeLegal()) {
                    $actualAssociatedMemberships['finalMembershipFee'] -= (float) $period->getMembershipfeeLegal() - (float) $membership->getPaymentRequest()?->getAmount();
                }
            }

            $pricing = $this->getDiocesePricing(
                $wasselynck->getIndice(),
                $period
            );
            if ($actualAssociatedMemberships['finalMembershipFee'] >= $pricing) {
                $actualAssociatedMemberships['isMaximum'] = true;
                $actualAssociatedMemberships['finalMembershipFee'] = $pricing;
            }

            $actualAssociatedMemberships['total'] = round(
                $actualAssociatedMemberships['finalMembershipFee'] - $actualAssociatedMemberships['totalMembershipFee']
                    - (
                        null !== $membershipSupplement && null !== $membershipSupplement->getPaymentRequest() && PaymentRequest::STATUS_CANCELED !== $membershipSupplement->getPaymentRequest()->getStatus()
                            ? (float) $membershipSupplement->getPaymentRequest()->getAmount()
                            : 0
                    ),
                2
            );
        }

        return ['data' => $actualAssociatedMemberships, 'results' => $actualAssociatedMembershipsResult, 'wasselynck' => $wasselynck, 'membershipSupplement' => $membershipSupplement];
    }
}
