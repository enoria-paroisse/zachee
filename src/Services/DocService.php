<?php

/**
 * @copyright Copyright (c) 2025 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Services;

use App\Entity\Doc;
use App\Entity\Folder;
use Symfony\Bundle\SecurityBundle\Security;

class DocService
{
    public function __construct(private readonly Security $security)
    {
    }

    /**
     * @return array{'folder': Folder, 'subFolders': mixed[], 'docs': Doc[]}[]
     */
    public function makeSubTree(Folder $folder): array
    {
        $subFolders = $folder->getFolders();

        if (0 === count($subFolders)) {
            return [];
        }

        $tree = [];
        foreach ($subFolders as $subFolder) {
            $tree[] = $this->makeTreeElem($subFolder);
        }

        return $tree;
    }

    /**
     * @return array{'folder': Folder, 'subFolders': mixed[], 'docs': Doc[]}
     */
    public function makeTreeElem(Folder $folder): array
    {
        return [
            'folder' => $folder,
            'subFolders' => $this->makeSubTree($folder),
            'docs' => $folder->getDocs()->toArray(),
        ];
    }

    /**
     * @param array{'folder': Folder, 'subFolders': mixed[], 'docs': Doc[]}[] $tree
     *
     * @return array{'folder': Folder, 'subFolders': mixed[], 'docs': Doc[]}[]
     */
    public function filterTreeByPermissions(array $tree): array
    {
        $newTree = [];
        foreach ($tree as $elem) {
            $elem['subFolders'] = $this->filterTreeByPermissions($elem['subFolders']);

            $elem['docs'] = array_filter($elem['docs'], function (Doc $doc) {
                return $this->security->isGranted('show_doc', $doc);
            });
            $newTree[] = $elem;
        }

        return array_filter($newTree, function ($elem) {
            return 0 < count($elem['docs']) || 0 < count($elem['subFolders']) || $this->security->isGranted('show_doc', $elem['folder']);
        });
    }

    /**
     * @param array{'folder': Folder, 'subFolders': mixed[], 'docs': Doc[]}[] $tree
     * @param string[]                                                        $tags
     *
     * @return array{'folder': Folder, 'subFolders': mixed[], 'docs': Doc[]}[]
     */
    public function filterTreeByTags(array $tree, array $tags): array
    {
        $newTree = [];
        foreach ($tree as $elem) {
            $elem['subFolders'] = $this->filterTreeByTags($elem['subFolders'], $tags);

            $elem['docs'] = array_filter($elem['docs'], function (Doc $doc) use ($tags) {
                return 0 === count(array_diff($tags, array_unique($this->getFullTagsList($doc))));
            });
            $newTree[] = $elem;
        }

        return array_filter($newTree, function ($elem) use ($tags) {
            return 0 < count($elem['docs']) || 0 < count($elem['subFolders']) || 0 === count(array_diff($tags, $this->getFullTagsList($elem['folder'])));
        });
    }

    /**
     * @return string[]
     */
    public function getFullTagsList(Doc|Folder $subject): array
    {
        $tags = $subject->getTags();
        $folders = $subject->getFolders();
        foreach ($folders as $folder) {
            $tags = array_merge($tags, $folder->getTags());
        }

        return $tags;
    }
}
