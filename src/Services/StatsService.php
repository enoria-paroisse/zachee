<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Services;

use App\Entity\Instance;
use App\Entity\Membership;
use App\Entity\Organization;
use App\Entity\Period;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class StatsService
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    /**
     * @return array{
     *     new_membership_by_month: array{x: string, y: mixed}[],
     *     membership_by_year: array{organization: array{x: string, y: mixed}[], person: array{x: string, y: mixed}[]},
     *     entities_by_instance: array{label: string[], data: int[]},
     *     entities_by_diocese: array{label: string[], data: int[]},
     *     organization_types: array{name: string|null, nb: int}[]
     * }
     */
    public function getInternalStats(Period $period): array
    {
        return [
            'new_membership_by_month' => $this->getNewMembershipByMonth(),
            'membership_by_year' => [
                'organization' => $this->getMembershipByYear('organization'),
                'person' => $this->getMembershipByYear('person'),
            ],
            'entities_by_instance' => $this->getEntitiesByInstance(),
            'entities_by_diocese' => $this->getEntitiesByBiggestDioceses(),
            'organization_types' => $this->getOrganizationsByType($period),
        ];
    }

    // @phpstan-ignore-next-line
    public function getEnoriaStats(Instance $instance, array $stats = []): array
    {
        if (!$stats) {
            $stats = $this->getEnoriaEmptyStats();
        }

        $instanceButton = [
            'instance' => $instance,
            'status' => 'warning',
        ];

        if (null === $instance->getToken()) {
            throw new StatsException(sprintf('Pas de token défini pour l’instance %s', $instance->getName()));
        }

        try {
            $httpClient = HttpClient::create();
            $response = $httpClient->request('GET', $instance->getUrl().'/api/stats', ['auth_bearer' => $instance->getToken()]);
            if (200 !== $response->getStatusCode()) {
                throw new StatsException(
                    sprintf(
                        'Impossible de récupérer les statistiques de l’instance %s (%s/api/stats) %s',
                        $instance->getName(),
                        $instance->getUrl(),
                        $response->getStatusCode(),
                    )
                );
            }

            $array = $response->toArray();
            if (array_key_exists('adresses', $array)) {
                foreach ($array['adresses'] as $ad) {
                    $stats['adresses'][] = $ad;
                }
            }
            if (array_key_exists('entites', $array)) {
                $stats['entites']['dioceses'] += $array['entites']['dioceses'];
                $stats['entites']['paroisses'] += $array['entites']['paroisses'];
            }
            if (array_key_exists('users', $array)) {
                $stats['users'] += (int) $array['users'];
            }
            if (array_key_exists('groupes', $array)) {
                $stats['groupes'] += (int) $array['groupes'];
            }
            if (array_key_exists('logs', $array)) {
                $stats['logs'] += (int) $array['logs'];
            }
            if (array_key_exists('salles', $array)) {
                $stats['salles'] += (int) $array['salles'];
            }
            if (array_key_exists('sacrements', $array)) {
                $stats['sacrements'] += (int) $array['sacrements'];
            }
            if (array_key_exists('funerailles', $array)) {
                $stats['funerailles'] += (int) $array['funerailles'];
            }
            if (array_key_exists('messes', $array)) {
                $stats['messes'] += (int) $array['messes'];
            }
            if (array_key_exists('reservations', $array)) {
                $stats['reservations'] += (int) $array['reservations'];
            }
            if (array_key_exists('personnes', $array)) {
                $stats['personnes']['total'] += (int) $array['personnes']['total'];
                $stats['personnes']['details']['M']['A'] += (int) $array['personnes']['details']['M']['A'];
                $stats['personnes']['details']['M']['J'] += (int) $array['personnes']['details']['M']['J'];
                $stats['personnes']['details']['F']['J'] += (int) $array['personnes']['details']['F']['J'];
                $stats['personnes']['details']['F']['A'] += (int) $array['personnes']['details']['F']['A'];
            }

            $fmt = new \IntlDateFormatter('fr_FR');
            $fmt->setPattern('Y MMM');

            if (array_key_exists('ConnexionsParMois', $array)) {
                $connections = [];
                foreach ($array['ConnexionsParMois'] as $connArr) {
                    $date = \DateTime::createFromFormat('Y-m-d', $connArr['tri'].'-01');
                    if (array_key_exists($connArr['tri'], $connections)) {
                        $connections[$connArr['tri']]['y'] += (int) $connArr['nb'];
                    } else {
                        $connections[$connArr['tri']] = [
                            'x' => $date instanceof \DateTime ? $fmt->format($date) : '',
                            'y' => (int) $connArr['nb'],
                            'tri' => $connArr['tri'],
                        ];
                    }
                }
                ksort($connections);
                $stats['ConnexionsParMois'] = array_values($connections);
            }

            if (array_key_exists('EntreeLogsParMois', $array)) {
                $activity = [];
                foreach ($array['EntreeLogsParMois'] as $journalArr) {
                    $date = \DateTime::createFromFormat('Y-m-d', $journalArr['tri'].'-01');
                    if (array_key_exists($journalArr['tri'], $activity)) {
                        $activity[$journalArr['tri']]['y'] += (int) $journalArr['nb'];
                    } else {
                        $activity[$journalArr['tri']] = [
                            'x' => $date instanceof \DateTime ? $fmt->format($date) : '',
                            'y' => (int) $journalArr['nb'],
                            'tri' => $journalArr['tri'],
                        ];
                    }
                    ksort($activity);
                    $stats['EntreeLogsParMois'] = array_values($activity);
                }
            }

            $instanceButton['status'] = 'info';
        } catch (TransportExceptionInterface $e) {
            throw new StatsException(
                sprintf(
                    'Impossible de récupérer les statistiques de l’instance %s (%s/api/stats) %s',
                    $instance->getName(),
                    $instance->getUrl(),
                    $e->getMessage()
                )
            );
        }

        return [
            'stats' => $stats,
            'instanceButton' => $instanceButton,
        ];
    }

    /**
     * @return array{
     *     ConnexionsParMois: array<string, int>,
     *     EntreeLogsParMois: array<string, int>,
     *     entites: array{paroisses: int, dioceses: int},
     *     personnes: array{
     *         total: int,
     *         details: array{
     *             M: array{A: int, J: int},
     *             F: array{A: int, J: int},
     *         }
     *     },
     *     adresses: array<int, array{entite: string, lat: string, lgt: string}>,
     *     users: int,
     *     groupes: int,
     *     logs: int,
     *     salles: int,
     *     reservations: int,
     *     sacrements: int,
     *     funerailles: int,
     *     messes: int,
     * }
     */
    public function getEnoriaEmptyStats(): array
    {
        return [
            'ConnexionsParMois' => [],
            'EntreeLogsParMois' => [],
            'entites' => ['paroisses' => 0, 'dioceses' => 0],
            'personnes' => [
                'total' => 0,
                'details' => [
                    'M' => [
                        'A' => 0,
                        'J' => 0,
                    ],
                    'F' => [
                        'A' => 0,
                        'J' => 0,
                    ],
                ], ],
            'adresses' => [],
            'users' => 0,
            'groupes' => 0,
            'logs' => 0,
            'salles' => 0,
            'reservations' => 0,
            'sacrements' => 0,
            'funerailles' => 0,
            'messes' => 0,
        ];
    }

    /**
     * @return array{x: string, y: mixed}[]
     */
    public function getNewMembershipByMonth(): array
    {
        $mpms = $this->entityManager->getRepository(Membership::class)
            ->createQueryBuilder('m')
            ->select('COUNT(m) as nb, DATE_FORMAT(m.createdAt, \'%Y-%m-01\') as month')
            ->groupBy('month')
            ->where('m.first = true')
            ->orderBy('month')
            ->getQuery()
            ->getResult()
        ;

        $results = [];
        foreach ($mpms as $mpm) {
            $date = \DateTime::createFromFormat('Y-m-d', $mpm['month']);
            $results[] = [
                'x' => $date instanceof \DateTime ? $date->format('Y M') : '',
                'y' => $mpm['nb'],
            ];
        }

        return $results;
    }

    /**
     * @return array{x: string, y: mixed}[]
     */
    public function getMembershipByYear(string $type = 'organization'): array
    {
        $mpys = $this->entityManager->getRepository(Membership::class)
            ->createQueryBuilder('m')
            ->select('COUNT(m) as nb, DATE_FORMAT(p.start, \'%Y-01-01\') as year')
            ->leftJoin('m.period', 'p')
            ->where('m.status in(:validated, :in_progress)')
            ->andWhere('m.person IS '.('person' === $type ? 'NOT ' : '').'NULL')
            ->andWhere('m.organization IS '.('organization' === $type ? 'NOT ' : '').'NULL')
            ->groupBy('p')
            ->setParameter('validated', Membership::STATUS_VALIDATED)
            ->setParameter('in_progress', Membership::STATUS_IN_PROGRESS)
            ->getQuery()
            ->getResult()
        ;

        $results = [];
        foreach ($mpys as $mpy) {
            $date = \DateTime::createFromFormat('Y-m-d', $mpy['year']);
            $results[$mpy['year']] = [
                'x' => $date instanceof \DateTime ? $date->format('Y') : '',
                'y' => $mpy['nb'],
            ];
        }
        ksort($results);

        return array_values($results);
    }

    /**
     * @return array{name: null|string, nb: int}[]
     */
    public function getOrganizationsByType(Period $period): array
    {
        return $this->entityManager->getRepository(Membership::class)
            ->createQueryBuilder('m')
            ->select('COUNT(m) as nb, t.name')
            ->leftJoin('m.organization', 'o')
            ->leftJoin('o.type', 't')
            ->where('m.organization IS NOT NULL')
            ->andWhere('m.period = :period')
            ->andWhere('m.status in(:validated, :in_progress)')
            ->groupBy('t.id')
            ->setParameter('period', $period)
            ->setParameter('validated', Membership::STATUS_VALIDATED)
            ->setParameter('in_progress', Membership::STATUS_IN_PROGRESS)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return array{label: string[], data: int[]}
     */
    public function getEntitiesByInstance(): array
    {
        $entitiesByInstance = $this->entityManager->getRepository(Instance::class)
            ->createQueryBuilder('i')
            ->select('COUNT(e) as nb, i.name as name')
            ->leftJoin('i.entities', 'e')
            ->groupBy('i')
            ->getQuery()
            ->getResult()
        ;

        $results = [
            'label' => [],
            'data' => [],
        ];
        foreach ($entitiesByInstance as $entityByInstance) {
            $results['label'][] = $entityByInstance['name'];
            $results['data'][] = $entityByInstance['nb'];
        }

        return $results;
    }

    /**
     * @return array{label: string[], data: int[]}
     */
    public function getEntitiesByBiggestDioceses(): array
    {
        $entitiesByDiocese = $this->entityManager->getRepository(Organization::class)
            ->createQueryBuilder('o')
            ->select('COUNT(o.id) as nb, d.name')
            ->join('o.diocese', 'd')
            ->groupBy('d')
            ->orderBy('COUNT(o.id)', 'desc')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult()
        ;

        $results = [
            'label' => [],
            'data' => [],
        ];
        foreach ($entitiesByDiocese as $entityByDiocese) {
            $results['label'][] = $entityByDiocese['name'];
            $results['data'][] = $entityByDiocese['nb'];
        }

        return $results;
    }
}
