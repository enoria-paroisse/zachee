<?php

/**
 * @copyright Copyright (c) 2025 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Services;

use App\Entity\Person;
use App\Mailer\UserMailer;
use App\Repository\PersonRepository;
use Doctrine\ORM\NonUniqueResultException;
use GuzzleHttp\Exception\ClientException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Random\RandomException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

class PublicLoginService
{
    public function __construct(
        private readonly PersonRepository $personRepository,
        private readonly OvhService $ovhService,
        private readonly UserMailer $mailer,
    ) {
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getLoggedPerson(Request $request): ?Person
    {
        $session = $request->getSession();

        if (!$session->has('public_login_person') || !$session->get('public_logged', false)) {
            return null;
        }

        return $this->personRepository->findOneBy(['id' => $session->get('public_login_person')]);
    }

    /**
     * @throws ClientException
     */
    public function sendSmsCode(Person $person, int $code): void
    {
        if ($person->getPhoneNumber()) {
            $this->ovhService->sendSmsMessage(
                'Zachée: votre code pour vous identifier est '.$code.' @zachee.association-enoria.org #'.$code,
                [
                    PhoneNumberUtil::getInstance()->format($person->getPhoneNumber(), PhoneNumberFormat::E164),
                ]
            );
        } else {
            throw new \TypeError('Aucun numéro de téléphone trouvé');
        }
    }

    /**
     * @throws RandomException
     * @throws TransportExceptionInterface
     */
    public function createAndSendCode(Person $person, bool $phone, SessionInterface $session): void
    {
        $code = random_int(100000, 999999);

        if (null !== $person->getPhoneNumber() && $phone) {
            $method = 'phone';
            $this->sendSmsCode($person, $code);
        } else {
            $method = 'email';
            $this->mailer->sendCode($person, $code);
        }

        $session->set('public_login_person', $person->getId());
        $session->set('public_login_code', $code);
        $session->set('public_login_method', $method);
        $session->set('public_logged', false);
    }

    public function validateCode(int $code, SessionInterface $session): bool
    {
        if ($code === $session->get('public_login_code')) {
            $session->remove('public_login_code');
            $session->remove('public_login_method');
            $session->set('public_logged', true);

            return true;
        }

        return false;
    }

    public function reset(SessionInterface $session): void
    {
        $session->remove('public_login_person');
        $session->remove('public_login_code');
        $session->remove('public_login_method');
        $session->remove('public_logged');
    }
}
