<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Services;

use App\Entity\Newsletter;
use App\Entity\Person;
use Brevo\Client\Api\AccountApi;
use Brevo\Client\Api\AttributesApi;
use Brevo\Client\Api\ContactsApi;
use Brevo\Client\Api\ProcessApi;
use Brevo\Client\ApiException;
use Brevo\Client\Configuration;
use Brevo\Client\Model\CreatedProcessId;
use Brevo\Client\Model\RemoveContactFromList;
use Brevo\Client\Model\RequestContactImport;
use GuzzleHttp\Client;

class BrevoService
{
    public function __construct(
        private readonly string $apiKey,
        private readonly string $firstnameAttribute,
        private readonly string $lastnameAttribute,
    ) {
    }

    public function getFirstnameAttribute(): string
    {
        return $this->firstnameAttribute;
    }

    public function getLastnameAttribute(): string
    {
        return $this->lastnameAttribute;
    }

    public function getConfig(): Configuration
    {
        return Configuration::getDefaultConfiguration()->setApiKey('api-key', $this->apiKey);
    }

    public function getAccountApi(): AccountApi
    {
        return new AccountApi(
            new Client(),
            $this->getConfig(),
        );
    }

    public function getAttributesApi(): AttributesApi
    {
        return new AttributesApi(
            new Client(),
            $this->getConfig(),
        );
    }

    public function getContactsApi(): ContactsApi
    {
        return new ContactsApi(
            new Client(),
            $this->getConfig(),
        );
    }

    public function getProcessApi(): ProcessApi
    {
        return new ProcessApi(
            new Client(),
            $this->getConfig(),
        );
    }

    /**
     * @throws ApiException
     */
    public function cleanList(Newsletter $newsletter): void
    {
        $contactIdentifiers = new RemoveContactFromList();
        $contactIdentifiers->setAll(true);

        $result = $this->getContactsApi()->removeContactFromList(
            (int) $newsletter->getBrevoId(),
            $contactIdentifiers,
        );

        if ($result->getContacts()->getProcessId()) {
            $processApi = $this->getProcessApi();
            while ('completed' !== $processApi->getProcess($result->getContacts()->getProcessId())->getStatus()) {
                sleep(1);
            }
        }
    }

    /**
     * @param Newsletter[] $newsletters
     * @param Person[]     $people
     *
     * @throws ApiException
     */
    public function importContacts(array $newsletters, array $people): CreatedProcessId
    {
        /** @var int[] $ids */
        $ids = [];
        foreach ($newsletters as $newsletter) {
            $ids[] = (int) $newsletter->getBrevoId();
        }

        $body = 'EMAIL;'.$this->firstnameAttribute.';'.$this->lastnameAttribute."\n";
        foreach ($people as $person) {
            if ($person->getEmail()) {
                $body .= $person->getEmail().';'.$person->getFirstname().';'.$person->getLastname()."\n";
            }
        }

        $requestContactImport = $this->createRequestContactImport($ids);
        $requestContactImport->setFileBody($body);

        return $this->getContactsApi()->importContacts($requestContactImport);
    }

    /**
     * @param int[] $ids
     */
    public function createRequestContactImport(array $ids): RequestContactImport
    {
        $requestContactImport = new RequestContactImport();
        $requestContactImport->setListIds($ids)
            ->setEmailBlacklist(false)
            ->setSmsBlacklist(false)
            ->setUpdateExistingContacts(true)
        ;

        return $requestContactImport;
    }
}
