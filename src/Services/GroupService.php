<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Services;

use App\Entity\Group;
use App\Repository\GroupRepository;
use Doctrine\ORM\EntityManagerInterface;

class GroupService
{
    public function __construct(
        private readonly AggregateService $aggregateService,
        private readonly GroupRepository $groupRepository,
        private readonly EntityManagerInterface $entityManager,
    ) {
        $this->aggregateService->setPeriod(null);
    }

    /**
     * @throws \Exception
     */
    public function sync(Group $group): void
    {
        if (null !== $group->getAggregate()) {
            $people = $this->aggregateService->getPeople($group->getAggregate());
            // clean group
            $this->entityManager->getConnection()->executeQuery('DELETE from group_person WHERE group_id = '.$group->getId());

            // add people
            foreach ($people as $person) {
                $group->addPerson($person);
            }

            $this->groupRepository->save($group, true);
        }
    }
}
