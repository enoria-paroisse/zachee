<?php

/**
 * @copyright Copyright (c) 2025 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Services;

use Random\RandomException;
use Scheb\TwoFactorBundle\Model\BackupCodeInterface;
use Scheb\TwoFactorBundle\Model\PersisterInterface;
use Scheb\TwoFactorBundle\Security\TwoFactor\Backup\BackupCodeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;

class BackupCodeManager implements BackupCodeManagerInterface
{
    public function __construct(
        private readonly PersisterInterface $persister,
        private readonly RequestStack $requestStack,
    ) {
    }

    public function isBackupCode(object $user, string $code): bool
    {
        if ($user instanceof BackupCodeInterface) {
            return $user->isBackupCode($code);
        }

        return false;
    }

    public function invalidateBackupCode(object $user, string $code): void
    {
        if (!$user instanceof BackupCodeInterface) {
            return;
        }

        $user->invalidateBackupCode($code);
        $this->persister->persist($user);

        $session = $this->requestStack->getCurrentRequest()?->getSession();

        if ($session instanceof Session) {
            $session->getFlashBag()->add('warning', 'Vous avez utilisé un code de secours à usage unique, celui-ci ne pourra plus être utilisé. Pensez à générer un nouveau code dans votre profil');
        }
    }

    /**
     * @throws RandomException
     */
    public function generateBackupCode(): string
    {
        return (string) random_int(1000000000000000, 9999999999999999);
    }
}
