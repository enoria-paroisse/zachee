<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Services;

use App\Entity\PaymentRequestFile;
use App\Repository\PaymentRequestFileRepository;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Environment;

class PaymentRequestService
{
    public function __construct(
        private readonly PaymentRequestFileRepository $paymentRequestFileRepository,
        private readonly PdfService $pdfService,
        private readonly Environment $environment,
        private readonly MembershipFeeService $membershipFeeService,
        private readonly string $targetDirectory,
        private readonly string $ribFile,
    ) {
    }

    public function generatePdf(PaymentRequestFile $file): PdfService
    {
        // Payment for membership
        if ($file->getPaymentRequest() && $file->getPaymentRequest()->getMembership()) {
            return $this->pdfService->renderHtml($this->environment->render('membership/public/letter.pdf.twig', [
                'rootDirectory' => $this->pdfService->getRootDirectory(),
                'membership' => $file->getPaymentRequest()->getMembership(),
            ]));
        }
        // Payment for diocese membership supplement
        if ($file->getPaymentRequest()
            && $file->getPaymentRequest()->getDioceseMembershipSupplement()
            && $file->getPaymentRequest()->getDioceseMembershipSupplement()->getDiocese()
            && $file->getPaymentRequest()->getDioceseMembershipSupplement()->getPeriod()
        ) {
            $actualAssociatedMemberships = $this->membershipFeeService->getDioceseAssociatedMembershipData(
                $file->getPaymentRequest()->getDioceseMembershipSupplement()->getDiocese(),
                $file->getPaymentRequest()->getDioceseMembershipSupplement()->getPeriod()
            );

            return $this->pdfService->renderHtml($this->environment->render('pdf/accounting/payment_request_diocese_supplement.pdf.twig', [
                'rootDirectory' => $this->pdfService->getRootDirectory(),
                'file' => $file,
                'actualAssociatedMemberships' => $actualAssociatedMemberships['data'],
            ]));
        }
        // Payment for SMS billing
        if ($file->getPaymentRequest() && $file->getPaymentRequest()->getSmsBilling()) {
            return $this->pdfService->renderHtml($this->environment->render('pdf/accounting/payment_request_sms.pdf.twig', [
                'rootDirectory' => $this->pdfService->getRootDirectory(),
                'file' => $file,
                'smsBilling' => $file->getPaymentRequest()->getSmsBilling(),
            ]));
        }

        // Other payment
        return $this->pdfService->renderHtml($this->environment->render('pdf/accounting/payment_request.pdf.twig', [
            'rootDirectory' => $this->pdfService->getRootDirectory(),
            'file' => $file,
        ]));
    }

    public function generatePdfAndSave(PaymentRequestFile $file): void
    {
        $pdf = $this->generatePdf($file);
        $filename = uniqid('pr_'.$file->getRefPrefix().sprintf('%04d', $file->getRefNumber()).'_', true).'.pdf';
        file_put_contents($this->targetDirectory.'/'.$filename, $pdf->output());

        if ($file->getFile()) {
            $fs = new Filesystem();
            $fs->remove($this->targetDirectory.'/'.$file->getFile());
        }
        $file->setFile($filename);
        $this->paymentRequestFileRepository->save($file, true);
    }

    public function getTargetDirectory(): string
    {
        return $this->targetDirectory;
    }

    public function getRibFile(): string
    {
        return $this->ribFile;
    }
}
