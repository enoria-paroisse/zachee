<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Services;

use App\Entity\OrganizationType;
use App\Repository\OrganizationTypeRepository;
use Symfony\Component\String\Slugger\SluggerInterface;

class OrganizationTypeTagService
{
    public function __construct(
        private readonly OrganizationTypeRepository $organizationTypeRepository,
        private readonly SluggerInterface $slugger
    ) {
    }

    /**
     * @return string[]
     */
    public function getTags(): array
    {
        $types = [];
        $organizationTypesEntities = $this->organizationTypeRepository->findAll();
        foreach ($organizationTypesEntities as $type) {
            $types[] = $this->generateTagFromType($type);
        }

        return $types;
    }

    public function generateTagFromString(string $type): string
    {
        return 'organization_'.strtolower($this->slugger->slug($type, '_'));
    }

    public function generateTagFromType(OrganizationType $type): string
    {
        return 'organization_'.strtolower($this->slugger->slug($type->getName() ?? 'undefined', '_'));
    }
}
