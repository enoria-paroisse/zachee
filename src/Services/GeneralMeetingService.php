<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Services;

use App\Entity\GeneralMeeting;
use App\Entity\Person;
use App\Entity\Summons;
use App\Entity\TypeAssociatedPerson;
use Doctrine\ORM\EntityManagerInterface;

readonly class GeneralMeetingService
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    /**
     * @param TypeAssociatedPerson[] $types
     */
    public function presenceToggle(GeneralMeeting $generalMeeting, Person $person, array $types, bool $save = true): bool
    {
        $present = $generalMeeting->getArePresent()->contains($person);

        if ($present) {
            $this->setAbsent($generalMeeting, $person, $types);
        } else {
            $this->setPresent($generalMeeting, $person, $types);
        }

        if ($save) {
            $this->entityManager->flush();
        }

        return !$present;
    }

    /**
     * @return Summons[]
     */
    public function getNaturalSummonsList(GeneralMeeting $generalMeeting, Person $person): array
    {
        /**
         * @var Summons[] $naturalSummonsList
         */
        $naturalSummonsList = [];

        foreach ($person->getMemberships() as $membership) {
            foreach ($membership->getSummons() as $summons) {
                if ($generalMeeting === $summons->getGeneralMeeting()) {
                    $naturalSummonsList[] = $summons;
                }
            }
        }

        return $naturalSummonsList;
    }

    /**
     * @param TypeAssociatedPerson[] $types
     *
     * @return Summons[]
     */
    public function getLegalSummonsList(GeneralMeeting $generalMeeting, Person $person, array $types): array
    {
        /**
         * @var Summons[] $legalSummonsList
         */
        $legalSummonsList = [];

        // Legal member
        foreach ($person->getAssociatedOrganizations() as $associatedOrganization) {
            if ($associatedOrganization->getType() && in_array($associatedOrganization->getType(), $types, true)) {
                foreach ($associatedOrganization->getOrganization()->getMemberships() as $membership) {
                    foreach ($membership->getSummons() as $summons) {
                        if ($generalMeeting === $summons->getGeneralMeeting()) {
                            $legalSummonsList[] = $summons;
                        }
                    }
                }
            }
        }

        return $legalSummonsList;
    }

    /**
     * @param TypeAssociatedPerson[] $types
     *
     * @return Summons[]
     */
    public function getSummonsList(GeneralMeeting $generalMeeting, Person $person, array $types): array
    {
        return array_merge(
            $this->getNaturalSummonsList($generalMeeting, $person),
            $this->getLegalSummonsList($generalMeeting, $person, $types)
        );
    }

    /**
     * @param TypeAssociatedPerson[] $types
     */
    private function setPresent(GeneralMeeting $generalMeeting, Person $person, array $types): void
    {
        $generalMeeting->getArePresent()->add($person);

        $summonsList = $this->getSummonsList($generalMeeting, $person, $types);

        foreach ($summonsList as $summons) {
            $summons->setIsPresent(true);
            foreach ($summons->getMandates() as $mandate) {
                $mandate->setIsPresent(true);
            }
        }
    }

    /**
     * @param TypeAssociatedPerson[] $types
     */
    private function setAbsent(GeneralMeeting $generalMeeting, Person $person, array $types): void
    {
        $generalMeeting->getArePresent()->removeElement($person);

        /**
         * @var Summons[][] $mandates;
         */
        $mandates = [];

        // natural
        $naturalSummonsList = $this->getNaturalSummonsList($generalMeeting, $person);
        foreach ($naturalSummonsList as $summons) {
            $summons->setIsPresent(false);
            $mandates[] = $summons->getMandates()->toArray();
        }

        // legal
        $legalSummonsList = $this->getLegalSummonsList($generalMeeting, $person, $types);
        foreach ($legalSummonsList as $summons) {
            if ($summons->getMembership() && $summons->getMembership()->getOrganization()) {
                $present = false;
                foreach ($summons->getMembership()->getOrganization()->getAssociatedPeople() as $associatedPeople) {
                    if (in_array($associatedPeople->getType(), $types, true)) {
                        if ($generalMeeting->getArePresent()->contains($associatedPeople->getPerson())) {
                            $present = true;
                        }
                    }
                }
                $summons->setIsPresent($present);
            }
            $mandates[] = $summons->getMandates()->toArray();
        }

        // check mandates
        // given
        $summonsList = array_merge($naturalSummonsList, $legalSummonsList);
        foreach ($summonsList as $summons) {
            if ($summons->getMandate()) {
                $summons->setIsPresent($summons->getMandate()->isIsPresent());
            }
        }
        // received
        $mandatesList = array_merge(...$mandates);
        foreach ($mandatesList as $mandate) {
            $this->updatePresence($generalMeeting, $mandate, $types);
        }
    }

    /**
     * @param TypeAssociatedPerson[] $types
     */
    private function updatePresence(GeneralMeeting $generalMeeting, Summons $summons, array $types): void
    {
        // check mandate
        if ($summons->getMandate() && $summons->getMandate()->isIsPresent()) {
            $summons->setIsPresent(true);

            return;
        }

        // is natural
        if ($summons->getMembership() && $summons->getMembership()->getPerson()) {
            if ($generalMeeting->getArePresent()->contains($summons->getMembership()->getPerson())) {
                $summons->setIsPresent(true);

                foreach ($summons->getMandates() as $mandates) {
                    $mandates->setIsPresent(true);
                }
            } else {
                $summons->setIsPresent(false);
            }

            return;
        }

        // is legal
        if ($summons->getMembership() && $summons->getMembership()->getOrganization()) {
            $present = false;
            foreach ($summons->getMembership()->getOrganization()->getAssociatedPeople() as $associatedPeople) {
                if (in_array($associatedPeople->getType(), $types, true)) {
                    if ($generalMeeting->getArePresent()->contains($associatedPeople->getPerson())) {
                        $present = true;
                    }
                }
            }

            if ($present) {
                $summons->setIsPresent(true);

                foreach ($summons->getMandates() as $mandates) {
                    $mandates->setIsPresent(true);
                }
            } else {
                $summons->setIsPresent(false);
            }
        }
    }
}
