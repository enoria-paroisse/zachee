<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Services;

use App\Entity\NewsletterSubscription;
use App\Entity\Person;
use App\Repository\NewsletterRepository;
use App\Repository\NewsletterSubscriptionRepository;
use Doctrine\DBAL\ArrayParameterType;
use Doctrine\ORM\EntityManagerInterface;

class NewsletterService
{
    public function __construct(
        private readonly AggregateService $aggregateService,
        private readonly BrevoService $brevoService,
        private readonly EntityManagerInterface $entityManager,
        private readonly NewsletterRepository $newsletterRepository,
        private readonly NewsletterSubscriptionRepository $newsletterSubscriptionRepository,
    ) {
        $this->aggregateService->setPeriod(null);
    }

    /**
     * @throws \Exception
     */
    public function sync(): void
    {
        $newsletters = $this->newsletterRepository->findAllWithAggregates(true);

        // clean subscriptions
        $this->entityManager->getConnection()->executeQuery('DELETE from newsletter_subscription');

        // apply aggregates
        $first = true;
        $sql = 'INSERT INTO newsletter_subscription (person_id, newsletter_id, status) VALUES ';
        foreach ($newsletters as $newsletter) {
            $newsletter->setLastSync(new \DateTime())
                ->setSuccess(false)
            ;
            $this->newsletterRepository->save($newsletter);

            if ($newsletter->isExist()) {
                $peopleLists = [];
                foreach ($newsletter->getAggregates() as $aggregate) {
                    $peopleLists[] = $this->aggregateService->getPeople($aggregate);
                }

                /** @var Person[] $people */
                $people = array_filter(
                    array_unique(array_merge(...$peopleLists)),
                    static function (Person $person) {
                        return null !== $person->getEmail();
                    }
                );

                foreach ($people as $person) {
                    if (!$first) {
                        $sql .= ', ';
                    } else {
                        $first = false;
                    }
                    $sql .= '('.$person->getId().', '.$newsletter->getId().', "'.NewsletterSubscription::STATUS_NON_SYNC.'")';
                }
            }
        }
        $this->entityManager->flush();
        // they are subscriptions
        if (!$first) {
            $this->entityManager->getConnection()->executeQuery($sql);
        }

        // Brevo sync
        $subscribed = [];
        $blacklisted = [];
        foreach ($newsletters as $newsletter) {
            if (0 < $this->brevoService->getContactsApi()->getList((int) $newsletter->getBrevoId())->getTotalSubscribers()) {
                $this->brevoService->cleanList($newsletter);
            }

            /** @var Person[] $people */
            $people = [];
            foreach ($this->newsletterSubscriptionRepository->findAllByNewsletter($newsletter) as $subscription) {
                if (null !== $subscription->getPerson()) {
                    $people[] = $subscription->getPerson();
                }
            }
            if (0 < count($people)) {
                $this->brevoService->importContacts(
                    [$newsletter],
                    $people,
                );
            }
            $newsletter->setSuccess(true);
            $this->newsletterRepository->save($newsletter, true);

            $end = false;
            $offset = 0;
            while (!$end) {
                $contacts = $this->brevoService->getContactsApi()->getContactsFromList(
                    (int) $newsletter->getBrevoId(),
                    null,
                    500,
                    $offset
                );
                $offset += 500;
                if (0 === count($contacts->getContacts())) {
                    $end = true;
                } else {
                    /** @var string[] $contact */
                    foreach ($contacts->getContacts() as $contact) {
                        if ($contact['emailBlacklisted']) {
                            $blacklisted[] = $contact['email'];
                        } else {
                            $subscribed[] = $contact['email'];
                        }
                    }
                }
            }
        }

        // update status
        $sql = 'UPDATE newsletter_subscription s LEFT JOIN person p ON s.person_id = p.id SET s.status = :status WHERE p.email IN (:emails)';
        if (0 < count($subscribed)) {
            $this->entityManager->getConnection()->executeQuery($sql, [
                'status' => NewsletterSubscription::STATUS_SUBSCRIBED,
                'emails' => $subscribed,
            ], ['emails' => ArrayParameterType::STRING]);
        }
        if (0 < count($blacklisted)) {
            $this->entityManager->getConnection()->executeQuery($sql, [
                'status' => NewsletterSubscription::STATUS_BLACKLISTED,
                'emails' => $blacklisted,
            ], ['emails' => ArrayParameterType::STRING]);
        }
    }
}
