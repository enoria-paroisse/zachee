<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Services;

use Dompdf\Dompdf;
use Dompdf\FontMetrics;
use Dompdf\Options;
use FontLib\Font;
use Symfony\Component\HttpFoundation\Response;

class PdfService
{
    private readonly Options $options;
    private ?Dompdf $pdf = null;
    private string $size = 'A4';
    private string $orientation = 'portrait';

    public function __construct(private readonly string $kernelDirectory)
    {
        $this->options = new Options();

        $this->options->setDefaultFont('linux-libertine')
            ->setFontDir($kernelDirectory.'/assets/fonts')
            ->setFontCache($kernelDirectory.'/assets/fonts')
            ->setChroot($kernelDirectory)
            ->setDpi(300)
            ->setIsRemoteEnabled(false)
        ;
    }

    public function getRootDirectory(): string
    {
        return $this->kernelDirectory;
    }

    public function setOption(string $attribute, mixed $value = null): self
    {
        $this->options->set($attribute, $value);

        return $this;
    }

    /**
     * @param array<string, mixed>|string $attributes
     *
     * @return $this
     */
    public function setOptions(array|string $attributes, mixed $value = null): self
    {
        $this->options->set($attributes, $value);

        return $this;
    }

    public function setPaper(string $size, string $orientation = 'portrait'): self
    {
        $this->size = $size;
        $this->orientation = $orientation;

        return $this;
    }

    public function renderHtml(string $html): self
    {
        $pdf = $this->loadDompdf();
        $pdf->loadHtml($html);
        $pdf->render();

        return $this;
    }

    /**
     * @param array<string, mixed> $options
     */
    public function stream(string $filename, array $options = []): Response
    {
        if ($this->pdf instanceof Dompdf) {
            $this->pdf->stream($filename, $options);

            return new Response('The PDF file has been succesfully generated !');
        }

        return new Response("The PDF file don't been generated !", 500);
    }

    /**
     * @return null|string
     */
    public function output()
    {
        if ($this->pdf instanceof Dompdf) {
            return $this->pdf->output();
        }

        return null;
    }

    public function getFontMetrics(): FontMetrics
    {
        $pdf = $this->loadDompdf();

        return $pdf->getFontMetrics();
    }

    /**
     * Inspired from https://gist.github.com/woodyhayday/f8dc36cc7ec922bc1894f33eb2b0e928.
     */
    public function installFontFamily(
        string $fontname,
        string $normal,
        ?string $bold = null,
        ?string $italic = null,
        ?string $boldItalic = null,
        bool $debug = false
    ): void {
        $dompdf = $this->loadDompdf();
        $fontMetrics = $dompdf->getFontMetrics();

        if (!is_readable($normal)) {
            throw new \RuntimeException("Unable to read '{$normal}'.");
        }
        $dir = dirname($normal);
        $basename = basename($normal);
        $lastDot = strrpos($basename, '.');
        if (false !== $lastDot) {
            $file = substr($basename, 0, $lastDot);
            $ext = strtolower(substr($basename, $lastDot));
        } else {
            $file = $basename;
            $ext = '';
        }

        if (!in_array($ext, ['.ttf', '.otf'])) {
            throw new \RuntimeException("Unable to process fonts of type '{$ext}'.");
        }

        // Try $file_Bold.$ext etc.
        $path = "{$dir}/{$file}";
        $patterns = [
            'bold' => ['_Bold', 'b', 'B', 'bd', 'BD'],
            'italic' => ['_Italic', 'i', 'I'],
            'boldItalic' => ['_Bold_Italic', 'bi', 'BI', 'ib', 'IB'],
        ];

        foreach ($patterns as $type => $patternsArray) {
            if (!isset(${$type}) || !is_readable(${$type})) {
                foreach ($patternsArray as $pattern) {
                    if (is_readable("{$path}{$pattern}{$ext}")) {
                        ${$type} = "{$path}{$pattern}{$ext}";

                        break;
                    }
                }

                if ($debug && is_null(${$type})) {
                    echo "Unable to find {$type} face file.\n";
                }
            }
        }

        $fonts = compact('normal', 'bold', 'italic', 'boldItalic');
        $entry = [];

        // Copy the files to the font directory.
        foreach ($fonts as $var => $src) {
            if ('boldItalic' === $var) {
                $var = 'bold_italic';
            }

            /** @var string $fontDir */
            $fontDir = $dompdf->getOptions()->get('fontDir');

            if (is_null($src)) {
                $entry[$var] = $fontDir.'/'.mb_substr(basename($normal), 0, -4);

                continue;
            }

            // Verify that the fonts exist and are readable
            if (!is_readable($src)) {
                throw new \RuntimeException("Requested font '{$src}' is not readable");
            }
            $dest = $fontDir.'/'.basename($src);

            if (!is_writable(dirname($dest))) {
                throw new \RuntimeException("Unable to write to destination '{$dest}'.");
            }
            if ($debug) {
                echo "Copying {$src} to {$dest}...\n";
            }

            if (!copy($src, $dest)) {
                throw new \RuntimeException("Unable to copy '{$src}' to '{$dest}'");
            }
            $entryName = mb_substr($dest, 0, -4);

            if ($debug) {
                echo "Generating Adobe Font Metrics for {$entryName}...\n";
            }

            $fontObj = Font::load($dest);
            if ($fontObj) {
                $fontObj->saveAdobeFontMetrics("{$entryName}.ufm");
                $fontObj->close();
            }

            $entry[$var] = $entryName;
        }

        // Store the fonts in the lookup table
        $fontMetrics->setFontFamily($fontname, $entry);

        // Save the changes
        $fontMetrics->saveFontFamilies();
    }

    private function loadDompdf(): Dompdf
    {
        $this->pdf = new Dompdf($this->options);
        $this->pdf->setPaper($this->size, $this->orientation);

        return $this->pdf;
    }
}
