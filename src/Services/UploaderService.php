<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Services;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploaderService
{
    public function __construct(private readonly string $targetDirectory)
    {
    }

    public function upload(UploadedFile $file, string $folder, ?string $replace = null, string $prefix = ''): string
    {
        $fileName = uniqid('' !== $prefix ? $prefix : $folder.'_', true).'.'.$file->guessExtension();

        try {
            if ($replace) {
                $this->delete($folder, $replace);
            }

            $file->move($this->getTargetDirectory().'/'.$folder, $fileName);
        } catch (FileException) {
            return '';
        }

        return $fileName;
    }

    public function delete(string $folder, string $filename): void
    {
        $fs = new Filesystem();
        $fs->remove($this->getTargetDirectory().'/'.$folder.'/'.$filename);
    }

    public function getTargetDirectory(): string
    {
        return $this->targetDirectory;
    }
}
