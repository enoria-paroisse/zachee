<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Services;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Ovh\Api;
use Ovh\Exceptions\InvalidParameterException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class OvhService
{
    private static ?Api $ovh = null;

    private string $smsServiceName = '';

    private string $smsSender = '';

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->smsServiceName = $parameterBag->get('app.ovh.sms.service_name');
        $this->smsSender = $parameterBag->get('app.ovh.sms.sender');

        if (null === self::$ovh) {
            try {
                self::$ovh = new Api(
                    $parameterBag->get('app.ovh.api_key'),
                    $parameterBag->get('app.ovh.api_secret'),
                    'ovh-eu',
                    $parameterBag->get('app.ovh.user_key')
                );
            } catch (InvalidParameterException) {
                self::$ovh = null;
            }
        }
    }

    public function getApi(): ?Api
    {
        return self::$ovh;
    }

    public function getSmsServiceName(): string
    {
        return $this->smsServiceName;
    }

    public function getSmsSender(): string
    {
        return $this->smsSender;
    }

    public function checkApi(): int
    {
        try {
            $smsServices = self::$ovh?->get('/sms');

            if (!is_array($smsServices) || !count($smsServices)) {
                $code = 501;
            } else {
                $code = 200;
            }

            return $code;
        } catch (RequestException $e) {
            $code = $e->getCode();

            if (403 === $code && mb_strpos($e->getMessage(), 'INVALID_KEY')) {
                $code = 4031;
            } elseif (403 === $code && mb_strpos($e->getMessage(), 'NOT_CREDENTIAL')) {
                $code = 4032;
            } elseif (403 === $code && mb_strpos($e->getMessage(), 'NOT_GRANTED_CALL')) {
                $code = 4033;
            }

            return $code;
        }
    }

    /**
     * @return array{
     *     code: int|mixed,
     *     error: string,
     *     serviceName: mixed,
     *     serviceDescription: mixed,
     *     creditsLeft: mixed,
     *     senders: mixed,
     *     sender: mixed
     * }
     *
     * @throws \JsonException
     */
    public function getSmsInfos(): array
    {
        $code = 200;
        $error = '';
        $senders = [];

        try {
            $result = self::$ovh?->get('/sms/'.$this->smsServiceName);
            $senders = self::$ovh?->get('/sms/'.$this->smsServiceName.'/senders');

            try {
                self::$ovh?->get('/sms/'.$this->smsServiceName.'/senders/'.$this->smsSender);
            } catch (ClientException $e) {
                $code = $e->getCode();
                if (404 === $code) {
                    $code = 4042;
                }
                $error = $e->getMessage();
            }
        } catch (ClientException $e) {
            $code = $e->getCode();
            if (404 === $code) {
                $code = 4041;
            }
            $error = $e->getMessage();
        }

        return [
            'code' => $code,
            'error' => $error,
            'serviceName' => $result['name'] ?? '',
            'serviceDescription' => $result['description'] ?? '',
            'creditsLeft' => $result['creditsLeft'] ?? '',
            'senders' => $senders,
            'sender' => $this->smsSender,
        ];
    }

    /**
     * @param string[] $cellphones
     *
     * @return array{sentMessage: string, totalCreditsRemoved: mixed, invalidReceivers: string[], ids: string[], validReceivers: string[], tag: mixed}
     */
    public function sendSmsMessage(string $content, array $cellphones): array
    {
        $postContent = [
            'charset' => 'UTF-8',
            'class' => 'phoneDisplay',
            'coding' => '7bit',
            'message' => $content,
            'noStopClause' => true,
            'priority' => 'high',
            'receivers' => $cellphones,
            'sender' => $this->smsSender,
            'senderForResponse' => 'false',
            'validityPeriod' => 2880,
        ];

        $result = self::$ovh?->post('/sms/'.$this->smsServiceName.'/jobs/', $postContent);

        return [
            'sentMessage' => $content,
            'totalCreditsRemoved' => $result['totalCreditsRemoved'] ?? 0,
            'invalidReceivers' => $result['invalidReceivers'] ?? [],
            'ids' => $result['ids'] ?? [],
            'validReceivers' => $result['validReceivers'] ?? [],
            'tag' => $result['tag'] ?? '',
        ];
    }
}
