<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Doctrine;

use ApiPlatform\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\Entity\AssociatedPerson;
use App\Entity\Organization;
use App\Entity\Person;
use App\Entity\User;
use App\Repository\AssociatedPersonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\SecurityBundle\Security;

final class CurrentUserExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    public function __construct(private readonly Security $security, private readonly EntityManagerInterface $em, private readonly AssociatedPersonRepository $associatedPersonRepository)
    {
    }

    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
    {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    public function applyToItem(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, ?Operation $operation = null, array $context = []): void
    {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass): void
    {
        if (Person::class !== $resourceClass && Organization::class !== $resourceClass && AssociatedPerson::class !== $resourceClass) {
            return;
        }

        $user = $this->security->getUser();
        if (!$user instanceof User || $this->security->isGranted('ROLE_BASIC_SHOW')) {
            return;
        }

        if (Person::class === $resourceClass) {
            $this->addWherePeople($queryBuilder, $user);
        } elseif (AssociatedPerson::class === $resourceClass) {
            $this->addWhereAssociatedPeople($queryBuilder, $user);
        } elseif (Organization::class === $resourceClass) {
            $this->addWhereOrganizations($queryBuilder, $user);
        }
    }

    private function addWherePeople(QueryBuilder $queryBuilder, User $user): void
    {
        $rootAlias = $queryBuilder->getRootAliases()[0];
        $queryBuilder->andWhere($this->em->getExpressionBuilder()->in(
            $rootAlias.'.id',
            $this->associatedPersonRepository->findRelatedByPersonQuery(['person.id'])->getDQL()
        ))
            ->setParameter(':person', $user->getPerson())
        ;
    }

    private function addWhereAssociatedPeople(QueryBuilder $queryBuilder, User $user): void
    {
        $rootAlias = $queryBuilder->getRootAliases()[0];
        $queryBuilder->andWhere($this->em->getExpressionBuilder()->in(
            $rootAlias.'.id',
            $this->associatedPersonRepository->findRelatedByPersonQuery(['a.id'])->getDQL()
        ))
            ->setParameter(':person', $user->getPerson())
        ;
    }

    private function addWhereOrganizations(QueryBuilder $queryBuilder, User $user): void
    {
        $rootAlias = $queryBuilder->getRootAliases()[0];
        $queryBuilder->andWhere($this->em->getExpressionBuilder()->in(
            $rootAlias.'.id',
            $this->associatedPersonRepository->findRelatedByPersonQuery(['organization.id'])->getDQL()
        ))
            ->setParameter(':person', $user->getPerson())
        ;
    }
}
