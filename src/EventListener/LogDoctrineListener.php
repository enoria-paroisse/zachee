<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\EventListener;

use App\Entity\Aggregate;
use App\Entity\Application;
use App\Entity\ApplicationUser;
use App\Entity\AssociatedPerson;
use App\Entity\CaMember;
use App\Entity\CronLog;
use App\Entity\CronTaskLog;
use App\Entity\Diocese;
use App\Entity\DioceseMembershipSupplement;
use App\Entity\Doc;
use App\Entity\Entity;
use App\Entity\Folder;
use App\Entity\GeneralMeeting;
use App\Entity\Group;
use App\Entity\Instance;
use App\Entity\Log;
use App\Entity\Membership;
use App\Entity\Newsletter;
use App\Entity\NewsletterFolder;
use App\Entity\NewsletterSubscription;
use App\Entity\Organization;
use App\Entity\OrganizationType;
use App\Entity\Payment;
use App\Entity\PaymentAllocation;
use App\Entity\PaymentRequest;
use App\Entity\Period;
use App\Entity\Person;
use App\Entity\Receipt;
use App\Entity\SmsBilling;
use App\Entity\SubOrganizationMembershipFee;
use App\Entity\SubOrganizationMembershipFeePeriod;
use App\Entity\Title;
use App\Entity\TypeAssociatedPerson;
use App\Entity\User;
use App\Entity\Wasselynck;
use App\Entity\WebauthnCredential;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\PostUpdateEventArgs;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Event\PreRemoveEventArgs;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

#[AsDoctrineListener('prePersist', 100)]
#[AsDoctrineListener('postUpdate', 100)]
#[AsDoctrineListener('preRemove', 100)]
class LogDoctrineListener
{
    public function __construct(private readonly Security $security, private readonly TranslatorInterface $translator)
    {
    }

    public function prePersist(PrePersistEventArgs $args): void
    {
        $this->logActivity(Log::TYPE_ADD, $args);
        $this->setUpdatedBy($args->getObject());
    }

    public function postUpdate(PostUpdateEventArgs $args): void
    {
        $this->setUpdatedBy($args->getObject());
        $this->logActivity(Log::TYPE_UPDATE, $args);
        $args->getObjectManager()->flush();
    }

    public function preRemove(PreRemoveEventArgs $args): void
    {
        $this->logActivity(Log::TYPE_DELETE, $args);
    }

    private function logActivity(string $type, PostUpdateEventArgs|PrePersistEventArgs|PreRemoveEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof Log
            || $entity instanceof Title
            || $entity instanceof TypeAssociatedPerson
            || $entity instanceof User
            || $entity instanceof CronLog
            || $entity instanceof CronTaskLog
            || $entity instanceof Newsletter
            || $entity instanceof NewsletterFolder
            || $entity instanceof NewsletterSubscription
            || $entity instanceof WebauthnCredential) {
            return;
        }

        if ($entity instanceof Wasselynck && Log::TYPE_UPDATE !== $type) {
            return;
        }

        $log = new Log();
        $log->setDate(new \DateTime())
            ->setType($type)
            ->setObject(str_replace('App\Entity\\', '', $entity::class))
        ;

        if ((Log::TYPE_UPDATE === $type) && method_exists($entity, 'getId')) {
            $log->setObjectId($entity->getId());
        }
        $user = $this->security->getUser();
        if ($user instanceof User) {
            $log->setUser($user);
        } elseif ('cli' === PHP_SAPI) {
            $log->setIsFromCli(true);
        }

        if ($entity instanceof AssociatedPerson) {
            $log->setDescription('Association de '.$entity->getPerson()->getFullname().' avec '.$entity->getOrganization());
        } elseif ($entity instanceof CaMember) {
            $log->setDescription('Membre du CA '.$entity->getPerson());
        } elseif ($entity instanceof Diocese) {
            $log->setDescription('Diocèse : '.$entity->getName());
        } elseif ($entity instanceof Entity) {
            $log->setDescription('Entité '.$entity->getName().' de l’instance '.$entity->getInstance()->getName());
        } elseif ($entity instanceof Instance) {
            $log->setDescription('Instance '.$entity->getName());
        } elseif ($entity instanceof Membership) {
            $organization = $entity->getOrganization();
            $person = $entity->getPerson();
            $msg = 'Adhésion de ';
            if ($organization) {
                $msg .= $organization->getName();
            } elseif ($person) {
                $msg .= $person->getFullname();
            }
            $msg .= ' pour la période '.$entity->getPeriod().' (';

            switch ($entity->getStatus()) {
                case Membership::STATUS_CANCELED:
                    $msg .= 'annulée)';

                    break;

                case Membership::STATUS_IN_PROGRESS:
                    $msg .= 'en cours)';

                    break;

                case Membership::STATUS_VALIDATED:
                    $msg .= 'validée)';

                    break;
            }
            $log->setDescription($msg);
        } elseif ($entity instanceof Organization) {
            $log->setDescription($entity->getName());
        } elseif ($entity instanceof OrganizationType) {
            $log->setDescription('Type d’organisation '.$entity->getName());
        } elseif ($entity instanceof Period) {
            $log->setDescription('Période '.$entity->getName());
        } elseif ($entity instanceof SubOrganizationMembershipFee) {
            $log->setDescription('Cotisation '.$entity->getName());
        } elseif ($entity instanceof SubOrganizationMembershipFeePeriod) {
            if ($entity->getSubOrganizationMembershipFee() && $entity->getPeriod()) {
                $log->setDescription('Cotisation '.$entity->getSubOrganizationMembershipFee()->getName().' pour la période '.$entity->getPeriod()->getName());
            }
        } elseif ($entity instanceof Person) {
            $log->setDescription($entity->getFullname());
        } elseif ($entity instanceof Group) {
            $log->setDescription($entity->getName());
        } elseif ($entity instanceof Wasselynck) {
            $diocese = $entity->getDiocese();
            $period = $entity->getPeriod();
            if ($diocese && $period) {
                $log->setDescription('Indice de Wasselynck du diocèse '.$diocese.' pour la période '.$period);
            }
        } elseif ($entity instanceof Folder) {
            $log->setDescription('Dossier '.$entity->getName());
        } elseif ($entity instanceof Doc) {
            $log->setDescription('Document '.$entity->getName());
        } elseif ($entity instanceof PaymentRequest) {
            $log->setDescription('Demande de paiement : '.$entity->getDescription());
        } elseif ($entity instanceof Payment) {
            $log->setDescription('Paiement');
        } elseif ($entity instanceof PaymentAllocation) {
            $log->setDescription('Paiement affecté à '.$entity->getPaymentRequest()?->getDescription());
        } elseif ($entity instanceof Receipt) {
            $log->setDescription('Reçu No '.$entity->getRef());
        } elseif ($entity instanceof SmsBilling) {
            $log->setDescription(
                'Rapprochement SMS '.$entity->getRef()
                .' ('.$entity->getEntity()?->getInstance()->getName().': '.$entity->getEntity()?->getName().')'
            );
        } elseif ($entity instanceof Aggregate) {
            $log->setDescription('Agrégat '.$entity->getName());
        } elseif ($entity instanceof DioceseMembershipSupplement) {
            return;
        } elseif ($entity instanceof GeneralMeeting) {
            $log->setDescription(
                'Assemblée Générale'
                .($entity->getType() ? ' '.$this->translator->trans($entity->getType(), [], 'forms') : '')
                .($entity->getDatetime() ? ' du '.date_format($entity->getDatetime(), 'd/m/Y') : '')
            );
        } elseif ($entity instanceof Application) {
            $log->setDescription($entity->getName());
        } elseif ($entity instanceof ApplicationUser) {
            $log->setDescription($entity->getApplication()->getName().' ('.$entity->getUsername().')');
        }

        $args->getObjectManager()->persist($log);
    }

    private function setUpdatedBy(object $entity): void
    {
        if (method_exists($entity, 'setUpdatedBy')) {
            $entity->setUpdatedBy($this->security->getUser());
        }
    }
}
