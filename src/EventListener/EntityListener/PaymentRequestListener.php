<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\EventListener\EntityListener;

use App\Entity\PaymentRequest;
use App\Repository\PaymentRequestRepository;
use App\Services\EnoriaException;
use App\Services\EnoriaService;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Events;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\FlashBagAwareSessionInterface;

#[AsEntityListener(event: Events::prePersist, method: 'prePersist', entity: PaymentRequest::class, priority: 200)]
#[AsEntityListener(event: Events::preUpdate, method: 'preUpdate', entity: PaymentRequest::class, priority: 200)]
class PaymentRequestListener
{
    public function __construct(
        private readonly PaymentRequestRepository $paymentRequestRepository,
        private readonly EnoriaService $enoriaService,
        protected RequestStack $requestStack,
    ) {
    }

    public function prePersist(PaymentRequest $paymentRequest): void
    {
        $this->updateStatus($paymentRequest);
    }

    public function preUpdate(PaymentRequest $paymentRequest): void
    {
        $this->updateStatus($paymentRequest);
        $this->updateEntityStatus($paymentRequest);
    }

    private function updateStatus(PaymentRequest $paymentRequest): void
    {
        if (PaymentRequest::STATUS_IN_PROGRESS === $paymentRequest->getStatus() && abs((float) $paymentRequest->getAmount()) <= $paymentRequest->getAmountPaid()) {
            $paymentRequest->setStatus(PaymentRequest::STATUS_COMPLETED);
            $this->paymentRequestRepository->save($paymentRequest);
        } elseif (PaymentRequest::STATUS_COMPLETED === $paymentRequest->getStatus() && abs((float) $paymentRequest->getAmount()) > $paymentRequest->getAmountPaid()) {
            $paymentRequest->setStatus(PaymentRequest::STATUS_IN_PROGRESS);
            $this->paymentRequestRepository->save($paymentRequest);
        }
    }

    private function updateEntityStatus(PaymentRequest $paymentRequest): void
    {
        if ($paymentRequest->getMembership() && null !== $paymentRequest->getMembership()->getEntity() && $paymentRequest->getMembership()->getEntity()->getInstance()->isSyncUpEnabled()) {
            $entity = $paymentRequest->getMembership()->getEntity();
            $updated = false;

            try {
                $updated = $this->enoriaService->autoUpdateEntityStatus($entity, $paymentRequest->getMembership(), false);
            } catch (EnoriaException $e) {
            }

            if ($updated) {
                $session = $this->requestStack->getCurrentRequest()?->getSession();
                if ($session instanceof FlashBagAwareSessionInterface) {
                    $session->getFlashBag()->add(
                        'success',
                        'Le statut de l’entité '.$entity->getName().' ('.$entity->getInstance()->getName().') a été mis à jour'
                    );
                }
            }
        }
    }
}
