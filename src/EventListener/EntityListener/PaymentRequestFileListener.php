<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\EventListener\EntityListener;

use App\Entity\PaymentRequestFile;
use App\Repository\PaymentRequestFileRepository;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Events;

#[AsEntityListener(event: Events::prePersist, method: 'prePersist', entity: PaymentRequestFile::class, priority: 200)]
#[AsEntityListener(event: Events::preUpdate, method: 'preUpdate', entity: PaymentRequestFile::class, priority: 200)]
class PaymentRequestFileListener
{
    public function __construct(
        private readonly PaymentRequestFileRepository $paymentRequestFileRepository
    ) {
    }

    public function prePersist(PaymentRequestFile $file): void
    {
        $this->updatePaymentRequestRef($file);
    }

    public function preUpdate(PaymentRequestFile $file): void
    {
        $this->updatePaymentRequestRef($file);
    }

    private function updatePaymentRequestRef(PaymentRequestFile $file): void
    {
        if (0 === $file->getRefNumber()) {
            $maxRef = $this->paymentRequestFileRepository->findMaxRefByPrefix($file->getRefPrefix());
            $increment = is_numeric($maxRef) ? $maxRef + 1 : 1;
            $file->setRefNumber($increment);
        }
        $file->setRef($file->getRefPrefix().' '.sprintf('%04d', $file->getRefNumber()));

        if ($file->getPaymentRequest()?->getSmsBilling()) {
            $file->getPaymentRequest()->setDescription($file->getRef().'');
        }
    }
}
