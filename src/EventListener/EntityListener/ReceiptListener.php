<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\EventListener\EntityListener;

use App\Entity\Payment;
use App\Entity\Receipt;
use App\Repository\PaymentRepository;
use App\Repository\ReceiptRepository;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Events;

#[AsEntityListener(event: Events::prePersist, method: 'prePersist', entity: Receipt::class, priority: 200)]
#[AsEntityListener(event: Events::preUpdate, method: 'preUpdate', entity: Receipt::class, priority: 200)]
class ReceiptListener
{
    public function __construct(
        private readonly PaymentRepository $paymentRepository,
        private readonly ReceiptRepository $receiptRepository,
    ) {
    }

    public function prePersist(Receipt $receipt): void
    {
        $this->updateReceiptRef($receipt);
    }

    public function preUpdate(Receipt $receipt): void
    {
        $this->updateReceiptRef($receipt);
    }

    private function updateReceiptRef(Receipt $receipt): void
    {
        if (0 === $receipt->getRefNumber()) {
            $maxRef = $this->receiptRepository->findMaxRefByPrefix($receipt->getRefPrefix());
            $increment = is_numeric($maxRef) ? $maxRef + 1 : 1;
            $receipt->setRefNumber($increment);
        }
        $receipt->setRef($receipt->getRefPrefix().' '.sprintf('%04d', $receipt->getRefNumber()));

        if ($receipt->getPayment() && Payment::STATUS_ALLOCATED === $receipt->getPayment()->getStatus()) {
            $receipt->getPayment()->setStatus(Payment::STATUS_VALIDATED);
            $this->paymentRepository->save($receipt->getPayment());
        }
    }
}
