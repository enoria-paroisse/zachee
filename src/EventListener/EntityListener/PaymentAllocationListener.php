<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\EventListener\EntityListener;

use App\Entity\Payment;
use App\Entity\PaymentAllocation;
use App\Entity\PaymentRequest;
use App\Repository\PaymentRepository;
use App\Repository\PaymentRequestRepository;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Events;

#[AsEntityListener(event: Events::prePersist, method: 'prePersist', entity: PaymentAllocation::class, priority: 200)]
#[AsEntityListener(event: Events::preUpdate, method: 'preUpdate', entity: PaymentAllocation::class, priority: 200)]
#[AsEntityListener(event: Events::preRemove, method: 'preRemove', entity: PaymentAllocation::class, priority: 200)]
class PaymentAllocationListener
{
    public function __construct(
        private readonly PaymentRepository $paymentRepository,
        private readonly PaymentRequestRepository $paymentRequestRepository,
    ) {
    }

    public function prePersist(PaymentAllocation $paymentAllocation): void
    {
        $paymentAllocation->getPayment()?->addPaymentAllocation($paymentAllocation);
        $paymentAllocation->getPaymentRequest()?->addPaymentAllocation($paymentAllocation);
        $this->updateStatus($paymentAllocation);
    }

    public function preUpdate(PaymentAllocation $paymentAllocation): void
    {
        $this->updateStatus($paymentAllocation);
    }

    public function preRemove(PaymentAllocation $paymentAllocation): void
    {
        if ($paymentAllocation->getPaymentRequest() && PaymentRequest::STATUS_COMPLETED === $paymentAllocation->getPaymentRequest()->getStatus()) {
            $paymentRequest = $paymentAllocation->getPaymentRequest();
            $paymentRequest->setStatus(PaymentRequest::STATUS_IN_PROGRESS);
            $paymentRequest->removePaymentAllocation($paymentAllocation);
            $this->paymentRequestRepository->save($paymentRequest);
        }

        if ($paymentAllocation->getPayment() && Payment::STATUS_ALLOCATED === $paymentAllocation->getPayment()->getStatus()) {
            $payment = $paymentAllocation->getPayment();
            $payment->setStatus(Payment::STATUS_REGISTERED);
            $payment->removePaymentAllocation($paymentAllocation);
            $this->paymentRepository->save($payment);
        }
    }

    private function updateStatus(PaymentAllocation $paymentAllocation): void
    {
        if ($paymentAllocation->getPaymentRequest()) {
            if (PaymentRequest::STATUS_IN_PROGRESS === $paymentAllocation->getPaymentRequest()->getStatus() && $paymentAllocation->getPaymentRequest()->getAmount() <= $paymentAllocation->getPaymentRequest()->getAmountPaid()) {
                $paymentAllocation->getPaymentRequest()->setStatus(PaymentRequest::STATUS_COMPLETED);
                $this->paymentRequestRepository->save($paymentAllocation->getPaymentRequest());
            } elseif (PaymentRequest::STATUS_COMPLETED === $paymentAllocation->getPaymentRequest()->getStatus() && $paymentAllocation->getPaymentRequest()->getAmount() > $paymentAllocation->getPaymentRequest()->getAmountPaid()) {
                $paymentAllocation->getPaymentRequest()->setStatus(PaymentRequest::STATUS_IN_PROGRESS);
                $this->paymentRequestRepository->save($paymentAllocation->getPaymentRequest());
            }
        }

        if ($paymentAllocation->getPayment()) {
            if (Payment::STATUS_REGISTERED === $paymentAllocation->getPayment()->getStatus() && $paymentAllocation->getPayment()->getAmount() <= $paymentAllocation->getPayment()->getAmountAllocated()) {
                $paymentAllocation->getPayment()->setStatus(Payment::STATUS_ALLOCATED);
                $this->paymentRepository->save($paymentAllocation->getPayment());
            } elseif (Payment::STATUS_ALLOCATED === $paymentAllocation->getPayment()->getStatus() && $paymentAllocation->getPayment()->getAmount() > $paymentAllocation->getPayment()->getAmountAllocated()) {
                $paymentAllocation->getPayment()->setStatus(Payment::STATUS_REGISTERED);
                $this->paymentRepository->save($paymentAllocation->getPayment());
            }
        }
    }
}
