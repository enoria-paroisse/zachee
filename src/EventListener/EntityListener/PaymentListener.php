<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\EventListener\EntityListener;

use App\Entity\Payment;
use App\Repository\PaymentRepository;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Events;

#[AsEntityListener(event: Events::prePersist, method: 'prePersist', entity: Payment::class, priority: 200)]
#[AsEntityListener(event: Events::preUpdate, method: 'preUpdate', entity: Payment::class, priority: 200)]
class PaymentListener
{
    public function __construct(
        private readonly PaymentRepository $paymentRepository,
    ) {
    }

    public function prePersist(Payment $payment): void
    {
        $this->updateStatus($payment);
    }

    public function preUpdate(Payment $payment): void
    {
        $this->updateStatus($payment);
    }

    private function updateStatus(Payment $payment): void
    {
        if (Payment::STATUS_REGISTERED === $payment->getStatus() && $payment->getAmount() <= $payment->getAmountAllocated()) {
            $payment->setStatus(Payment::STATUS_ALLOCATED);
            $this->paymentRepository->save($payment);
        } elseif (Payment::STATUS_ALLOCATED === $payment->getStatus() && $payment->getAmount() > $payment->getAmountAllocated()) {
            $payment->setStatus(Payment::STATUS_REGISTERED);
            $this->paymentRepository->save($payment);
        }
    }
}
