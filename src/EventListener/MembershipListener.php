<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\EventListener;

use App\Entity\Membership;
use App\Entity\PaymentRequest;
use App\Mailer\MembershipMailer;
use App\Repository\MembershipRepository;
use App\Services\EnoriaException;
use App\Services\EnoriaService;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\FlashBagAwareSessionInterface;

#[AsDoctrineListener('preUpdate', 500)]
class MembershipListener
{
    public function __construct(
        private readonly MembershipRepository $membershipRepository,
        private readonly MembershipMailer $mailer,
        protected RequestStack $requestStack,
        private readonly EnoriaService $enoriaService,
    ) {
    }

    public function preUpdate(PreUpdateEventArgs $args): void
    {
        $this->updateMembership($args->getObject());
        $entity = $args->getObject();
        if ($entity instanceof PaymentRequest) {
            $this->updateMembership($entity->getMembership());
        } elseif ($entity instanceof Membership) {
            $this->updateEntityStatus($entity);
        }
    }

    private function updateMembership(mixed $entity): void
    {
        if ($entity instanceof Membership && $entity->canValidate()) {
            $entity->setStatus(Membership::STATUS_VALIDATED);
            $this->membershipRepository->save($entity);
            $this->addValidationMessage($entity);
        }
    }

    private function addValidationMessage(Membership $membership): void
    {
        $this->mailer->sendFirstMembershipValidate($membership);
        $session = $this->requestStack->getCurrentRequest()?->getSession();
        if ($session instanceof FlashBagAwareSessionInterface) {
            $session->getFlashBag()->add(
                'success',
                'L’adhésion de "'.($membership->getOrganization() ?? $membership->getPerson()).'", pour la période '.$membership->getPeriod().', a été automatiquement validée'
            );

            if (true === $membership->getFirst() && null !== $membership->getOrganization()) {
                $session->getFlashBag()->add(
                    'success',
                    'Un mail de bienvenue a été envoyé à "'.$membership->getOrganization().'"'
                );
            }
        }
    }

    /**
     * @throws EnoriaException
     */
    private function updateEntityStatus(Membership $membership): void
    {
        if ($membership->getEntity() && $membership->getEntity()->getInstance()->isSyncUpEnabled()) {
            $updated = false;

            try {
                $updated = $this->enoriaService->autoUpdateEntityStatus($membership->getEntity(), $membership, false);
            } catch (EnoriaException $e) {
            }

            if ($updated) {
                $session = $this->requestStack->getCurrentRequest()?->getSession();
                if ($session instanceof FlashBagAwareSessionInterface) {
                    $session->getFlashBag()->add(
                        'success',
                        'Le statut de l’entité '.$membership->getEntity()->getName().' ('.$membership->getEntity()->getInstance()->getName().') a été mis à jour'
                    );
                }
            }
        }
    }
}
