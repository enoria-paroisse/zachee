<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Mailer;

use App\Entity\CaMember;
use App\Entity\Organization;
use App\Entity\Person;
use App\Entity\TypeAssociatedPerson;
use App\Entity\User;
use Scheb\TwoFactorBundle\Model\Email\TwoFactorInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

class Mailer
{
    public function __construct(
        protected MailerInterface $mailer,
        protected UrlGeneratorInterface $router,
        protected Environment $templating,
        protected ParameterBagInterface $parameters
    ) {
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendCaMemberPublicLink(CaMember $caMember, bool $renew = false): void
    {
        $person = $caMember->getPerson();

        if ($person && $person->getEmail()) {
            $message = (new TemplatedEmail())
                ->subject('[Association Enoria] Saisie des informations pour la déclaration en préfecture')
                ->from($this->getFromSecretariat())
                ->to($person->getEmail())
                ->replyTo(new Address('secretariat@association-enoria.org'))
                ->htmlTemplate('emails/link_ca-member_public.html.twig')
                ->textTemplate('emails/link_ca-member_public.txt.twig')
                ->context([
                    'caMember' => $caMember,
                ])
            ;
            $message->getHeaders()
                ->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply')
            ;

            $this->mailer->send($message);
        }
    }

    protected function getFrom(): Address
    {
        return new Address($this->parameters->get('app.from_email'), $this->parameters->get('app.name'));
    }

    protected function getFromTreasurer(): Address
    {
        return new Address($this->parameters->get('app.treasurer.from'), $this->parameters->get('app.treasurer.name'));
    }

    protected function getFromSecretariat(): Address
    {
        return new Address($this->parameters->get('app.secretariat.from'), $this->parameters->get('app.secretariat.name'));
    }

    /**
     * @param TypeAssociatedPerson[] $types
     *
     * @return array{toList: Address[], toNames: string[]}
     */
    protected function getToFromPersonOrOrganization(?Person $person = null, ?Organization $organization = null, array $types = []): array
    {
        $toList = [];
        $toNames = [];
        if ($person && $person->getEmail()) {
            $toList[] = $this->getAddressFromPerson($person);
            $toNames[] = $person->getFullname();
        } elseif ($organization) {
            $people = $organization->getAssociatedPeople();
            foreach ($people as $assPerson) {
                if ($assPerson->getPerson()->getEmail() && (0 === count($types) || in_array($assPerson->getType(), $types, true))) {
                    $toList[] = $this->getAddressFromPerson($assPerson->getPerson());
                    $toNames[] = $assPerson->getPerson()->getFullname();
                }
            }
        }

        return [
            'toList' => $toList,
            'toNames' => $toNames,
        ];
    }

    protected function getAddressFromUser(TwoFactorInterface|User $user): Address
    {
        // @phpstan-ignore method.notFound
        return $this->getAddressFromPerson($user->getPerson());
    }

    protected function getAddressFromPerson(Person $person): Address
    {
        return new Address($person->getEmail() ?: $this->parameters->get('app.from_email'), $person->getFullname());
    }

    /**
     * @param Address[] $recipients
     *
     * @return Address[]
     */
    protected function getAssociatedPersonAddress(mixed $associatedPerson, array $recipients): array
    {
        if (!empty($associatedPerson->getPerson()->getEmail())
            && ('Comptable' === $associatedPerson->getType()->getName() || 'Responsable' === $associatedPerson->getType()->getName() || 'Représentant' === $associatedPerson->getType()->getName() || 'Secrétaire' === $associatedPerson->getType()->getName())) {
            $recipients[] = new Address($associatedPerson->getPerson()->getEmail(), $associatedPerson->getPerson()->getFullname(false));
        }

        return $recipients;
    }
}
