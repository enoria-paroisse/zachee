<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Mailer;

use App\Entity\GeneralMeeting;
use App\Entity\Summons;
use App\Entity\TypeAssociatedPerson;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mime\Address;

class GeneralMeetingMailer extends Mailer
{
    /**
     * @param TypeAssociatedPerson[] $types
     *
     * @throws TransportExceptionInterface
     */
    public function sendSummons(Summons $summons, array $types): void
    {
        $getTo = $this->getTo($summons, $types);
        $toList = $getTo['toList'];
        $toNames = $getTo['toNames'];
        foreach ($toList as $to) {
            $message = (new TemplatedEmail())
                ->subject(
                    '[Association Enoria] Convocation à l’Assemblée Générale '
                    .(GeneralMeeting::TYPE_ORDINARY === $summons->getGeneralMeeting()?->getType() ? 'Ordinaire' : 'Extraordinaire')
                    .' du '.$summons->getGeneralMeeting()?->getDatetime()?->format('d/m/Y')
                )
                ->from($this->getFromSecretariat())
                ->to($to)
                ->replyTo($this->getFromSecretariat())
                ->htmlTemplate('emails/general_meeting/summons.html.twig')
                ->textTemplate('emails/general_meeting/summons.txt.twig')
                ->context([
                    'general_meeting' => $summons->getGeneralMeeting(),
                    'summons' => $summons,
                    'toList' => $toNames,
                    'domain' => $this->parameters->get('app.base_url'),
                ])
            ;
            $message->getHeaders()
                ->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply')
            ;

            $this->mailer->send($message);
        }
    }

    /**
     * @param TypeAssociatedPerson[] $types
     *
     * @throws TransportExceptionInterface
     */
    public function sendVideo(Summons $summons, array $types): void
    {
        $getTo = $this->getTo($summons, $types);
        $toList = $getTo['toList'];
        $toNames = $getTo['toNames'];
        foreach ($toList as $to) {
            $message = (new TemplatedEmail())
                ->subject(
                    '[Association Enoria] Lien vers la visio de l’Assemblée Générale '
                    .(GeneralMeeting::TYPE_ORDINARY === $summons->getGeneralMeeting()?->getType() ? 'Ordinaire' : 'Extraordinaire')
                    .' du '.$summons->getGeneralMeeting()?->getDatetime()?->format('d/m/Y')
                )
                ->from($this->getFromSecretariat())
                ->to($to)
                ->replyTo($this->getFromSecretariat())
                ->htmlTemplate('emails/general_meeting/video.html.twig')
                ->textTemplate('emails/general_meeting/video.txt.twig')
                ->context([
                    'general_meeting' => $summons->getGeneralMeeting(),
                    'summons' => $summons,
                    'toList' => $toNames,
                    'domain' => $this->parameters->get('app.base_url'),
                ])
            ;
            $message->getHeaders()
                ->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply')
            ;

            $this->mailer->send($message);
        }
    }

    /**
     * @param TypeAssociatedPerson[] $types
     *
     * @throws TransportExceptionInterface
     */
    public function sendReport(Summons $summons, array $types): void
    {
        $getTo = $this->getTo($summons, $types);
        $toList = $getTo['toList'];
        $toNames = $getTo['toNames'];
        foreach ($toList as $to) {
            $message = (new TemplatedEmail())
                ->subject(
                    '[Association Enoria] Compte-rendu de l’Assemblée Générale '
                    .(GeneralMeeting::TYPE_ORDINARY === $summons->getGeneralMeeting()?->getType() ? 'Ordinaire' : 'Extraordinaire')
                    .' du '.$summons->getGeneralMeeting()?->getDatetime()?->format('d/m/Y')
                )
                ->from($this->getFromSecretariat())
                ->to($to)
                ->replyTo($this->getFromSecretariat())
                ->htmlTemplate('emails/general_meeting/report.html.twig')
                ->textTemplate('emails/general_meeting/report.txt.twig')
                ->context([
                    'general_meeting' => $summons->getGeneralMeeting(),
                    'summons' => $summons,
                    'toList' => $toNames,
                    'domain' => $this->parameters->get('app.base_url'),
                ])
            ;
            $message->getHeaders()
                ->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply')
            ;

            $this->mailer->send($message);
        }
    }

    /**
     * @param TypeAssociatedPerson[] $types
     *
     * @return array{toList: Address[], toNames: string[]}
     */
    protected function getTo(Summons $summons, array $types): array
    {
        $person = $summons->getMembership()?->getPerson();
        $organization = $summons->getMembership()?->getOrganization();

        $result = $this->getToFromPersonOrOrganization($person, $organization, $types);

        return [
            'toList' => $result['toList'],
            'toNames' => $result['toNames'],
        ];
    }
}
