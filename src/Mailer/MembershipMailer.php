<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Mailer;

use App\Entity\Membership;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mime\Address;

class MembershipMailer extends Mailer
{
    /**
     * @throws TransportExceptionInterface
     */
    public function sendMembershipPublicLink(Membership $membership, bool $renew = false): void
    {
        $getTo = $this->getTo($membership);
        $toList = $getTo['toList'];
        $toNames = $getTo['toNames'];

        foreach ($toList as $to) {
            $message = (new TemplatedEmail())
                ->subject('[Association Enoria] '.(!$renew ? 'Suivi du processus d’adhésion' : 'Appel de cotisation et réadhésion').' pour '.$membership->getPeriod())
                ->from($this->getFromSecretariat())
                ->to($to)
                ->replyTo(new Address('secretariat@association-enoria.org'))
                ->htmlTemplate(!$renew ? 'emails/link_membership_public.html.twig' : 'emails/renew_membership.html.twig')
                ->textTemplate(!$renew ? 'emails/link_membership_public.txt.twig' : 'emails/renew_membership.txt.twig')
                ->context([
                    'membership' => $membership,
                    'toList' => $toNames,
                ])
            ;
            $message->getHeaders()
                ->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply')
            ;

            $this->mailer->send($message);
        }
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendMembershipRemind(Membership $membership): void
    {
        $getTo = $this->getTo($membership);
        $toList = $getTo['toList'];
        $toNames = $getTo['toNames'];

        foreach ($toList as $to) {
            $message = (new TemplatedEmail())
                ->subject('[Association Enoria] On n’attend plus que vous pour compléter votre adhésion')
                ->from($this->getFromSecretariat())
                ->to($to)
                ->replyTo(new Address('secretariat@association-enoria.org'))
                ->htmlTemplate('emails/membership_remind.html.twig')
                ->textTemplate('emails/membership_remind.txt.twig')
                ->context([
                    'membership' => $membership,
                    'toList' => $toNames,
                    'domain' => $this->parameters->get('app.base_url'),
                ])
            ;
            $message->getHeaders()
                ->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply')
            ;

            $this->mailer->send($message);
        }
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendFirstMembershipValidate(Membership $membership): void
    {
        if (true === $membership->getFirst() && null !== $membership->getOrganization()) {
            $getTo = $this->getTo($membership);
            $toList = $getTo['toList'];
            $toNames = $getTo['toNames'];

            $isDiocese = $membership->getOrganization()->getDiocese() && $membership->getOrganization() === $membership->getOrganization()->getDiocese()->getAssociatedOrganization();

            foreach ($toList as $to) {
                $message = (new TemplatedEmail())
                    ->subject('[Association Enoria] Bienvenue dans l’Association Enoria !')
                    ->from($this->getFromSecretariat())
                    ->to($to)
                    ->replyTo(new Address('secretariat@association-enoria.org'))
                    ->htmlTemplate('emails/membership/new_validate'.($isDiocese ? '_diocese' : '').'.html.twig')
                    ->textTemplate('emails/membership/new_validate'.($isDiocese ? '_diocese' : '').'.txt.twig')
                    ->context([
                        'membership' => $membership,
                        'toList' => $toNames,
                        'domain' => $this->parameters->get('app.base_url'),
                    ])
                ;
                $message->getHeaders()
                    ->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply')
                ;

                $this->mailer->send($message);
            }
        }
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendHelpRequestAccepted(Membership $membership): void
    {
        if (true === $membership->getHelpRequest() && Membership::HELP_ACCEPTED === $membership->getHelpStatus()) {
            $getTo = $this->getTo($membership);
            $toList = $getTo['toList'];
            $toNames = $getTo['toNames'];

            foreach ($toList as $to) {
                $message = (new TemplatedEmail())
                    ->subject('[Association Enoria] Demande de réduction de cotisation')
                    ->from($this->getFromSecretariat())
                    ->to($to)
                    ->replyTo(new Address('secretariat@association-enoria.org'))
                    ->htmlTemplate('emails/membership/help_request_accepted.html.twig')
                    ->textTemplate('emails/membership/help_request_accepted.txt.twig')
                    ->context([
                        'membership' => $membership,
                        'toList' => $toNames,
                        'domain' => $this->parameters->get('app.base_url'),
                    ])
                ;
                $message->getHeaders()
                    ->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply')
                ;

                $this->mailer->send($message);
            }
        }
    }

    /**
     * @return array{toList: Address[], toNames: string[]}
     */
    protected function getTo(Membership $membership): array
    {
        $person = $membership->getPerson();
        $organization = $membership->getOrganization();

        $result = $this->getToFromPersonOrOrganization($person, $organization);

        return [
            'toList' => array_merge([$this->getFrom()], $result['toList']),
            'toNames' => $result['toNames'],
        ];
    }
}
