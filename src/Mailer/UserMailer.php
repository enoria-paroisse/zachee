<?php

/**
 * @copyright Copyright (c) 2025 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Mailer;

use App\Entity\Person;
use App\Entity\User;
use Scheb\TwoFactorBundle\Mailer\AuthCodeMailerInterface;
use Scheb\TwoFactorBundle\Model\Email\TwoFactorInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserMailer extends Mailer implements AuthCodeMailerInterface
{
    /**
     * @throws TransportExceptionInterface
     */
    public function sendRegistration(User $user): void
    {
        $url = $this->router->generate(
            'users.activate',
            [
                'token' => $user->getToken(),
            ],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
        $message = (new TemplatedEmail())
            ->subject('[Association Enoria] Bienvenue '.$user->getPerson()->getFullname())
            ->from($this->getFrom())
            ->to($this->getAddressFromUser($user))
            ->htmlTemplate('emails/security/activation.html.twig')
            ->textTemplate('emails/security/activation.txt.twig')
            ->context([
                'user' => $user,
                'confirmation_url' => $url,
            ])
        ;
        $message->getHeaders()
            // this header tells auto-repliers ("email holiday mode") to not
            // reply to this message because it's an automated email
            ->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply')
        ;

        $this->mailer->send($message);
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendResetPassword(User $user): void
    {
        $message = (new TemplatedEmail())
            ->subject('[Association Enoria] Réinitialisation de votre mot de passe')
            ->from($this->getFrom())
            ->to($this->getAddressFromUser($user))
            ->htmlTemplate('emails/security/reset_password.html.twig')
            ->textTemplate('emails/security/reset_password.txt.twig')
            ->context([
                'name' => $user->getUserIdentifier(),
                'user' => $user,
                'person' => $user->getPerson(),
                'token' => $user->getToken(),
            ])
        ;
        $message->getHeaders()
            ->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply')
        ;

        $this->mailer->send($message);
    }

    /**
     * For 2FA.
     *
     * @throws TransportExceptionInterface
     */
    public function sendAuthCode(TwoFactorInterface $user): void
    {
        $authCode = $user->getEmailAuthCode();
        $message = new TemplatedEmail()
            ->subject('[Association Enoria] Votre code à usage unique')
            ->from($this->getFrom())
            ->to($this->getAddressFromUser($user))
            ->htmlTemplate('emails/security/2FAtoken.html.twig')
            ->textTemplate('emails/security/2FAtoken.txt.twig')
            ->context([
                'user' => $user,
                'authCode' => $authCode,
            ])
        ;
        $message->getHeaders()
            ->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply')
        ;

        $this->mailer->send($message);
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendCode(Person $person, int $code): void
    {
        $message = new TemplatedEmail()
            ->subject('[Association Enoria] Votre code d’authentification')
            ->from($this->getFrom())
            ->to($this->getAddressFromPerson($person))
            ->replyTo($this->getFrom())
            ->htmlTemplate('emails/security/code.html.twig')
            ->textTemplate('emails/security/code.txt.twig')
            ->context([
                'person' => $person,
                'code' => $code,
                'domain' => $this->parameters->get('app.base_url'),
            ])
        ;
        $message->getHeaders()
            ->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply')
        ;

        $this->mailer->send($message);
    }
}
