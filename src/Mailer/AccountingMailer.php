<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Mailer;

use App\Entity\Organization;
use App\Entity\PaymentRequestFile;
use App\Entity\Receipt;
use App\Services\PaymentRequestService;
use App\Services\ReceiptService;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mime\Address;

class AccountingMailer extends Mailer
{
    /**
     * @throws ReceiptException
     * @throws TransportExceptionInterface
     */
    public function sendReceipt(Receipt $receipt, ReceiptService $receiptService, bool $update = false): void
    {
        $recipients = [];

        if ($receipt->getfile() && $receipt->getPayment()) {
            if ($receipt->getPayment()->getPerson()) {
                $recipients[] = new Address($receipt->getPayment()->getPerson()->getEmail() ?? '', $receipt->getPayment()->getPerson()->getFullname());
            } elseif ($receipt->getPayment()->getOrganization()) {
                foreach ($receipt->getPayment()->getOrganization()->getAssociatedPeople() as $associatedPerson) {
                    $recipients = $this->getAssociatedPersonAddress($associatedPerson, $recipients);
                }
            }

            foreach ($receipt->getPayment()->getPaymentAllocations() as $paymentAllocation) {
                if ($paymentAllocation->getPaymentRequest()) {
                    if ($paymentAllocation->getPaymentRequest()->getPerson()) {
                        $recipients[] = new Address($paymentAllocation->getPaymentRequest()->getPerson()->getEmail() ?? '', $paymentAllocation->getPaymentRequest()->getPerson()->getFullname());
                    } elseif ($paymentAllocation->getPaymentRequest()->getOrganization()) {
                        foreach ($paymentAllocation->getPaymentRequest()->getOrganization()->getAssociatedPeople() as $associatedPerson) {
                            $recipients = $this->getAssociatedPersonAddress($associatedPerson, $recipients);
                        }
                    }
                }
            }
        }

        if (count($recipients)) {
            $receiptFileContent = file_get_contents($receiptService->getTargetDirectory().'/'.$receipt->getFile());

            if ($receiptFileContent) {
                $message = (new TemplatedEmail())
                    ->subject('[Association Enoria] Reçu n°'.$receipt->getRef().($update ? ' Annule et remplace' : ''))
                    ->from($this->getFrom())
                    ->attach($receiptFileContent, 'reçu_'.$receipt->getRef().'.pdf', 'application/pdf')
                    ->htmlTemplate('emails/accounting/receipt.html.twig')
                    ->textTemplate('emails/accounting/receipt.txt.twig')
                    ->context([
                        'receipt' => $receipt,
                        'update' => $update,
                        'toList' => array_map(static function (Address $address) {
                            return $address->getName();
                        }, $recipients),
                    ])
                ;

                foreach ($recipients as $recipient) {
                    $message->addTo($recipient);
                }

                $this->mailer->send($message);
            } else {
                throw new ReceiptException();
            }
        }
    }

    /**
     * @throws TransportExceptionInterface
     * @throws PaymentRequestException
     */
    public function sendPaymentRequest(PaymentRequestFile $file, PaymentRequestService $paymentRequestService, bool $update = false): void
    {
        $recipients = $this->getToFromPaymentRequestFile($file);

        if (count($recipients)) {
            $isRefund = $file->getPaymentRequest()?->getAmount() < 0;

            $fileContent = file_get_contents($paymentRequestService->getTargetDirectory().'/'.$file->getFile());
            $rib = file_get_contents($paymentRequestService->getRibFile());

            if ($fileContent) {
                $message = (new TemplatedEmail())
                    ->subject(
                        '[Association Enoria] '
                        .($isRefund ? 'Remboursement' : 'Demande de paiement')
                        .' '.$file->getRef().($update ? ' Annule et remplace' : '')
                    )
                    ->from($this->getFrom())
                    ->attach($fileContent, $file->getRef().'.pdf', 'application/pdf')
                    ->htmlTemplate('emails/accounting/payment_request.html.twig')
                    ->textTemplate('emails/accounting/payment_request.txt.twig')
                    ->context([
                        'file' => $file,
                        'update' => $update,
                        'isRefund' => $isRefund,
                        'toList' => array_map(static function (Address $address) {
                            return $address->getName();
                        }, $recipients),
                    ])
                ;

                if (false !== $rib && !$isRefund) {
                    $message->attach($rib, 'RIB.pdf', 'application/pdf');
                }

                foreach ($recipients as $recipient) {
                    $message->addTo($recipient);
                }

                $this->mailer->send($message);
            } else {
                throw new PaymentRequestException();
            }
        }
    }

    /**
     * @throws TransportExceptionInterface
     * @throws PaymentRequestException
     */
    public function sendPaymentRequestRemind(PaymentRequestFile $file, PaymentRequestService $paymentRequestService): void
    {
        $recipients = $this->getToFromPaymentRequestFile($file);

        if (count($recipients)) {
            $fileContent = file_get_contents($paymentRequestService->getTargetDirectory().'/'.$file->getFile());
            $rib = file_get_contents($paymentRequestService->getRibFile());

            if ($fileContent) {
                $message = (new TemplatedEmail())
                    ->subject('[Association Enoria] Relance - Demande de paiement '.$file->getRef())
                    ->from($this->getFrom())
                    ->attach($fileContent, $file->getRef().'.pdf', 'application/pdf')
                    ->htmlTemplate('emails/accounting/payment_request_remind.html.twig')
                    ->textTemplate('emails/accounting/payment_request_remind.txt.twig')
                    ->context([
                        'file' => $file,
                        'toList' => array_map(static function (Address $address) {
                            return $address->getName();
                        }, $recipients),
                        'domain' => $this->parameters->get('app.base_url'),
                    ])
                ;

                if (false !== $rib) {
                    $message->attach($rib, 'RIB.pdf', 'application/pdf');
                }

                foreach ($recipients as $recipient) {
                    $message->addTo($recipient);
                }

                $this->mailer->send($message);
            } else {
                throw new PaymentRequestException();
            }
        }
    }

    protected function getFrom(): Address
    {
        return $this->getFromTreasurer();
    }

    /**
     * @return Address[]
     */
    protected function getToFromPaymentRequestFile(PaymentRequestFile $file): array
    {
        $recipients = [];

        if ($file->getfile() && $file->getPaymentRequest()) {
            if ($file->getPaymentRequest()->getPerson()) {
                $recipients[] = new Address($file->getPaymentRequest()->getPerson()->getEmail() ?? '', $file->getPaymentRequest()->getPerson()->getFullname());
            } else {
                /** @var null|Organization $organization */
                $organization = null;
                if ($file->getPaymentRequest()->getOrganization()) {
                    $organization = $file->getPaymentRequest()->getOrganization();
                } elseif ($file->getPaymentRequest()->getMembership()) {
                    if ($file->getPaymentRequest()->getMembership()->getOrganization()) {
                        $organization = $file->getPaymentRequest()->getMembership()->getOrganization();
                    } elseif ($file->getPaymentRequest()->getMembership()->getPerson()) {
                        $recipients[] = new Address(
                            $file->getPaymentRequest()->getMembership()->getPerson()->getEmail() ?? '',
                            $file->getPaymentRequest()->getMembership()->getPerson()->getFullname()
                        );
                    }
                } elseif ($file->getPaymentRequest()->getDioceseMembershipSupplement()
                    && $file->getPaymentRequest()->getDioceseMembershipSupplement()->getDiocese()
                    && $file->getPaymentRequest()->getDioceseMembershipSupplement()->getDiocese()->getAssociatedOrganization()
                ) {
                    $organization = $file->getPaymentRequest()->getDioceseMembershipSupplement()->getDiocese()->getAssociatedOrganization();
                }
                if (null !== $organization) {
                    foreach ($organization->getAssociatedPeople() as $associatedPerson) {
                        $recipients = $this->getAssociatedPersonAddress($associatedPerson, $recipients);
                    }
                }
            }

            foreach ($file->getPaymentRequest()->getPaymentAllocations() as $paymentAllocation) {
                if ($paymentAllocation->getPaymentRequest()) {
                    if ($paymentAllocation->getPaymentRequest()->getPerson()) {
                        $recipients[] = new Address($paymentAllocation->getPaymentRequest()->getPerson()->getEmail() ?? '', $paymentAllocation->getPaymentRequest()->getPerson()->getFullname());
                    } elseif ($paymentAllocation->getPaymentRequest()->getOrganization()) {
                        foreach ($paymentAllocation->getPaymentRequest()->getOrganization()->getAssociatedPeople() as $associatedPerson) {
                            $recipients = $this->getAssociatedPersonAddress($associatedPerson, $recipients);
                        }
                    }
                }
            }
        }

        return $recipients;
    }
}
