<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace App\Controller\Traits;

use App\Entity\Period;
use App\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;

trait UserableTrait
{
    /**
     * @return null|object|UserInterface
     */
    abstract protected function getUser();

    protected function getAppUser(): User
    {
        $user = $this->getUser();

        if (!$user instanceof User) {
            throw new \RuntimeException('Invalid user entity');
        }

        return $user;
    }

    protected function getActivePeriod(): Period
    {
        $period = $this->getAppUser()->getPeriod();

        if (!$period instanceof Period) {
            throw new \RuntimeException('Invalid period entity');
        }

        return $period;
    }
}
