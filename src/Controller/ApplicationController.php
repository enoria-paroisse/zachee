<?php

/**
 * @copyright Copyright (c) 2025 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Application;
use App\Entity\ApplicationUser;
use App\Form\ApplicationType;
use App\Form\ApplicationUserType;
use App\Repository\ApplicationRepository;
use App\Services\UploaderService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/application')]
final class ApplicationController extends AbstractController
{
    #[IsGranted('ROLE_APPLICATION_SHOW')]
    #[Route(name: 'application.index', methods: ['GET'])]
    public function index(ApplicationRepository $applicationRepository): Response
    {
        return $this->render('application/index.html.twig', [
            'applications' => $applicationRepository->findAll(),
        ]);
    }

    #[IsGranted('ROLE_APPLICATION_EDIT')]
    #[Route('/new', name: 'application.new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager, UploaderService $uploaderService): Response
    {
        $application = new Application();
        $form = $this->createForm(ApplicationType::class, $application);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $icon = $form->get('icon')->getData();
            if (is_uploaded_file((string) $icon)) {
                $filename = $uploaderService->upload($icon, 'application-icons');
                $application->setIcon($filename);
            }
            $entityManager->persist($application);
            $entityManager->flush();
            $this->addFlash('success', 'L’application a bien été ajouté');

            return $this->redirectToRoute('application.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('application/new.html.twig', [
            'application' => $application,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_APPLICATION_SHOW')]
    #[Route('/{id:application}', name: 'application.show', methods: ['GET'])]
    public function show(Application $application): Response
    {
        return $this->render('application/show.html.twig', [
            'application' => $application,
        ]);
    }

    #[IsGranted('ROLE_APPLICATION_EDIT')]
    #[Route('/{id:application}/edit', name: 'application.edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Application $application, EntityManagerInterface $entityManager, UploaderService $uploaderService): Response
    {
        $form = $this->createForm(ApplicationType::class, $application);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $icon = $form->get('icon')->getData();
            if (is_uploaded_file((string) $icon)) {
                $filename = $uploaderService->upload($icon, 'application-icons');
                if ($application->getIcon()) {
                    $uploaderService->delete('application-icons', $application->getIcon());
                }
                $application->setIcon($filename);
            }
            $entityManager->flush();
            $this->addFlash('success', 'L’application a bien été modifiée');

            return $this->redirectToRoute('application.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('application/edit.html.twig', [
            'application' => $application,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_APPLICATION_EDIT')]
    #[Route('/icon/{id:application}', name: 'application.icon.delete', methods: ['DELETE'])]
    public function deleteIcon(Request $request, Application $application, EntityManagerInterface $entityManager, UploaderService $uploaderService): Response
    {
        if ($application->getIcon() && $this->isCsrfTokenValid('delete'.$application->getId(), $request->getPayload()->getString('_token'))) {
            $uploaderService->delete('application-icons', $application->getIcon());
            $application->setIcon(null);
            $entityManager->persist($application);
            $entityManager->flush();
            $this->addFlash('success', 'L’icône a bien été supprimée');
        }

        return $this->redirectToRoute('application.index', [], Response::HTTP_SEE_OTHER);
    }

    #[IsGranted('ROLE_APPLICATION_EDIT')]
    #[Route('/{id:application}', name: 'application.delete', methods: ['DELETE'])]
    public function delete(Request $request, Application $application, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$application->getId(), $request->getPayload()->getString('_token'))) {
            $entityManager->remove($application);
            $entityManager->flush();
            $this->addFlash('success', 'L’application a bien été supprimée');
        }

        return $this->redirectToRoute('application.index', [], Response::HTTP_SEE_OTHER);
    }

    #[IsGranted('ROLE_APPLICATION_EDIT')]
    #[Route('/{id:application}/user/new', name: 'application.user.new', methods: ['GET', 'POST'])]
    public function newUser(Application $application, Request $request, EntityManagerInterface $entityManager): Response
    {
        $applicationUser = new ApplicationUser();
        $applicationUser->setApplication($application);
        $form = $this->createForm(ApplicationUserType::class, $applicationUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($applicationUser);
            $entityManager->flush();
            $this->addFlash('success', 'Un utilisateur a bien été associé à l’application');

            return $this->redirectToRoute(
                'application.show',
                ['id' => $applicationUser->getApplication()->getId()],
                Response::HTTP_SEE_OTHER
            );
        }

        return $this->render('application/user/new.html.twig', [
            'referer' => $request->headers->get('referer'),
            'application' => $application,
            'application_user' => $applicationUser,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_APPLICATION_EDIT')]
    #[Route('/user/{id:applicationUser}/edit', name: 'application.user.edit', methods: ['GET', 'POST'])]
    public function editUser(Request $request, ApplicationUser $applicationUser, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ApplicationUserType::class, $applicationUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            $this->addFlash('success', 'Un utilisateur, associé à l’application, a bien été ajouté');

            return $this->redirectToRoute(
                'application.show',
                ['id' => $applicationUser->getApplication()->getId()],
                Response::HTTP_SEE_OTHER
            );
        }

        return $this->render('application/user/edit.html.twig', [
            'referer' => $request->headers->get('referer'),
            'application' => $applicationUser->getApplication(),
            'application_user' => $applicationUser,
            'form' => $form,
        ]);
    }

    #[IsGranted('ROLE_APPLICATION_EDIT')]
    #[Route('/user/{id:applicationUser}', name: 'application.user.delete', methods: ['DELETE'])]
    public function deleteUser(Request $request, ApplicationUser $applicationUser, EntityManagerInterface $entityManager): Response
    {
        $application = $applicationUser->getApplication();
        if ($this->isCsrfTokenValid('delete'.$applicationUser->getId(), $request->getPayload()->getString('_token'))) {
            $entityManager->remove($applicationUser);
            $entityManager->flush();
            $this->addFlash('success', 'Un utilisateur, associé à l’application, a bien été supprimé');
        }

        return $this->redirectToRoute('application.show', ['id' => $application->getId()], Response::HTTP_SEE_OTHER);
    }
}
