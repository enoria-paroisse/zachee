<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Controller\Traits\UserableTrait;
use App\Entity\Diocese;
use App\Entity\Wasselynck;
use App\Form\DioceseType;
use App\Repository\DioceseRepository;
use App\Services\MembershipFeeService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class DioceseController extends AbstractController
{
    use UserableTrait;

    public function __construct(private readonly DioceseRepository $dioceseRepository, private readonly EntityManagerInterface $em, private readonly MembershipFeeService $membershipFeeService)
    {
    }

    #[Route('diocese/', name: 'diocese.index', methods: ['GET'])]
    #[IsGranted('ROLE_BASIC_SHOW')]
    public function index(): Response
    {
        $dioceses = $this->dioceseRepository->findAllWithOrganizationsByPeriod($this->getActivePeriod());
        $dioceseData = [];
        foreach ($dioceses as $diocese) {
            $dioceseAssociatedMembershipData = $this->membershipFeeService->getDioceseAssociatedMembershipData($diocese, $this->getActivePeriod())['data'];
            $dioceseData[$diocese->getId()] = $dioceseAssociatedMembershipData;
        }

        return $this->render('diocese/index.html.twig', [
            'dioceses' => $dioceses,
            'dioceses_data' => $dioceseData,
        ]);
    }

    #[Route('diocese/show/{id}', name: 'diocese.show', methods: ['GET'])]
    #[IsGranted('basic_show', subject: 'diocese')]
    public function show(
        #[MapEntity(expr: 'repository.findByIdFullJoin(id)')]
        Diocese $diocese,
    ): Response {
        $actualMembership = null;
        $associatedOrganization = $diocese->getAssociatedOrganization();
        if ($associatedOrganization) {
            $memberships = $associatedOrganization->getMemberships();
            if (count($memberships)) {
                foreach ($memberships as $membership) {
                    if ($membership->getPeriod() === $this->getActivePeriod()) {
                        $actualMembership = $membership;
                    }
                }
            }
        }

        $actualAssociatedMemberships = $this->membershipFeeService->getDioceseAssociatedMembershipData($diocese, $this->getActivePeriod());

        return $this->render('diocese/show.html.twig', [
            'diocese' => $diocese,
            'actualMembership' => $actualMembership,
            'actualAssociatedMemberships' => $actualAssociatedMemberships['data'],
            'actualAssociatedMembershipsResult' => $actualAssociatedMemberships['results'],
            'wasselynck' => $actualAssociatedMemberships['wasselynck'],
            'membershipSupplement' => $actualAssociatedMemberships['membershipSupplement'],
        ]);
    }

    #[Route('diocese/create', name: 'diocese.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_DIOCESE_EDIT')]
    public function new(Request $request): Response
    {
        $wasselynck = new Wasselynck();
        $wasselynck->setPeriod($this->getActivePeriod())
            ->setIndice(0)
        ;
        $diocese = new Diocese();
        $diocese->addWasselynck($wasselynck);
        $form = $this->createForm(DioceseType::class, $diocese);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($diocese);
            $this->em->flush();
            $this->addFlash('success', 'Le diocèse a bien été ajouté');

            return $this->redirectToRoute('diocese.index');
        }

        return $this->render('diocese/new.html.twig', [
            'diocese' => $diocese,
            'form' => $form->createView(),
            'referer' => $request->headers->get('referer'),
        ]);
    }

    #[Route('diocese/edit/{id}', name: 'diocese.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_DIOCESE_EDIT')]
    public function edit(int $id, Request $request): Response
    {
        $diocese = $this->dioceseRepository->findOneWithCurrentPeriod($id, $this->getActivePeriod());
        if (null === $diocese) {
            throw $this->createNotFoundException();
        }
        if (0 === count($diocese->getWasselynck())) {
            $wasselynck = new Wasselynck();
            $wasselynck->setPeriod($this->getActivePeriod())
                ->setIndice(0)
            ;
            $diocese->addWasselynck($wasselynck);
        }

        $form = $this->createForm(DioceseType::class, $diocese);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('success', 'Le diocèse a bien été modifié');

            return $this->redirectToRoute('diocese.show', ['id' => $diocese->getId()]);
        }

        return $this->render('diocese/edit.html.twig', [
            'diocese' => $diocese,
            'form' => $form->createView(),
            'referer' => $request->headers->get('referer'),
        ]);
    }

    #[Route('diocese/delete/{id:diocese}', name: 'diocese.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_DIOCESE_EDIT')]
    public function delete(Diocese $diocese, Request $request): Response
    {
        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('delete'.$diocese->getId(), $csrfToken)) {
            if (!$diocese->getOrganizations()->count()) {
                $this->em->remove($diocese);
                $this->em->flush();
                $this->addFlash('success', 'Le diocèse a bien été supprimé');
            } else {
                $this->addFlash('error', 'Impossible de supprimer ce diocèse : des organisations lui sont encore rattachées');

                return $this->redirectToRoute('diocese.show', ['id' => $diocese->getId()]);
            }
        }

        return $this->redirectToRoute('diocese.index');
    }
}
