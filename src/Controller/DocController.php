<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Controller\Traits\UserableTrait;
use App\Entity\Doc;
use App\Entity\Folder;
use App\Form\Doc\DocType;
use App\Form\Doc\FolderType;
use App\Form\Doc\TagType;
use App\Repository\FolderRepository;
use App\Services\DocService;
use App\Services\UploaderService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/doc')]
class DocController extends AbstractController
{
    use UserableTrait;

    public function __construct(private readonly ManagerRegistry $managerRegistry)
    {
    }

    /**
     * @param string[] $forceTags
     */
    #[Route('/', name: 'doc.index', methods: ['GET', 'POST'])]
    public function index(
        Request $request,
        FolderRepository $folderRepository,
        DocService $docService,
        array $forceTags = [],
        bool $hasForm = true,
        string $icon = 'book',
        string $title = 'Base documentaire'
    ): Response {
        // Get all docs and filter with permissions (roles)
        $rootFolders = $folderRepository->findAllRoot();
        $tree = [];
        foreach ($rootFolders as $rootFolder) {
            $tree[] = $docService->makeTreeElem($rootFolder);
        }
        $tree = $docService->filterTreeByPermissions($tree);

        // Get filter $tags
        $tags = [];
        $form = null;

        if ($hasForm && $this->isGranted('ROLE_DOC_EDIT', $tags)) {
            $form = $this->createForm(TagType::class);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $tags = (array) $form->get('tags')->getData();
            }
            $form = $form->createView();
        }
        $tags = array_unique(array_merge($tags, $forceTags));

        // If needed, filter by tags
        if (0 < count($tags)) {
            $tree = $docService->filterTreeByTags($tree, $tags);
        }

        return $this->render('doc/index.html.twig', [
            'icon' => $icon,
            'title' => $title,
            'form' => $form,
            'rootTree' => $tree,
        ]);
    }

    #[Route('/help', name: 'doc.help', methods: ['GET', 'POST'])]
    public function help(
        Request $request,
        FolderRepository $folderRepository,
        DocService $docService,
    ): Response {
        return $this->index(
            $request,
            $folderRepository,
            $docService,
            ['help'],
            false,
            'question-circle',
            'Centre d’aide'
        );
    }

    #[Route('/folder/new', name: 'doc.folder.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_DOC_EDIT')]
    public function newFolder(Request $request): Response
    {
        $folder = new Folder();
        $form = $this->createForm(FolderType::class, $folder);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->persist($folder);
            $this->managerRegistry->getManager()->flush();
            $this->addFlash('success', 'Le dossier a bien été ajouté');

            return $this->redirectToRoute('doc.index');
        }

        return $this->render('doc/new_folder.html.twig', [
            'folder' => $folder,
            'form' => $form->createView(),
            'referer' => $request->headers->get('referer'),
        ]);
    }

    #[Route('/folder/edit/{id:folder}', name: 'doc.folder.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_DOC_EDIT')]
    public function editFolder(Request $request, Folder $folder): Response
    {
        $form = $this->createForm(FolderType::class, $folder);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $folder->getParents();
                $this->managerRegistry->getManager()->flush();
                $this->addFlash('success', 'Le dossier a bien été modifié');

                return $this->redirectToRoute('doc.index');
            } catch (\DomainException) {
                $this->addFlash('warning', 'En sélectionnant ce dossier parent, la branche ne sera plus attachée à la racine !');
            }
        }

        return $this->render('doc/edit_folder.html.twig', [
            'folder' => $folder,
            'form' => $form->createView(),
            'referer' => $request->headers->get('referer'),
        ]);
    }

    #[Route('/new', name: 'doc.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_DOC_EDIT')]
    public function newDoc(Request $request, UploaderService $uploaderService): Response
    {
        $doc = new Doc();
        $form = $this->createForm(DocType::class, $doc);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('file')->getData();
            if (is_uploaded_file((string) $file)) {
                $filename = $uploaderService->upload($file, 'doc');
                $doc->setFile($filename);
                $this->managerRegistry->getManager()->persist($doc);
                $this->managerRegistry->getManager()->flush();
                $this->addFlash('success', 'Le document a bien été ajouté');
            } else {
                $this->addFlash('error', 'Une erreur est survenue');
            }

            return $this->redirectToRoute('doc.index');
        }

        return $this->render('doc/new_doc.html.twig', [
            'doc' => $doc,
            'form' => $form->createView(),
            'referer' => $request->headers->get('referer'),
        ]);
    }

    #[Route('/edit/{id:doc}', name: 'doc.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_DOC_EDIT')]
    public function editDoc(Request $request, UploaderService $uploaderService, Doc $doc): Response
    {
        $form = $this->createForm(DocType::class, $doc);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('file')->getData();
            if (is_uploaded_file((string) $file)) {
                $filename = $uploaderService->upload($file, 'doc', $doc->getFile());
                $doc->setFile($filename);
                $this->addFlash('success', 'Un nouveau fichier a été associé au document');
            }

            $this->managerRegistry->getManager()->flush();
            $this->addFlash('success', 'Le document a bien été modifé');

            return $this->redirectToRoute('doc.index');
        }

        return $this->render('doc/edit_doc.html.twig', [
            'doc' => $doc,
            'form' => $form->createView(),
            'referer' => $request->headers->get('referer'),
        ]);
    }

    #[Route('/folder/delete/{id:folder}', name: 'doc.folder.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_DOC_EDIT')]
    public function deleteFolder(Folder $folder, Request $request): Response
    {
        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('delete'.$folder->getId(), $csrfToken)) {
            if (!$folder->getFolders()->count() && !$folder->getDocs()->count()) {
                $this->managerRegistry->getManager()->remove($folder);
                $this->managerRegistry->getManager()->flush();
                $this->addFlash('success', 'Le dossier a bien été supprimé');
            } else {
                $this->addFlash('error', 'Impossible de supprimer ce dossier, car il n’est pas vide');

                return $this->redirectToRoute('doc.folder.edit', ['id' => $folder->getId()]);
            }
        }

        return $this->redirectToRoute('doc.index');
    }

    #[Route('/delete/{id:doc}', name: 'doc.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_DOC_EDIT')]
    public function delete(Doc $doc, Request $request, UploaderService $uploaderService): Response
    {
        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('delete'.$doc->getId(), $csrfToken)) {
            if (0 < $doc->getGeneralMeetingsSummons()->count() || 0 < $doc->getGeneralMeetingsReport()->count()) {
                $this->addFlash('error', 'Impossible de supprimer ce fichier, car il est associé à au moins une Assemblée Générale');

                return $this->redirectToRoute('doc.edit', ['id' => $doc->getId()]);
            }

            $uploaderService->delete('doc', $doc->getFile());
            $this->managerRegistry->getManager()->remove($doc);
            $this->managerRegistry->getManager()->flush();
            $this->addFlash('success', 'Le document a bien été supprimé');
        }

        return $this->redirectToRoute('doc.index');
    }
}
