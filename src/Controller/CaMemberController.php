<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Controller\Traits\UserableTrait;
use App\Entity\CaMember;
use App\Entity\Membership;
use App\Form\CaMemberPublicType;
use App\Form\CaMemberType;
use App\Mailer\Mailer;
use App\Repository\CaMemberRepository;
use App\Repository\MembershipRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/ca-member')]
class CaMemberController extends AbstractController
{
    use UserableTrait;

    public function __construct(private readonly CaMemberRepository $caMemberRepository, private readonly EntityManagerInterface $em)
    {
    }

    #[Route('/', name: 'ca-member.index', methods: ['GET'])]
    public function index(): Response
    {
        return $this->render('ca-member/index.html.twig', [
            'membersActive' => $this->caMemberRepository->findAllFullJoin(true),
            'membersInactive' => $this->caMemberRepository->findAllFullJoin(false),
        ]);
    }

    #[Route('/add', name: 'ca-member.add', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_CA_EDIT')]
    public function add(Request $request, MembershipRepository $membershipRepository): Response
    {
        $caMember = new CaMember();
        $caMember->setNationality('Française');
        $form = $this->createForm(CaMemberType::class, $caMember);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->autoSetEnd($membershipRepository, $caMember);
            $this->em->persist($caMember);
            $this->em->flush();
            $this->addFlash('success', 'La personne a bien été ajoutée au conseil d’administration');

            return $this->redirectToRoute('ca-member.index');
        }

        return $this->render('ca-member/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/edit/{id:caMember}', name: 'ca-member.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_CA_EDIT')]
    public function edit(CaMember $caMember, Request $request): Response
    {
        $form = $this->createForm(CaMemberType::class, $caMember, ['edit' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('removeInformations')->getData()) {
                $caMember->setBirthDate(null);
                $caMember->setBirthCity(null);
                $caMember->setBirthDepartment(null);
                $caMember->setBirthCountry(null);
                $caMember->setNationality(null);
                $caMember->setProfession(null);
                $caMember->setInformationsValidated(false);
            }
            $this->em->flush();
            $this->addFlash('success', 'Le membre a bien été modifié');

            return $this->redirectToRoute('ca-member.index');
        }

        return $this->render('ca-member/edit.html.twig', [
            'caMember' => $caMember,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/remove/{id:caMember}', name: 'ca-member.remove', methods: ['DELETE'])]
    #[IsGranted('ROLE_CA_EDIT')]
    public function remove(CaMember $caMember, Request $request): Response
    {
        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('remove'.$caMember->getId(), $csrfToken)) {
            if ($caMember->isActive()) {
                $caMember->setActive(false);
            } else {
                $this->em->remove($caMember);
            }
            $this->em->flush();
            $this->addFlash('success', 'La personne a bien été retirée du conseil d’administration');
        }

        return $this->redirectToRoute('ca-member.index');
    }

    #[Route('/restore/{id:caMember}', name: 'ca-member.restore', methods: ['GET'])]
    #[IsGranted('ROLE_CA_EDIT')]
    public function restore(CaMember $caMember): Response
    {
        $caMember->setActive(true);
        $this->em->flush();
        $this->addFlash('success', 'La personne a bien été restaurée dans le conseil d’administration');

        return $this->redirectToRoute('ca-member.index');
    }

    #[Route('/renew/{id:caMember}', name: 'ca-member.renew', methods: ['GET'])]
    #[IsGranted('ROLE_CA_EDIT')]
    public function renew(CaMember $caMember, MembershipRepository $membershipRepository): Response
    {
        $newCaMember = new CaMember();
        if ($caMember->getPerson()) {
            $newCaMember->setPerson($caMember->getPerson());
        }
        $newCaMember->setRole($caMember->getRole());
        $newCaMember->setBirthDate($caMember->getBirthDate());
        $newCaMember->setBirthDate($caMember->getBirthDate());
        $newCaMember->setBirthCity($caMember->getBirthCity());
        $newCaMember->setBirthDepartment($caMember->getBirthDepartment());
        $newCaMember->setBirthCountry($caMember->getBirthCountry());
        $newCaMember->setNationality($caMember->getNationality());
        $newCaMember->setProfession($caMember->getProfession());

        $newCaMember->setActive(true);
        $caMember->setActive(false);
        $caMember->setEnd(new \DateTime());
        $newCaMember->setBegin($caMember->getEnd());
        $this->autoSetEnd($membershipRepository, $newCaMember);

        $this->em->persist($newCaMember);
        $this->em->flush();
        $this->addFlash('success', 'Le mandat au conseil d’administration a bien été renouvelé');

        return $this->redirectToRoute('ca-member.index');
    }

    #[Route('/create-public-form/{id:caMember}', name: 'ca-member.create-public-form', methods: ['GET'])]
    #[IsGranted('ROLE_CA_EDIT')]
    public function createPublicForm(CaMember $caMember, Mailer $mailer): Response
    {
        if ($caMember->isInformationsValidated()) {
            $this->addFlash('warning', 'Impossible de générer le formulaire public, car les informations ont déjà été validées');
        } elseif (!$caMember->getToken()) {
            $caMember->setToken(md5(random_bytes(20)));
            $this->em->flush();
            $this->addFlash('success', 'Le formulaire publique a été créé');
        }

        if (!$caMember->isInformationsValidated()) {
            try {
                $mailer->sendCaMemberPublicLink($caMember);
                $this->addFlash(
                    'success',
                    'Le lien public a été envoyé'
                );
            } catch (TransportExceptionInterface) {
                $this->addFlash(
                    'error',
                    'Une erreur a eu lieu lors de l’envoi du mail : ',
                );
            }
        }

        return $this->redirectToRoute('ca-member.index');
    }

    #[Route('/send-public-form/{id:caMember}', name: 'ca-member.send-public-form', methods: ['GET'])]
    #[IsGranted('ROLE_CA_EDIT')]
    public function sendPublicForm(CaMember $caMember, Mailer $mailer): Response
    {
        if ($caMember->isInformationsValidated()) {
            $this->addFlash('warning', 'Impossible d’envoyer le lien vers le formulaire public, car les informations ont déjà été validées');
        } else {
            try {
                $mailer->sendCaMemberPublicLink($caMember);
                $this->addFlash(
                    'success',
                    'Le lien public a été envoyé'
                );
            } catch (TransportExceptionInterface) {
                $this->addFlash(
                    'error',
                    'Une erreur a eu lieu lors de l’envoi du mail : ',
                );
            }
        }

        return $this->redirectToRoute('ca-member.index');
    }

    #[Route('/public-form/{token:caMember}', name: 'ca-member.public-form', methods: ['GET', 'POST'])]
    public function publicForm(CaMember $caMember, Request $request): Response
    {
        $form = $this->createForm(CaMemberPublicType::class, $caMember);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('success', 'Les informations ont bien été mis-à-jour');

            return $this->redirectToRoute('ca-member.public-form', ['token' => $caMember->getToken()]);
        }

        return $this->render('ca-member/public-form.html.twig', [
            'caMember' => $caMember,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/validate/{id:caMember}', name: 'ca-member.validate', methods: ['GET'])]
    #[IsGranted('ROLE_CA_EDIT')]
    public function validate(CaMember $caMember): Response
    {
        if ($caMember->isInformationsValidated()) {
            $this->addFlash('warning', 'Les informations ont déjà été validées');
        } else {
            $caMember->setToken(null);
            $caMember->setInformationsValidated(true);
            $this->em->flush();
            $this->addFlash('success', 'Les informations ont été validées');
        }

        return $this->redirectToRoute('ca-member.index');
    }

    private function autoSetEnd(MembershipRepository $membershipRepository, CaMember $caMember): void
    {
        $membership = $membershipRepository->findOneBy(['person' => $caMember->getPerson(), 'period' => $this->getActivePeriod()]);
        if (null !== $membership && null !== $caMember->getBegin() && !in_array($membership->getType(), [Membership::TYPE_EX_OFFICIO, Membership::TYPE_FOUNDING], true)) {
            $end = \DateTime::createFromInterface($caMember->getBegin());
            $end->add(new \DateInterval('P3Y'));
            $caMember->setEnd($end);
        }
    }
}
