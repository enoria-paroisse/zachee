<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Controller\Traits\UserableTrait;
use App\Entity\Period;
use App\Entity\SubOrganizationMembershipFeePeriod;
use App\Entity\Wasselynck;
use App\Form\PeriodType;
use App\Form\SubOrganizationMembershipFeePeriodType;
use App\Repository\PeriodRepository;
use App\Repository\SubOrganizationMembershipFeeRepository;
use App\Repository\UserRepository;
use App\Repository\WasselynckRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/period')]
class PeriodController extends AbstractController
{
    use UserableTrait;

    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function periodMenu(PeriodRepository $periodRepository): Response
    {
        return $this->render('period/_menu.html.twig', [
            'periods' => $periodRepository->findBy([], ['start' => 'asc']),
        ]);
    }

    #[Route('/add', name: 'period.add', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function add(Request $request, WasselynckRepository $wasselynckRepository, SubOrganizationMembershipFeeRepository $subOrganizationMembershipFeeRepository): Response
    {
        $period = new Period();
        $period->setStart(new \DateTime(date('Y').'-01-01'))
            ->setEnd(new \DateTime(date('Y').'-12-31'))
            ->setDioceseBaseMaxMembershipfee($this->getActivePeriod()->getDioceseBaseMaxMembershipfee())
            ->setDioceseBaseWasselynck($this->getActivePeriod()->getDioceseBaseWasselynck())
            ->setMembershipfeeLegal($this->getActivePeriod()->getMembershipfeeLegal())
            ->setMembershipfeeNatural($this->getActivePeriod()->getMembershipfeeNatural())
        ;
        foreach ($subOrganizationMembershipFeeRepository->findBy([], ['name' => 'asc']) as $subOrganizationMembershipFee) {
            $subOrganizationMembershipFeePeriod = new SubOrganizationMembershipFeePeriod();
            $subOrganizationMembershipFeePeriod->setMembershipFee('0.00')->setSubOrganizationMembershipFee($subOrganizationMembershipFee);
            $period->addSubOrganizationMembershipFeePeriod($subOrganizationMembershipFeePeriod);
        }

        $form = $this->createForm(PeriodType::class, $period);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($period);

            foreach ($wasselynckRepository->findBy(['period' => $this->getActivePeriod()]) as $previousWasselynck) {
                $wasselynck = new Wasselynck();
                $wasselynck->setPeriod($period)
                    ->setDiocese($previousWasselynck->getDiocese())
                    ->setIndice($previousWasselynck->getIndice())
                ;
                $this->em->persist($wasselynck);
            }

            $this->em->flush();
            $this->addFlash('success', 'La période a bien été ajoutée');

            return $this->redirectToRoute('config.index');
        }

        return $this->render('period/add.html.twig', [
            'form' => $form->createView(),
            'period' => $period,
        ]);
    }

    #[Route('/edit/{id:period}', name: 'period.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function edit(Period $period, Request $request): Response
    {
        $form = $this->createForm(PeriodType::class, $period);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('success', 'La période a bien été modifiée');

            return $this->redirectToRoute('config.index');
        }

        return $this->render('period/edit.html.twig', [
            'period' => $period,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/suborganizationmembershipfee/edit/{id:subOrganizationMembershipFeePeriod}', name: 'period.suborganizationmembershipfee.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function subOrganizationMembershipFeePeriodEdit(SubOrganizationMembershipFeePeriod $subOrganizationMembershipFeePeriod, Request $request): Response
    {
        $form = $this->createForm(SubOrganizationMembershipFeePeriodType::class, $subOrganizationMembershipFeePeriod);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('success', 'La cotisation a bien été modifiée');

            return $this->redirectToRoute('config.index');
        }

        return $this->render('period/suborganizationmembershipfee-edit.html.twig', [
            'period' => $subOrganizationMembershipFeePeriod->getPeriod(),
            'subOrganizationMembershipFeePeriod' => $subOrganizationMembershipFeePeriod,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/change/{id:period}', name: 'period.change', methods: ['GET'])]
    public function change(Period $period, Request $request): Response
    {
        $this->getAppUser()->setPeriod($period);
        $this->em->flush();
        $this->addFlash('warning', 'La période courante est maintenant '.$period->getName());

        return $this->redirect($request->headers->get('referer') ?: '/');
    }

    #[Route('/delete/{id:period}', name: 'period.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_ADMIN')]
    public function delete(
        Period $period,
        Request $request,
        UserRepository $userRepository,
        PeriodRepository $periodRepository
    ): Response {
        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('delete'.$period->getId(), $csrfToken)) {
            // On doit changer la période active des utilisateurs
            $users = $userRepository->findByPeriod($period);
            $currentPeriod = $periodRepository->findCurrentPeriod();

            if (null === $currentPeriod || $period === $currentPeriod) {
                $currentPeriod = $periodRepository->findOneNotBe($period);
            }

            if (null === $currentPeriod) {
                $this->addFlash('error', 'Impossible de supprimer la période : il doit y en avoir au moins une');
            } else {
                foreach ($users as $user) {
                    $user->setPeriod($currentPeriod);
                }
                // Suppression
                $this->em->remove($period);

                try {
                    $this->em->flush();
                    $this->addFlash('success', 'La période a bien été supprimée');
                } catch (ForeignKeyConstraintViolationException) {
                    $this->addFlash('error', 'Impossible de supprimer la période, car des adhésions ou des paiements y sont liés');
                }
            }
        }

        return $this->redirectToRoute('config.index');
    }
}
