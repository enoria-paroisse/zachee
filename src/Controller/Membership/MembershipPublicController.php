<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Membership;

use App\Entity\AssociatedPerson;
use App\Entity\Membership;
use App\Entity\Organization;
use App\Entity\Person;
use App\Entity\Title;
use App\Form\Membership\MembershipPublicInfoEditType;
use App\Form\Membership\MembershipPublicInfoType;
use App\Form\Membership\MembershipPublicSheetType;
use App\Form\OrganizationType;
use App\Form\PersonType;
use App\Repository\DocRepository;
use App\Repository\TitleRepository;
use App\Repository\TypeAssociatedPersonRepository;
use App\Services\OrganizationTypeTagService;
use App\Services\PdfService;
use App\Services\UploaderService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/public')]
class MembershipPublicController extends AbstractController
{
    public function __construct(private readonly ManagerRegistry $managerRegistry)
    {
    }

    #[Route('/{token:membership}', name: 'membership_public.show', methods: ['GET'])]
    public function show(
        Membership $membership,
        DocRepository $docRepository,
        OrganizationTypeTagService $organizationTypeTag,
        Request $request
    ): Response {
        $request->getSession()->set('membership_public_sheet', $membership->getSheet());
        if ($membership->getPaymentRequest()?->getPaymentAllocations()->count() > 0) {
            $receipts = [];
            foreach ($membership->getPaymentRequest()->getPaymentAllocations() as $paymentAllocation) {
                if ($paymentAllocation->getPayment()?->getReceipt()) {
                    $receipts[] = $paymentAllocation->getPayment()->getReceipt()->getFile();
                }
            }
            $request->getSession()->set('membership_public_receipts', $receipts);
        }

        $tags = [];
        if ($membership->isLegalPerson()) {
            $tags[] = 'membership_public_legal';
            if ($membership->getOrganization() && null !== $membership->getOrganization()->getType()) {
                $tags[] = $organizationTypeTag->generateTagFromType($membership->getOrganization()->getType());
            }
        } else {
            $tags[] = 'membership_public_natural';
        }

        return $this->render('membership/public/show.html.twig', [
            'membership' => $membership,
            'docs' => $docRepository->findDeepByTags($tags, false),
        ]);
    }

    #[Route('/sheet/{token:membership}', name: 'membership_public.sheet', methods: ['GET', 'POST'])]
    public function sheet(Membership $membership, Request $request, UploaderService $uploaderService): Response
    {
        if (Membership::STATUS_IN_PROGRESS !== $membership->getStatus()) {
            $this->addFlash('error', 'Impossible d’effectuer cette action sur une adhésion clôturée.');

            return $this->redirectToRoute('membership_public.show', ['token' => $membership->getToken()]);
        }

        if ($membership->getSheet()) {
            $this->addFlash('error', 'Une fiche a déjà été transmise.');

            return $this->redirectToRoute('membership_public.show', ['token' => $membership->getToken()]);
        }

        $form = $this->createForm(MembershipPublicSheetType::class, $membership);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sheet = $form->get('sheet')->getData();
            if (is_uploaded_file((string) $sheet)) {
                $filename = $uploaderService->upload($sheet, 'sheets');
                $membership->setSheet($filename, true);
                $this->managerRegistry->getManager()->flush();
                $this->addFlash('success', 'La fiche a bien été ajoutée');
            } else {
                $this->addFlash('error', 'Une erreur est survenue');
            }

            return $this->redirectToRoute('membership_public.show', ['token' => $membership->getToken()]);
        }

        return $this->render('membership/public/sheet.html.twig', [
            'membership' => $membership,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/infos/{token:membership}', name: 'membership_public.infos', methods: ['GET', 'POST'])]
    public function infos(Membership $membership, Request $request, TypeAssociatedPersonRepository $typeAssociatedPersonRepository): Response
    {
        if (!$this->infosAreEditable($membership)) {
            return $this->redirectToRoute('membership_public.show', ['token' => $membership->getToken()]);
        }

        $form = $this->createForm(MembershipPublicInfoType::class, $membership);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();
            $this->addFlash('success', 'Les informations ont été soumises à la validation');

            return $this->redirectToRoute('membership_public.show', ['token' => $membership->getToken()]);
        }

        return $this->render('membership/public/infos.html.twig', [
            'membership' => $membership,
            'form' => $form->createView(),
            'types' => $typeAssociatedPersonRepository->findAll(),
        ]);
    }

    #[Route('/edit_main/{token:membership}', name: 'membership_public.edit_main', methods: ['GET', 'POST'])]
    public function editMain(Membership $membership, Request $request): Response
    {
        if (!$this->infosAreEditable($membership)) {
            return $this->redirectToRoute('membership_public.show', ['token' => $membership->getToken()]);
        }

        if ($membership->isNaturalPerson()) {
            $form = $this->createForm(PersonType::class, $membership->getPerson());
        } else {
            $form = $this->createForm(OrganizationType::class, $membership->getOrganization());
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();
            $this->addFlash('success', 'Les informations ont été modifiées, n’oubliez pas de les valider !');

            return $this->redirectToRoute('membership_public.infos', ['token' => $membership->getToken()]);
        }

        return $this->render('membership/public/edit_main.html.twig', [
            'membership' => $membership,
            'form' => $form->createView(),
            'referer' => $request->headers->get('referer'),
        ]);
    }

    #[Route('/remove/{associated_id}/{token:membership}', name: 'membership_public.remove', methods: ['DELETE'])]
    public function remove(
        #[MapEntity(mapping: ['associated_id' => 'id'])]
        AssociatedPerson $associatedPerson,
        Membership $membership,
    ): Response {
        if (!$this->infosAreEditable($membership)) {
            return $this->redirectToRoute('membership_public.show', ['token' => $membership->getToken()]);
        }

        if ($associatedPerson->getOrganization() !== $membership->getOrganization()) {
            throw $this->createNotFoundException();
        }

        $em = $this->managerRegistry->getManager();
        $em->remove($associatedPerson);
        $em->flush();
        $this->addFlash('success', 'La personne a bien été retirée de votre organisation');

        return $this->redirectToRoute('membership_public.infos', ['token' => $membership->getToken()]);
    }

    #[Route('/add/{token:membership}', name: 'membership_public.add', methods: ['GET', 'POST'])]
    public function add(
        Membership $membership,
        Request $request,
        TitleRepository $titleRepository,
        TypeAssociatedPersonRepository $typeAssociatedPersonRepository
    ): Response {
        if (!$this->infosAreEditable($membership)) {
            return $this->redirectToRoute('membership_public.show', ['token' => $membership->getToken()]);
        }
        if (!$membership->isLegalPerson()) {
            throw $this->createNotFoundException();
        }

        /** @var Organization $organization */
        $organization = $membership->getOrganization();

        /** @var Title $title */
        $title = $titleRepository->findOneBy(['name' => 'Monsieur']);
        $associatedPerson = new AssociatedPerson();
        $person = new Person();
        $person->setFirstname('')
            ->setLastname('')
            ->setTitle($title)
        ;
        $associatedPerson->setOrganization($organization)
            ->setPerson($person)
        ;
        $form = $this->createForm(MembershipPublicInfoEditType::class, $associatedPerson);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->managerRegistry->getManager();
            $em->persist($person);
            $em->persist($associatedPerson);
            $em->flush();
            $this->addFlash('success', 'Les informations ont été modifiées, n’oubliez pas de les valider !');

            return $this->redirectToRoute('membership_public.infos', ['token' => $membership->getToken()]);
        }

        return $this->render('membership/public/add.html.twig', [
            'membership' => $membership,
            'associatedPerson' => $associatedPerson,
            'types' => $typeAssociatedPersonRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    #[Route('/edit/{associated_id}/{token:membership}', name: 'membership_public.edit', methods: ['GET', 'POST'])]
    public function edit(
        #[MapEntity(mapping: ['associated_id' => 'id'])]
        AssociatedPerson $associatedPerson,
        Membership $membership,
        Request $request,
        TypeAssociatedPersonRepository $typeAssociatedPersonRepository
    ): Response {
        if (!$this->infosAreEditable($membership)) {
            return $this->redirectToRoute('membership_public.show', ['token' => $membership->getToken()]);
        }

        if ($associatedPerson->getOrganization() !== $membership->getOrganization()) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(MembershipPublicInfoEditType::class, $associatedPerson, ['secure_name' => true, 'delete_address' => true]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (null !== $associatedPerson->getPerson()->getAddress()) {
                if (true === $form->get('person')->get('deleteAddress')->getData()) {
                    $this->managerRegistry->getManager()->remove($associatedPerson->getPerson()->getAddress());
                    $associatedPerson->getPerson()->setAddress(null);
                } else {
                    $this->managerRegistry->getManager()->persist($associatedPerson->getPerson()->getAddress());
                }
            }
            $this->managerRegistry->getManager()->flush();
            $this->addFlash('success', 'Les informations ont été modifiées, n’oubliez pas de les valider !');

            return $this->redirectToRoute('membership_public.infos', ['token' => $membership->getToken()]);
        }

        return $this->render('membership/public/edit.html.twig', [
            'membership' => $membership,
            'associatedPerson' => $associatedPerson,
            'types' => $typeAssociatedPersonRepository->findAll(),
            'form' => $form->createView(),
        ]);
    }

    #[Route('/letter/{token:membership}', name: 'membership_public.letter')]
    public function letter(Membership $membership, PdfService $pdfService): Response
    {
        $pdfService->renderHtml($this->renderView('membership/public/letter.pdf.twig', [
            'rootDirectory' => $pdfService->getRootDirectory(),
            'membership' => $membership,
        ]));

        return $pdfService->stream('letter.pdf', [
            'Attachment' => true,
        ]);
    }

    protected function infosAreEditable(Membership $membership): bool
    {
        if (Membership::STATUS_IN_PROGRESS !== $membership->getStatus()) {
            $this->addFlash('error', 'Impossible d’effectuer cette action sur une adhésion clôturée.');

            return false;
        }

        if ($membership->getInformationSubmitted()) {
            $this->addFlash('error', 'Les informations ont déjà été soumises à la validation.');

            return false;
        }

        return true;
    }
}
