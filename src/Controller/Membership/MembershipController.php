<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Membership;

use App\Controller\Traits\UserableTrait;
use App\Entity\Membership;
use App\Form\Membership\MembershipCancellationType;
use App\Form\Membership\MembershipEditType;
use App\Repository\MembershipRepository;
use App\Services\UploaderService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/membership')]
class MembershipController extends AbstractController
{
    use UserableTrait;

    public function __construct(private readonly ManagerRegistry $managerRegistry)
    {
    }

    #[Route('/', name: 'membership.index', methods: ['GET'])]
    #[IsGranted('ROLE_BASIC_SHOW')]
    public function index(MembershipRepository $membershipRepository): Response
    {
        return $this->render('membership/index.html.twig', [
            'cancel' => false,
            'legalPersons' => $membershipRepository->findLegalPersonValidatedByPeriod($this->getActivePeriod()),
            'naturalPersons' => $membershipRepository->findNaturalPersonValidatedByPeriod($this->getActivePeriod()),
        ]);
    }

    #[Route('/{id:membership}/edit', name: 'membership.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_MEMBERSHIP_EDIT')]
    public function edit(Request $request, Membership $membership, UploaderService $uploaderService): Response
    {
        if (Membership::STATUS_VALIDATED !== $membership->getStatus()) {
            $this->addFlash('error', 'Seule une adhésion validée peut être modifiée par ce formulaire');

            return $this->redirectToRoute('membership.index');
        }

        $form = $this->createForm(MembershipEditType::class, $membership);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sheet = $form->get('sheet')->getData();
            $sheetActions = (array) $form->get('sheet_actions')->getData();
            if (in_array('default', $sheetActions, true)) {
                $membership->setSheet('ficheTransmise.pdf');
            } elseif (in_array('remove', $sheetActions, true)) {
                $membership->setSheet(null, true);
            }
            if (is_uploaded_file((string) $sheet)) {
                $filename = $uploaderService->upload($sheet, 'sheets');
                $membership->setSheet($filename);
                $this->addFlash('success', 'La fiche a bien été ajoutée');
            }

            $this->managerRegistry->getManager()->flush();
            $this->addFlash('success', 'L’adhésion a bien été modifiée');

            return $this->redirectToRoute('membership.index');
        }

        return $this->render('membership/edit.html.twig', [
            'membership' => $membership,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id:membership}/cancel/edit', name: 'membership.cancel.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_MEMBERSHIP_EDIT')]
    public function cancelEdit(Request $request, Membership $membership, UploaderService $uploaderService): Response
    {
        if (Membership::STATUS_CANCELED !== $membership->getStatus()) {
            $this->addFlash('error', 'Seule une adhésion annulée peut être modifiée par ce formulaire');

            return $this->redirectToRoute('membership.index');
        }

        $form = $this->createForm(MembershipCancellationType::class, $membership);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();
            $this->addFlash('success', 'L’adhésion a bien été modifiée');

            if ($membership->getOrganization()) {
                return $this->redirectToRoute('organization.show', ['id' => $membership->getOrganization()->getId()]);
            }
            if ($membership->getPerson()) {
                return $this->redirectToRoute('person.show', ['id' => $membership->getPerson()->getId()]);
            }
        }

        return $this->render('membership/cancel_edit.html.twig', [
            'membership' => $membership,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id:membership}/validate-infos', name: 'membership.validate-infos', methods: ['GET'])]
    #[IsGranted('ROLE_MEMBERSHIP_EDIT')]
    public function validateInfos(Membership $membership, EntityManagerInterface $entityManager): Response
    {
        $membership->setInformationValidated(true);
        $entityManager->flush();
        $this->addFlash('success', 'Les informations ont bien été validées');

        if (null !== $membership->getPerson()) {
            return $this->redirectToRoute('person.show', ['id' => $membership->getPerson()->getId()]);
        }

        return $this->redirectToRoute('organization.show', ['id' => $membership->getOrganization()?->getId()]);
    }

    #[Route('/{id:membership}', name: 'membership.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_MEMBERSHIP_EDIT')]
    public function delete(Request $request, Membership $membership): Response
    {
        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('delete'.$membership->getId(), $csrfToken)) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($membership);
            $entityManager->flush();
        }

        return $this->redirectToRoute('membership.index');
    }
}
