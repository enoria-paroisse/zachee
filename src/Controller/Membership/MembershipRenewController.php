<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Membership;

use App\Controller\Traits\UserableTrait;
use App\Entity\Membership;
use App\Entity\PaymentRequest;
use App\Entity\Period;
use App\Repository\MembershipRepository;
use App\Repository\PaymentRequestRepository;
use App\Services\MembershipFeeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/request/renew')]
#[IsGranted('ROLE_MEMBERSHIP_EDIT')]
class MembershipRenewController extends AbstractController
{
    use UserableTrait;

    #[Route('/index', name: 'membership_request.renew.index', methods: ['GET'])]
    public function renew(): Response
    {
        $alert = false;
        if ($this->getAppUser()->getPeriod() && $this->getAppUser()->getPeriod()->getStart() > new \DateTime()) {
            $alert = true;
        }

        return $this->render('membership/request/renew.html.twig', [
            'alert' => $alert,
        ]);
    }

    #[Route('/importable/{period}', name: 'membership_request.renew.importable', methods: ['GET'])]
    public function importable(Period $period, MembershipRepository $membershipRepository): Response
    {
        if ($period === $this->getAppUser()->getPeriod()) {
            return $this->json([
                'period' => [
                    'id' => $period->getId(),
                    'name' => $period->getName(),
                ],
                'error' => 'Vous ne pouvez pas importer depuis la période courante.',
            ], 500);
        }

        /** @var Membership[] $dioceses */
        $dioceses = [];

        /** @var Membership[] $organizations */
        $organizations = [];

        /** @var Membership[] $people */
        $people = [];

        /** @var Membership[] $dioceses */
        $diocesesAlready = [];

        /** @var Membership[] $organizations */
        $organizationsAlready = [];

        /** @var Membership[] $people */
        $peopleAlready = [];

        $importable = [
            'dioceses' => [],
            'organizations' => [],
            'people' => [],
        ];

        $memberships = $membershipRepository->findByPeriodFullJoin($period);
        foreach ($memberships as $membership) {
            if (null !== $this->getAppUser()->getPeriod() && null !== $membershipRepository->findOneByPeriodOrganizationPersonFullJoin(
                $this->getAppUser()->getPeriod(),
                $membership->getOrganization(),
                $membership->getPerson(),
            )) {
                $already = true;
            } else {
                $already = false;
            }

            if ($membership->getPerson()) {
                if ($already) {
                    $peopleAlready[] = $this->createItem($membership);
                } else {
                    $people[] = $this->createItem($membership);
                    if (Membership::STATUS_VALIDATED === $membership->getStatus()) {
                        $importable['people'][] = $membership->getId();
                    }
                }
            } elseif ($membership->getOrganization()) {
                if ($membership->getOrganization()->getDiocese()
                    && $membership->getOrganization()->getDiocese()->getAssociatedOrganization()
                    && $membership->getOrganization()->getDiocese()->getAssociatedOrganization() === $membership->getOrganization()) {
                    if ($already) {
                        $diocesesAlready[] = $this->createItem($membership);
                    } else {
                        $dioceses[] = $this->createItem($membership);
                        if (Membership::STATUS_VALIDATED === $membership->getStatus()) {
                            $importable['dioceses'][] = $membership->getId();
                        }
                    }
                } else {
                    if ($already) {
                        $organizationsAlready[] = $this->createItem($membership);
                    } else {
                        $organizations[] = $this->createItem($membership);
                        if (Membership::STATUS_VALIDATED === $membership->getStatus()) {
                            $importable['organizations'][] = $membership->getId();
                        }
                    }
                }
            }
        }

        return $this->json([
            'period' => [
                'id' => $period->getId(),
                'name' => $period->getName(),
            ],
            'memberships' => [
                'dioceses' => $dioceses,
                'organizations' => $organizations,
                'people' => $people,
                'diocesesAlready' => $diocesesAlready,
                'organizationsAlready' => $organizationsAlready,
                'peopleAlready' => $peopleAlready,
                'importable' => $importable,
            ],
        ]);
    }

    #[Route('/exec/{membership}', name: 'membership_request.renew.exec')]
    #[IsGranted('ROLE_MEMBERSHIP_EDIT')]
    public function renewExec(
        Membership $membership,
        MembershipFeeService $membershipFeeService,
        MembershipRepository $membershipRepository,
        PaymentRequestRepository $paymentRequestRepository
    ): Response {
        $id = 0;
        $name = '';
        $idMember = 0;
        $type = '';

        if (null !== $membership->getPerson()) {
            $name = $membership->getPerson()->getFullname();
            $idMember = $membership->getPerson()->getId();
            $type = 'person';
        } elseif (null !== $membership->getOrganization()) {
            $name = $membership->getOrganization()->getName();
            $idMember = $membership->getOrganization()->getId();

            if ($membership->getOrganization()->getDiocese() && $membership->getOrganization()->getDiocese()->getAssociatedOrganization() === $membership->getOrganization()) {
                $type = 'diocese';
            } else {
                $type = 'organization';
            }
        }

        if ($membership->getPeriod() === $this->getAppUser()->getPeriod()) {
            $error = 'Cette adhésion appartient à la période courante.';
            $code = 401;
        } elseif (Membership::STATUS_VALIDATED !== $membership->getStatus()) {
            $error = 'Cette adhésion n’est pas validée, elle n’est donc pas importable.';
            $code = 401;
        } elseif (null !== $this->getAppUser()->getPeriod() && null !== $membershipRepository->findOneByPeriodOrganizationPersonFullJoin(
            $this->getAppUser()->getPeriod(),
            $membership->getOrganization(),
            $membership->getPerson(),
        )) {
            $error = 'Une adhésion existe déjà pour ce membre dans la période courante.';
            $code = 401;
        } else {
            $newMembership = clone $membership;
            $paymentRequest = new PaymentRequest();
            $newMembership->setPaymentRequest($paymentRequest);
            $newMembership->setPeriod($this->getAppUser()->getPeriod())
                ->setStatus(Membership::STATUS_IN_PROGRESS)
                ->setFirst(false)
                ->setInformationSubmitted(false)
                ->setInformationValidated(false)
                ->setHelpRequest(false)
                ->setHelpStatus(Membership::HELP_UNTREATED)
                ->setToken(null)
                ->setLastReminder(null)
                ->setNotes('')
                ->setMembershipFee($membershipFeeService->calculateFee($newMembership))
            ;

            $paymentRequest->setMembership($newMembership);
            $membershipRepository->save($newMembership);
            $paymentRequest->setDatetime($newMembership->getCreatedAt());
            $paymentRequestRepository->save($paymentRequest, true);

            $id = $newMembership->getId();
            $error = '';
            $code = 200;
        }

        return $this->json([
            'id' => $id,
            'idOriginal' => $membership->getId(),
            'type' => $type,
            'idMember' => $idMember,
            'name' => $name,
            'error' => $error,
        ], $code);
    }

    /**
     * @return array{id: int, idMember: int, name: string, status: string}
     */
    protected function createItem(Membership $membership): array
    {
        $name = '';
        $idMember = 0;
        if (null !== $membership->getPerson()) {
            $name = $membership->getPerson()->getFullname();
            $idMember = $membership->getPerson()->getId();
        } elseif (null !== $membership->getOrganization()) {
            $name = $membership->getOrganization()->getName();
            $idMember = $membership->getOrganization()->getId();
        }

        return [
            'id' => $membership->getId() ?? 0,
            'idMember' => $idMember ?? 0,
            'name' => $name ?? '',
            'status' => $membership->getStatus() ?? '',
        ];
    }
}
