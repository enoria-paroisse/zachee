<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Membership;

use App\Controller\Traits\UserableTrait;
use App\Entity\Membership;
use App\Entity\PaymentRequest;
use App\Form\EntityStatusType;
use App\Form\Membership\MembershipRequestAddType;
use App\Form\Membership\MembershipRequestEditType;
use App\Mailer\MembershipMailer;
use App\Repository\MembershipRepository;
use App\Services\MembershipFeeService;
use App\Services\UploaderService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/request')]
class MembershipRequestController extends AbstractController
{
    use UserableTrait;

    public function __construct(private readonly ManagerRegistry $managerRegistry)
    {
    }

    #[Route('/', name: 'membership_request.index', methods: ['GET'])]
    #[IsGranted('ROLE_MEMBERSHIP_REQUEST_SHOW')]
    public function index(MembershipRepository $membershipRepository): Response
    {
        $form = $this->createForm(EntityStatusType::class);

        return $this->render('membership/request/index.html.twig', [
            'cancel' => false,
            'legalPersons' => $membershipRepository->findLegalPersonInProgressByPeriod($this->getActivePeriod()),
            'naturalPersons' => $membershipRepository->findNaturalPersonInProgressByPeriod($this->getActivePeriod()),
            'form' => $form->createView(),
        ]);
    }

    #[Route('/cancel', name: 'membership_request.cancel_list', methods: ['GET'])]
    #[IsGranted('ROLE_MEMBERSHIP_REQUEST_SHOW')]
    public function cancelList(MembershipRepository $membershipRepository): Response
    {
        return $this->render('membership/request/index.html.twig', [
            'cancel' => true,
            'legalPersons' => $membershipRepository->findLegalPersonCanceledByPeriod($this->getActivePeriod()),
            'naturalPersons' => $membershipRepository->findNaturalPersonCanceledByPeriod($this->getActivePeriod()),
        ]);
    }

    #[Route('/new', name: 'membership_request.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_MEMBERSHIP_EDIT')]
    public function new(Request $request, MembershipFeeService $membershipFeeService): Response
    {
        $paymentRequest = new PaymentRequest();
        $membership = new Membership();
        $membership->setPeriod($this->getActivePeriod())
            ->setStatus(Membership::STATUS_IN_PROGRESS)
            ->setMembershipFee(0)
            ->setInformationSubmitted(false)
            ->setInformationValidated(false)
            ->setHelpRequest(false)
            ->setHelpStatus(Membership::HELP_UNTREATED)
            ->setFirst(true)
            ->setPaymentRequest($paymentRequest)
        ;
        $form = $this->createForm(MembershipRequestAddType::class, $membership);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $membership->setMembershipFee($membershipFeeService->calculateFee($membership));
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($membership);
            $paymentRequest->setDatetime($membership->getCreatedAt());
            $entityManager->flush();
            $paymentRequest->setMembership($membership);
            $entityManager->flush();
            $this->addFlash('success', 'La demande d’adhésion a bien été ajoutée');

            return $this->redirectToRoute('membership_request.index');
        }

        return $this->render('membership/request/new.html.twig', [
            'membership' => $membership,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id:membership}/edit', name: 'membership_request.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_MEMBERSHIP_EDIT')]
    public function edit(Request $request, Membership $membership, UploaderService $uploaderService, MembershipMailer $membershipMailer): Response
    {
        if (Membership::STATUS_VALIDATED === $membership->getStatus()) {
            $this->addFlash('error', 'Une adhésion validée ne peut pas être modifiée');

            return $this->redirectToRoute('membership_request.index');
        }

        $oldHelpStatus = $membership->getHelpStatus();

        $form = $this->createForm(MembershipRequestEditType::class, $membership);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sheet = $form->get('sheet')->getData();
            $sheetActions = (array) $form->get('sheet_actions')->getData();
            if (in_array('default', $sheetActions, true)) {
                $membership->setSheet('ficheTransmise.pdf');
                $membership->setSheetValidated(true);
            } elseif (in_array('remove', $sheetActions, true)) {
                $membership->setSheet(null);
                $membership->setSheetValidated(false);
            }
            if (is_uploaded_file((string) $sheet)) {
                $filename = $uploaderService->upload($sheet, 'sheets');
                $membership->setSheet($filename);
                $this->addFlash('success', 'La fiche a bien été ajoutée');
                $membership->setSheetValidated(true);
            }

            if (!$membership->getInformationSubmitted()) {
                $membership->setInformationValidated(false);
            }

            if (true === $membership->getHelpRequest() && Membership::HELP_ACCEPTED === $membership->getHelpStatus() && $oldHelpStatus !== $membership->getHelpStatus()) {
                try {
                    $membershipMailer->sendHelpRequestAccepted($membership);
                    $this->addFlash(
                        'success',
                        'Le mail de validation de réduction de cotisation a été envoyé'
                    );
                } catch (TransportExceptionInterface) {
                    $this->addFlash(
                        'error',
                        'Une erreur a eu lieu lors de l’envoi du mail de validation de réduction de cotisation',
                    );
                }
            }

            $this->managerRegistry->getManager()->flush();
            $this->addFlash('success', 'La demande d’adhésion a bien été modifiée');

            return $this->redirectToRoute('membership_request.index');
        }

        return $this->render('membership/request/edit.html.twig', [
            'membership' => $membership,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/cancel/{id:membership}', name: 'membership_request.cancel', methods: ['GET'])]
    #[IsGranted('ROLE_MEMBERSHIP_EDIT')]
    public function cancel(Request $request, Membership $membership): Response
    {
        $membership->setStatus(Membership::STATUS_CANCELED);
        $membership->setCancellationDate(new \DateTime());
        $this->managerRegistry->getManager()->flush();

        $this->addFlash('success', 'L’adhésion a été annulée');

        return $this->redirectToRoute('membership_request.cancel_list');
    }

    #[Route('/uncancel/{id:membership}', name: 'membership_request.uncancel', methods: ['GET'])]
    #[IsGranted('ROLE_MEMBERSHIP_EDIT')]
    public function uncancel(Request $request, Membership $membership): Response
    {
        $membership->setStatus(Membership::STATUS_IN_PROGRESS);
        $membership->setCancellationDate(null);
        $this->managerRegistry->getManager()->flush();

        $this->addFlash('success', 'L’adhésion a été réactivée');

        return $this->redirectToRoute('membership_request.index');
    }

    #[Route('/toggle-reminder/{id:membership}', name: 'membership_request.toggle-reminder', methods: ['GET'])]
    #[IsGranted('ROLE_MEMBERSHIP_EDIT')]
    public function toggleReminder(Membership $membership): Response
    {
        $membership->setReminderEnabled(!$membership->isReminderEnabled());
        if ($membership->isReminderEnabled()) {
            $this->addFlash('success', 'Les relances ont été activées');
        } else {
            $this->addFlash('success', 'Les relances ont été suspendues');
        }

        $this->managerRegistry->getManager()->flush();

        return $this->redirectToRoute('membership_request.index');
    }

    #[Route('/mail/{id:membership}', name: 'membership_request.mail', methods: ['GET'])]
    #[IsGranted('ROLE_MEMBERSHIP_EDIT')]
    public function mail(Membership $membership, MembershipMailer $mailer): Response
    {
        if (!$membership->getToken()) {
            $membership->setToken(md5(random_bytes(20)));
            $this->managerRegistry->getManager()->flush();
            $this->addFlash('success', 'Le lien public a été créé');
        }

        try {
            $mailer->sendMembershipPublicLink($membership);
            $membership->touchLastReminder();
            $this->managerRegistry->getManager()->flush();
            $this->addFlash(
                'success',
                'Le lien public a été envoyé'
            );
        } catch (TransportExceptionInterface) {
            $this->addFlash(
                'error',
                'Une erreur a eu lieu lors de l’envoi du mail : ',
            );
        }

        return $this->redirectToRoute('membership_request.index');
    }

    #[Route(
        '/all/renew{type}',
        name: 'membership_request.mailall',
        requirements: ['type' => 'legal|natural|diocese'],
        defaults: ['type' => 'legal'],
        methods: ['GET']
    )]
    #[IsGranted('ROLE_MEMBERSHIP_EDIT')]
    public function mailAll(string $type, Request $request, MembershipRepository $membershipRepository, MembershipMailer $mailer): Response
    {
        $dioceseId = 0;

        /** @var Membership[] $memberships */
        $memberships = [];
        $ids = array_filter(explode(',', $request->query->get('m', '')));
        if ($ids) {
            $memberships = $membershipRepository->findBy(['id' => $ids]);
            if ('diocese' == $type && count($memberships) && $memberships[0]->getOrganization() && $memberships[0]->getOrganization()->getDiocese()) {
                $dioceseId = $memberships[0]->getOrganization()->getDiocese()->getId();
            }
            $memberships = array_filter($memberships, function ($membership) {
                return Membership::STATUS_IN_PROGRESS === $membership->getStatus();
            });
        } elseif ('legal' === $type) {
            $memberships = $membershipRepository->findLegalPersonInProgressByPeriod($this->getActivePeriod());
        } elseif ('natural' == $type) {
            $memberships = $membershipRepository->findNaturalPersonInProgressByPeriod($this->getActivePeriod());
        }

        foreach ($memberships as $membership) {
            $name = '';
            if ($membership->getPerson()) {
                $name = $membership->getPerson()->getFullName();
            } elseif ($membership->getOrganization()) {
                $name = $membership->getOrganization()->getName();
            }
            if (!$membership->getToken()) {
                $membership->setToken(md5(random_bytes(20)));
                $this->managerRegistry->getManager()->flush();
                $this->addFlash('success', 'Le lien public a été créé pour '.$name);
            }

            if ($membership->isReminderEnabled()) {
                try {
                    $mailer->sendMembershipPublicLink($membership, true);
                    $membership->touchLastReminder();
                    $this->managerRegistry->getManager()->flush();
                    $this->addFlash(
                        'success',
                        'Le message de réadhésion a été envoyé pour '.$name
                    );
                } catch (TransportExceptionInterface) {
                    $this->addFlash(
                        'error',
                        'Une erreur a eu lieu lors de l’envoi du mail pour '.$name,
                    );
                }
            } else {
                $this->addFlash(
                    'notice',
                    'Les relances ont été désactivées pour '.$name.', le mail n’a donc pas été envoyé',
                );
            }
        }

        if (0 < $dioceseId) {
            return $this->redirectToRoute('diocese.show', ['id' => $dioceseId]);
        }

        return $this->redirectToRoute('membership_request.index');
    }

    #[Route('/validate/{id:membership}', name: 'membership_request.validate', methods: ['GET'])]
    #[IsGranted('ROLE_MEMBERSHIP_EDIT')]
    public function validate(Membership $membership, MembershipMailer $mailer): Response
    {
        if (Membership::STATUS_IN_PROGRESS !== $membership->getStatus()) {
            $this->addFlash(
                'error',
                'Seule une adhésion en cours peut être validée',
            );
        } else {
            $membership->setStatus(Membership::STATUS_VALIDATED);

            try {
                $mailer->sendFirstMembershipValidate($membership);
                if (true === $membership->getFirst() && null !== $membership->getOrganization()) {
                    $this->addFlash(
                        'success',
                        'Un mail de bienvenue a été envoyé à "'.$membership->getOrganization().'"'
                    );
                }
            } catch (TransportExceptionInterface $e) {
            }

            $this->managerRegistry->getManager()->flush();
            $this->addFlash(
                'success',
                'L’adhésion a été validée',
            );
        }

        return $this->redirectToRoute('membership_request.index');
    }
}
