<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Controller\Traits\UserableTrait;
use App\Entity\Newsletter;
use App\Entity\NewsletterFolder;
use App\Repository\NewsletterFolderRepository;
use App\Repository\NewsletterRepository;
use App\Repository\NewsletterSubscriptionRepository;
use App\Services\BrevoService;
use App\Services\NewsletterService;
use Brevo\Client\ApiException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/newsletter')]
class NewsletterController extends AbstractController
{
    use UserableTrait;

    #[Route('/', name: 'newsletter.index', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function index(
        BrevoService $brevoService,
        NewsletterRepository $newsletterRepository,
        NewsletterFolderRepository $folderRepository,
    ): Response {
        $brevo = [
            'error' => false,
            'errorApiKey' => false,
            'errorMessage' => '',
            'account' => [],
            'subscription' => 0,
            'sms' => 0,
            'attributes' => [],
        ];
        $folders = $folderRepository->findBy([], ['name' => 'asc']);
        $newsletters = [];

        foreach ($newsletterRepository->findBy([], ['name' => 'asc']) as $newsletter) {
            $newsletters[(int) $newsletter->getBrevoId()] = [
                'entity' => $newsletter,
                'uniqueSubscribers' => 0,
                'totalBlacklisted' => 0,
            ];
        }

        try {
            $brevo['account'] = $brevoService->getAccountApi()->getAccount();
            foreach ($brevo['account']->getPlan() as $plan) {
                if ('subscription' === $plan->getType()) {
                    $brevo['subscription'] = $plan->getCredits();
                } elseif ('sms' === $plan->getType()) {
                    $brevo['sms'] = $plan->getCredits();
                }
            }

            foreach ($brevoService->getAttributesApi()->getAttributes()->getAttributes() as $attribute) {
                $brevo['attributes'][] = $attribute->getName();
            }

            foreach ($folders as $folder) {
                if ($folder->isEnabled()) {
                    /** @var (int|string)[][] $brevoFolderLists */
                    $brevoFolderLists = $brevoService->getContactsApi()->getFolderLists($folder->getBrevoId() ?? 0, 50, 0)->getLists();
                    foreach ($brevoFolderLists as $list) {
                        if (array_key_exists((int) $list['id'], $newsletters)) {
                            $newsletters[(int) $list['id']]['uniqueSubscribers'] = $list['uniqueSubscribers'];
                            $newsletters[(int) $list['id']]['totalBlacklisted'] = $list['totalBlacklisted'];
                        }
                    }
                }
            }

            usort($newsletters, function ($a, $b) {
                return strcmp($a['entity']->getFolder()?->getName() ?? '', $b['entity']->getFolder()?->getName() ?? '');
            });
        } catch (\Exception $e) {
            $brevo['error'] = true;
            if ($e instanceof ApiException && 401 === $e->getCode()) {
                $brevo['errorApiKey'] = true;
            } else {
                $brevo['errorMessage'] = $e->getMessage();
            }
        }

        return $this->render('newsletter/index.html.twig', [
            'brevo' => $brevo,
            'folders' => $folders,
            'newsletters' => $newsletters,
            'firstnameAttribute' => $brevoService->getFirstnameAttribute(),
            'lastnameAttribute' => $brevoService->getLastnameAttribute(),
        ]);
    }

    #[Route('/folder/sync', name: 'newsletter.folder.sync', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function folderSync(
        BrevoService $brevoService,
        NewsletterFolderRepository $folderRepository,
        EntityManagerInterface $entityManager,
    ): Response {
        $folders = [];
        foreach ($folderRepository->findAll() as $folder) {
            $folder->setExist(false);
            $folders[$folder->getBrevoId()] = $folder;
        }

        try {
            /** @var (int|string)[][] $brevo */
            $brevo = $brevoService->getContactsApi()->getFolders(50, 0, 'asc')->getFolders();
            foreach ($brevo as $folder) {
                if (array_key_exists($folder['id'], $folders)) {
                    $folders[$folder['id']]
                        ->setExist(true)
                        ->setName((string) $folder['name'])
                    ;
                } else {
                    $newFolder = new NewsletterFolder();
                    $newFolder->setName((string) $folder['name'])
                        ->setBrevoId((int) $folder['id'])
                        ->setExist(true)
                        ->setEnabled(false)
                    ;
                    $folders[$folder['id']] = $newFolder;
                }
            }
            foreach ($folders as $folder) {
                if (!$folder->isExist()) {
                    $folder->setEnabled(false);
                }
                $entityManager->persist($folder);
            }
            $entityManager->flush();
        } catch (\Exception $e) {
            $this->addFlash(
                'error',
                'La synchronisation des dossiers a échouée',
            );
        }

        return $this->redirectToRoute('newsletter.index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/folder/toggle/{id:folder}', name: 'newsletter.folder.toggle', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function folderToggle(NewsletterFolder $folder, NewsletterFolderRepository $folderRepository): Response
    {
        $folder->setEnabled(!$folder->isEnabled());
        $folderRepository->save($folder, true);

        return $this->redirectToRoute('newsletter.index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/folder/{id:folder}', name: 'newsletter.folder.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_ADMIN')]
    public function folderDelete(Request $request, NewsletterFolder $folder, NewsletterFolderRepository $folderRepository): Response
    {
        if ($folder->isExist()) {
            $this->addFlash(
                'error',
                'Le dossier existe encore dans Brevo',
            );
        } elseif (0 < count($folder->getLists())) {
            $this->addFlash(
                'error',
                'Veuillez supprimer les listes de ce dossier présentes dans Zachée',
            );
        } else {
            $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
            if ($this->isCsrfTokenValid('delete'.$folder->getId(), $csrfToken)) {
                $folderRepository->remove($folder, true);
                $this->addFlash(
                    'success',
                    'Le dossier a bien été supprimé'
                );
            }
        }

        return $this->redirectToRoute('newsletter.index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/list/sync', name: 'newsletter.list.sync', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function listSync(
        BrevoService $brevoService,
        NewsletterFolderRepository $folderRepository,
        NewsletterRepository $newsletterRepository,
        EntityManagerInterface $entityManager,
    ): Response {
        $folders = $folderRepository->findBy(['enabled' => true]);

        $newsletters = [];
        foreach ($newsletterRepository->findAll() as $newsletter) {
            $newsletter->setExist(false);
            $newsletters[$newsletter->getBrevoId()] = $newsletter;
        }

        try {
            foreach ($folders as $folder) {
                /** @var (int|string)[][] $brevo */
                $brevo = $brevoService->getContactsApi()->getFolderLists($folder->getBrevoId() ?? 0, 50, 0)->getLists();
                foreach ($brevo as $newsletter) {
                    if (array_key_exists($newsletter['id'], $newsletters)) {
                        $newsletters[$newsletter['id']]->setExist(true)
                            ->setName((string) $newsletter['name'])
                        ;
                    } else {
                        $newNewsletter = new Newsletter();
                        $newNewsletter->setName((string) $newsletter['name'])
                            ->setBrevoId((int) $newsletter['id'])
                            ->setFolder($folder)
                            ->setExist(true)
                        ;
                        $newsletters[$newsletter['id']] = $newNewsletter;
                    }
                }
            }
            foreach ($newsletters as $newsletter) {
                $entityManager->persist($newsletter);
            }
            $entityManager->flush();
        } catch (\Exception $e) {
            $this->addFlash(
                'error',
                'La synchronisation des listes a échouée',
            );
        }

        return $this->redirectToRoute('newsletter.index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/list/{id:list}', name: 'newsletter.list.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_ADMIN')]
    public function listDelete(Request $request, Newsletter $list, NewsletterRepository $newsletterRepository): Response
    {
        if ($list->isExist()) {
            $this->addFlash(
                'error',
                'La liste existe encore dans Brevo',
            );
        } elseif (0 < count($list->getSubscriptions())) {
            $this->addFlash(
                'error',
                'Veuillez lancer une synchronisation avant de supprimer la liste. Il y a encore des personnes inscrites à cette liste dans Zachée.',
            );
        } else {
            $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
            if ($this->isCsrfTokenValid('delete'.$list->getId(), $csrfToken)) {
                $newsletterRepository->remove($list, true);
                $this->addFlash(
                    'success',
                    'La liste a bien été supprimé'
                );
            }
        }

        return $this->redirectToRoute('newsletter.index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/sync', name: 'newsletter.sync', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function sync(NewsletterService $newsletterService): Response
    {
        try {
            $newsletterService->sync();

            $this->addFlash(
                'success',
                'La synchronisation réussie',
            );
        } catch (\Exception $e) {
            $this->addFlash(
                'warning',
                'La synchronisation a échouée : '.$e->getMessage(),
            );
        }

        return $this->redirectToRoute('newsletter.index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/show/{id}', name: 'newsletter.show', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function show(
        #[MapEntity(expr: 'repository.findByIdFullJoin(id)')]
        Newsletter $newsletter,
        NewsletterSubscriptionRepository $newsletterSubscriptionRepository,
    ): Response {
        return $this->render('newsletter/show.html.twig', [
            'newsletter' => $newsletter,
            'subscriptions' => $newsletterSubscriptionRepository->findAllByNewsletter($newsletter),
        ]);
    }
}
