<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Entity;
use App\Entity\Instance;
use App\Entity\Membership;
use App\Form\EntityStatusType;
use App\Form\EntityType;
use App\Repository\MembershipRepository;
use App\Services\EnoriaException;
use App\Services\EnoriaService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/entity')]
class EntityController extends AbstractController
{
    public function __construct(private readonly ManagerRegistry $managerRegistry)
    {
    }

    #[Route('/new/{id:instance}', name: 'entity.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function new(Request $request, Instance $instance): Response
    {
        $entity = new Entity();
        $entity->setInstance($instance);
        $form = $this->createForm(EntityType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();
            $this->addFlash('success', 'L’entité a bien été ajoutée');

            return $this->redirectToRoute('instance.show', ['id' => $entity->getInstance()->getId()]);
        }

        return $this->render('entity/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id:entity}/edit', name: 'entity.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function edit(Request $request, Entity $entity): Response
    {
        $form = $this->createForm(EntityType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();
            $this->addFlash('success', 'L’entité a bien été modifiée');

            return $this->redirectToRoute('instance.show', ['id' => $entity->getInstance()->getId()]);
        }

        return $this->render('entity/edit.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id:entity}', name: 'entity.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_ADMIN')]
    public function delete(Request $request, Entity $entity, MembershipRepository $membershipRepository): Response
    {
        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('delete'.$entity->getId(), $csrfToken)) {
            $memberships = $membershipRepository->findBy(['entity' => $entity->getId()]);
            array_walk($memberships, static function (Membership $membership): void {
                $membership->setEntity(null);
            });

            $entityManager = $this->managerRegistry->getManager();
            $entityManager->remove($entity);
            $entityManager->flush();
            $this->addFlash('success', 'L’entité a bien été supprimée');
        }

        return $this->redirectToRoute('instance.show', ['id' => $entity->getInstance()->getId()]);
    }

    #[Route('/update-status', name: 'entity.updateStatus', methods: ['POST'])]
    #[IsGranted(new Expression('is_granted("ROLE_MEMBERSHIP_EDIT") or is_granted("ROLE_ACCOUNTING") or is_granted("ROLE_ADMIN")'))]
    public function updateStatus(Request $request, EnoriaService $enoriaService): Response
    {
        $form = $this->createForm(EntityStatusType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            /** @var Entity $entity */
            $entity = $data['entity'];

            /** @var string $status */
            $status = $data['status'];

            try {
                $enoriaService->updateEntityStatus($entity, $status);
                $this->addFlash('success', 'Le statut de l’entité a bien été mis à jour');
            } catch (EnoriaException $e) {
                $this->addFlash('error', 'Une erreur est survenue dans la mise à jour du statut de l’entité ('.$e->getMessage().')');
            }
        } elseif ($form->isSubmitted()) {
            $this->addFlash('error', 'Une erreur est survenue dans la validation du formulaire');
        }

        return $this->redirect($request->headers->get('referer') ?: '/');
    }
}
