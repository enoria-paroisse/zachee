<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Controller\Traits\UserableTrait;
use App\Entity\Address;
use App\Entity\AssociatedPerson;
use App\Entity\Organization;
use App\Entity\OrganizationType as OrganizationTypeEntity;
use App\Form\AssociatedPersonType;
use App\Form\OrganizationType;
use App\Form\OrganizationTypeType;
use App\Repository\DocRepository;
use App\Repository\FolderRepository;
use App\Repository\OrganizationRepository;
use App\Repository\OrganizationTypeRepository;
use App\Repository\PaymentRequestRepository;
use App\Repository\TypeAssociatedPersonRepository;
use App\Services\OrganizationTypeTagService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/organization')]
class OrganizationController extends AbstractController
{
    use UserableTrait;

    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    #[Route('/', name: 'organization.index', methods: ['GET'])]
    #[IsGranted('ROLE_BASIC_SHOW')]
    public function index(OrganizationRepository $organizationRepository, OrganizationTypeRepository $organizationTypeRepository): Response
    {
        return $this->render('organization/index.html.twig', [
            'organizations' => $organizationRepository->findAllWithMembershipAndPeopleByPeriod($this->getActivePeriod()),
            'types' => $organizationTypeRepository->findAll(),
        ]);
    }

    #[Route('/type/show/{id}', name: 'organization.type.show', methods: ['GET'])]
    #[IsGranted('ROLE_BASIC_SHOW')]
    public function showType(int $id, OrganizationTypeRepository $organizationTypeRepository): Response
    {
        return $this->render('organization/type/show.html.twig', [
            'type' => $organizationTypeRepository->findByIdAndPeriod($id, $this->getActivePeriod()),
            'types' => $organizationTypeRepository->findAll(),
        ]);
    }

    #[Route('/show/{id}', name: 'organization.show', methods: ['GET'])]
    public function show(
        #[MapEntity(expr: 'repository.findByIdFullJoin(id)')]
        Organization $organization,
        PaymentRequestRepository $paymentRequestRepository,
    ): Response {
        $this->denyAccessUnlessGranted('basic_show', $organization);

        $paymentRequests = $organization->getPaymentRequests();

        $memberships = $organization->getMemberships();
        $actualMembership = null;
        if (count($memberships)) {
            foreach ($memberships as $membership) {
                if ($membership->getPeriod() === $this->getActivePeriod()) {
                    $actualMembership = $membership;
                }
                if (null !== $membership->getPaymentRequest()) {
                    $paymentRequests[] = $membership->getPaymentRequest();
                }
            }
        }

        if (null !== $organization->getDiocese()
            && null !== $organization->getDiocese()->getAssociatedOrganization()
            && $organization === $organization->getDiocese()->getAssociatedOrganization()) {
            $paymentRequests = array_merge(
                $paymentRequests->toArray(),
                $paymentRequestRepository->findDioceseMembershipSupplement($organization->getDiocese())
            );
        }

        return $this->render('organization/show.html.twig', [
            'organization' => $organization,
            'memberships' => $memberships,
            'actualMembership' => $actualMembership,
            'paymentRequests' => $paymentRequests,
            'payments' => $organization->getPayments(),
        ]);
    }

    #[Route('/create', name: 'organization.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ORGANIZATION_EDIT')]
    public function new(Request $request): Response
    {
        $organization = new Organization();
        $address = new Address();
        $address->setCountry('FR');
        $organization->setAddress($address);
        $form = $this->createForm(OrganizationType::class, $organization);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($organization);
            $this->em->flush();
            $this->addFlash('success', 'L’organisation a bien été ajoutée');

            return $this->redirectToRoute('organization.show', ['id' => $organization->getId()]);
        }

        return $this->render('organization/new.html.twig', [
            'organization' => $organization,
            'form' => $form->createView(),
            'referer' => $request->headers->get('referer'),
        ]);
    }

    #[Route('/edit/{id}', name: 'organization.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ORGANIZATION_EDIT')]
    public function edit(
        #[MapEntity(expr: 'repository.findByIdFullJoin(id)')]
        Organization $organization,
        Request $request,
    ): Response {
        $form = $this->createForm(OrganizationType::class, $organization);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('success', 'L’organisation a bien été modifiée');

            return $this->redirectToRoute('organization.show', ['id' => $organization->getId()]);
        }

        return $this->render('organization/edit.html.twig', [
            'organization' => $organization,
            'form' => $form->createView(),
            'referer' => $request->headers->get('referer'),
        ]);
    }

    #[Route('/delete/{id}', name: 'organization.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_ORGANIZATION_EDIT')]
    public function delete(
        #[MapEntity(expr: 'repository.findByIdFullJoin(id)')]
        Organization $organization,
        Request $request,
    ): Response {
        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('delete'.$organization->getId(), $csrfToken)) {
            if (null !== $organization->getDiocese() && $organization === $organization->getDiocese()->getAssociatedOrganization()) {
                $this->addFlash('error', 'Impossible de supprimer cette organisation : elle est associée à un diocèse');
            } else {
                $this->em->remove($organization);

                try {
                    $this->em->flush();
                    $this->addFlash('success', 'L’organisation a bien été supprimée');

                    return $this->redirectToRoute('organization.index');
                } catch (ForeignKeyConstraintViolationException) {
                    $this->addFlash('error', 'Impossible de supprimer cette organisation : elle est liée à des données (inscriptions, comptabilité, ...)');
                }
            }
        }

        return $this->redirectToRoute('organization.show', ['id' => $organization->getId()]);
    }

    #[Route('/associate/{id}', name: 'organization.associate', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ORGANIZATION_EDIT')]
    public function associate(
        #[MapEntity(expr: 'repository.findByIdFullJoin(id)')]
        Organization $organization,
        Request $request,
        TypeAssociatedPersonRepository $typeAssociatedPersonRepository,
    ): Response {
        $associatedPerson = new AssociatedPerson();
        $associatedPerson->setOrganization($organization);

        $form = $this->createForm(AssociatedPersonType::class, $associatedPerson);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($associatedPerson);
            $this->em->flush();
            $this->addFlash('success', 'La personne a bien été associée');

            return $this->redirectToRoute('organization.show', ['id' => $organization->getId()]);
        }

        return $this->render('organization/associate.html.twig', [
            'organization' => $organization,
            'form' => $form->createView(),
            'types' => $typeAssociatedPersonRepository->findAll(),
        ]);
    }

    #[Route('/associate/edit/{id:associatedPerson}', name: 'organization.associate.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ORGANIZATION_EDIT')]
    public function associateEdit(AssociatedPerson $associatedPerson, Request $request, TypeAssociatedPersonRepository $typeAssociatedPersonRepository): Response
    {
        $form = $this->createForm(AssociatedPersonType::class, $associatedPerson);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->addFlash('success', 'L’association personne-organisation a bien été modifiée');

            return $this->redirectToRoute('organization.show', ['id' => $associatedPerson->getOrganization()->getId()]);
        }

        return $this->render('organization/associate_edit.html.twig', [
            'associatedPerson' => $associatedPerson,
            'form' => $form->createView(),
            'types' => $typeAssociatedPersonRepository->findAll(),
        ]);
    }

    #[Route('/associate/delete/{id:associatedPerson}', name: 'organization.associate.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_ORGANIZATION_EDIT')]
    public function associateDelete(AssociatedPerson $associatedPerson, Request $request): Response
    {
        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('delete'.$associatedPerson->getId(), $csrfToken)) {
            $this->em->remove($associatedPerson);
            $this->em->flush();
            $this->addFlash('success', 'L’association personne-organisation a bien été supprimée');
        }

        return $this->redirectToRoute('organization.show', ['id' => $associatedPerson->getOrganization()->getId()]);
    }

    #[Route('/type/new', name: 'organization.type.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function newType(Request $request, OrganizationTypeRepository $organizationTypeRepository): Response
    {
        $organizationTypeEntity = new OrganizationTypeEntity();
        $form = $this->createForm(OrganizationTypeType::class, $organizationTypeEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $organizationTypeRepository->save($organizationTypeEntity, true);
            $this->addFlash('success', 'Le type a bien été ajouté');

            return $this->redirectToRoute('organization.type.show', ['id' => $organizationTypeEntity->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('organization/type/new.html.twig', [
            'form' => $form,
            'types' => $organizationTypeRepository->findAll(),
        ]);
    }

    #[Route('/type/{id:organizationTypeEntity}/edit', name: 'organization.type.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function editType(
        Request $request,
        OrganizationTypeEntity $organizationTypeEntity,
        OrganizationTypeRepository $organizationTypeRepository,
        OrganizationTypeTagService $organizationTypeTag,
        DocRepository $docRepository,
        FolderRepository $folderRepository,
    ): Response {
        $oldSlug = $organizationTypeTag->generateTagFromString($organizationTypeEntity->getName() ?? '');

        $form = $this->createForm(OrganizationTypeType::class, $organizationTypeEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newSlug = $organizationTypeTag->generateTagFromString($organizationTypeEntity->getName() ?? '');
            $docs = $docRepository->findByTags([$oldSlug]);
            $folders = $folderRepository->findByTags([$oldSlug]);
            foreach ($docs as $doc) {
                $doc->setTags(array_merge(array_diff($doc->getTags(), [$oldSlug]), [$newSlug]));
                $docRepository->save($doc);
            }
            foreach ($folders as $folder) {
                $folder->setTags(array_merge(array_diff($folder->getTags(), [$oldSlug]), [$newSlug]));
                $folderRepository->save($folder);
            }

            $organizationTypeRepository->save($organizationTypeEntity, true);
            $this->addFlash('success', 'Le type a bien été modifié');

            return $this->redirectToRoute('organization.type.show', ['id' => $organizationTypeEntity->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('organization/type/edit.html.twig', [
            'type' => $organizationTypeEntity,
            'form' => $form,
            'types' => $organizationTypeRepository->findAll(),
        ]);
    }

    #[Route('/type/{id:organizationTypeEntity}', name: 'organization.type.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_ADMIN')]
    public function deleteType(Request $request, OrganizationTypeEntity $organizationTypeEntity, OrganizationTypeRepository $organizationTypeRepository): Response
    {
        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('delete'.$organizationTypeEntity->getId(), $csrfToken)) {
            if (count($organizationTypeEntity->getOrganizations()) > 0) {
                $this->addFlash('warning', 'Vous ne pouvez-vous pas supprimer un type avec des organisations associées');

                return $this->redirectToRoute('organization.type.edit', ['id' => $organizationTypeEntity->getId()], Response::HTTP_SEE_OTHER);
            }
            $organizationTypeRepository->remove($organizationTypeEntity, true);
            $this->addFlash('success', 'Le type a bien été supprimé');
        }

        return $this->redirectToRoute('organization.index', [], Response::HTTP_SEE_OTHER);
    }
}
