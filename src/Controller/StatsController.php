<?php

/**
 * @copyright Copyright (c) 2021 Raphaël Cournault <president@association-enoria.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Controller\Traits\UserableTrait;
use App\Entity\Instance;
use App\Repository\InstanceRepository;
use App\Services\StatsException;
use App\Services\StatsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/stats')]
class StatsController extends AbstractController
{
    use UserableTrait;

    #[Route('/', name: 'stats.index', methods: ['GET'])]
    #[IsGranted('ROLE_STATS_SHOW')]
    public function index(InstanceRepository $instanceRepository): Response
    {
        $instances = $instanceRepository->findAll();
        $instancesButtons = [];

        foreach ($instances as $instance) {
            $instancesButtons[] = [
                'id' => $instance->getId(),
                'name' => $instance->getName(),
                'url' => $this->generateUrl('stats.byinstance', ['id' => $instance->getId()]),
                'status' => 'secondary',
                'loading' => true,
            ];
        }

        return $this->render('stats/index.html.twig', [
            'instances' => $instancesButtons,
        ]);
    }

    #[Route('/internal', name: 'stats.internal.index', methods: ['GET'])]
    #[IsGranted('ROLE_STATS_SHOW')]
    public function internalIndex(StatsService $statsService): Response
    {
        return $this->render('stats/internal/index.html.twig', [
            'stats' => $statsService->getInternalStats($this->getActivePeriod()),
        ]);
    }

    #[Route('/{id:instance}.json', name: 'stats.byinstance.json', methods: ['GET'])]
    #[IsGranted('ROLE_STATS_SHOW')]
    public function byInstanceJson(Instance $instance, StatsService $statsService): Response
    {
        try {
            $result = $statsService->getEnoriaStats($instance);
        } catch (StatsException $statsException) {
            return $this->json(['status' => 500, 'message' => $statsException->getMessage()], 500);
        }

        return $this->json($result['stats']);
    }

    #[Route('/{id:instance}', name: 'stats.byinstance', methods: ['GET'])]
    #[IsGranted('ROLE_STATS_SHOW')]
    public function byInstance(Instance $instance): Response
    {
        return $this->render('stats/byInstance.html.twig', [
            'instance' => $instance,
        ]);
    }
}
