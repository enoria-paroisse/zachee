<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Group;
use App\Entity\Person;
use App\Form\GroupAddType;
use App\Form\GroupType;
use App\Repository\GroupRepository;
use App\Repository\PersonRepository;
use App\Services\GroupService;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/volunteer')]
class VolunteerController extends AbstractController
{
    #[Route('/', name: 'volunteer.index', methods: ['GET'])]
    #[IsGranted('group_list_show')]
    public function index(GroupRepository $groupRepository, PersonRepository $personRepository): Response
    {
        return $this->render('volunteer/index.html.twig', [
            'groups' => $groupRepository->findBy([], ['name' => 'ASC']),
            'volunteers' => $personRepository->findAllVolunteers(),
        ]);
    }

    #[Route('/group/new', name: 'volunteer.group.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_VOLUNTEER_EDIT')]
    public function newGroup(Request $request, GroupRepository $groupRepository): Response
    {
        $group = new Group();
        $form = $this->createForm(GroupType::class, $group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $groupRepository->save($group, true);
            $this->addFlash('success', 'Le groupe a bien été ajouté.');

            return $this->redirectToRoute('volunteer.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('volunteer/new_group.html.twig', [
            'group' => $group,
            'form' => $form,
        ]);
    }

    #[Route('/group/{id}', name: 'volunteer.group.show', methods: ['GET'])]
    #[IsGranted('basic_show', subject: 'group')]
    public function showGroup(
        #[MapEntity(expr: 'repository.findByIdFullJoin(id)')]
        Group $group,
        GroupRepository $groupRepository
    ): Response {
        return $this->render('volunteer/show_group.html.twig', [
            'groups' => $groupRepository->findBy([], ['name' => 'ASC']),
            'group' => $group,
        ]);
    }

    #[Route('/group/{id:group}/edit', name: 'volunteer.group.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_VOLUNTEER_EDIT')]
    public function editGroup(Request $request, Group $group, GroupRepository $groupRepository): Response
    {
        $form = $this->createForm(GroupType::class, $group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $groupRepository->save($group, true);
            $this->addFlash('success', 'Le groupe a bien été modifié.');

            return $this->redirectToRoute('volunteer.group.show', ['id' => $group->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('volunteer/edit_group.html.twig', [
            'group' => $group,
            'form' => $form,
        ]);
    }

    #[Route('/group/{id:group}', name: 'volunteer.group.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_VOLUNTEER_EDIT')]
    public function deleteGroup(Request $request, Group $group, GroupRepository $groupRepository): Response
    {
        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('delete'.$group->getId(), $csrfToken)) {
            if (count($group->getPeople()) > 0) {
                $this->addFlash('warning', 'Impossible de supprimer un groupe qui n’est pas vide');

                return $this->redirectToRoute('volunteer.group.show', ['id' => $group->getId()], Response::HTTP_SEE_OTHER);
            }

            $groupRepository->remove($group, true);

            $this->addFlash('success', 'Le groupe a bien été supprimé.');
        }

        return $this->redirectToRoute('volunteer.index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/group/add/{id:group}', name: 'volunteer.group.add', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_VOLUNTEER_EDIT')]
    public function addToGroup(Request $request, Group $group, GroupRepository $groupRepository): Response
    {
        if (null !== $group->getAggregate()) {
            $this->addFlash(
                'error',
                'Vous ne pouvez pas modifier un groupe associé à un agrégat.'
            );

            return $this->redirectToRoute('volunteer.group.show', ['id' => $group->getId()], Response::HTTP_SEE_OTHER);
        }
        $form = $this->createForm(GroupAddType::class, null, ['group' => $group]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($form->get('members')->getData() as $person) {
                $group->addPerson($person);
            }
            $groupRepository->save($group, true);

            $this->addFlash('success', 'Des bénévoles ont bien été ajoutés.');

            return $this->redirectToRoute('volunteer.group.show', ['id' => $group->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('volunteer/add_to_group.html.twig', [
            'group' => $group,
            'form' => $form,
        ]);
    }

    #[Route('/group/{group}/add/{person}/', name: 'volunteer.group.add.quick', methods: ['GET'])]
    #[IsGranted('ROLE_VOLUNTEER_EDIT')]
    public function addQuickToGroup(Group $group, Person $person, GroupRepository $groupRepository): Response
    {
        if (null !== $group->getAggregate()) {
            $this->addFlash(
                'error',
                'Vous ne pouvez pas ajouter une personne à un groupe associé à un agrégat.'
            );
        } else {
            $group->addPerson($person);
            $groupRepository->save($group, true);
            $this->addFlash(
                'success',
                'Cette personne a bien été ajouté au groupe « '.$group.' »'
            );
        }

        return $this->redirectToRoute('person.show', ['id' => $person->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/group/{group}/remove/{person}', name: 'volunteer.group.remove', methods: ['DELETE'])]
    #[IsGranted('ROLE_VOLUNTEER_EDIT')]
    public function removeFromGroup(Request $request, Group $group, Person $person, GroupRepository $groupRepository): Response
    {
        if (null !== $group->getAggregate()) {
            $this->addFlash(
                'error',
                'Vous ne pouvez pas supprimer un groupe associé à un agrégat.'
            );

            return $this->redirectToRoute('volunteer.group.show', ['id' => $group->getId()], Response::HTTP_SEE_OTHER);
        }
        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('delete'.$person->getId(), $csrfToken)) {
            $group->removePerson($person);
            $groupRepository->save($group, true);

            $this->addFlash('success', 'Le bénévole a bien été retiré.');
        }

        return $this->redirectToRoute('volunteer.group.show', ['id' => $group->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/group/{id:group}/sync', name: 'volunteer.group.sync', methods: ['GET'])]
    #[IsGranted('ROLE_VOLUNTEER_EDIT')]
    public function sync(Group $group, GroupService $groupService): Response
    {
        if (null === $group->getAggregate()) {
            $this->addFlash(
                'error',
                'Vous ne pouvez synchroniser que les groupes liés à un agrégat.'
            );

            return $this->redirectToRoute('volunteer.group.show', ['id' => $group->getId()], Response::HTTP_SEE_OTHER);
        }

        try {
            $groupService->sync($group);
            $this->addFlash(
                'success',
                'Le groupe a été synchronisé avec l’agrégat '.$group->getAggregate().'.'
            );
        } catch (\Exception $e) {
            $this->addFlash(
                'error',
                'Le groupe n’a pas pu être synchronisé avec l’agrégat '.$group->getAggregate().'.'
            );
        }

        return $this->redirectToRoute('volunteer.group.show', ['id' => $group->getId()], Response::HTTP_SEE_OTHER);
    }
}
