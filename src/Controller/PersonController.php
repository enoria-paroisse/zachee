<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Controller\Traits\UserableTrait;
use App\Entity\Address;
use App\Entity\AssociatedPerson;
use App\Entity\CaMember;
use App\Entity\GeneralMeeting;
use App\Entity\Group;
use App\Entity\Membership;
use App\Entity\Payment;
use App\Entity\PaymentRequest;
use App\Entity\Person;
use App\Entity\User;
use App\Form\AssociatedOrganizationType;
use App\Form\DuplicateSearchType;
use App\Form\PersonType;
use App\Repository\CaMemberRepository;
use App\Repository\GroupRepository;
use App\Repository\PersonRepository;
use App\Repository\TypeAssociatedPersonRepository;
use App\Repository\UserRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class PersonController extends AbstractController
{
    use UserableTrait;

    public function __construct(private readonly PersonRepository $personRepository, private readonly EntityManagerInterface $em)
    {
    }

    #[Route('person/', name: 'person.index', methods: ['GET'])]
    #[IsGranted('ROLE_BASIC_SHOW')]
    public function index(): Response
    {
        return $this->render('person/index.html.twig', [
            'people' => $this->personRepository->findAllWithMembershipAndOrganizationsByPeriod($this->getActivePeriod()),
        ]);
    }

    #[Route('person/deletable', name: 'person.deletable', methods: ['GET'])]
    #[IsGranted('ROLE_PERSON_EDIT')]
    public function deletable(): Response
    {
        return $this->render('person/deletable.html.twig', [
            'people' => $this->personRepository->findAllDeletable(),
        ]);
    }

    #[Route('person/show/{id}', name: 'person.show', methods: ['GET'])]
    public function show(
        #[MapEntity(expr: 'repository.findByIdFullJoin(id)')]
        Person $person,
        Request $request,
        CaMemberRepository $caMemberRepository,
        GroupRepository $groupRepository,
    ): Response {
        $this->denyAccessUnlessGranted('basic_show', $person);

        $paymentRequests = $person->getPaymentRequests()->toArray();
        $actualMembership = null;
        foreach ($person->getMemberships() as $membership) {
            if ($membership->getPeriod() === $this->getActivePeriod()) {
                $actualMembership = $membership;
            }
            if (null !== $membership->getPaymentRequest()) {
                $paymentRequests[] = $membership->getPaymentRequest();
            }
        }

        return $this->render('person/show.html.twig', [
            'person' => $person,
            'caMembers' => $caMemberRepository->findByPerson($person),
            'referer' => $request->headers->get('referer'),
            'paymentRequests' => $paymentRequests,
            'payments' => $person->getPayments(),
            'groups' => $groupRepository->findBy(['aggregate' => null]),
            'actualMembership' => $actualMembership,
        ]);
    }

    #[Route('person/create', name: 'person.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_PERSON_EDIT')]
    public function new(Request $request): Response
    {
        $person = new Person();
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($person);
            $this->em->flush();
            $this->addFlash('success', 'La personne a bien été ajoutée');

            return $this->redirectToRoute('person.show', ['id' => $person->getId()]);
        }

        return $this->render('person/new.html.twig', [
            'person' => $person,
            'form' => $form->createView(),
            'referer' => $request->headers->get('referer'),
        ]);
    }

    #[Route('person/edit/{id}', name: 'person.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_PERSON_EDIT')]
    public function edit(
        #[MapEntity(expr: 'repository.findByIdFullJoin(id)')]
        Person $person,
        Request $request,
    ): Response {
        $form = $this->createForm(PersonType::class, $person, ['delete_address' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (null !== $person->getAddress()) {
                if (true === $form->get('deleteAddress')->getData()) {
                    $this->em->remove($person->getAddress());
                    $person->setAddress(null);
                } else {
                    $this->em->persist($person->getAddress());
                }
            }
            $this->em->flush();
            $this->addFlash('success', 'La personne a bien été modifiée');

            return $this->redirectToRoute('person.show', ['id' => $person->getId()]);
        }

        return $this->render('person/edit.html.twig', [
            'person' => $person,
            'form' => $form->createView(),
            'referer' => $request->headers->get('referer'),
        ]);
    }

    #[Route('person/delete/{id}', name: 'person.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_PERSON_EDIT')]
    public function delete(
        #[MapEntity(expr: 'repository.findByIdFullJoin(id)')]
        Person $person,
        Request $request,
        CaMemberRepository $caMemberRepository,
        UserRepository $userRepository
    ): Response {
        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('delete'.$person->getId(), $csrfToken)) {
            if (0 !== count($caMemberRepository->findByPerson($person))) {
                $this->addFlash('error', 'Impossible de supprimer cette personne, car elle est membre du conseil d’administration');
            } elseif (null !== $userRepository->findOneByPerson($person)) {
                $this->addFlash('error', 'Impossible de supprimer cette personne, car un utilisateur lui est associé');
            } else {
                $this->em->remove($person);

                try {
                    $this->em->flush();
                    $this->addFlash('success', 'La personne a bien été supprimée');

                    return $this->redirectToRoute('person.deletable');
                } catch (ForeignKeyConstraintViolationException) {
                    $this->addFlash('error', 'Impossible de supprimer cette personne : elle est liée à des données (inscriptions, comptabilité, ...)');
                }
            }
        }

        return $this->redirectToRoute('person.show', ['id' => $person->getId()]);
    }

    #[Route('/associate/{id}', name: 'person.associate', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ORGANIZATION_EDIT')]
    public function associate(
        #[MapEntity(expr: 'repository.findByIdFullJoin(id)')]
        Person $person,
        Request $request,
        TypeAssociatedPersonRepository $typeAssociatedPersonRepository,
    ): Response {
        $associatedPerson = new AssociatedPerson();
        $associatedPerson->setPerson($person);

        $form = $this->createForm(AssociatedOrganizationType::class, $associatedPerson);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($associatedPerson);
            $this->em->flush();
            $this->addFlash('success', 'La personne a bien été associée');

            return $this->redirectToRoute('person.show', ['id' => $person->getId()]);
        }

        return $this->render('person/associate.html.twig', [
            'person' => $person,
            'form' => $form->createView(),
            'types' => $typeAssociatedPersonRepository->findAll(),
        ]);
    }

    #[Route('person/duplicates', name: 'person.duplicates', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function duplicates(Request $request): Response
    {
        /** @var Person[] $people */
        $people = [];

        $formSearch = $this->createForm(DuplicateSearchType::class);
        $formSearch->handleRequest($request);

        if ($formSearch->isSubmitted() && $formSearch->isValid()) {
            $people = $this->personRepository->findDuplicateByParams($request->request->all('duplicate_search'));
        }

        return $this->render('person/duplicates.html.twig', [
            'people' => $people,
            'formSearch' => $formSearch->createView(),
        ]);
    }

    #[Route('person/fusion', name: 'person.fusion', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function fusion(Request $request, PersonRepository $personRepository, CaMemberRepository $caMemberRepository): Response
    {
        /** @var null|string $submittedToken */
        $submittedToken = $request->request->get('token', null);

        /** @var string $peopleList */
        $peopleList = $request->request->get('people', '');
        if (null !== $submittedToken && $this->isCsrfTokenValid('fusion', $submittedToken) && 0 === count($request->query->all('selected-people'))) {
            $people = $personRepository->findDuplicateByIds(explode(',', $peopleList));

            if (1 < count($people)) {
                /** @var Person[] $peopleById */
                $peopleById = [];

                /** @var null|User $user */
                $user = null;

                /** @var AssociatedPerson[][] $associatedOrganizations */
                $associatedOrganizations = [];

                /** @var CaMember[][] $caMembers */
                $caMembers = [];

                /** @var Group[][] $groups */
                $groups = [];

                /** @var GeneralMeeting[][] $generalMeetings */
                $generalMeetings = [];

                /** @var Membership[][] $memberships */
                $memberships = [];

                /** @var PaymentRequest[][] $paymentRequests */
                $paymentRequests = [];

                /** @var Payment[][] $payments */
                $payments = [];
                foreach ($people as $person) {
                    $peopleById[$person->getId()] = $person;
                    if (null !== $person->getUser()) {
                        $user = $person->getUser();
                    }
                    $associatedOrganizations[] = $person->getAssociatedOrganizations()->toArray();
                    $caMembers[] = $caMemberRepository->findByPerson($person);
                    $groups[] = $person->getMemberGroups()->toArray();
                    $generalMeetings[] = $person->getGeneralMeetings()->toArray();
                    $memberships[] = $person->getMemberships()->toArray();
                    $paymentRequests[] = $person->getPaymentRequests()->toArray();
                    $payments[] = $person->getPayments()->toArray();
                }

                $keep = $people[0];

                /** @var AssociatedPerson $associatedOrganization */
                foreach (array_merge(...$associatedOrganizations) as $associatedOrganization) {
                    if ($keep !== $associatedOrganization->getPerson()) {
                        $newAssociation = new AssociatedPerson();
                        $newAssociation->setPerson($keep)
                            ->setOrganization($associatedOrganization->getOrganization())
                            ->setType($associatedOrganization->getType())
                        ;
                        $this->em->persist($newAssociation);
                    }
                }

                /** @var CaMember $caMember */
                foreach (array_merge(...$caMembers) as $caMember) {
                    $caMember->setPerson($keep);
                    $this->em->persist($caMember);
                }

                /** @var Group $group */
                foreach (array_unique(array_merge(...$groups)) as $group) {
                    $keep->addMemberGroup($group);
                }

                /** @var GeneralMeeting $generalMeeting */
                foreach (array_merge(...$generalMeetings) as $generalMeeting) {
                    $keep->addGeneralMeeting($generalMeeting);
                }

                /** @var Membership $membership */
                foreach (array_merge(...$memberships) as $membership) {
                    $membership->setPerson($keep);
                    $this->em->persist($membership);
                }

                /** @var PaymentRequest $paymentRequest */
                foreach (array_merge(...$paymentRequests) as $paymentRequest) {
                    $paymentRequest->setPerson($keep);
                    $this->em->persist($paymentRequest);
                }

                /** @var Payment $payment */
                foreach (array_merge(...$payments) as $payment) {
                    $payment->setPerson($keep);
                    $this->em->persist($payment);
                }

                $this->em->persist($keep);
                $this->em->flush();

                $title = $request->request->get('fusion-title', null);
                // @phpstan-ignore-next-line instanceof.alwaysTrue
                if (null !== $title && $peopleById[$title] instanceof Person && null !== $peopleById[$title]->getTitle()) {
                    $keep->setTitle($peopleById[$title]->getTitle());
                }
                $lastname = $request->request->get('fusion-lastname', null);
                // @phpstan-ignore-next-line instanceof.alwaysTrue
                if (null !== $lastname && $peopleById[$lastname] instanceof Person && null !== $peopleById[$lastname]->getLastname()) {
                    $keep->setLastname($peopleById[$lastname]->getLastname());
                }
                $firstname = $request->request->get('fusion-firstname', null);
                // @phpstan-ignore-next-line instanceof.alwaysTrue
                if (null !== $firstname && $peopleById[$firstname] instanceof Person && null !== $peopleById[$firstname]->getFirstname()) {
                    $keep->setFirstname($peopleById[$firstname]->getFirstname());
                }
                $sex = $request->request->get('fusion-sex', null);
                // @phpstan-ignore-next-line instanceof.alwaysTrue
                if (null !== $sex && $peopleById[$sex] instanceof Person && null !== $peopleById[$sex]->getSex()) {
                    $keep->setSex($peopleById[$sex]->getSex());
                }
                $email = $request->request->get('fusion-email', null);
                // @phpstan-ignore-next-line instanceof.alwaysTrue
                if (null !== $email && $peopleById[$email] instanceof Person) {
                    $keep->setEmail($peopleById[$email]->getEmail());
                }
                $phone = $request->request->get('fusion-phone', null);
                // @phpstan-ignore-next-line instanceof.alwaysTrue
                if (null !== $phone && $peopleById[$phone] instanceof Person) {
                    $keep->setPhone($peopleById[$phone]->getPhone());
                }
                $address = $request->request->get('fusion-address', null);
                // @phpstan-ignore-next-line instanceof.alwaysTrue
                if (null !== $address && $peopleById[$address] instanceof Person) {
                    $newAddress = $peopleById[$address]->getAddress();
                    $oldAddress = $keep->getAddress();
                    if (null !== $newAddress) {
                        if (null === $oldAddress) {
                            $oldAddress = new Address();
                        }
                        $oldAddress
                            ->setAddressLineA($newAddress->getAddressLineA() ?? '')
                            ->setAddressLineB($newAddress->getAddressLineB() ?? '')
                            ->setPostalCode($newAddress->getPostalCode() ?? '')
                            ->setCity($newAddress->getCity() ?? '')
                            ->setCountry($newAddress->getCountry() ?? '')
                        ;
                        $this->em->persist($oldAddress);
                    } else {
                        if (null !== $oldAddress) {
                            $this->em->remove($oldAddress);
                        }
                        $keep->setAddress(null);
                    }
                }

                $keep->setUser($user);
                $user?->setPerson($keep);

                $this->em->persist($keep);
                foreach ($people as $person) {
                    if ($keep !== $person) {
                        $this->em->remove($person);
                    }
                }
                $this->em->flush();

                $this->addFlash(
                    'success',
                    'Les contacts ont été fusionnés'
                );

                return $this->redirectToRoute('person.duplicates');
            }
        }

        $people = $personRepository->findDuplicateByIds($request->query->all('selected-people'));

        if (2 > count($people)) {
            $this->addFlash(
                'warning',
                'Veuillez sélectionner au moins deux contacts à fusionner'
            );

            return $this->redirectToRoute('person.duplicates');
        }

        if (1 < array_reduce($people, static function ($carry, $item) {
            if (null !== $item->getUser()) {
                ++$carry;
            }

            return $carry;
        }, 0)) {
            $this->addFlash(
                'warning',
                'Il y a plus d’une personne sélectionnée associée à un utilisateur'
            );

            return $this->redirectToRoute('person.duplicates');
        }

        $caMembers = [];
        foreach ($people as $person) {
            $caMembers[$person->getId()] = $caMemberRepository->findByPerson($person);
        }

        return $this->render('person/fusion.html.twig', [
            'people' => $people,
            'caMembers' => $caMembers,
        ]);
    }
}
