<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Application;
use App\Entity\Doc;
use App\Entity\Membership;
use App\Entity\PaymentRequestFile;
use App\Entity\Receipt;
use App\Entity\Summons;
use App\Services\UploaderService;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/uploads')]
class ShowUploadsController extends AbstractController
{
    #[Route('/doc/{file:doc}', name: 'show_uploads.doc', methods: ['GET'])]
    #[IsGranted('show_uploaded_doc', 'doc')]
    public function doc(Doc $doc, UploaderService $uploaderService): Response
    {
        return new BinaryFileResponse($uploaderService->getTargetDirectory().'/doc/'.$doc->getFile());
    }

    #[Route('/sheets/{sheet:membership}', name: 'show_uploads.sheet', methods: ['GET'])]
    #[IsGranted(new Expression('is_granted("membership_show", subject) or is_granted("show_uploaded_sheet", subject)'), 'membership')]
    public function sheet(Membership $membership, UploaderService $uploaderService): Response
    {
        return new BinaryFileResponse($uploaderService->getTargetDirectory().'/sheets/'.$membership->getSheet());
    }

    #[Route('/mandates/{fileMandate:summons}', name: 'show_uploads.mandate', methods: ['GET'])]
    #[IsGranted('ROLE_GENERAL_MEETING_SHOW')]
    public function mandate(Summons $summons, UploaderService $uploaderService): Response
    {
        return new BinaryFileResponse($uploaderService->getTargetDirectory().'/mandates/'.$summons->getFileMandate());
    }

    #[Route('/paymentRequests/{file}', name: 'show_uploads.payment_request', methods: ['GET'])]
    #[IsGranted('accounting_show', subject: new Expression('args["file"].getPaymentRequest()'))]
    public function paymentRequest(
        #[MapEntity(mapping: ['file' => 'file'])]
        PaymentRequestFile $file,
        UploaderService $uploaderService
    ): Response {
        return new BinaryFileResponse($uploaderService->getTargetDirectory().'/paymentRequests/'.$file->getFile());
    }

    #[Route('/receipts/{file:receipt}', name: 'show_uploads.receipt', methods: ['GET'])]
    #[IsGranted(new Expression('is_granted("accounting_show", subject) or is_granted("show_uploaded_receipt", subject)'), subject: new Expression('args["receipt"].getPayment()'))]
    public function receipt(Receipt $receipt, UploaderService $uploaderService): Response
    {
        return new BinaryFileResponse($uploaderService->getTargetDirectory().'/receipts/'.$receipt->getFile());
    }

    #[Route('/application-icons/{icon:application}', name: 'show_uploads.application-icons', methods: ['GET'])]
    public function applicationIcon(Application $application, UploaderService $uploaderService): Response
    {
        return new BinaryFileResponse($uploaderService->getTargetDirectory().'/application-icons/'.$application->getIcon());
    }
}
