<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Accounting;

use App\Controller\Traits\UserableTrait;
use App\Entity\Payment;
use App\Entity\Receipt;
use App\Mailer\AccountingMailer;
use App\Mailer\ReceiptException;
use App\Repository\ReceiptRepository;
use App\Services\ReceiptService;
use App\Services\UploaderService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/accounting/receipt')]
class ReceiptController extends AbstractController
{
    use UserableTrait;

    public function __construct(
        private readonly ReceiptService $receiptService,
        private readonly UploaderService $uploaderService,
        private readonly AccountingMailer $accountingMailer
    ) {
    }

    #[Route('/new/{id:payment}', name: 'accounting.receipt.new', methods: ['POST'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function new(Request $request, Payment $payment, ReceiptRepository $receiptRepository): Response
    {
        $prefix = $request->request->getInt('prefix');
        $number = $request->request->getInt('number');

        if (!is_numeric($request->request->get('prefix')) || !is_numeric($request->request->get('number'))) {
            $this->addFlash('warning', 'Le préfixe et le numéro d’un reçu doivent être un entier');
        } elseif (count($receiptRepository->findBy(['refPrefix' => $prefix, 'refNumber' => $number])) > 0) {
            $this->addFlash('warning', 'Cette combinaison de numéro et de préfixe est déjà attribuée à un reçu');
        } else {
            $receipt = new Receipt();
            $receipt->setPayment($payment);
            $receipt->setRefPrefix($prefix);
            $receipt->setRefNumber($number);

            $receiptRepository->save($receipt, true);
            $this->addFlash('success', 'Le reçu a bien été ajouté.');

            if ('on' === $request->request->get('upload', 'off')) {
                $this->uploadReceipt($request->files->get('receiptFile'), $receipt, $receiptRepository);
            } elseif ('off' === $request->request->get('upload', 'off')) {
                $this->generate($receipt);
            }

            if ('on' === $request->request->get('sendMail', 'off')) {
                $this->send($receipt);
            }
        }

        return $this->redirectToRoute('accounting.payment.show', ['id' => $payment->getId()]);
    }

    #[Route('/edit/{id:receipt}', name: 'accounting.receipt.update', methods: ['POST'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function edit(Request $request, Receipt $receipt, ReceiptRepository $receiptRepository): Response
    {
        $prefix = $request->request->getInt('prefix');
        $number = $request->request->getInt('number');

        if (!is_numeric($request->request->get('prefix')) || !is_numeric($request->request->get('number'))) {
            $this->addFlash('warning', 'Le préfixe et le numéro d’un reçu doivent être un entier');
        } elseif (($prefix !== $receipt->getRefPrefix() || $number !== $receipt->getRefNumber()) && count($receiptRepository->findBy(['refPrefix' => $prefix, 'refNumber' => $number])) > 0) {
            $this->addFlash('warning', 'Cette combinaison de numéro et de préfixe est sans doute déjà attribuée à un reçu');
        } else {
            $receipt->setRefPrefix($prefix);
            $receipt->setRefNumber($number);

            $receiptRepository->save($receipt, true);
            $this->addFlash('success', 'Le reçu a bien été modifié.');

            if ('on' === $request->request->get('upload', 'off')) {
                $this->uploadReceipt($request->files->get('receiptFile'), $receipt, $receiptRepository);
            } elseif ('off' === $request->request->get('upload', 'off')) {
                $this->generate($receipt, true);
            }

            if ('on' === $request->request->get('sendMail', 'off')) {
                $this->send($receipt, true);
            }
        }

        return $this->redirectToRoute('accounting.payment.show', ['id' => $receipt->getPayment()?->getId()]);
    }

    #[Route('/show/{id:receipt}', name: 'accounting.receipt.show', methods: ['GET'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function show(Receipt $receipt): Response
    {
        return $this->redirectToRoute('accounting.payment.show', ['id' => $receipt->getPayment()?->getId()]);
    }

    #[Route('/archive', name: 'accounting.receipt.archive', methods: ['GET'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function archive(ReceiptRepository $receiptRepository): Response
    {
        $tempFile = tempnam(sys_get_temp_dir(), 'zachee_receipts');
        $zip = new \ZipArchive();

        if (false === $tempFile) {
            $this->addFlash('warning', 'Impossible de créer l’archive : le fichier temporaire ne peut être créé');

            return $this->redirectToRoute('accounting.receipt');
        }

        $res = $zip->open($tempFile, \ZipArchive::OVERWRITE);
        $receipts = $receiptRepository->findByPeriod($this->getActivePeriod());

        if (true === $res && count($receipts) > 0) {
            try {
                foreach ($receipts as $receipt) {
                    if ($receipt->getFile()) {
                        $zip->addFile(
                            $this->getParameter('directory.upload').'/receipts/'.$receipt->getFile(),
                            'r'.str_replace(' ', '', $receipt->getRef() ?? '').'.pdf',
                        );
                    }
                }
                $zip->close();

                $response = new BinaryFileResponse($tempFile);
                $response->headers->set('Content-Type', 'application/zip');
                $response->setContentDisposition(
                    ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    'receipts_'.$this->getActivePeriod()->getName().'.zip',
                );

                return $response;
            } catch (\ErrorException) {
                $this->addFlash('warning', 'Impossible de créer l’archive : un fichier semble manquant');

                return $this->redirectToRoute('accounting.receipt');
            }
        }

        $this->addFlash('warning', 'Impossible de créer l’archive'.(0 === count($receipts) ? ' : aucun fichier pour cette période' : ''));

        return $this->redirectToRoute('accounting.receipt');
    }

    private function generate(Receipt $receipt, bool $update = false): void
    {
        $this->receiptService->generatePdfAndSave($receipt);
        $this->addFlash('success', 'Le reçu a bien été '.($update ? 're' : '').'généré.');
    }

    private function uploadReceipt(?UploadedFile $uploaded, Receipt $receipt, ReceiptRepository $receiptRepository): void
    {
        if ($uploaded && 'application/pdf' !== $uploaded->getMimeType()) {
            $this->addFlash('warning', 'Le fichier doit être un PDF.');
        } elseif ($uploaded) {
            $receipt->setFile($this->uploaderService->upload($uploaded, 'receipts', $receipt->getFile()));
            $receiptRepository->save($receipt, true);
            $this->addFlash('success', 'Le fichier a bien été enregistré.');
        }
    }

    private function send(Receipt $receipt, bool $update = false): void
    {
        try {
            $this->accountingMailer->sendReceipt($receipt, $this->receiptService, $update);
            $this->addFlash(
                'success',
                'Le mail avec le reçu a été envoyé avec succès'
            );
        } catch (TransportExceptionInterface $e) {
            $this->addFlash(
                'error',
                'Une erreur a eu lieu lors de l’envoi du mail : '.$e->getMessage(),
            );
        } catch (ReceiptException $e) {
            $this->addFlash(
                'error',
                'Une erreur a eu lieu lors de l’envoi du mail : impossible de joindre le reçu',
            );
        }
    }
}
