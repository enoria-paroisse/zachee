<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Accounting;

use App\Entity\Payment;
use App\Entity\PaymentAllocation;
use App\Entity\PaymentRequest;
use App\Form\PaymentType;
use App\Repository\PaymentAllocationRepository;
use App\Repository\PaymentRepository;
use App\Repository\PaymentRequestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Workflow\Exception\LogicException;
use Symfony\Component\Workflow\WorkflowInterface;

#[Route('/accounting/payment')]
class PaymentController extends AbstractController
{
    public function __construct(private WorkflowInterface $paymentStateMachine)
    {
    }

    #[Route('/new', name: 'accounting.payment.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function new(Request $request, PaymentRepository $paymentRepository): Response
    {
        $payment = new Payment();
        $payment->setDatetime(new \DateTime('now'))->setStatus(Payment::STATUS_REGISTERED);
        $form = $this->createForm(PaymentType::class, $payment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $paymentRepository->save($payment, true);
            $this->addFlash('success', 'Le paiement a bien été ajouté.');

            return $this->redirectToRoute('accounting.payment.show', ['id' => $payment->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('accounting/payment/new.html.twig', [
            'payment' => $payment,
            'form' => $form,
        ]);
    }

    #[Route('/{id:payment}', name: 'accounting.payment.show', methods: ['GET'])]
    #[IsGranted('accounting_show', subject: 'payment')]
    public function show(Payment $payment): Response
    {
        return $this->render('accounting/payment/show.html.twig', [
            'payment' => $payment,
        ]);
    }

    #[Route('/{id:payment}/edit', name: 'accounting.payment.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function edit(Request $request, Payment $payment, PaymentRepository $paymentRepository): Response
    {
        if (Payment::STATUS_VALIDATED === $payment->getStatus()) {
            $this->addFlash('warning', 'Un paiement validé ne peut pas être modifié.');

            return $this->redirectToRoute('accounting.payment.show', ['id' => $payment->getId()], Response::HTTP_SEE_OTHER);
        }

        $form = $this->createForm(PaymentType::class, $payment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $paymentRepository->save($payment, true);
            $this->addFlash('success', 'Le paiement a bien été modifié.');

            return $this->redirectToRoute('accounting.payment.show', ['id' => $payment->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('accounting/payment/edit.html.twig', [
            'payment' => $payment,
            'form' => $form,
        ]);
    }

    #[Route('/cancel/{id:payment}', name: 'accounting.payment.cancel', methods: ['GET'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function cancel(Payment $payment, PaymentRepository $paymentRepository): Response
    {
        try {
            $this->paymentStateMachine->apply($payment, 'cancel');
            $paymentRepository->save($payment, true);
            $this->addFlash('success', 'Le paiement a bien été annulé.');
        } catch (LogicException) {
            $this->addFlash('warning', 'Seul un paiement non validé peut être annulé.');
        }

        return $this->redirectToRoute('accounting.payment.show', ['id' => $payment->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/invalidate/{id:payment}', name: 'accounting.payment.invalidate', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function invalidate(Payment $payment, PaymentRepository $paymentRepository): Response
    {
        try {
            $payment->setStatus(Payment::STATUS_ALLOCATED);
            $paymentRepository->save($payment, true);
            $this->addFlash('success', 'Le paiement a bien été annulé.');
        } catch (LogicException) {
            $this->addFlash('warning', 'Seul un paiement validé peut être invalidé.');
        }

        return $this->redirectToRoute('accounting.payment.show', ['id' => $payment->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/restore/{id:payment}', name: 'accounting.payment.restore', methods: ['GET'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function restore(Payment $payment, PaymentRepository $paymentRepository): Response
    {
        try {
            $this->paymentStateMachine->apply($payment, 'restore');
            $paymentRepository->save($payment, true);
            $this->addFlash('success', 'Le paiement a bien été restauré.');
        } catch (LogicException) {
            $this->addFlash('warning', 'Seul un paiement annulé peut être restauré.');
        }

        return $this->redirectToRoute('accounting.payment.show', ['id' => $payment->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/allocate-donation/{id:payment}', name: 'accounting.payment.allocate-donation', methods: ['GET'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function allocateDonation(
        Payment $payment,
        PaymentRequestRepository $paymentRequestRepository,
        PaymentAllocationRepository $paymentAllocationRepository
    ): Response {
        $result = [
            'payment' => [
                'id' => $payment->getId(),
            ],
        ];

        if ($payment->getAmountAllocated() >= $payment->getAmount()) {
            $result['error'] = 'Un paiement totalement alloué ne peut être affecté à un don.';

            return $this->json($result, 403);
        }
        if (Payment::STATUS_REGISTERED !== $payment->getStatus()) {
            $result['error'] = 'Un paiement validé, annulé ou totalement alloué ne peut être affecté à un don.';

            return $this->json($result, 403);
        }

        $result['donationAmount'] = (string) ((int) $payment->getAmount() - (int) $payment->getAmountAllocated());

        $paymentRequest = new PaymentRequest();
        $paymentRequest
            ->setAmount($result['donationAmount'])
            ->setPerson($payment->getPerson())
            ->setOrganization($payment->getOrganization())
            ->setDatetime(new \DateTime())
            ->setDescription('Don')
        ;
        $paymentRequestRepository->save($paymentRequest);
        $paymentAllocation = new PaymentAllocation();
        $paymentAllocation
            ->setPayment($payment)
            ->setPaymentRequest($paymentRequest)
            ->setAmount($result['donationAmount'])
        ;
        $paymentAllocationRepository->save($paymentAllocation, true);

        $result['success'] = true;

        return $this->json($result, 200);
    }
}
