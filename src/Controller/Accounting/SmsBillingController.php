<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Accounting;

use App\Controller\Traits\UserableTrait;
use App\Entity\PaymentRequest;
use App\Entity\SmsBilling;
use App\Form\SmsBillingPaymentRequestType;
use App\Repository\InstanceRepository;
use App\Repository\PaymentRequestRepository;
use App\Repository\SmsBillingRepository;
use App\Services\EnoriaException;
use App\Services\EnoriaService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/accounting/sms')]
class SmsBillingController extends AbstractController
{
    use UserableTrait;

    #[Route('/', name: 'accounting.sms.index', methods: ['GET'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function index(InstanceRepository $instanceRepository, SmsBillingRepository $smsBillingRepository): Response
    {
        $instances = $instanceRepository->findSmsBillingEnabled();

        return $this->render('accounting/sms/index.html.twig', [
            'instances' => $instances,
            'smsBilling' => $smsBillingRepository->findAll(),
        ]);
    }

    #[Route('/sync', name: 'accounting.sms.sync', methods: ['GET'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function sync(InstanceRepository $instanceRepository, EnoriaService $enoria): Response
    {
        $instances = $instanceRepository->findAll();
        foreach ($instances as $instance) {
            try {
                $enoria->syncSms($instance);
                $this->addFlash(
                    'success',
                    'Synchronisation des rapprochements SMS avec l’instance '.$instance->getName().' réussie',
                );
            } catch (EnoriaException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage(),
                );
            } catch (\Exception $e) {
                $this->addFlash(
                    'error',
                    sprintf(
                        'Synchronisation des rapprochements SMS avec l’instance %s échouée (%s)',
                        $instance->getName(),
                        $e->getMessage(),
                    ),
                );
            }
        }

        return $this->redirectToRoute('accounting.sms.index');
    }

    #[Route('/associate/{id:smsBilling}', name: 'accounting.sms.associate', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function associate(SmsBilling $smsBilling, Request $request, SmsBillingRepository $smsBillingRepository): Response
    {
        if ($smsBilling->getPaymentRequest()) {
            $this->addFlash('error', 'Une demande de paiement est déjà associée à cette facturation SMS');

            return $this->redirectToRoute('accounting.sms.index');
        }

        $form = $this->createForm(
            SmsBillingPaymentRequestType::class,
            $smsBilling,
            ['period' => $this->getAppUser()->getPeriod()]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var null|PaymentRequest $paymentRequest */
            $paymentRequest = $smsBilling->getPaymentRequest();
            if ($paymentRequest && $paymentRequest->getMembership()) {
                $this->addFlash('error', 'Cette demande de paiement ne peut pas être associée à la facturation SMS : elle concerne une adhésion');
            } elseif ($paymentRequest && $paymentRequest->getDioceseMembershipSupplement()) {
                $this->addFlash('error', 'Cette demande de paiement ne peut pas être associée à la facturation SMS : elle concerne un complément d’adhésion');
            } else {
                $smsBillingRepository->save($smsBilling, true);
                $this->addFlash('success', 'La demande de paiement a bien été associée à la facturation SMS');

                return $this->redirectToRoute('accounting.sms.index');
            }
        }

        return $this->render('accounting/sms/associate.html.twig', [
            'smsBilling' => $smsBilling,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/dissociate/{id:smsBilling}', name: 'accounting.sms.dissociate', methods: ['DELETE'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function dissociate(SmsBilling $smsBilling, Request $request, SmsBillingRepository $smsBillingRepository): Response
    {
        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('delete'.$smsBilling->getId(), $csrfToken)) {
            $smsBilling->setPaymentRequest(null);
            $smsBillingRepository->save($smsBilling, true);
            $this->addFlash('success', 'La demande de paiement a bien été dissociée de la facturation SMS');
        }

        return $this->redirectToRoute('accounting.sms.index');
    }

    #[Route('/create/{id:smsBilling}', name: 'accounting.sms.create', methods: ['GET', 'PUT'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function create(SmsBilling $smsBilling, Request $request, PaymentRequestRepository $paymentRequestRepository): Response
    {
        if ($smsBilling->getPaymentRequest()) {
            $this->addFlash('error', 'Une demande de paiement est déjà associée à cette facturation SMS');

            return $this->redirectToRoute('accounting.sms.index');
        }

        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('create'.$smsBilling->getId(), $csrfToken)) {
            if ($smsBilling->getEntity() && $smsBilling->getEntity()->getMemberships()->last() && $smsBilling->getEntity()->getMemberships()->last()->getOrganization()) {
                $paymentRequest = new PaymentRequest();
                $paymentRequest->setAmount(($smsBilling->getTotal() * 0.07).'')
                    ->setDatetime($smsBilling->getDate() ?? new \DateTime())
                    ->setSmsBilling($smsBilling)
                    ->setDescription('Remboursement SMS - '.$smsBilling->getEntity()->getInstance().' - '.$smsBilling->getEntity())
                    ->setOrganization($smsBilling->getEntity()->getMemberships()->last()->getOrganization())
                    ->setStatus(PaymentRequest::STATUS_IN_PROGRESS)
                ;
                $paymentRequestRepository->save($paymentRequest, true);

                $this->addFlash('success', 'La demande de paiement a bien été dissociée de la facturation SMS');

                return $this->redirectToRoute('accounting.payment_request.show', ['id' => $paymentRequest->getId()]);
            }
            $this->addFlash('error', 'L’entité associée à cette facturation SMS n’est associée à aucune adhésion et/ou aucune organisation');
        }

        return $this->render('accounting/sms/create.html.twig', [
            'smsBilling' => $smsBilling,
        ]);
    }
}
