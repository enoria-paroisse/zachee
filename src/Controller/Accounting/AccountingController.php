<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Accounting;

use App\Controller\Traits\UserableTrait;
use App\Entity\Payment;
use App\Entity\PaymentRequest;
use App\Repository\PaymentRepository;
use App\Repository\PaymentRequestRepository;
use App\Repository\ReceiptRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/accounting')]
class AccountingController extends AbstractController
{
    use UserableTrait;

    #[Route('/', name: 'accounting.index', methods: ['GET'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function index(PaymentRequestRepository $paymentRequestRepository, PaymentRepository $paymentRepository): Response
    {
        return $this->render('accounting/index.html.twig', [
            'needPayments' => $paymentRequestRepository->findByStatus(PaymentRequest::STATUS_IN_PROGRESS),
            'unallocatedPayments' => $paymentRepository->findByStatus(Payment::STATUS_REGISTERED),
            'allocatedPayments' => $paymentRepository->findByPeriodAndStatus($this->getActivePeriod(), Payment::STATUS_ALLOCATED),
            'validatedPayments' => $paymentRepository->findByPeriodAndStatus($this->getActivePeriod(), Payment::STATUS_VALIDATED),
            'completedPayments' => $paymentRequestRepository->findByPeriodAndStatus($this->getActivePeriod(), PaymentRequest::STATUS_COMPLETED),
            'canceledPaymentRequests' => $paymentRequestRepository->findByPeriodAndStatus($this->getActivePeriod(), PaymentRequest::STATUS_CANCELED),
            'canceledPayments' => $paymentRepository->findByPeriodAndStatus($this->getActivePeriod(), Payment::STATUS_CANCELED),
        ]);
    }

    #[Route('/receipt', name: 'accounting.receipt', methods: ['GET'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function receipt(ReceiptRepository $receiptRepository, PaymentRepository $paymentRepository): Response
    {
        return $this->render('accounting/receipt.html.twig', [
            'receipts' => $receiptRepository->findByPeriod($this->getActivePeriod()),
            'missingReceipts' => $paymentRepository->findMissingReceipt(),
            'missingFiles' => $receiptRepository->findMissingFile(),
        ]);
    }
}
