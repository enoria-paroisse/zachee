<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Accounting;

use App\Controller\Traits\UserableTrait;
use App\Entity\Diocese;
use App\Entity\DioceseMembershipSupplement;
use App\Entity\PaymentRequest;
use App\Entity\PaymentRequestFile;
use App\Form\PaymentRequestType;
use App\Mailer\AccountingMailer;
use App\Mailer\PaymentRequestException;
use App\Repository\DioceseMembershipSupplementRepository;
use App\Repository\PaymentRequestFileRepository;
use App\Repository\PaymentRequestRepository;
use App\Services\MembershipFeeService;
use App\Services\PaymentRequestService;
use App\Services\UploaderService;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/accounting/payment_request')]
class PaymentRequestController extends AbstractController
{
    use UserableTrait;

    public function __construct(
        private readonly PaymentRequestService $paymentRequestService,
        private readonly UploaderService $uploaderService,
        private readonly AccountingMailer $accountingMailer
    ) {
    }

    #[Route('/new', name: 'accounting.payment_request.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function new(Request $request, PaymentRequestRepository $paymentRequestRepository): Response
    {
        $paymentRequest = new PaymentRequest();
        $paymentRequest->setDatetime(new \DateTime('now'))->setStatus(PaymentRequest::STATUS_IN_PROGRESS);
        $form = $this->createForm(PaymentRequestType::class, $paymentRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $paymentRequestRepository->save($paymentRequest, true);
            $this->addFlash('success', 'La demande de paiement a bien été ajoutée.');

            return $this->redirectToRoute('accounting.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('accounting/payment_request/new.html.twig', [
            'payment_request' => $paymentRequest,
            'form' => $form,
        ]);
    }

    #[Route('/{id:paymentRequest}', name: 'accounting.payment_request.show', methods: ['GET'])]
    #[IsGranted('accounting_show', subject: 'paymentRequest')]
    public function show(PaymentRequest $paymentRequest): Response
    {
        return $this->render('accounting/payment_request/show.html.twig', [
            'payment_request' => $paymentRequest,
        ]);
    }

    #[Route('/{id:paymentRequest}/edit', name: 'accounting.payment_request.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function edit(Request $request, PaymentRequest $paymentRequest, PaymentRequestRepository $paymentRequestRepository): Response
    {
        if ($paymentRequest->getMembership()) {
            $this->addFlash('warning', 'Cette demande de paiement est liée à une adhésion : pour la modifier, il faut modifier l’adhésion.');

            return $this->redirectToRoute('accounting.index', [], Response::HTTP_SEE_OTHER);
        }

        $form = $this->createForm(PaymentRequestType::class, $paymentRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $paymentRequestRepository->save($paymentRequest, true);
            $this->addFlash('success', 'La demande de paiement a bien été modifiée.');

            return $this->redirectToRoute('accounting.payment_request.show', ['id' => $paymentRequest->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('accounting/payment_request/edit.html.twig', [
            'payment_request' => $paymentRequest,
            'form' => $form,
        ]);
    }

    #[Route('/cancel/{id:paymentRequest}', name: 'accounting.payment_request.cancel', methods: ['GET'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function cancel(PaymentRequest $paymentRequest, PaymentRequestRepository $paymentRequestRepository): Response
    {
        if ($paymentRequest->getMembership()) {
            $this->addFlash('warning', 'Cette demande de paiement est liée à une adhésion : pour l’annuler, il faut annuler l’adhésion.');

            return $this->redirectToRoute('accounting.index', [], Response::HTTP_SEE_OTHER);
        }

        $paymentRequest->setStatus(PaymentRequest::STATUS_CANCELED);
        $paymentRequestRepository->save($paymentRequest, true);
        $this->addFlash('success', 'La demande de paiement a bien été annulée.');

        return $this->redirectToRoute('accounting.index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/restore/{id:paymentRequest}', name: 'accounting.payment_request.restore', methods: ['GET'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function restore(PaymentRequest $paymentRequest, PaymentRequestRepository $paymentRequestRepository): Response
    {
        if ($paymentRequest->getMembership()) {
            $this->addFlash('warning', 'Cette demande de paiement est liée à une adhésion : pour la restaurer, il faut restaurer l’adhésion.');

            return $this->redirectToRoute('accounting.index', [], Response::HTTP_SEE_OTHER);
        }

        $paymentRequest->setStatus(PaymentRequest::STATUS_IN_PROGRESS);
        $paymentRequestRepository->save($paymentRequest, true);
        $this->addFlash('success', 'La demande de paiement a bien été restaurée.');

        return $this->redirectToRoute('accounting.index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/dioceseSupplement/new/{id:diocese}', name: 'accounting.payment_request.diocese_supplement.new', methods: ['GET'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function dioceseSupplementNew(
        Diocese $diocese,
        PaymentRequestRepository $paymentRequestRepository,
        DioceseMembershipSupplementRepository $dioceseMemberShipSupplementRepository,
        MembershipFeeService $membershipFeeService,
    ): Response {
        if (null !== $dioceseMemberShipSupplementRepository->findOneBy(['diocese' => $diocese, 'period' => $this->getActivePeriod()])) {
            $this->addFlash('warning', 'Un complément a déjà été demandé pour cette période ou un trop perçu déjà enregistré');

            return $this->redirectToRoute('diocese.show', ['id' => $diocese->getId()]);
        }

        $actualAssociatedMemberships = $membershipFeeService->getDioceseAssociatedMembershipData($diocese, $this->getActivePeriod());
        if (0.0 == $actualAssociatedMemberships['data']['total']) {
            $this->addFlash('warning', 'Un complément ne peut-être demandé que pour un solde à percevoir et il n’y a aucun trop perçu à enregistrer');

            return $this->redirectToRoute('diocese.show', ['id' => $diocese->getId()]);
        }

        $paymentRequest = new PaymentRequest();
        $paymentRequest
            ->setDatetime(new \DateTime('now'))
            ->setAmount($actualAssociatedMemberships['data']['total'])
            ->setDescription(
                ($actualAssociatedMemberships['data']['total'] > 0 ? 'Solde des cotisations' : 'Trop perçu')
                .' du diocèse '.$this->concatName($diocese->getName() ?? '').' pour '.$this->getActivePeriod()->getName()
            )
        ;
        $dioceseMemberShipSupplement = new DioceseMembershipSupplement();
        $dioceseMemberShipSupplement
            ->setDiocese($diocese)
            ->setPeriod($this->getActivePeriod())
            ->setPaymentRequest($paymentRequest)
        ;

        $paymentRequestRepository->save($paymentRequest);
        $dioceseMemberShipSupplementRepository->save($dioceseMemberShipSupplement, true);
        $paymentRequest->setDioceseMembershipSupplement($dioceseMemberShipSupplement);
        $paymentRequestRepository->save($paymentRequest, true);
        $this->addFlash(
            'success',
            $actualAssociatedMemberships['data']['total'] > 0
                ? 'La demande de complément a bien été ajoutée'
                : 'Le trop perçu a bien été enregistré'
        );

        return $this->redirectToRoute('accounting.payment_request.show', ['id' => $paymentRequest->getId()]);
    }

    #[Route('/file/new/{id:paymentRequest}', name: 'accounting.payment_request.file.new', methods: ['POST'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function fileNew(
        Request $request,
        PaymentRequest $paymentRequest,
        PaymentRequestFileRepository $paymentRequestFileRepository,
        PaymentRequestRepository $paymentRequestRepository,
    ): Response {
        $prefix = $request->request->getString('prefix');
        $number = $request->request->getInt('number');

        if (!is_numeric($request->request->get('number'))) {
            $this->addFlash('warning', 'Le numéro d’un fichier doit être un entier');
        } elseif (count($paymentRequestFileRepository->findBy(['refPrefix' => $prefix, 'refNumber' => $number])) > 0) {
            $this->addFlash('warning', 'Cette combinaison de numéro et de préfixe est déjà attribuée à un fichier');
        } else {
            $file = new PaymentRequestFile();
            $file->setPaymentRequest($paymentRequest)
                ->setRefPrefix($prefix)
                ->setRefNumber($number)
            ;

            $paymentRequestFileRepository->save($file, true);
            $this->addFlash('success', 'Le fichier a bien été ajouté.');

            if ('on' === $request->request->get('upload', 'off')) {
                $this->uploadFile($request->files->get('file'), $file, $paymentRequestFileRepository);
            } elseif ('off' === $request->request->get('upload', 'off')) {
                $this->generate($file);
            }

            if ('on' === $request->request->get('sendMail', 'off')) {
                $this->send($file, $paymentRequestRepository);
            }
        }

        return $this->redirectToRoute('accounting.payment_request.show', ['id' => $paymentRequest->getId()]);
    }

    #[Route('/file/edit/{id}', name: 'accounting.payment_request.file.update', methods: ['POST'])]
    #[IsGranted('ROLE_ACCOUNTING')]
    public function fileEdit(
        Request $request,
        #[MapEntity(expr: 'repository.findByIdFullJoin(id)')]
        PaymentRequestFile $paymentRequestFile,
        PaymentRequestFileRepository $paymentRequestFileRepository,
        PaymentRequestRepository $paymentRequestRepository,
    ): Response {
        $prefix = $request->request->getString('prefix');
        $number = $request->request->getInt('number');

        if (!is_numeric($request->request->get('number'))) {
            $this->addFlash('warning', 'Le numéro d’un fichier doit être un entier');
        } elseif (($prefix !== $paymentRequestFile->getRefPrefix() || $number !== $paymentRequestFile->getRefNumber()) && count($paymentRequestFileRepository->findBy(['refPrefix' => $prefix, 'refNumber' => $number])) > 0) {
            $this->addFlash('warning', 'Cette combinaison de numéro et de préfixe est sans doute déjà attribuée à un fichier');
        } else {
            $paymentRequestFile->setRefPrefix($prefix)
                ->setRefNumber($number)
            ;

            if ('on' === $request->request->get('upload', 'off')) {
                $this->uploadFile($request->files->get('file'), $paymentRequestFile, $paymentRequestFileRepository);
            } elseif ('off' === $request->request->get('upload', 'off')) {
                $this->generate($paymentRequestFile, true);
            }

            if ('on' === $request->request->get('sendMail', 'off')) {
                $this->send($paymentRequestFile, $paymentRequestRepository, true);
            }

            $paymentRequestFileRepository->save($paymentRequestFile, true);
            $this->addFlash('success', 'Le fichier a bien été modifié.');
        }

        return $this->redirectToRoute('accounting.payment_request.show', ['id' => $paymentRequestFile->getPaymentRequest()?->getId()]);
    }

    private function concatName(string $name): string
    {
        if (in_array(strtolower($name[0]), ['a', 'e', 'i', 'o', 'u', 'y'])) {
            return 'd’'.$name;
        }

        return 'de '.$name;
    }

    private function generate(PaymentRequestFile $file, bool $update = false): void
    {
        $this->paymentRequestService->generatePdfAndSave($file);
        $this->addFlash('success', 'Le fichier a bien été '.($update ? 're' : '').'généré.');
    }

    private function uploadFile(?UploadedFile $uploaded, PaymentRequestFile $file, PaymentRequestFileRepository $paymentRequestFileRepository): void
    {
        if ($uploaded && 'application/pdf' !== $uploaded->getMimeType()) {
            $this->addFlash('warning', 'Le fichier doit être un PDF.');
        } elseif ($uploaded) {
            $file->setFile($this->uploaderService->upload($uploaded, 'paymentRequests', $file->getFile()));
            $paymentRequestFileRepository->save($file, true);
            $this->addFlash('success', 'Le fichier a bien été enregistré.');
        }
    }

    private function send(PaymentRequestFile $file, PaymentRequestRepository $paymentRequestRepository, bool $update = false): void
    {
        try {
            $this->accountingMailer->sendPaymentRequest($file, $this->paymentRequestService, $update);
            $paymentRequest = $file->getPaymentRequest();
            $this->addFlash(
                'success',
                'Le mail avec la demande de paiement a été envoyé avec succès'
            );
            if (null !== $paymentRequest && null === $paymentRequest->getMembership()) {
                $paymentRequest->setLastReminder(new \DateTime());
                $paymentRequestRepository->save($paymentRequest, true);
                $this->addFlash(
                    'notice',
                    'Cette demande sera automatiquement relancée tous les 15 jours.'
                );
            }
        } catch (TransportExceptionInterface $e) {
            $this->addFlash(
                'error',
                'Une erreur a eu lieu lors de l’envoi du mail : '.$e->getMessage(),
            );
        } catch (PaymentRequestException $e) {
            $this->addFlash(
                'error',
                'Une erreur a eu lieu lors de l’envoi du mail : impossible de joindre la demande de paiement',
            );
        }
    }
}
