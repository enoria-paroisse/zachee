<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Repository\CronLogRepository;
use App\Repository\CronTaskLogRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('cron')]
#[IsGranted('ROLE_ADMIN')]
class CronController extends AbstractController
{
    #[Route('/', name: 'cron.index', methods: ['GET'])]
    public function index(CronLogRepository $cronLogRepository, CronTaskLogRepository $cronTaskLogRepository): Response
    {
        return $this->render('cron/index.html.twig', [
            'cronLogs' => $cronLogRepository->findBy([], ['datetime' => 'desc']),
            'taskLogs' => $cronTaskLogRepository->findAll(),
        ]);
    }
}
