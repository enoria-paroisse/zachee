<?php

/**
 * @copyright Copyright (c) 2025 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Repository\AggregateRepository;
use App\Repository\DioceseRepository;
use App\Repository\DocRepository;
use App\Repository\GroupRepository;
use App\Repository\OrganizationRepository;
use App\Repository\PersonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/search')]
class SearchController extends AbstractController
{
    #[Route('/{search}', name: 'search', requirements: ['search' => '.+'], methods: ['GET'])]
    public function search(
        string $search,
        PersonRepository $personRepository,
        OrganizationRepository $organizationRepository,
        DioceseRepository $dioceseRepository,
        GroupRepository $groupRepository,
        AggregateRepository $aggregateRepository,
        DocRepository $docRepository,
    ): Response {
        $result = [
            'people' => [],
            'organizations' => [],
            'dioceses' => [],
            'volunteerGroups' => [],
            'aggregates' => [],
            'docs' => [],
        ];

        try {
            // People
            $people = $personRepository->search($search);
            foreach ($people as $person) {
                if ($this->isGranted('basic_show', $person)) {
                    $result['people'][] = [
                        'id' => $person->getId(),
                        'firstname' => $person->getFirstname(),
                        'lastname' => $person->getLastname(),
                    ];
                }
            }

            // Organizations
            $organizations = $organizationRepository->search($search);
            foreach ($organizations as $organization) {
                if ($this->isGranted('basic_show', $organization)) {
                    $result['organizations'][] = [
                        'id' => $organization->getId(),
                        'name' => $organization->getName(),
                    ];
                }
            }

            // Dioceses
            $dioceses = $dioceseRepository->search($search);
            foreach ($dioceses as $diocese) {
                if ($this->isGranted('basic_show', $diocese)) {
                    $result['dioceses'][] = [
                        'id' => $diocese->getId(),
                        'name' => $diocese->getName(),
                    ];
                }
            }

            // Volunteer groups
            $groups = $groupRepository->search($search);
            foreach ($groups as $group) {
                if ($this->isGranted('basic_show', $group)) {
                    $result['volunteerGroups'][] = [
                        'id' => $group->getId(),
                        'name' => $group->getName(),
                    ];
                }
            }

            // Aggregates
            if ($this->isGranted('ROLE_BASIC_SHOW')) {
                $aggregates = $aggregateRepository->search($search);
                foreach ($aggregates as $aggregate) {
                    $result['aggregates'][] = [
                        'id' => $aggregate->getId(),
                        'name' => $aggregate->getName(),
                    ];
                }
            }

            // Docs
            $docs = $docRepository->search($search);
            foreach ($docs as $doc) {
                dump($doc->getName());
                if ($this->isGranted('show_uploaded_doc', $doc)) {
                    $result['docs'][] = [
                        'file' => $doc->getFile(),
                        'name' => $doc->getName(),
                    ];
                }
            }
        } catch (\Throwable $e) {
            return $this->json([
                'search' => $search,
                'error' => true,
                'message' => $e->getMessage(),
                'result' => $result,
            ], 500);
        }

        return $this->json([
            'search' => $search,
            'result' => $result,
        ], 200);
    }
}
