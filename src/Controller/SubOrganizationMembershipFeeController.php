<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Membership;
use App\Entity\SubOrganizationMembershipFee;
use App\Entity\SubOrganizationMembershipFeePeriod;
use App\Form\SubOrganizationMembershipFeeType;
use App\Repository\DioceseRepository;
use App\Repository\PeriodRepository;
use App\Repository\SubOrganizationMembershipFeeRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/suborganizationmembership')]
class SubOrganizationMembershipFeeController extends AbstractController
{
    public function __construct(private readonly ManagerRegistry $managerRegistry)
    {
    }

    #[Route('/new', name: 'suborganizationmembership.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function new(Request $request, PeriodRepository $periodRepository): Response
    {
        $subOrganizationMembershipFee = new SubOrganizationMembershipFee();
        $form = $this->createForm(SubOrganizationMembershipFeeType::class, $subOrganizationMembershipFee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            foreach ($periodRepository->findAll() as $period) {
                $subOrganizationMembershipFeePeriod = new SubOrganizationMembershipFeePeriod();
                $subOrganizationMembershipFeePeriod->setMembershipFee('0.00')
                    ->setSubOrganizationMembershipFee($subOrganizationMembershipFee)
                    ->setPeriod($period)
                ;
                $entityManager->persist($subOrganizationMembershipFeePeriod);
            }
            $entityManager->persist($subOrganizationMembershipFee);
            $entityManager->flush();
            $this->addFlash('success', 'Le type cotisation a bien été ajouté');

            return $this->redirectToRoute('config.index');
        }

        return $this->render('suborganizationmembership/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id:suborganizationmembership}/edit', name: 'suborganizationmembership.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function edit(Request $request, SubOrganizationMembershipFee $suborganizationmembership): Response
    {
        $form = $this->createForm(SubOrganizationMembershipFeeType::class, $suborganizationmembership);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();
            $this->addFlash('success', 'Le type cotisation a bien été modifié');

            return $this->redirectToRoute('config.index');
        }

        return $this->render('suborganizationmembership/edit.html.twig', [
            'suborganizationmembership' => $suborganizationmembership,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id:suborganizationmembership}', name: 'suborganizationmembership.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_ADMIN')]
    public function delete(Request $request, SubOrganizationMembershipFee $suborganizationmembership, SubOrganizationMembershipFeeRepository $subOrganizationMembershipFeeRepository, DioceseRepository $dioceseRepository): Response
    {
        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('delete'.$suborganizationmembership->getId(), $csrfToken)) {
            $entityManager = $this->managerRegistry->getManager();

            // change membership fee for each diocese
            $replace = $subOrganizationMembershipFeeRepository->findOneOther($suborganizationmembership->getId());
            if (null === $replace) {
                $this->addFlash('error', 'Il doit exister au moins un type de cotisation');
            } else {
                foreach ($dioceseRepository->findBy(['subOrganizationMembershipFee' => $suborganizationmembership->getId()]) as $diocese) {
                    $diocese->setSubOrganizationMembershipFee($replace);
                }

                $entityManager->remove($suborganizationmembership);
                $entityManager->flush();

                $this->addFlash('success', 'Le type cotisation a bien été supprimé');
            }
        }

        return $this->redirectToRoute('config.index');
    }
}
