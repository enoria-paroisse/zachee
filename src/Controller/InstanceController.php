<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Instance;
use App\Form\EntityStatusType;
use App\Form\InstanceType;
use App\Repository\EntityRepository;
use App\Repository\InstanceRepository;
use App\Services\EnoriaException;
use App\Services\EnoriaService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/instance')]
class InstanceController extends AbstractController
{
    public function __construct(private readonly ManagerRegistry $managerRegistry)
    {
    }

    #[Route('/new', name: 'instance.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function new(Request $request): Response
    {
        $instance = new Instance();
        $form = $this->createForm(InstanceType::class, $instance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($instance);
            $entityManager->flush();
            $this->addFlash('success', 'L’instance a bien été ajoutée');

            return $this->redirectToRoute('config.index');
        }

        return $this->render('instance/new.html.twig', [
            'instance' => $instance,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/sync', name: 'instance.sync', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function sync(InstanceRepository $instanceRepository, EnoriaService $enoria): Response
    {
        $instances = $instanceRepository->findAll();
        foreach ($instances as $instance) {
            try {
                $enoria->syncInstance($instance);
                $this->addFlash(
                    'success',
                    'Instance '.$instance->getName().' synchronisée',
                );
            } catch (EnoriaException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage(),
                );
            }
        }

        return $this->redirectToRoute('config.index');
    }

    #[Route('/{id:instance}', name: 'instance.show', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function show(Instance $instance, EntityRepository $entityRepository): Response
    {
        $form = $this->createForm(EntityStatusType::class, null, ['instance' => $instance]);

        return $this->render('instance/show.html.twig', [
            'instance' => $instance,
            'entities' => $entityRepository->findByInstanceFullJoin($instance),
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id:instance}/edit', name: 'instance.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function edit(Request $request, Instance $instance): Response
    {
        $form = $this->createForm(InstanceType::class, $instance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->managerRegistry->getManager()->flush();
            $this->addFlash('success', 'L’instance a bien été modifiée');

            return $this->redirectToRoute('config.index');
        }

        return $this->render('instance/edit.html.twig', [
            'instance' => $instance,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id:instance}', name: 'instance.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_ADMIN')]
    public function delete(Request $request, Instance $instance): Response
    {
        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('delete'.$instance->getId(), $csrfToken)) {
            if (!$instance->getEntities()->count()) {
                $entityManager = $this->managerRegistry->getManager();
                $entityManager->remove($instance);
                $entityManager->flush();
                $this->addFlash('success', 'L’instance a bien été supprimée');
            } else {
                $this->addFlash('error', 'Impossible de supprimer cette instance : des entités lui sont encore rattachées');
            }
        }

        return $this->redirectToRoute('config.index');
    }
}
