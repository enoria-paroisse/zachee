<?php

/**
 * @copyright Copyright (c) 2025 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Controller\Traits\UserableTrait;
use App\Entity\WebauthnCredential;
use App\Form\ChangePasswordType;
use App\Repository\WebauthnCredentialRepository;
use App\Services\BackupCodeManager;
use Doctrine\ORM\EntityManagerInterface;
use Endroid\QrCode\Builder\BuilderInterface;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\RoundBlockSizeMode;
use Endroid\QrCode\Writer\PngWriter;
use Scheb\TwoFactorBundle\Security\TwoFactor\Provider\Google\GoogleAuthenticatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

#[Route('/profile')]
class ProfileController extends AbstractController
{
    use UserableTrait;

    #[Route('/', name: 'users.profile', methods: ['GET'])]
    public function profile(Request $request, WebauthnCredentialRepository $webauthnCredentialRepository): Response
    {
        $webAuthnCookie = $request->cookies->get('zachee-webauthn-store');

        return $this->render('security/profile.html.twig', [
            'browserHaveWebAuthnCookie' => is_string($webAuthnCookie) && '' !== $webAuthnCookie,
            'webauthnCredentials' => $webauthnCredentialRepository->findAllForUser($this->getAppUser()),
        ]);
    }

    #[Route('/change_password', name: 'users.change_password', methods: ['GET', 'POST'])]
    public function changePassword(
        Request $request,
        UserPasswordHasherInterface $encoder,
        EntityManagerInterface $entityManager
    ): Response {
        $form = $this->createForm(ChangePasswordType::class);
        $form->handleRequest($request);
        $plainPassword = $form->get('plainPassword')->getData();

        if ($form->isSubmitted() && $form->isValid() && is_string($plainPassword)) {
            $this->getAppUser()->setPassword($encoder->hashPassword(
                $this->getAppUser(),
                $plainPassword
            ));
            $entityManager->flush();

            $this->addFlash('success', 'Votre mot de passe a été modifié avec succès');

            return $this->redirectToRoute('users.profile');
        }

        return $this->render('security/change_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/2fa/email-enable', name: 'profile.2fa.email.enable', methods: ['GET'])]
    public function emailAuthEnable(EntityManagerInterface $entityManager): Response
    {
        if ($this->isGranted('IS_IMPERSONATOR')) {
            throw $this->createAccessDeniedException();
        }

        $this->getAppUser()->setEmailAuthEnabled(true);
        $entityManager->flush();

        $this->addFlash('success', 'L’authentification à 2 facteurs par email ou SMS a bien été activée');

        return $this->redirectToRoute('users.profile');
    }

    #[Route('/2fa/revoke-trusted-devices', name: 'profile.2fa.revoke-trusted-devices', methods: ['GET'])]
    public function revokeTrustedDevices(EntityManagerInterface $entityManager): Response
    {
        if ($this->isGranted('IS_IMPERSONATOR')) {
            throw $this->createAccessDeniedException();
        }

        $this->getAppUser()->revokeTrustedTokenVersion();
        $entityManager->flush();

        $this->addFlash('success', 'Tous mes appareils de confiance ont bien été révoqués');

        return $this->redirectToRoute('users.profile');
    }

    #[Route('/2fa/generate-backup-code', name: 'profile.2fa.generate-backup-code', methods: ['POST'])]
    public function generateBackupCode(
        Request $request,
        CsrfTokenManagerInterface $csrfTokenManager,
        BackupCodeManager $backupCodeManager,
        EntityManagerInterface $entityManager,
    ): Response {
        if ($this->isGranted('IS_IMPERSONATOR')) {
            throw $this->createAccessDeniedException();
        }

        $submittedToken = (string) $request->getPayload()->get('token', '');
        if (!$this->isCsrfTokenValid('generate-backup', $submittedToken)) {
            return $this->json([
                'status' => 'error',
                'message' => 'Invalid token',
            ], Response::HTTP_BAD_REQUEST);
        }

        $code = $backupCodeManager->generateBackupCode();
        $this->getAppUser()->addBackUpCode($code);
        $entityManager->flush();

        $token = $csrfTokenManager->refreshToken('generate-backup')->getValue();

        return $this->json([
            'status' => 'success',
            'code' => $code,
            'token' => $token,
        ]);
    }

    #[Route('/2fa/google-auth/enable', name: 'profile.2fa.google-auth.enable', methods: ['GET', 'POST'])]
    public function enableGoogleAuthenticator(
        Request $request,
        SessionInterface $session,
        GoogleAuthenticatorInterface $googleAuthenticator,
        BuilderInterface $builder,
        EntityManagerInterface $entityManager,
    ): Response {
        if ($this->isGranted('IS_IMPERSONATOR')) {
            throw $this->createAccessDeniedException();
        }

        if ($this->getAppUser()->isGoogleAuthenticatorEnabled()) {
            return $this->json([
                'status' => 'error',
                'error' => 'Google authenticator est déjà activé',
            ], Response::HTTP_BAD_REQUEST);
        }

        /** @var ?string $secret */
        $secret = $session->get('google_authenticator_secret');

        if ($request->isMethod('GET')) {
            if (!$secret) {
                $secret = $googleAuthenticator->generateSecret();
                $session->set('google_authenticator_secret', $secret);
            }
            $this->getAppUser()->setGoogleAuthenticatorSecret($secret); // No flush !

            $qrCode = $builder->build(
                writer: new PngWriter(),
                data: $googleAuthenticator->getQRContent($this->getAppUser()),
                encoding: new Encoding('UTF-8'),
                errorCorrectionLevel: ErrorCorrectionLevel::High,
                size: 200,
                margin: 0,
                roundBlockSizeMode: RoundBlockSizeMode::Margin,
            );

            return $this->json([
                'status' => 'in_progress',
                'qrCode' => $qrCode->getDataUri(),
            ]);
        }

        $submittedToken = (string) $request->getPayload()->get('token', '');
        if (!$this->isCsrfTokenValid('google-auth', $submittedToken)) {
            return $this->json([
                'status' => 'error',
                'error' => 'Invalid token',
            ], Response::HTTP_BAD_REQUEST);
        }

        if (!$secret) {
            return $this->json([
                'status' => 'error',
                'error' => 'Erreur lors de l’activation de Google Authenticator',
            ], 500);
        }

        $this->getAppUser()->setGoogleAuthenticatorSecret($secret);
        $code = (string) $request->getPayload()->get('code', '');
        if (!$code || $googleAuthenticator->checkCode($this->getAppUser(), $code)) {
            return $this->json([
                'status' => 'error',
                'error' => 'Le code n’est pas valide',
            ], Response::HTTP_FORBIDDEN);
        }
        $entityManager->flush();
        $session->remove('google_authenticator_secret');

        return $this->json([
            'status' => 'success',
        ]);
    }

    #[Route('/2fa/google-auth/disable', name: 'profile.2fa.google-auth.disable', methods: ['POST'])]
    public function disableGoogleAuth(Request $request, EntityManagerInterface $entityManager): Response
    {
        if ($this->isGranted('IS_IMPERSONATOR')) {
            throw $this->createAccessDeniedException();
        }

        $submittedToken = (string) $request->getPayload()->get('_token', '');
        if ($this->isCsrfTokenValid('google-auth-disable', $submittedToken)) {
            $this->getAppUser()->setGoogleAuthenticatorSecret(null);
            $entityManager->flush();

            $this->addFlash('success', 'Google Authenticator a bien été désactivé');
        }

        return $this->redirectToRoute('users.profile');
    }

    #[Route('/webauthn/devices/rename', name: 'webauthn.device.rename', methods: ['POST'])]
    public function renameCredential(Request $request, WebauthnCredentialRepository $webauthnCredentialRepository): Response
    {
        // Here, credentialId can be the ID or base64_encode(publicKeyCredentialId).
        $credentialId = $request->getPayload()->getString('credentialId');
        $keyName = $request->getPayload()->getString('keyName');

        // Remove the credential from the user. If the ID is not well formatted, nothing will happen.
        $webauthnCredentialRepository->renameCredentialForUser($this->getAppUser(), $credentialId, $keyName);

        return $this->json(['status' => 'success']);
    }

    #[Route('/webauthn/devices/{id:credential}/delete', name: 'webauthn.device.delete', methods: ['DELETE'])]
    public function removeCredential(
        WebauthnCredential $credential,
        Request $request,
        WebauthnCredentialRepository $webauthnCredentialRepository
    ): Response {
        if ($this->isGranted('IS_IMPERSONATOR')) {
            throw $this->createAccessDeniedException();
        }

        $name = $credential->getKeyName() ?? $credential->getId();
        $submittedToken = (string) $request->getPayload()->get('_token', '');
        if ($this->isCsrfTokenValid('delete_webauthn'.$credential->getId(), $submittedToken)) {
            $this->getAppUser()->setGoogleAuthenticatorSecret(null);
            $webauthnCredentialRepository->removeCredentialForUser($this->getAppUser(), $credential->getId());

            $this->addFlash('success', 'La clé ['.$name.'] a bien été révoquée');
        }

        return $this->redirectToRoute('users.profile');
    }

    #[Route('/webauthn/create-cookie', name: 'webauthn.create_cookie', methods: ['GET'])]
    public function createCookie(): Response
    {
        // This cookie is the only way to display the "connect with a passKey" button in the login page for now
        // TODO : find a better way to do this, and remove this method
        $cookie = Cookie::create('zachee-webauthn-store')
            ->withValue($this->getAppUser()->getUsername())
            ->withExpires(time() + 3600 * 24 * 365)
            ->withPath('/')
            ->withSecure()
            ->withHttpOnly()
        ;

        $response = new JsonResponse(['status' => 'success']);
        $response->headers->setCookie($cookie);

        return $response;
    }

    #[Route('/webauthn/delete-cookie', name: 'webauthn.delete_cookie', methods: ['DELETE'])]
    public function deleteCookie(Request $request): Response
    {
        if ($this->isGranted('IS_IMPERSONATOR')) {
            throw $this->createAccessDeniedException();
        }

        $response = new RedirectResponse($this->generateUrl('users.profile'));

        $submittedToken = (string) $request->getPayload()->get('_token', '');
        if ($this->isCsrfTokenValid('delete_webauthn_cookie', $submittedToken)) {
            $cookie = Cookie::create('zachee-webauthn-store')
                ->withValue('')
                ->withExpires(time() - 3600)
                ->withPath('/')
                ->withSecure()
                ->withHttpOnly()
            ;
            $response->headers->setCookie($cookie);
        }

        return $response;
    }
}
