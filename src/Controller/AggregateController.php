<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Controller\Traits\UserableTrait;
use App\Entity\Aggregate;
use App\Form\AggregateType;
use App\Repository\AggregateRepository;
use App\Repository\GroupRepository;
use App\Services\AggregateService;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/aggregate')]
class AggregateController extends AbstractController
{
    use UserableTrait;

    #[Route('/', name: 'aggregate.index', methods: ['GET'])]
    #[IsGranted('ROLE_BASIC_SHOW')]
    public function index(
        AggregateRepository $aggregateRepository
    ): Response {
        return $this->render('aggregate/index.html.twig', [
            'aggregates' => $aggregateRepository->findAllFullJoin(),
        ]);
    }

    #[Route('/new', name: 'aggregate.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_AGGREGATE_EDIT')]
    public function new(Request $request, AggregateRepository $aggregateRepository): Response
    {
        $aggregate = new Aggregate();
        $form = $this->createForm(AggregateType::class, $aggregate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $aggregateRepository->save($aggregate, true);
            $this->addFlash(
                'success',
                'L’agrégat a bien été créé'
            );

            return $this->redirectToRoute('aggregate.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('aggregate/new.html.twig', [
            'aggregate' => $aggregate,
            'form' => $form,
        ]);
    }

    #[Route('/{id:aggregate}/edit', name: 'aggregate.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_AGGREGATE_EDIT')]
    public function edit(Request $request, Aggregate $aggregate, AggregateRepository $aggregateRepository): Response
    {
        $form = $this->createForm(AggregateType::class, $aggregate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $aggregateRepository->save($aggregate, true);
            $this->addFlash(
                'success',
                'L’agrégat a bien été modifié'
            );

            return $this->redirectToRoute('aggregate.show', ['id' => $aggregate->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('aggregate/edit.html.twig', [
            'aggregate' => $aggregate,
            'form' => $form,
        ]);
    }

    #[Route('/{id}/show', name: 'aggregate.show', methods: ['GET'])]
    #[IsGranted('ROLE_BASIC_SHOW')]
    public function show(int $id, AggregateRepository $aggregateRepository, AggregateService $aggregateService): Response
    {
        try {
            $aggregate = $aggregateRepository->findByIdFullJoin($id, $this->getActivePeriod());

            if (null === $aggregate) {
                throw new NonUniqueResultException();
            }

            $aggregateService->setPeriod($this->getAppUser()->getPeriod());

            return $this->render('aggregate/show.html.twig', [
                'aggregate' => $aggregate,
                'people' => $aggregateService->getPeople($aggregate),
            ]);
        } catch (NonUniqueResultException $e) {
            throw $this->createNotFoundException('Aggregate not found!');
        }
    }

    #[Route('/{id:aggregate}', name: 'aggregate.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_AGGREGATE_EDIT')]
    public function delete(Request $request, Aggregate $aggregate, AggregateRepository $aggregateRepository, GroupRepository $groupRepository): Response
    {
        if (0 < count($groupRepository->findBy(['aggregate' => $aggregate]))) {
            $this->addFlash(
                'error',
                'Au moins un groupe utilise encore cet agrégat pour lister ses membres'
            );

            return $this->redirectToRoute('aggregate.show', ['id' => $aggregate->getId()], Response::HTTP_SEE_OTHER);
        }

        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('delete'.$aggregate->getId(), $csrfToken)) {
            $aggregateRepository->remove($aggregate, true);
            $this->addFlash(
                'success',
                'L’agrégat a bien été supprimé'
            );
        }

        return $this->redirectToRoute('aggregate.index', [], Response::HTTP_SEE_OTHER);
    }
}
