<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Controller\Traits\UserableTrait;
use App\Repository\ApplicationRepository;
use App\Repository\AssociatedPersonRepository;
use App\Repository\MembershipRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class HomeController extends AbstractController
{
    use UserableTrait;

    #[Route('/', name: 'home', methods: ['GET'])]
    public function index(
        MembershipRepository $membershipRepository,
        AssociatedPersonRepository $associatedPersonRepository,
        ApplicationRepository $applicationRepository,
    ): Response {
        $associatedOrganizations = [];
        foreach ($associatedPersonRepository->findByPerson($this->getAppUser()->getPerson()) as $associatedPerson) {
            $associatedOrganizations[] = $associatedPerson->getOrganization();
        }

        $infos = [];
        if ($this->isGranted('ROLE_MEMBERSHIP_EDIT')) {
            $infos = $membershipRepository->findBy(['informationSubmitted' => true, 'informationValidated' => false]);
        }

        $applications = [];
        if ($this->isGranted('ROLE_APPLICATION_SHOW')) {
            $applications = $applicationRepository->findAll();
        } else {
            foreach ($this->getAppUser()->getPerson()->getApplicationUsers()->toArray() as $userApplication) {
                $applications[] = $userApplication->getApplication();
            }
        }

        return $this->render('pages/home.html.twig', [
            'actualMembership' => $membershipRepository->findOneBy([
                'person' => $this->getAppUser()->getPerson(),
                'period' => $this->getAppUser()->getPeriod(),
            ]),
            'associatedOrganizations' => array_unique($associatedOrganizations),
            'infos' => $infos,
            'applications' => $applications,
        ]);
    }

    #[Route('/changelog', name: 'changelog', methods: ['GET'])]
    public function changelog(): Response
    {
        return $this->render('pages/changelog.html.twig');
    }

    #[Route('/legal', name: 'legal', methods: ['GET'])]
    public function legal(): Response
    {
        return $this->render('pages/legal.html.twig', ['base' => 'base']);
    }

    #[Route('/public/legal', name: 'public.legal', methods: ['GET'])]
    public function publicLegal(): Response
    {
        return $this->render('pages/legal.html.twig', ['base' => 'public']);
    }

    #[Route('/privacy-policy', name: 'privacy-policy', methods: ['GET'])]
    public function privacyPolicy(): Response
    {
        return $this->render('pages/privacy-policy.html.twig', ['base' => 'base']);
    }

    #[Route('/public/privacy-policy', name: 'public.privacy-policy', methods: ['GET'])]
    public function publicPrivacyPolicy(): Response
    {
        return $this->render('pages/privacy-policy.html.twig', ['base' => 'public']);
    }

    #[Route('/cgu', name: 'cgu', methods: ['GET'])]
    public function cgu(): Response
    {
        return $this->render('pages/cgu.html.twig', ['base' => 'base']);
    }

    #[Route('/public/cgu', name: 'public.cgu', methods: ['GET'])]
    public function publicCgu(): Response
    {
        return $this->render('pages/cgu.html.twig', ['base' => 'public']);
    }

    #[Route('/cgu/validate', name: 'cgu.validate', methods: ['GET'])]
    public function cguValidate(UserRepository $userRepository): Response
    {
        if (null === $this->getAppUser()->getCguValidatedAt()) {
            $this->getAppUser()->setCguValidatedAt(new \DateTimeImmutable());
            $userRepository->save($this->getAppUser(), true);
        }

        return $this->redirectToRoute('home');
    }
}
