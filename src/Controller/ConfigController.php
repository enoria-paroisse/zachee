<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Controller\Traits\UserableTrait;
use App\Entity\Title;
use App\Entity\TypeAssociatedPerson;
use App\Form\Config\TitleType;
use App\Form\Config\TypeAssociatedPersonType;
use App\Repository\InstanceRepository;
use App\Repository\PeriodRepository;
use App\Repository\SubOrganizationMembershipFeeRepository;
use App\Repository\TitleRepository;
use App\Repository\TypeAssociatedPersonRepository;
use App\Repository\UserRepository;
use App\Services\BrevoService;
use App\Services\OvhService;
use Brevo\Client\ApiException;
use Doctrine\ORM\EntityManagerInterface;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberType;
use libphonenumber\PhoneNumberUtil;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/config')]
class ConfigController extends AbstractController
{
    use UserableTrait;

    #[Route('/', name: 'config.index', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function index(
        PeriodRepository $periodRepository,
        InstanceRepository $instanceRepository,
        SubOrganizationMembershipFeeRepository $subOrganizationMembershipFeeRepository,
        BrevoService $brevoService,
        OvhService $ovhService,
        TitleRepository $titleRepository,
        TypeAssociatedPersonRepository $typeAssociatedPersonRepository,
    ): Response {
        $brevo = [
            'error' => false,
            'errorApiKey' => false,
            'errorMessage' => '',
            'account' => [],
            'useSmtp' => false,
        ];

        try {
            $brevo['account'] = $brevoService->getAccountApi()->getAccount();
            $mailerDsn = $this->getParameter('mailer.dsn');
            if (str_ends_with($mailerDsn, $brevo['account']->getRelay()->getData()->getRelay().':'.$brevo['account']->getRelay()->getData()->getPort()) && (str_starts_with($mailerDsn, 'smtp://'.$brevo['account']->getRelay()->getData()->getUserName())
                    || str_starts_with($mailerDsn, 'smtp://'.urlencode($brevo['account']->getRelay()->getData()->getUserName())))
            ) {
                $brevo['useSmtp'] = true;
            }
        } catch (\Exception $e) {
            $brevo['error'] = true;
            if ($e instanceof ApiException && 401 === $e->getCode()) {
                $brevo['errorApiKey'] = true;
            } else {
                $brevo['errorMessage'] = $e->getMessage();
            }
        }

        $mails = [
            'admin' => [
                'name' => $this->getParameter('app.name'),
                'mail' => $this->getParameter('app.from_email'),
            ],
            'treasurer' => [
                'name' => $this->getParameter('app.treasurer.name'),
                'mail' => $this->getParameter('app.treasurer.from'),
            ],
            'secretariat' => [
                'name' => $this->getParameter('app.secretariat.name'),
                'mail' => $this->getParameter('app.secretariat.from'),
            ],
        ];

        $sms = [
            'connected' => false,
            'error' => '',
            'serviceName' => '',
            'serviceDescription' => '',
            'creditsLeft' => '',
            'senders' => [],
            'sender' => '',
        ];
        $smsCheckApi = $ovhService->checkApi();
        if (4031 === $smsCheckApi) {
            $sms['error'] = 'La clé API OVH est invalide';
        } elseif (400 === $smsCheckApi) {
            $sms['error'] = 'La valeur API secret est invalide';
        } elseif (4032 === $smsCheckApi) {
            $sms['error'] = 'La clé utilisateur est invalide';
        } elseif (4033 === $smsCheckApi) {
            $sms['error'] = 'L’utilisateur associé à l’API n’a pas accès aux SMS';
        } elseif (501 === $smsCheckApi) {
            $sms['error'] = 'Aucun service SMS trouvé';
        } elseif (200 === $smsCheckApi) {
            $infos = $ovhService->getSmsInfos();
            $sms['connected'] = true;
            $sms['serviceName'] = $infos['serviceName'];
            $sms['serviceDescription'] = $infos['serviceDescription'];
            $sms['creditsLeft'] = $infos['creditsLeft'];
            $sms['senders'] = $infos['senders'];
            $sms['sender'] = $infos['sender'];
            if (4041 === $infos['code']) {
                $sms['error'] = 'Le service '.$ovhService->getSmsServiceName().' défini dans la configuration n’existe pas';
            } elseif (4042 === $infos['code']) {
                $sms['error'] = 'L’expéditeur '.$ovhService->getSmsSender().' défini dans la configuration n’existe pas';
            } elseif (200 !== $infos['code']) {
                $sms['error'] = 'Error '.$infos['code'].': '.$infos['error'];
            }
        } else {
            $sms['error'] = 'Erreur interne';
        }

        return $this->render('config/index.html.twig', [
            'periods' => $periodRepository->findBy([], ['start' => 'asc']),
            'instances' => $instanceRepository->findAll(),
            'subOrganizationMembershipFees' => $subOrganizationMembershipFeeRepository->findBy([], ['name' => 'asc']),
            'mails' => $mails,
            'brevo' => $brevo,
            'sms' => $sms,
            'titles' => $titleRepository->findAll(),
            'types' => $typeAssociatedPersonRepository->findAll(),
        ]);
    }

    #[Route('/mail_test', name: 'config.mail_test', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function mailTest(MailerInterface $mailer): Response
    {
        $from = new Address($this->getParameter('app.from_email'), $this->getParameter('app.name'));
        $message = (new TemplatedEmail())
            ->subject('[Association Enoria] Test')
            ->from($from)
            ->to(new Address(
                $this->getAppUser()->getPerson()->getEmail() ?: $this->getParameter('app.from_email'),
                $this->getAppUser()->getPerson()->getFullname()
            ))
            ->htmlTemplate('emails/test.html.twig')
            ->textTemplate('emails/test.txt.twig')
            ->text('Test de la configuration des mails')
        ;

        try {
            $mailer->send($message);
            $this->addFlash(
                'success',
                'Le mail a été envoyé avec succès, vérifiez votre boîte mail ('.$this->getAppUser()->getPerson()->getEmail().')'
            );
        } catch (TransportExceptionInterface $e) {
            $this->addFlash(
                'error',
                'Une erreur a eu lieu lors de l’envoi du mail : '.$e->getMessage(),
            );
        }

        return $this->redirectToRoute('config.index');
    }

    #[Route('/sms_test', name: 'config.sms_test', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function smsTest(OvhService $ovhService): Response
    {
        $phoneNumber = $this->getAppUser()->getPerson()->getPhone();
        $phoneNumberUtil = PhoneNumberUtil::getInstance();

        if (null === $phoneNumber || PhoneNumberType::MOBILE !== $phoneNumberUtil->getNumberType($phoneNumber)) {
            $this->addFlash(
                'error',
                'Votre numéro de téléphone personnel doit être renseigné et doit correspondre à une ligne mobile.',
            );

            return $this->redirectToRoute('config.index');
        }

        $phone = $phoneNumberUtil->format($phoneNumber, PhoneNumberFormat::E164);

        try {
            $ovhService->sendSmsMessage(
                'Test depuis Zachée',
                [
                    $phoneNumberUtil->format($phoneNumber, PhoneNumberFormat::E164),
                ]
            );
            $this->addFlash(
                'success',
                'Le SMS a été envoyé avec succès, vérifiez votre téléphone ('.$phoneNumberUtil->format($phoneNumber, PhoneNumberFormat::INTERNATIONAL).')'
            );
        } catch (\Exception $e) {
            $this->addFlash(
                'error',
                'Une erreur a eu lieu lors de l’envoi du SMS : '.$e->getMessage(),
            );
        }

        return $this->redirectToRoute('config.index');
    }

    #[Route('/error_test', name: 'config.error_test', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function errorTest(Request $request): never
    {
        throw new \RuntimeException(sprintf('Test du reporting des erreurs dans Mattermost (serveur : %s)', $request->getHttpHost()));
    }

    #[Route('/title/new', name: 'config.title.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function titleNew(Request $request, TitleRepository $titleRepository): Response
    {
        $title = new Title();
        $form = $this->createForm(TitleType::class, $title);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $titleRepository->save($title, true);
            $this->addFlash('success', 'Le titre a bien été ajouté.');

            return $this->redirectToRoute('config.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('config/title/new.html.twig', [
            'title' => $title,
            'form' => $form,
        ]);
    }

    #[Route('/title/{id:title}/edit', name: 'config.title.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function titleEdit(Request $request, Title $title, TitleRepository $titleRepository): Response
    {
        $form = $this->createForm(TitleType::class, $title);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $titleRepository->save($title, true);
            $this->addFlash('success', 'Le titre a bien été modifié.');

            return $this->redirectToRoute('config.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('config/title/edit.html.twig', [
            'title' => $title,
            'form' => $form,
        ]);
    }

    #[Route('/type-associated-person/new', name: 'config.type-associated-person.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function typeAssociatedPersonNew(Request $request, TypeAssociatedPersonRepository $typeAssociatedPersonRepository): Response
    {
        $typeAssociatedPerson = new TypeAssociatedPerson();
        $typeAssociatedPerson->setIcon('user');
        $form = $this->createForm(TypeAssociatedPersonType::class, $typeAssociatedPerson);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $typeAssociatedPersonRepository->save($typeAssociatedPerson, true);
            $this->addFlash('success', 'Le type d’association a bien été ajouté.');

            return $this->redirectToRoute('config.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('config/type_associated_person/new.html.twig', [
            'typeAssociatedPerson' => $typeAssociatedPerson,
            'form' => $form,
        ]);
    }

    #[Route('/type-associated-person/{id:typeAssociatedPerson}/edit', name: 'config.type-associated-person.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function typeAssociatedPersonEdit(
        Request $request,
        TypeAssociatedPerson $typeAssociatedPerson,
        TypeAssociatedPersonRepository $typeAssociatedPersonRepository
    ): Response {
        $form = $this->createForm(TypeAssociatedPersonType::class, $typeAssociatedPerson);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $typeAssociatedPersonRepository->save($typeAssociatedPerson, true);
            $this->addFlash('success', 'Le type d’association a bien été modifié.');

            return $this->redirectToRoute('config.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('config/type_associated_person/edit.html.twig', [
            'typeAssociatedPerson' => $typeAssociatedPerson,
            'form' => $form,
        ]);
    }

    #[Route('/cgu-renew-validation', name: 'config.cgu-renew-validation', methods: ['DELETE'])]
    #[IsGranted('ROLE_ADMIN')]
    public function cguRenewValidation(
        Request $request,
        UserRepository $userRepository,
        EntityManagerInterface $entityManager,
    ): Response {
        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('renew_cgu_validation', $csrfToken)) {
            $users = $userRepository->findAll();
            foreach ($users as $user) {
                $user->setCguValidatedAt(null);
                $entityManager->persist($user);
            }
            $entityManager->flush();

            $this->addFlash('success', 'La validation des CGU a bien été redemandée');
        }

        return $this->redirectToRoute('config.index');
    }
}
