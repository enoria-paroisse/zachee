<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Repository\LogRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class LogController extends AbstractController
{
    #[Route('log/', name: 'log.index', methods: ['GET'])]
    #[IsGranted('show_log')]
    public function index(LogRepository $logRepository): Response
    {
        if ($this->isGranted('ROLE_ADMIN')) {
            $logs = $logRepository->findBy([], ['date' => 'DESC'], 1000);
        } else {
            $logs = $logRepository->findBy(['user' => $this->getUser()], ['date' => 'DESC'], 1000);
        }

        return $this->render('log/index.html.twig', [
            'logs' => $logs,
        ]);
    }
}
