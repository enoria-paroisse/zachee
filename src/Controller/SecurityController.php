<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Controller\Traits\UserableTrait;
use App\Entity\Log;
use App\Entity\User;
use App\Form\ResetPasswordType;
use App\Form\RolesUserType;
use App\Form\UserType;
use App\Mailer\UserMailer;
use App\Repository\PeriodRepository;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    use UserableTrait;

    public function __construct(private readonly ManagerRegistry $managerRegistry)
    {
    }

    #[Route('/login', name: 'login', methods: ['GET', 'POST'])]
    public function login(AuthenticationUtils $authenticationUtils, Request $request): Response
    {
        $webAuthnCookie = $request->cookies->get('zachee-webauthn-store');
        if (is_string($webAuthnCookie) && '' !== $webAuthnCookie) {
            $saveUserName = $webAuthnCookie;
            $webAuthnDisplay = true;
        } else {
            $webAuthnDisplay = false;
            $saveUserName = '';
        }

        return $this->render('security/login.html.twig', [
            'last_username' => $authenticationUtils->getLastUsername(),
            'error' => $authenticationUtils->getLastAuthenticationError(),
            'webAuthnDisplay' => $webAuthnDisplay,
            'saveUserName' => $saveUserName,
        ]);
    }

    #[Route('/logout', name: 'logout', methods: ['GET'])]
    public function logout(): never
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    #[Route('/users/create', name: 'users.create', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER_EDIT')]
    public function create(Request $request, UserMailer $mailer, PeriodRepository $periodRepository): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setEnabled(false)
                // TODO Set a random password here ?
                ->setToken(md5(random_bytes(20)))
                ->setPeriod($periodRepository->findCurrentPeriod() ?: $periodRepository->findOneBy([]))
            ;

            $entityManager = $this->managerRegistry->getManager();
            $entityManager->persist($user);
            $this->addLog(Log::TYPE_ADD, $user);
            $entityManager->flush();

            $mailer->sendRegistration($user);

            $this->addFlash(
                'success',
                'L’utilisateur a bien été ajouté, il va recevoir un mail pour valider son compte et choisir son mot de passe'
            );

            return $this->redirectToRoute('users.index');
        }

        return $this->render('security/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/login/activate', name: 'users.activate', methods: ['GET', 'POST'])]
    public function activate(
        Request $request,
        UserRepository $userRepository,
        UserPasswordHasherInterface $encoder
    ): ?Response {
        $token = $request->query->get('token');
        $user = $userRepository->findOneBy(['token' => $token]);
        if (null === $user) {
            throw $this->createNotFoundException(sprintf('L’utilisateur avec le token « %s » n’existe pas', $token));
        }

        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);
        $plainPassword = $form->get('plainPassword')->getData();

        if ($form->isSubmitted() && $form->isValid() && is_string($plainPassword)) {
            $user->setPassword($encoder->hashPassword(
                $user,
                $plainPassword
            ));
            $user->setToken(null)
                ->setEnabled(true)
            ;

            $this->managerRegistry->getManager()->flush();
            $this->addFlash('success', sprintf(
                'Félicitations %s, votre compte est maintenant activé, vous pouvez maintenant vous connecter avec l’identifiant « %s » et votre nouveau mot de passe.',
                $user->getPerson(),
                $user->getUserIdentifier(),
            ));

            return $this->redirectToRoute('login');
        }

        return $this->render('security/activate.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/login/forgot_password', name: 'users.forgot_password', methods: ['GET', 'POST'])]
    public function forgotPassword(Request $request, UserMailer $mailer, UserRepository $userRepository): Response
    {
        if ($request->isMethod('POST')) {
            $email = (string) $request->request->get('email');
            $username = (string) $request->request->get('username');
            $user = $userRepository->findByUsernameAndMail($username, $email);

            if ($user) {
                $token = md5(random_bytes(20));
                $user->setToken($token);

                $entityManager = $this->managerRegistry->getManager();
                $entityManager->flush();
            } else {
                $this->addFlash('error', 'Cet utilisateur est introuvable, vérifier l’email et le nom d’utilisateur');

                return $this->redirectToRoute('users.forgot_password');
            }

            try {
                $mailer->sendResetPassword($user);
                $this->addFlash('success', 'Un email vous a été adressé pour changer votre mot de passe');
            } catch (TransportExceptionInterface) {
                $this->addFlash('warning', 'Impossible d’envoyer le mail de confirmation, contacter un administrateur');
            }
        }

        return $this->render('security/forgot_password.html.twig');
    }

    #[Route('/login/reset_password', name: 'users.reset_password', methods: ['GET', 'POST'])]
    public function resetPassword(
        Request $request,
        UserRepository $userRepository,
        UserPasswordHasherInterface $encoder
    ): ?Response {
        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);

        $username = $request->get('username');
        $token = $request->get('token');
        $user = $userRepository->findOneBy(['username' => $username, 'token' => $token]);

        if (!$user) {
            throw $this->createAccessDeniedException('Le lien de réinitialisation du mot de passe est périmé !');
        }
        $plainPassword = $form->get('plainPassword')->getData();

        if ($form->isSubmitted() && $form->isValid() && is_string($plainPassword)) {
            $user->setPassword($encoder->hashPassword(
                $user,
                $plainPassword
            ));
            $user->setToken(null);

            $this->managerRegistry->getManager()->flush();

            $this->addFlash('success', sprintf(
                'Votre mot de passe a été modifié avec succès, vous pouvez maintenant vous connecter avec l’identifiant « %s » et votre nouveau mot de passe.',
                $user->getUserIdentifier(),
            ));

            return $this->redirectToRoute('login');
        }

        return $this->render('security/reset_password.html.twig', [
            'user' => $user,
            'username' => $username,
            'token' => $token,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/users', name: 'users.index', methods: ['GET'])]
    #[IsGranted('ROLE_USER_SHOW')]
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('security/index.html.twig', [
            'usersEnabled' => $userRepository->findAllFullJoin(true),
            'usersDisabled' => $userRepository->findAllFullJoin(false),
        ]);
    }

    #[Route('/users/toggle_enabled/{id:user}', name: 'users.toggle_enabled', methods: ['GET'])]
    #[IsGranted('ROLE_USER_EDIT')]
    public function toggleEnabled(User $user): Response
    {
        if (!$user->isEnabled() && null === $user->getPassword()) {
            $this->addFlash('error', 'Vous ne pouvez pas activer/désactiver cet utilisateur, car il ne s’est jamais connecté');
        } elseif ($this->getAppUser() === $user) {
            $this->addFlash('error', 'Vous ne pouvez pas vous désactiver vous-même');
        } else {
            $user->setEnabled(!$user->isEnabled());
            $this->managerRegistry->getManager()->flush();
            $this->addFlash('success', $user->getUserIdentifier().' a été '.($user->isEnabled() ? '' : 'dés').'activé');
        }

        return $this->redirectToRoute('users.index');
    }

    #[Route('/users/edit/{id:user}', name: 'users.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER_EDIT')]
    #[IsGranted('user_access', subject: 'user')]
    public function edit(User $user, Request $request): Response
    {
        $form = $this->createForm(UserType::class, $user, [
            'roles_form' => true,
            'can_set_admin' => $this->isGranted('ROLE_ADMIN'),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setRoles(RolesUserType::transformData((array) $form->get('roles')->getData()));

            $this->addLog(Log::TYPE_UPDATE, $user);
            $this->managerRegistry->getManager()->flush();

            $this->addFlash(
                'success',
                'L’utilisateur a bien été modifié'
            );

            return $this->redirectToRoute('users.index');
        }

        return $this->render('security/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/delete/{id:user}', name: 'users.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_USER_EDIT')]
    public function delete(User $user, Request $request): Response
    {
        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $csrfToken)) {
            if (count($user->getLogs()) > 0) {
                $this->addFlash(
                    'error',
                    'Impossible de supprimer l’utilisateur, car il a effectué des actions dans Zachée'
                );
            } elseif ($user->getEnabled()) {
                $this->addFlash(
                    'error',
                    'Impossible de supprimer l’utilisateur activé'
                );
            } else {
                $this->managerRegistry->getManager()->remove($user);
                $this->addLog(Log::TYPE_DELETE, $user);
                $this->managerRegistry->getManager()->flush();
                $this->addFlash(
                    'success',
                    'L’utilisateur a bien été supprimé'
                );
            }
        }

        return $this->redirectToRoute('users.index');
    }

    #[Route('/users/link/{id:user}', name: 'users.link', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER_EDIT')]
    #[IsGranted('user_access', subject: 'user')]
    public function sendMailActivation(User $user, UserMailer $mailer): Response
    {
        $mailer->sendRegistration($user);

        return $this->redirectToRoute('users.index');
    }

    protected function addLog(string $type, User $user): void
    {
        $log = new Log();
        $log->setDate(new \DateTime())
            ->setType($type)
            ->setObject('User')
            ->setObjectId(Log::TYPE_ADD === $type ? null : $user->getId())
            ->setUser($this->getAppUser())
            ->setDescription('Utilisateur '.$user->getUserIdentifier().' '.$user->getPerson()->getFullname())
        ;
        $this->managerRegistry->getManager()->persist($log);
    }
}
