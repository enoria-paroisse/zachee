<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\GeneralMeeting;

use App\Entity\AssociatedPerson;
use App\Entity\GeneralMeeting;
use App\Entity\Membership;
use App\Entity\Organization;
use App\Entity\PaymentRequest;
use App\Entity\Person;
use App\Entity\Summons;
use App\Entity\Title;
use App\Form\GeneralMeeting\AddAssociatedExistingPersonType;
use App\Form\GeneralMeeting\MandateType;
use App\Form\Membership\MembershipPublicInfoEditType;
use App\Form\PublicLoginCodeType;
use App\Form\PublicLoginType;
use App\Mailer\GeneralMeetingMailer;
use App\Repository\PersonRepository;
use App\Repository\SummonsRepository;
use App\Repository\TitleRepository;
use App\Repository\TypeAssociatedPersonRepository;
use App\Services\OvhService;
use App\Services\PublicLoginService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use GuzzleHttp\Exception\ClientException;
use Random\RandomException;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/public/general-meeting')]
class GeneralMeetingPublicController extends AbstractController
{
    public function __construct(
        private readonly PersonRepository $personRepository,
        private readonly PublicLoginService $publicServiceLogin,
    ) {
    }

    #[Route('/{token:generalMeeting}', name: 'general_meeting.public.index', methods: ['GET', 'POST'])]
    public function index(
        Request $request,
        GeneralMeeting $generalMeeting,
        TypeAssociatedPersonRepository $typeAssociatedPersonRepository,
        SummonsRepository $summonsRepository,
        TitleRepository $titleRepository,
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
    ): Response {
        $person = $this->publicServiceLogin->getLoggedPerson($request);
        if (null === $person) {
            return $this->redirectToRoute(
                'general_meeting.public.login',
                ['token' => $generalMeeting->getToken()],
                Response::HTTP_SEE_OTHER
            );
        }

        $types = $typeAssociatedPersonRepository->findByTags(['ag']);
        $typesById = [];
        foreach ($types as $type) {
            $typesById[$type->getId()] = $type;
        }

        /**
         * @var Summons[] $summonsList
         */
        $summonsList = [];

        // Natural member
        $natural = false;
        foreach ($person->getMemberships() as $membership) {
            foreach ($membership->getSummons() as $summons) {
                if ($generalMeeting === $summons->getGeneralMeeting()) {
                    $natural = true;
                    $summonsList[] = $summons;
                }
            }
        }

        // Legal member
        $organizations = [];
        foreach ($person->getAssociatedOrganizations() as $associatedOrganization) {
            if ($associatedOrganization->getType() && in_array($associatedOrganization->getType(), $types, true)) {
                foreach ($associatedOrganization->getOrganization()->getMemberships() as $membership) {
                    foreach ($membership->getSummons() as $summons) {
                        if ($generalMeeting === $summons->getGeneralMeeting()) {
                            if ($associatedOrganization->getType()->getId() && !array_key_exists($associatedOrganization->getType()->getId(), $organizations)) {
                                $organizations[$associatedOrganization->getType()->getId()] = [];
                            }
                            $organizations[$associatedOrganization->getType()->getId()][] = $associatedOrganization->getOrganization();
                            $summonsList[] = $summons;
                        }
                    }
                }
            }
        }

        // Create forms
        $formsMandate = [];
        $formsPerson = [];
        $formsSelect = [];
        foreach ($summonsList as $summons) {
            // Mandate
            $form = $formFactory->createNamed('mandate_'.$summons->getId(), MandateType::class, $summons);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                if ($generalMeeting->isLocked()) {
                    throw $this->createAccessDeniedException('Impossible avec une Assemblée Générale verrouillée');
                }

                if (PaymentRequest::STATUS_COMPLETED !== $summons->getMandate()?->getMembership()?->getPaymentRequest()?->getStatus()) {
                    $this->addFlash('error', 'Le pouvoir ne peut être donné qu’à un membre à jour de sa cotisation');

                    return $this->redirectToRoute(
                        'general_meeting.public.index',
                        ['token' => $generalMeeting->getToken()],
                        Response::HTTP_SEE_OTHER
                    );
                }
                if (Summons::MAX_MANDATES <= count($summons->getMandate()->getMandates())) {
                    $this->addFlash('error', 'Un membre ne peut pas recevoir plus de '.Summons::MAX_MANDATES.' pouvoirs');

                    return $this->redirectToRoute(
                        'general_meeting.public.index',
                        ['token' => $generalMeeting->getToken()],
                        Response::HTTP_SEE_OTHER
                    );
                }
                $summons->setWhoGiveMandate($person);
                $summonsRepository->save($summons, true);
                $this->addFlash('success', 'Le pouvoir a bien été enregistré');
            }

            $formsMandate[$summons->getId()] = $form->createView();

            // Representative
            if (null !== $summons->getMembership()?->getOrganization()) {
                /** @var Membership $membership */
                $membership = $summons->getMembership();

                /** @var Organization $organization */
                $organization = $summons->getMembership()->getOrganization();

                /** @var Title $title */
                $title = $titleRepository->findOneBy(['name' => 'Monsieur']);
                $associatedPerson = new AssociatedPerson();
                $person = new Person();
                $person->setFirstname('')
                    ->setLastname('')
                    ->setTitle($title)
                ;
                $associatedPerson->setOrganization($organization)
                    ->setPerson($person)
                ;
                $form = $formFactory->createNamed('add_'.$summons->getId(), MembershipPublicInfoEditType::class, $associatedPerson, ['types' => $types]);
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    if ($generalMeeting->isLocked()) {
                        throw $this->createAccessDeniedException('Impossible avec une Assemblée Générale verrouillée');
                    }

                    $membership->setInformationValidated(false);
                    $entityManager->persist($person);
                    $entityManager->persist($associatedPerson);
                    $entityManager->persist($membership);
                    $entityManager->flush();
                    $this->addFlash('success', 'La personne pouvant représenter votre organisation a bien été ajoutée');
                }
                $formsPerson[$summons->getId()] = $form->createView();

                $associatedPerson = new AssociatedPerson();
                $associatedPerson->setOrganization($organization);
                $form = $formFactory->createNamed('select_'.$summons->getId(), AddAssociatedExistingPersonType::class, $associatedPerson, ['types' => $types]);
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    if ($generalMeeting->isLocked()) {
                        throw $this->createAccessDeniedException('Impossible avec une Assemblée Générale verrouillée');
                    }

                    $membership->setInformationValidated(false);
                    $entityManager->persist($associatedPerson);
                    $entityManager->persist($membership);
                    $entityManager->flush();
                    $this->addFlash('success', 'La personne pouvant représenter votre organisation a bien été enregistrée');
                }
                $formsSelect[$summons->getId()] = $form->createView();
            }
        }

        return $this->render('general_meeting/public/index.html.twig', [
            'general_meeting' => $generalMeeting,
            'person' => $person,
            'types' => $types,
            'typesById' => $typesById,
            'natural' => $natural,
            'organizations' => $organizations,
            'summonsList' => $summonsList,
            'formsMandate' => $formsMandate,
            'formsPerson' => $formsPerson,
            'formsSelect' => $formsSelect,
        ]);
    }

    #[Route('/{token:generalMeeting}/mandate/{summons_id}/remove', name: 'general_meeting.public.mandate.remove', methods: ['GET'])]
    public function mandateRemove(
        Request $request,
        GeneralMeeting $generalMeeting,
        #[MapEntity(mapping: ['summons_id' => 'id'])]
        Summons $summons,
        TypeAssociatedPersonRepository $typeAssociatedPersonRepository,
        SummonsRepository $summonsRepository,
    ): Response {
        $person = $this->publicServiceLogin->getLoggedPerson($request);
        if (null === $person) {
            return $this->redirectToRoute(
                'general_meeting.public.login',
                ['token' => $generalMeeting->getToken()],
                Response::HTTP_SEE_OTHER
            );
        }

        // check permissions
        if ($generalMeeting !== $summons->getGeneralMeeting()) {
            throw $this->createNotFoundException('Summons and General Meeting do not match');
        }
        if ($person !== $summons->getMembership()?->getPerson() && null === $summons->getMembership()?->getOrganization()) {
            throw $this->createAccessDeniedException('Vous n’avez pas le droit de modifier ce pouvoir');
        }
        if (null !== $summons->getMembership()->getOrganization()) {
            $havePermission = false;
            $types = $typeAssociatedPersonRepository->findByTags(['ag']);
            foreach ($summons->getMembership()->getOrganization()->getAssociatedPeople() as $associatedPerson) {
                if ($person === $associatedPerson->getPerson()
                    && $associatedPerson->getType()
                    && in_array($associatedPerson->getType(), $types, true)
                ) {
                    $havePermission = true;
                }
            }
            if (!$havePermission) {
                throw $this->createAccessDeniedException('Vous n’avez pas le droit de modifier ce pouvoir');
            }
        }

        $summons->setMandate(null);
        $summons->setWhoGiveMandate(null);
        $summonsRepository->save($summons, true);
        $this->addFlash('success', 'Le pouvoir a bien été retiré');

        return $this->redirectToRoute(
            'general_meeting.public.index',
            ['token' => $generalMeeting->getToken()],
            Response::HTTP_SEE_OTHER
        );
    }

    #[Route('/{token:generalMeeting}/login', name: 'general_meeting.public.login', methods: ['GET', 'POST'])]
    public function login(
        Request $request,
        GeneralMeeting $generalMeeting,
        TypeAssociatedPersonRepository $typeAssociatedPersonRepository,
        GeneralMeetingMailer $mailer,
        OvhService $ovhService,
    ): Response {
        $person = null;
        $session = $request->getSession();
        $types = $typeAssociatedPersonRepository->findByTags(['ag']);

        $form = $this->createForm(PublicLoginType::class);

        if (!$session->has('public_login_person') || !$session->has('public_login_code') || !$session->has('public_login_method')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                if (null === $form->get('email')->getData() && null === $form->get('phone')->getData()) {
                    $this->addFlash('error', 'Vous devez renseigner votre email ou votre numéro de téléphone.');
                } else {
                    try {
                        $person = $this->personRepository->findForPublicLogin(
                            $generalMeeting,
                            $types,
                            $form->get('lastname')->getData(),
                            $form->get('email')->getData(),
                            $form->get('phone')->getData()
                        );

                        if (null === $person) {
                            $this->addFlash('error', 'Nous ne vous avons pas trouvé.');
                        } else {
                            $this->publicServiceLogin->createAndSendCode($person, null !== $form->get('phone')->getData(), $session);

                            return $this->redirectToRoute('general_meeting.public.code', ['token' => $generalMeeting->getToken()], Response::HTTP_SEE_OTHER);
                        }
                    } catch (NonUniqueResultException $e) {
                        $this->addFlash('error', 'Plusieurs personnes correspondent à ces données, impossible de vous authentifier.');
                    } catch (TransportExceptionInterface $e) {
                        $this->addFlash('error', 'Impossible de vous envoyer le mail.');
                    } catch (ClientException $e) {
                        $this->addFlash('error', 'Impossible de vous envoyer le SMS.');
                    } catch (RandomException $e) {
                        $this->addFlash('error', 'Impossible de générer le code d’authentification.');
                    }
                }
            }
        } else {
            return $this->redirectToRoute('general_meeting.public.code', ['token' => $generalMeeting->getToken()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('general_meeting/public/login.html.twig', [
            'general_meeting' => $generalMeeting,
            'person' => $person,
            'types' => $types,
            'form' => $form,
        ]);
    }

    #[Route('/{token:generalMeeting}/code', name: 'general_meeting.public.code', methods: ['GET', 'POST'])]
    public function code(
        Request $request,
        GeneralMeeting $generalMeeting,
    ): Response {
        $session = $request->getSession();

        if (!$session->has('public_login_person') || !$session->has('public_login_code')) {
            return $this->redirectToRoute('general_meeting.public.login', ['token' => $generalMeeting->getToken()], Response::HTTP_SEE_OTHER);
        }

        $person = $this->personRepository->findOneBy(['id' => $session->get('public_login_person')]);

        $form = $this->createForm(PublicLoginCodeType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->publicServiceLogin->validateCode((int) $form->get('code')->getData(), $session)) {
                return $this->redirectToRoute('general_meeting.public.index', ['token' => $generalMeeting->getToken()], Response::HTTP_SEE_OTHER);
            }

            $form->get('code')->addError(new FormError('Le code n’est pas valide.'));

            $this->addFlash('error', 'Le code n’est pas valide.');
        }

        return $this->render('general_meeting/public/code.html.twig', [
            'general_meeting' => $generalMeeting,
            'person' => $person,
            'form' => $form,
            'method' => $session->get('public_login_method', 'email'),
        ]);
    }

    #[Route('/{token:generalMeeting}/reset', name: 'general_meeting.public.reset', methods: ['GET'])]
    public function reset(Request $request, GeneralMeeting $generalMeeting): Response
    {
        $session = $request->getSession();

        $this->publicServiceLogin->reset($session);

        return $this->redirectToRoute('general_meeting.public.login', ['token' => $generalMeeting->getToken()], Response::HTTP_SEE_OTHER);
    }
}
