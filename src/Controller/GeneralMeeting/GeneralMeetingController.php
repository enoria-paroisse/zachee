<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\GeneralMeeting;

use App\Entity\GeneralMeeting;
use App\Entity\Person;
use App\Entity\Summons;
use App\Form\GeneralMeeting\GeneralMeetingType;
use App\Mailer\GeneralMeetingMailer;
use App\Repository\GeneralMeetingRepository;
use App\Repository\PersonRepository;
use App\Repository\TypeAssociatedPersonRepository;
use App\Services\GeneralMeetingService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/general-meeting')]
class GeneralMeetingController extends AbstractController
{
    #[Route('/', name: 'general_meeting.index', methods: ['GET'])]
    public function index(GeneralMeetingRepository $generalMeetingRepository, TypeAssociatedPersonRepository $typeAssociatedPersonRepository): Response
    {
        $generalMeetings = $generalMeetingRepository->findBy([], ['datetime' => 'desc']);
        $generalMeeting = count($generalMeetings)
            ? $generalMeetingRepository->findByIdFullJoin($generalMeetings[0]->getId() ?? 0)
            : null;

        return $this->render('general_meeting/index.html.twig', [
            'general_meetings' => $generalMeetings,
            'general_meeting' => $generalMeeting,
            'types' => $typeAssociatedPersonRepository->findByTags(['ag']),
        ]);
    }

    #[Route('/new', name: 'general_meeting.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_GENERAL_MEETING_EDIT')]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $generalMeeting = new GeneralMeeting();
        $form = $this->createForm(GeneralMeetingType::class, $generalMeeting);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $generalMeeting->setToken(md5(random_bytes(20)));
            $entityManager->persist($generalMeeting);
            $entityManager->flush();
            $this->addFlash('success', 'L’Assemblée Générale a bien été ajoutée');

            return $this->redirectToRoute('general_meeting.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('general_meeting/new.html.twig', [
            'general_meeting' => $generalMeeting,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'general_meeting.show', methods: ['GET'])]
    public function show(
        #[MapEntity(expr: 'repository.findByIdFullJoin(id)')]
        GeneralMeeting $generalMeeting,
        GeneralMeetingRepository $generalMeetingRepository,
        TypeAssociatedPersonRepository $typeAssociatedPersonRepository
    ): Response {
        $generalMeetings = $generalMeetingRepository->findBy([], ['datetime' => 'desc']);

        return $this->render('general_meeting/index.html.twig', [
            'general_meetings' => $generalMeetings,
            'general_meeting' => $generalMeeting,
            'types' => $typeAssociatedPersonRepository->findByTags(['ag']),
        ]);
    }

    #[Route('/{id:generalMeeting}/edit', name: 'general_meeting.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_GENERAL_MEETING_EDIT')]
    public function edit(Request $request, GeneralMeeting $generalMeeting, EntityManagerInterface $entityManager): Response
    {
        if ($generalMeeting->isLocked()) {
            throw $this->createAccessDeniedException('Impossible de modifier une Assemblée Générale verrouillée');
        }

        // to be able to remove a relation
        $summonsDocs = $generalMeeting->getSummonsDocs()->toArray();
        $reportDocs = $generalMeeting->getReportDocs()->toArray();

        $form = $this->createForm(GeneralMeetingType::class, $generalMeeting);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // remove old relations
            $summonsDocsNew = $generalMeeting->getSummonsDocs()->toArray();
            $reportDocsNew = $generalMeeting->getReportDocs()->toArray();
            foreach ($summonsDocs as $doc) {
                if (!in_array($doc, $summonsDocsNew, true)) {
                    $generalMeeting->removeSummonsDoc($doc);
                }
            }
            foreach ($reportDocs as $doc) {
                if (!in_array($doc, $reportDocsNew, true)) {
                    $generalMeeting->removeReportDoc($doc);
                }
            }

            $entityManager->flush();
            $this->addFlash('success', 'L’Assemblée Générale a bien été modifiée');

            return $this->redirectToRoute('general_meeting.show', ['id' => $generalMeeting->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('general_meeting/edit.html.twig', [
            'general_meeting' => $generalMeeting,
            'form' => $form,
        ]);
    }

    #[Route('/{id:generalMeeting}', name: 'general_meeting.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_GENERAL_MEETING_EDIT')]
    public function delete(Request $request, GeneralMeeting $generalMeeting, EntityManagerInterface $entityManager): Response
    {
        if ($generalMeeting->isLocked()) {
            throw $this->createAccessDeniedException('Impossible de modifier une Assemblée Générale verrouillée');
        }

        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if ($this->isCsrfTokenValid('delete'.$generalMeeting->getId(), $csrfToken)) {
            if (0 < count($generalMeeting->getSummons())) {
                $this->addFlash('error', 'Impossible de supprimer une Assemblée Générale avec des convocations');
            } else {
                foreach ($generalMeeting->getSummonsDocs() as $summonsDoc) {
                    $summonsDoc->removeGeneralMeetingSummons($generalMeeting);
                }
                foreach ($generalMeeting->getReportDocs() as $reportDoc) {
                    $reportDoc->removeGeneralMeetingReport($generalMeeting);
                }
                $entityManager->remove($generalMeeting);
                $entityManager->flush();
                $this->addFlash('success', 'L’Assemblée Générale a bien été supprimée');
            }
        }

        return $this->redirectToRoute('general_meeting.index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id:generalMeeting}/send/summons', name: 'general_meeting.send.summons.all', methods: ['GET'])]
    #[IsGranted('ROLE_GENERAL_MEETING_EDIT')]
    public function sendSummonsAll(
        GeneralMeeting $generalMeeting,
        TypeAssociatedPersonRepository $typeAssociatedPersonRepository,
        GeneralMeetingMailer $mailer,
        EntityManagerInterface $entityManager,
    ): Response {
        if ($generalMeeting->isLocked()) {
            throw $this->createAccessDeniedException('Impossible avec une Assemblée Générale verrouillée');
        }

        if (0 === $generalMeeting->getSummonsDocs()->count()) {
            $this->addFlash('error', 'Au moins un document de convocation doit être associé à l’Assemblée Générale.');
        } else {
            $types = $typeAssociatedPersonRepository->findByTags(['ag']);
            $summonsList = $generalMeeting->getSummons();

            try {
                foreach ($summonsList as $summons) {
                    if (null === $summons->getDate()) {
                        $mailer->sendSummons($summons, $types);
                        $summons->setDate(new \DateTime());
                        $entityManager->flush();
                    }
                }
            } catch (TransportExceptionInterface $e) {
                $this->addFlash('error', 'Une erreur est survenue dans l’envoi des convocations.');
            }
            $this->addFlash('success', 'Les convocations ont bien été envoyées.');
        }

        return $this->redirectToRoute('general_meeting.show', ['id' => $generalMeeting->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id:generalMeeting}/send/summons/{summons_id}', name: 'general_meeting.send.summons.one', methods: ['GET'])]
    #[IsGranted('ROLE_GENERAL_MEETING_EDIT')]
    public function sendSummonsOne(
        GeneralMeeting $generalMeeting,
        #[MapEntity(mapping: ['summons_id' => 'id'])]
        Summons $summons,
        TypeAssociatedPersonRepository $typeAssociatedPersonRepository,
        GeneralMeetingMailer $mailer,
        EntityManagerInterface $entityManager,
    ): Response {
        if ($generalMeeting->isLocked()) {
            throw $this->createAccessDeniedException('Impossible avec une Assemblée Générale verrouillée');
        }

        if (0 === $generalMeeting->getSummonsDocs()->count()) {
            $this->addFlash('error', 'Au moins un document de convocation doit être associé à l’Assemblée Générale.');
        } else {
            $types = $typeAssociatedPersonRepository->findByTags(['ag']);

            try {
                $mailer->sendSummons($summons, $types);
                $summons->setDate(new \DateTime());
                $entityManager->flush();

                $this->addFlash('success', 'La convocation a bien été envoyée.');
            } catch (TransportExceptionInterface $e) {
                $this->addFlash('error', 'Une erreur est survenue dans l’envoi de la convocation.');
            }
        }

        return $this->redirectToRoute('general_meeting.show', ['id' => $generalMeeting->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id:generalMeeting}/send/video', name: 'general_meeting.send.video.all', methods: ['GET'])]
    #[IsGranted('ROLE_GENERAL_MEETING_EDIT')]
    public function sendVideoAll(
        GeneralMeeting $generalMeeting,
        TypeAssociatedPersonRepository $typeAssociatedPersonRepository,
        GeneralMeetingMailer $mailer,
        EntityManagerInterface $entityManager,
    ): Response {
        if ($generalMeeting->isLocked()) {
            throw $this->createAccessDeniedException('Impossible avec une Assemblée Générale verrouillée');
        }

        if (null === $generalMeeting->getMeetingLink() || '' === $generalMeeting->getMeetingLink()) {
            $this->addFlash('error', 'Un lien vers la visio doit être définie pour l’Assemblée Générale.');
        } else {
            $types = $typeAssociatedPersonRepository->findByTags(['ag']);
            $summonsList = $generalMeeting->getSummons();

            try {
                foreach ($summonsList as $summons) {
                    if (null === $summons->getDateVideo()) {
                        $mailer->sendVideo($summons, $types);
                        $summons->setDateVideo(new \DateTime());
                        $entityManager->flush();
                    }
                }
            } catch (TransportExceptionInterface $e) {
                $this->addFlash('error', 'Une erreur est survenue dans l’envoi des mails avec le lien vers la visio.');
            }
            $this->addFlash('success', 'Les mails avec le lien vers la visio ont bien été envoyés.');
        }

        return $this->redirectToRoute('general_meeting.show', ['id' => $generalMeeting->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id:generalMeeting}/send/video/{summons_id}', name: 'general_meeting.send.video.one', methods: ['GET'])]
    #[IsGranted('ROLE_GENERAL_MEETING_EDIT')]
    public function sendVideoOne(
        GeneralMeeting $generalMeeting,
        #[MapEntity(mapping: ['summons_id' => 'id'])]
        Summons $summons,
        TypeAssociatedPersonRepository $typeAssociatedPersonRepository,
        GeneralMeetingMailer $mailer,
        EntityManagerInterface $entityManager,
    ): Response {
        if ($generalMeeting->isLocked()) {
            throw $this->createAccessDeniedException('Impossible avec une Assemblée Générale verrouillée');
        }

        if (null === $generalMeeting->getMeetingLink() || '' === $generalMeeting->getMeetingLink()) {
            $this->addFlash('error', 'Un lien vers la visio doit être définie pour l’Assemblée Générale.');
        } else {
            $types = $typeAssociatedPersonRepository->findByTags(['ag']);

            try {
                $mailer->sendVideo($summons, $types);
                $summons->setDateVideo(new \DateTime());
                $entityManager->flush();
            } catch (TransportExceptionInterface $e) {
                $this->addFlash('error', 'Une erreur est survenue dans l’envoi du mail avec le lien vers la visio.');
            }
            $this->addFlash('success', 'Le mail avec le lien vers la visio a bien été envoyé.');
        }

        return $this->redirectToRoute('general_meeting.show', ['id' => $generalMeeting->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id:generalMeeting}/send/report', name: 'general_meeting.send.report.all', methods: ['GET'])]
    #[IsGranted('ROLE_GENERAL_MEETING_EDIT')]
    public function sendReportAll(
        GeneralMeeting $generalMeeting,
        TypeAssociatedPersonRepository $typeAssociatedPersonRepository,
        GeneralMeetingMailer $mailer,
        EntityManagerInterface $entityManager,
    ): Response {
        if (0 === $generalMeeting->getReportDocs()->count()) {
            $this->addFlash('error', 'Au moins un document de compte rendu doit être associé à l’Assemblée Générale.');
        } else {
            $types = $typeAssociatedPersonRepository->findByTags(['ag']);
            $summonsList = $generalMeeting->getSummons();

            try {
                foreach ($summonsList as $summons) {
                    if (null === $summons->getDateReport()) {
                        $mailer->sendReport($summons, $types);
                        $summons->setDateReport(new \DateTime());
                        $entityManager->flush();
                    }
                }
            } catch (TransportExceptionInterface $e) {
                $this->addFlash('error', 'Une erreur est survenue dans l’envoi du compte rendu.');
            }
            $this->addFlash('success', 'Le compte rendu bien été envoyé.');
        }

        return $this->redirectToRoute('general_meeting.show', ['id' => $generalMeeting->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id:generalMeeting}/send/report/{summons_id}', name: 'general_meeting.send.report.one', methods: ['GET'])]
    #[IsGranted('ROLE_GENERAL_MEETING_EDIT')]
    public function sendReportOne(
        GeneralMeeting $generalMeeting,
        #[MapEntity(mapping: ['summons_id' => 'id'])]
        Summons $summons,
        TypeAssociatedPersonRepository $typeAssociatedPersonRepository,
        GeneralMeetingMailer $mailer,
        EntityManagerInterface $entityManager,
    ): Response {
        if (0 === $generalMeeting->getReportDocs()->count()) {
            $this->addFlash('error', 'Au moins un document de compte rendu doit être associé à l’Assemblée Générale.');
        } else {
            $types = $typeAssociatedPersonRepository->findByTags(['ag']);

            try {
                $mailer->sendReport($summons, $types);
                $summons->setDateReport(new \DateTime());
                $entityManager->flush();

                $this->addFlash('success', 'Le compte rendu a bien été envoyé.');
            } catch (TransportExceptionInterface $e) {
                $this->addFlash('error', 'Une erreur est survenue dans l’envoi du compte rendu.');
            }
        }

        return $this->redirectToRoute('general_meeting.show', ['id' => $generalMeeting->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id:generalMeeting}/lock', name: 'general_meeting.lock', methods: ['GET'])]
    #[IsGranted('ROLE_GENERAL_MEETING_EDIT')]
    public function lock(GeneralMeeting $generalMeeting, GeneralMeetingRepository $generalMeetingRepository): Response
    {
        $generalMeeting->setLocked(!$generalMeeting->isLocked());
        $generalMeetingRepository->save($generalMeeting, true);

        $this->addFlash('success', 'L’Assemblée Générale a bien été '.(!$generalMeeting->isLocked() ? 'dé' : '').'verrouillée.');

        return $this->redirectToRoute('general_meeting.show', ['id' => $generalMeeting->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id:generalMeeting}/presence', name: 'general_meeting.presence', methods: ['GET'])]
    #[IsGranted('ROLE_GENERAL_MEETING_SHOW')]
    public function presence(GeneralMeeting $generalMeeting): Response
    {
        return $this->render('general_meeting/presence.html.twig', [
            'general_meeting' => $generalMeeting,
        ]);
    }

    #[Route('/{id:generalMeeting}/presence.json', name: 'general_meeting.presence.json', methods: ['GET'])]
    #[IsGranted('ROLE_GENERAL_MEETING_SHOW')]
    public function presenceJson(
        GeneralMeeting $generalMeeting,
        TypeAssociatedPersonRepository $typeAssociatedPersonRepository,
        PersonRepository $personRepository,
    ): Response {
        $types = $typeAssociatedPersonRepository->findByTags(['ag']);

        $people = [];
        foreach ($personRepository->findByGeneralMeeting($generalMeeting, $types) as $person) {
            $result = [
                'id' => $person->getId(),
                'firstname' => $person->getFirstname(),
                'lastname' => $person->getLastname(),
                'title' => $person->getTitle()?->getName() ?? '',
                'present' => $generalMeeting->getArePresent()->contains($person),
            ];
            $organizations = [];
            foreach ($person->getAssociatedOrganizations() as $associatedOrganization) {
                if (in_array($associatedOrganization->getType(), $types, true)) {
                    $organizations[] = [
                        'id' => $associatedOrganization->getOrganization()->getId(),
                        'name' => $associatedOrganization->getOrganization()->getName(),
                        'type' => $associatedOrganization->getType()->getName(),
                        'icon' => $associatedOrganization->getType()->getIcon(),
                    ];
                }
            }
            $result['organizations'] = $organizations;
            $people[] = $result;
        }

        return $this->json([
            'generalMeeting' => [
                'id' => $generalMeeting->getId(),
            ],
            'people' => $people,
        ]);
    }

    #[Route('/{generalMeeting_id}/presence/toggle/{person_id}', name: 'general_meeting.presence.toggle', methods: ['GET'])]
    #[IsGranted('ROLE_GENERAL_MEETING_EDIT')]
    public function presenceToggle(
        #[MapEntity(mapping: ['generalMeeting_id' => 'id'])]
        GeneralMeeting $generalMeeting,
        #[MapEntity(mapping: ['person_id' => 'id'])]
        Person $person,
        TypeAssociatedPersonRepository $typeAssociatedPersonRepository,
        GeneralMeetingService $generalMeetingService,
    ): Response {
        if ($generalMeeting->isLocked()) {
            throw $this->createAccessDeniedException('Impossible avec une Assemblée Générale verrouillée');
        }

        $present = $generalMeetingService->presenceToggle($generalMeeting, $person, $typeAssociatedPersonRepository->findByTags(['ag']));

        return $this->json([
            'id' => $person->getId(),
            'firstname' => $person->getFirstname(),
            'lastname' => $person->getLastname(),
            'title' => $person->getTitle()?->getName() ?? '',
            'present' => $present,
        ]);
    }
}
