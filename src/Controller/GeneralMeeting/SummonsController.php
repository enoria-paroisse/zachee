<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\GeneralMeeting;

use App\Controller\Traits\UserableTrait;
use App\Entity\GeneralMeeting;
use App\Entity\Summons;
use App\Form\GeneralMeeting\SummonsType;
use App\Repository\MembershipRepository;
use App\Repository\PeriodRepository;
use App\Services\UploaderService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/general-meeting/summons')]
#[IsGranted('ROLE_GENERAL_MEETING_SHOW')]
class SummonsController extends AbstractController
{
    use UserableTrait;

    #[Route('/generate/{id:generalMeeting}', name: 'general_meeting.summons.generate', methods: ['GET'])]
    #[IsGranted('ROLE_GENERAL_MEETING_EDIT')]
    public function generate(
        GeneralMeeting $generalMeeting,
        PeriodRepository $periodRepository,
        MembershipRepository $membershipRepository,
        EntityManagerInterface $entityManager
    ): Response {
        if ($generalMeeting->isLocked()) {
            throw $this->createAccessDeniedException('Impossible avec une Assemblée Générale verrouillée');
        }

        $period = null !== $generalMeeting->getDatetime() ? $periodRepository->findPeriodByDate($generalMeeting->getDatetime()) : null;
        if (null === $period) {
            $this->addFlash(
                'error',
                'Impossible de générer les convocations, car aucune période ne correspond  au '.$generalMeeting->getDatetime()?->format('d/m/Y')
            );
        } else {
            $nbCreated = 0;
            $memberships = $membershipRepository->findAvailableForGeneralMeeting($period);
            foreach ($memberships as $membership) {
                $createSummons = true;
                foreach ($membership->getSummons() as $summons) {
                    if ($generalMeeting === $summons->getGeneralMeeting()) {
                        $createSummons = false;
                    }
                }
                if ($createSummons) {
                    $summons = new Summons();
                    $summons->setMembership($membership)
                        ->setGeneralMeeting($generalMeeting)
                    ;
                    ++$nbCreated;
                    $entityManager->persist($summons);
                }
            }
            $entityManager->flush();

            $this->addFlash(
                'success',
                $nbCreated.' nouvelles convocations générées à partir des adhésions de la période '.$period->getName()
            );
        }

        return $this->redirectToRoute(
            'general_meeting.show',
            ['id' => $generalMeeting->getId()],
            Response::HTTP_SEE_OTHER
        );
    }

    #[Route('/{id:summons}/edit', name: 'general_meeting.summons.edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_GENERAL_MEETING_EDIT')]
    public function edit(Request $request, Summons $summons, EntityManagerInterface $entityManager, UploaderService $uploaderService): Response
    {
        if ($summons->getGeneralMeeting()?->isLocked()) {
            throw $this->createAccessDeniedException('Impossible de modifier une convocation avec une Assemblée Générale verrouillée');
        }

        $prevMandate = $summons->getMandate()?->getId();

        $form = $this->createForm(SummonsType::class, $summons);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mandateActions = (array) $form->get('mandate_actions')->getData();
            if (in_array('remove', $mandateActions, true)) {
                $summons->setMandate(null);
                $summons->setWhoGiveMandate(null);
                if ($summons->getFileMandate()) {
                    $uploaderService->delete('mandates', $summons->getFileMandate());
                }
                $summons->setFileMandate(null);

                $entityManager->flush();
                $this->addFlash(
                    'success',
                    'Le pouvoir a bien été supprimé'
                );
            } elseif ($summons->getMandate() && Summons::MAX_MANDATES <= count($summons->getMandate()->getMandates())) {
                $this->addFlash(
                    'error',
                    'Un membre ne peut recevoir plus de '.Summons::MAX_MANDATES.' pouvoirs'
                );
            } else {
                $fileMandate = $form->get('fileMandate')->getData();
                if (is_uploaded_file((string) $fileMandate)) {
                    if (null === $summons->getMandate()) {
                        $this->addFlash('error', 'Vous ne pouvez ajouter un fichier que si un pouvoir est enregistré !');
                    } else {
                        $filename = $uploaderService->upload($fileMandate, 'mandates', $summons->getFileMandate(), 'mandate_');
                        $summons->setFileMandate($filename);
                        $summons->setWhoGiveMandate(null);
                        $this->addFlash('success', 'Le fichier avec le pouvoir a bien été ajoutée');
                    }
                }

                if ($prevMandate !== $summons->getMandate()?->getId()) {
                    $this->addFlash(
                        'warning',
                        'Attention, vous venez de modifier manuellement le pouvoir. Veillez à enregistrer le document dans lequel le membre donne son pouvoir'
                    );
                    $summons->setWhoGiveMandate(null);
                }

                $entityManager->flush();
                $this->addFlash(
                    'success',
                    'La convocations a bien été modifiée'
                );
            }

            return $this->redirectToRoute(
                'general_meeting.show',
                ['id' => $summons->getGeneralMeeting()?->getId()],
                Response::HTTP_SEE_OTHER
            );
        }

        return $this->render('general_meeting/summons/edit.html.twig', [
            'summons' => $summons,
            'form' => $form,
        ]);
    }

    #[Route('/{id:summons}', name: 'general_meeting.summons.delete', methods: ['DELETE'])]
    #[IsGranted('ROLE_GENERAL_MEETING_EDIT')]
    public function delete(Request $request, Summons $summons, EntityManagerInterface $entityManager): Response
    {
        $generalMeeting = $summons->getGeneralMeeting();

        $csrfToken = $request->request->has('_token') ? (string) $request->request->get('_token') : null;
        if (0 < count($summons->getMandates())) {
            $this->addFlash(
                'error',
                'Impossible de supprimer cette convocation, car ce membre a déjà reçu des pouvoirs.'
            );
        } elseif ($this->isCsrfTokenValid('delete'.$summons->getId(), $csrfToken)) {
            $entityManager->remove($summons);
            $entityManager->flush();
            $this->addFlash(
                'success',
                'La convocations a bien été supprimée'
            );
        }

        return $this->redirectToRoute('general_meeting.show', ['id' => $generalMeeting?->getId()], Response::HTTP_SEE_OTHER);
    }
}
