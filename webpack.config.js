let Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or subdirectory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     */
    .addEntry('app', './assets/js/app.js')
    .addEntry('intl-tel-input', './assets/js/intl-tel-input.js')
    .addEntry('internal-stats', './assets/js/internal-stats.js')
    // COMPONENTS
    // - Accounting
    .addEntry('Allocations', './assets/js/components/Accounting/Allocations.jsx')
    .addEntry('PaymentReceipt', './assets/js/components/Accounting/PaymentReceipt.jsx')
    .addEntry('PaymentRequestFile', './assets/js/components/Accounting/PaymentRequestFile.jsx')
    // - GeneralMeeting
    .addEntry('Presences', './assets/js/components/GeneralMeeting/Presences.jsx')
    // - Membership
    .addEntry('Renew', './assets/js/components/Membership/Renew.jsx')
    // - Stats
    .addEntry('Stats', './assets/js/components/Stats/Stats.jsx')
    // - Toolbar
    .addEntry('Toolbar', './assets/js/components/Toolbar/Toolbar.jsx')
    // STYLES
    .addStyleEntry('main', './assets/styles/main.scss')
    .addStyleEntry('public-layout', './assets/styles/public-layout.scss')
    .addStyleEntry('empty', './assets/styles/empty.scss')
    .addStyleEntry('empty-card', './assets/styles/empty-card.scss')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // configure Babel
    // .configureBabel((config) => {
    //     config.plugins.push('@babel/a-babel-plugin');
    // })

    // enables and configure @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = '3.38';
    })

    // enables Sass/SCSS support
    .enableSassLoader()

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    //.enableIntegrityHashes(Encore.isProduction())

    // uncomment if you're having problems with a jQuery plugin
    .autoProvidejQuery()

    // uncomment if you use API Platform Admin (composer req api-admin)
    .enableReactPreset();

module.exports = Encore.getWebpackConfig();
