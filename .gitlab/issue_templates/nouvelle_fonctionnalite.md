## Titre concis

Exposez ici la nouvelle fonctionnalité attendue avec précision

## Lieu de l'application

Où doit figurer la nouvelle fonctionnalité

## Quel serait le comportement attendu ?

Indiquez ce qui devrait se produire

## Esquisse graphique/consignes d'ergonomie

Éléments graphiques à créer
