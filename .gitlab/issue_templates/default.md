## Titre concis

Exposez ici le problème de manière la plus concise possible

## Lieu de l'application

Lien vers la page qui produit l'erreur

## Procédure pour reproduire

Exposez ici comment vous avez fait pour rencontrer le problème.

## Quel est le comportement actuel ?

Indiquez ce qui se produit

## Quel serait le comportement attendu ?

Indiquez ce qui devrait se produire

## Erreur exacte si disponible ou image du souci (avant/après)

Capture d'écran pour aider à cerner le problème

## Diagnostic éventuel

Si vous avez une idée d'où le souci pourrait provenir, indiquez le ici
