module.exports = {
    parser: '@babel/eslint-parser',
    parserOptions: {
        'requireConfigFile': false,
    },
    extends: [
        'eslint:recommended',
        'plugin:react/recommended',
        'prettier' // Need to be the last
    ],
    globals: {
        'global': true,
        '$': true,
    },
    env: {
        amd: true,
        browser: true,
    },
    plugins: ['prettier'],
    rules: {
        'max-len': [
            'error',
            {
                code: 250,
                ignoreUrls: true,
                ignoreComments: false,
                ignoreRegExpLiterals: true,
                ignoreStrings: false,
                ignoreTemplateLiterals: false,
            }
        ],
        indent: ['error', 4],
        'prettier/prettier': [
            'error',
            {
                tabWidth: 4,
            },
        ],
    },
    settings: {
        react: {
            version: 'detect',
        },
    },
};
