<?php

$finder = (new PhpCsFixer\Finder())
    ->in(__DIR__)
    ->exclude('.cache')
    ->exclude('var')
    ->exclude('public')
    ->exclude('node_modules')
;

return (new PhpCsFixer\Config())
    ->setRiskyAllowed(true)
    ->setRules([
        '@Symfony' => true,
        '@PHP83Migration' => true,
        '@PhpCsFixer' => true,
        '@PSR2' => true,
        'void_return' => true,
        'array_syntax' => ['syntax' => 'short'],
        'single_line_empty_body' => false,
        'return_assignment' => false,
    ])
    ->setFinder($finder)
;
