Zachée
======

## Installation de l'application avec Docker (recommandé)

[Consulter la documentation technique](docs/INSTALL.md)

Les commandes PHP et composer devront être préfixées par : 

        bin/tools

Les commandes Js (NodeJs, yarn ...) seront préfixées par :

        bin/node-tools

## Installation sur le serveur

Exécuter les commandes suivantes, après avoir récupéré les sources : 

        composer dump-env prod
        composer install --no-dev --optimize-autoloader
        APP_ENV=prod APP_DEBUG=0 php bin/console cache:clear
        bin/console doctrine:migrations:migrate
        yarn install --pure-lockfile
        yarn encore production

Pour une initialisation complète, lancez : 

        bin/console doctrine:fixtures:load

Une commande doit s'exécuter toutes les nuits, vous devez donc ajouter celle-ci à vos tâches cron :

        bin/console app:cron

## Mode maintenance ##

Si un fichier `MAINTENANCE` existe à la racine de l'application, Zachée sera en mode
maintenance et un message sera affiché. Ce message peut être personnalisé avec le
contenu, pouvant être du code HTML, de ce fichier.

Pour créer un fichier vide simplement, la commande suivante suffit :

        touch MAINTENANCE

## Ajouter des fonts pour la génération de PDF

Pour ajouter des polices de caractères disponibles pour la génération de PDF, exécuter
la commande suivante :

        bin/console app:fonts:add font-family-name src-to-normal-font.ttf src-to-bold.ttf src-to-italic.tff src-to-bold-italic.ttf

Vous pouvez lister les polices disponibles avec :

        bin/console app:fonts:list

## Sauvegarde ##

Penser à sauvegarder le contenu de ``var/uploads`` !

