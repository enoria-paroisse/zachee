#!/usr/bin/env sh
set -ex

bin/console doctrine:database:create -n --if-not-exists

bin/update-db.sh

if ! bin/console doctrine:query:sql 'SELECT id as first_user_id FROM user LIMIT 1' | grep -q 'first_user_id'; then
  echo "The DB seems to be empty, loading fixtures..."
  bin/console doctrine:fixtures:load -n
fi
