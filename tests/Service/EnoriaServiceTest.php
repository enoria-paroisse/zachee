<?php

namespace App\Tests\Service;

use App\Entity\Membership;
use App\Entity\Organization;
use App\Entity\PaymentRequest;
use App\Entity\Period;
use App\Services\EnoriaService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class EnoriaServiceTest extends KernelTestCase
{
    public function testGetGoodEntityStatusA1(): void
    {
        $membership = new Membership();
        $paymentRequest = new PaymentRequest();
        $paymentRequest
            ->setStatus(PaymentRequest::STATUS_COMPLETED)
        ;
        $organization = new Organization();
        $organization->setName('Test Organization');
        $membership
            ->setType(Membership::TYPE_MEMBER)
            ->setOrganization($organization)
            ->setPeriod(null)
            ->setPaymentRequest($paymentRequest)
            ->setStatus(Membership::STATUS_IN_PROGRESS)
            ->setInformationValidated(true)
            ->setSheet('ok')
        ;

        $this->assertSame('A', EnoriaService::getGoodEntityStatus($membership));
    }

    public function testGetGoodEntityStatusA2(): void
    {
        $membership = new Membership();
        $paymentRequest = new PaymentRequest();
        $paymentRequest
            ->setStatus(PaymentRequest::STATUS_COMPLETED)
            ->setLastReminder(new \DateTime())
        ;
        $organization = new Organization();
        $organization->setName('Test Organization');
        $membership
            ->setType(Membership::TYPE_MEMBER)
            ->setOrganization($organization)
            ->setPeriod(null)
            ->setPaymentRequest($paymentRequest)
            ->setStatus(Membership::STATUS_IN_PROGRESS)
            ->setInformationValidated(true)
            ->setSheet('ok')
        ;

        $this->assertSame('A', EnoriaService::getGoodEntityStatus($membership));
    }

    public function testGetGoodEntityStatusAI1(): void
    {
        $membership = new Membership();
        $paymentRequest = new PaymentRequest();
        $paymentRequest
            ->setStatus(PaymentRequest::STATUS_COMPLETED)
            ->setLastReminder(new \DateTime())
        ;
        $organization = new Organization();
        $organization->setName('Test Organization');
        $membership
            ->setType(Membership::TYPE_MEMBER)
            ->setOrganization($organization)
            ->setPeriod(null)
            ->setPaymentRequest($paymentRequest)
            ->setStatus(Membership::STATUS_IN_PROGRESS)
            ->setInformationValidated(false)
            ->setSheet('ok')
        ;

        $this->assertSame('AI', EnoriaService::getGoodEntityStatus($membership));
    }

    public function testGetGoodEntityStatusAI2(): void
    {
        $membership = new Membership();
        $paymentRequest = new PaymentRequest();
        $paymentRequest
            ->setStatus(PaymentRequest::STATUS_COMPLETED)
            ->setLastReminder(new \DateTime())
        ;
        $organization = new Organization();
        $organization->setName('Test Organization');
        $membership
            ->setType(Membership::TYPE_MEMBER)
            ->setOrganization($organization)
            ->setPeriod(null)
            ->setPaymentRequest($paymentRequest)
            ->setStatus(Membership::STATUS_IN_PROGRESS)
            ->setInformationValidated(true)
            ->setSheet(null)
        ;

        $this->assertSame('AI', EnoriaService::getGoodEntityStatus($membership));
    }

    public function testGetGoodEntityStatusRP1(): void
    {
        $membership = new Membership();
        $paymentRequest = new PaymentRequest();
        $paymentRequest
            ->setStatus(PaymentRequest::STATUS_IN_PROGRESS)
            ->setLastReminder(new \DateTime())
            ->setDatetime(date_sub(new \DateTime(), new \DateInterval('P32D')))
        ;
        $organization = new Organization();
        $organization->setName('Test Organization');
        $membership
            ->setType(Membership::TYPE_MEMBER)
            ->setOrganization($organization)
            ->setPeriod(null)
            ->setPaymentRequest($paymentRequest)
            ->setStatus(Membership::STATUS_IN_PROGRESS)
            ->setInformationValidated(true)
            ->setSheet('ok')
        ;

        $this->assertSame('RP', EnoriaService::getGoodEntityStatus($membership));
    }

    public function testGetGoodEntityStatusRP2(): void
    {
        $membership = new Membership();
        $paymentRequest = new PaymentRequest();
        $paymentRequest
            ->setStatus(PaymentRequest::STATUS_IN_PROGRESS)
            ->setDatetime(date_sub(new \DateTime(), new \DateInterval('P32D')))
        ;
        $organization = new Organization();
        $organization->setName('Test Organization');
        $membership
            ->setType(Membership::TYPE_MEMBER)
            ->setOrganization($organization)
            ->setPeriod(null)
            ->setPaymentRequest($paymentRequest)
            ->setStatus(Membership::STATUS_IN_PROGRESS)
            ->setInformationValidated(true)
            ->setSheet('ok')
            ->setLastReminder(new \DateTime())
        ;

        $this->assertSame('RP', EnoriaService::getGoodEntityStatus($membership));
    }

    public function testGetGoodEntityStatusAP1(): void
    {
        $membership = new Membership();
        $paymentRequest = new PaymentRequest();
        $paymentRequest
            ->setStatus(PaymentRequest::STATUS_IN_PROGRESS)
            ->setLastReminder(new \DateTime())
        ;
        $organization = new Organization();
        $organization->setName('Test Organization');
        $membership
            ->setType(Membership::TYPE_MEMBER)
            ->setOrganization($organization)
            ->setPeriod(null)
            ->setPaymentRequest($paymentRequest)
            ->setStatus(Membership::STATUS_IN_PROGRESS)
            ->setInformationValidated(true)
        ;

        $this->assertSame('AP', EnoriaService::getGoodEntityStatus($membership));
    }

    public function testGetGoodEntityStatusAP2(): void
    {
        $membership = new Membership();
        $paymentRequest = new PaymentRequest();
        $paymentRequest
            ->setStatus(PaymentRequest::STATUS_IN_PROGRESS)
        ;
        $organization = new Organization();
        $organization->setName('Test Organization');
        $membership
            ->setType(Membership::TYPE_MEMBER)
            ->setOrganization($organization)
            ->setPeriod(null)
            ->setPaymentRequest($paymentRequest)
            ->setStatus(Membership::STATUS_IN_PROGRESS)
            ->setInformationValidated(true)
            ->setLastReminder(new \DateTime())
        ;

        $this->assertSame('AP', EnoriaService::getGoodEntityStatus($membership));
    }

    protected function createPeriod(): Period
    {
        $period = new Period();
        $period
            ->setName('2024')
            ->setStart(new \DateTime('2024-01-01'))
            ->setEnd(new \DateTime('2024-12-31'))
            ->setMembershipfeeNatural(12.34)
            ->setMembershipfeeLegal(56.78)
            ->setDioceseBaseWasselynck(10000)
            ->setDioceseBaseMaxMembershipfee('14000')
        ;

        return $period;
    }
}
