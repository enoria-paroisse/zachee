<?php

namespace App\Tests\Service;

use App\Entity\Address;
use App\Entity\Diocese;
use App\Entity\Membership;
use App\Entity\Organization;
use App\Entity\OrganizationType;
use App\Entity\Period;
use App\Entity\Person;
use App\Entity\SubOrganizationMembershipFee;
use App\Entity\SubOrganizationMembershipFeePeriod;
use App\Entity\Wasselynck;
use App\Repository\MembershipRepository;
use App\Services\MembershipFeeService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class MembershipServiceTest extends KernelTestCase
{
    private ?EntityManagerInterface $entityManager;
    private ?MembershipRepository $membershipRepository;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;

        $this->membershipRepository = $this->entityManager->getRepository(Membership::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testDiocesePricing(): void
    {
        $membershipFeeService = new MembershipFeeService($this->membershipRepository);

        $this->assertSame(13860.0, $membershipFeeService->getDiocesePricing(9801, $this->createPeriod()));
    }

    public function testMembershipFeeWithoutPeriod(): void
    {
        $membershipFeeService = new MembershipFeeService($this->membershipRepository);

        $membership = new Membership();
        $membership->setPeriod(null)->setType(Membership::TYPE_MEMBER);

        $this->assertSame(0.0, $membershipFeeService->calculateFee($membership));
    }

    public function testMembershipFeeHonorary(): void
    {
        $membershipFeeService = new MembershipFeeService($this->membershipRepository);

        $membership = new Membership();
        $membership
            ->setPeriod($this->createPeriod())
            ->setPerson(new Person())
            ->setType(Membership::TYPE_HONORARY)
        ;

        $this->assertSame(0.0, $membershipFeeService->calculateFee($membership));
    }

    public function testMembershipFeeFounding(): void
    {
        $membershipFeeService = new MembershipFeeService($this->membershipRepository);

        $membership = new Membership();
        $membership
            ->setPeriod($this->createPeriod())
            ->setPerson(new Person())
            ->setType(Membership::TYPE_FOUNDING)
        ;

        $this->assertSame(0.0, $membershipFeeService->calculateFee($membership));
    }

    public function testMembershipFeeExOfficio(): void
    {
        $membershipFeeService = new MembershipFeeService($this->membershipRepository);

        $membership = new Membership();
        $membership
            ->setPeriod($this->createPeriod())
            ->setPerson(new Person())
            ->setType(Membership::TYPE_EX_OFFICIO)
        ;

        $this->assertSame(0.0, $membershipFeeService->calculateFee($membership));
    }

    public function testMembershipFeeNatural(): void
    {
        $membershipFeeService = new MembershipFeeService($this->membershipRepository);

        $membership = new Membership();
        $membership
            ->setPeriod($this->createPeriod())
            ->setPerson(new Person())
            ->setType(Membership::TYPE_MEMBER)
        ;

        $this->assertSame(12.34, $membershipFeeService->calculateFee($membership));
    }

    public function testMembershipFeeLegal(): void
    {
        $membershipFeeService = new MembershipFeeService($this->membershipRepository);

        $organization = new Organization();
        $membership = new Membership();
        $membership
            ->setPeriod($this->createPeriod())
            ->setOrganization($organization)
            ->setType(Membership::TYPE_MEMBER)
        ;

        $this->assertSame(56.78, $membershipFeeService->calculateFee($membership));
    }

    public function testMembershipSelfHosting(): void
    {
        $membershipFeeService = new MembershipFeeService($this->membershipRepository);

        $period = $this->createPeriod();
        $organization = new Organization();
        $organization->setDiocese($this->createDiocese($period));
        $membership = new Membership();
        $membership
            ->setPeriod($period)
            ->setOrganization($organization)
            ->setType(Membership::TYPE_MEMBER)
            ->setHosting(Membership::HOSTING_SELF)
        ;

        // 25 %
        $this->assertSame(2474.87, $membershipFeeService->calculateFee($membership));
    }

    public function testMembershipDioceseMember(): void
    {
        $membershipFeeService = new MembershipFeeService($this->membershipRepository);

        $subOrganizationMembershipFee = $this->createSubOrganizationMembershipFee();
        $this->entityManager->persist($subOrganizationMembershipFee);

        $period = $this->createPeriod();
        $this->entityManager->persist($period);

        $subOrganizationMembershipFeePeriod = $this->createSubOrganizationMembershipFeePeriod($period, $subOrganizationMembershipFee);
        $this->entityManager->persist($subOrganizationMembershipFeePeriod);

        $organizationType = new OrganizationType();
        $organizationType->setName('un type');
        $this->entityManager->persist($organizationType);

        $address = new Address();
        $address->setAddressLineA('rue Notre-Dame')
            ->setCity('Meaux')
            ->setCountry('FR')
            ->setPostalCode('77100')
        ;
        $this->entityManager->persist($address);
        $addressBis = new Address();
        $addressBis->setAddressLineA('rue Notre-Dame')
            ->setCity('Meaux')
            ->setCountry('FR')
            ->setPostalCode('77100')
        ;
        $this->entityManager->persist($addressBis);

        $wasselynck = new Wasselynck();
        $wasselynck
            ->setIndice(5000)
            ->setPeriod($period)
        ;
        $diocese = new Diocese();
        $diocese
            ->setName('Mon super diocèse')
            ->addWasselynck($wasselynck)
            ->setSubOrganizationMembershipFee($subOrganizationMembershipFee)
        ;
        $organizationDiocese = new Organization();
        $organizationDiocese
            ->setName('Un diocèse')
            ->setDiocese($diocese)
            ->setAddress($address)
            ->setType($organizationType)
        ;
        $diocese->setAssociatedOrganization($organizationDiocese);
        $membershipDiocese = new Membership();
        $membershipDiocese
            ->setPeriod($period)
            ->setOrganization($organizationDiocese)
            ->setType(Membership::TYPE_MEMBER)
            ->setHosting(Membership::HOSTING_ENORIA)
            ->setInformationSubmitted(true)
            ->setInformationValidated(true)
            ->setHelpRequest(false)
            ->setFirst(false)
            ->setStatus(Membership::STATUS_VALIDATED)
        ;
        $this->entityManager->persist($organizationDiocese);
        $this->entityManager->persist($wasselynck);
        $this->entityManager->persist($diocese);
        $this->entityManager->persist($membershipDiocese);

        $organization = new Organization();
        $organization
            ->setName('Une organisation')
            ->setDiocese($diocese)
            ->setAddress($addressBis)
            ->setType($organizationType)
        ;
        $this->entityManager->persist($organization);

        $membership = new Membership();
        $membership
            ->setPeriod($period)
            ->setOrganization($organization)
            ->setType(Membership::TYPE_MEMBER)
            ->setHosting(Membership::HOSTING_ENORIA)
        ;

        $this->entityManager->flush();

        $this->assertSame(100.00, $membershipFeeService->calculateFee($membership));

        $membershipDiocese->setStatus(Membership::STATUS_CANCELED);
        $this->assertSame(56.78, $membershipFeeService->calculateFee($membership));

        $membershipDiocese->setStatus(Membership::STATUS_IN_PROGRESS);
        $this->assertSame(100.00, $membershipFeeService->calculateFee($membership));
    }

    protected function createPeriod(): Period
    {
        $period = new Period();
        $period
            ->setName('2024')
            ->setStart(new \DateTime('2024-01-01'))
            ->setEnd(new \DateTime('2024-12-31'))
            ->setMembershipfeeNatural(12.34)
            ->setMembershipfeeLegal(56.78)
            ->setDioceseBaseWasselynck(10000)
            ->setDioceseBaseMaxMembershipfee('14000')
        ;

        return $period;
    }

    protected function createDiocese(Period $period): Diocese
    {
        $wasselynckA = new Wasselynck();
        $wasselynckA
            ->setIndice(5000)
            ->setPeriod($period)
        ;
        $wasselynckB = new Wasselynck();
        $wasselynckB
            ->setIndice(2500)
            ->setPeriod(new Period())
        ;

        $diocese = new Diocese();
        $diocese
            ->setName('Mon super diocèse')
            ->addWasselynck($wasselynckA)
            ->addWasselynck($wasselynckB)
        ;

        return $diocese;
    }

    protected function createSubOrganizationMembershipFee(): SubOrganizationMembershipFee
    {
        $subOrganizationMembershipFee = new SubOrganizationMembershipFee();
        $subOrganizationMembershipFee->setName('Base');

        return $subOrganizationMembershipFee;
    }

    protected function createSubOrganizationMembershipFeePeriod(Period $period, SubOrganizationMembershipFee $subOrganizationMembershipFee): SubOrganizationMembershipFeePeriod
    {
        $subOrganizationMembershipFeePeriod = new SubOrganizationMembershipFeePeriod();
        $subOrganizationMembershipFeePeriod->setMembershipFee('100.00')
            ->setPeriod($period)
            ->setSubOrganizationMembershipFee($subOrganizationMembershipFee)
        ;
        $period->addSubOrganizationMembershipFeePeriod($subOrganizationMembershipFeePeriod);

        return $subOrganizationMembershipFeePeriod;
    }
}
