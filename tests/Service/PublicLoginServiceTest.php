<?php

namespace App\Tests\Service;

use App\Entity\Person;
use App\Entity\Title;
use App\Mailer\UserMailer;
use App\Repository\PersonRepository;
use App\Services\OvhService;
use App\Services\PublicLoginService;
use Doctrine\ORM\EntityManagerInterface;
use libphonenumber\PhoneNumberUtil;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class FakeManagerRegistry extends ManagerRegistry
{
}

/**
 * @internal
 *
 * @coversNothing
 */
class PublicLoginServiceTest extends KernelTestCase
{
    private ?EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;
    }

    public function testGetLoggedPersonNotConnected(): void
    {
        $personRepository = $this->createMock(PersonRepository::class);
        $personRepository
            ->method('findOneBy')
            ->willReturn(new Person())
        ;
        $publicLoginService = $this->createPublicLoginService($personRepository);

        $this->assertSame(
            null,
            $publicLoginService->getLoggedPerson($this->createRequest())
        );
        $this->assertSame(
            null,
            $publicLoginService->getLoggedPerson($this->createRequest(0, true))
        );
        $this->assertSame(
            null,
            $publicLoginService->getLoggedPerson($this->createRequest(1, false))
        );
    }

    public function testGetLoggedPersonConnected(): void
    {
        $person = new Person();
        $personRepository = $this->createMock(PersonRepository::class);
        $personRepository
            ->method('findOneBy')
            ->willReturn($person)
        ;
        $publicLoginService = $this->createPublicLoginService($personRepository);

        $this->assertSame(
            $person,
            $publicLoginService->getLoggedPerson($this->createRequest(1, true))
        );
    }

    public function testGetLoggedNotFind(): void
    {
        $personRepository = $this->createMock(PersonRepository::class);
        $personRepository
            ->method('findOneBy')
            ->willReturn(null)
        ;
        $publicLoginService = $this->createPublicLoginService($personRepository);

        $this->assertSame(
            null,
            $publicLoginService->getLoggedPerson($this->createRequest(1, true))
        );
    }

    public function testSendSmsCode(): void
    {
        $ovhService = $this->createMock(OvhService::class);
        $ovhService
            ->method('sendSmsMessage')
            ->willReturnCallback(function (string $message, array $phones) {
                $this->assertSame('Zachée: votre code pour vous identifier est 123 @zachee.association-enoria.org #123', $message);
                $this->assertSame(['+480000000000'], $phones);

                return [];
            })
        ;
        $personRepository = $this->createMock(PersonRepository::class);

        $phoneUtil = PhoneNumberUtil::getInstance();
        $person = new Person();
        $person->setPhone($phoneUtil->parse('+480000000000'));

        $publicLoginService = $this->createPublicLoginService($personRepository, $ovhService);
        $publicLoginService->sendSmsCode($person, 123);
    }

    public function testSendSmsCodeNoPhone(): void
    {
        $personRepository = $this->createMock(PersonRepository::class);

        $person = new Person();
        $person->setPhone(null);

        $publicLoginService = $this->createPublicLoginService($personRepository);

        $this->expectException(\TypeError::class);
        $this->expectExceptionMessage('Aucun numéro de téléphone trouvé');

        $publicLoginService->sendSmsCode($person, 123);
    }

    public function testCreateAndSendCodePhone(): void
    {
        $title = new Title();
        $title->setSex('M')->setName('Monsieur');
        $person = $this->createPerson($title);
        $this->entityManager->persist($title);
        $this->entityManager->persist($person);
        $this->entityManager->flush();

        $ovhService = $this->createMock(OvhService::class);
        $ovhService
            ->method('sendSmsMessage')
            ->willReturnCallback(function (string $message, array $phones) {
                $this->assertMatchesRegularExpression('/Zachée: votre code pour vous identifier est \d+ @zachee.association-enoria.org #\d+/', $message);
                $this->assertSame(['+480000000000'], $phones);

                return [];
            })
        ;

        $personRepository = $this->createMock(PersonRepository::class);
        $publicLoginService = $this->createPublicLoginService($personRepository, $ovhService);
        $sessionResult = [];
        $session = $this->createMock(Session::class);
        $session
            ->method('set')
            ->willReturnCallback(function (string $name, $value) use (&$sessionResult): void {
                if ('public_login_code' !== $name) {
                    $sessionResult[] = [$name, $value];
                }
            })
        ;

        $publicLoginService->createAndSendCode($person, true, $session);
        $this->assertSame([
            ['public_login_person', $person->getId()],
            ['public_login_method', 'phone'],
            ['public_logged', false],
        ], $sessionResult);
    }

    public function testCreateAndSendCodeEmail(): void
    {
        $title = new Title();
        $title->setSex('M')->setName('Monsieur');
        $person = $this->createPerson($title);
        $this->entityManager->persist($title);
        $this->entityManager->persist($person);
        $this->entityManager->flush();

        $mailer = $this->createMock(UserMailer::class);
        $mailer
            ->method('sendCode')
            ->willReturnCallback(function (Person $personArg, $code) use ($person) {
                $this->assertSame($person, $personArg);
                $this->assertIsInt($code);

                return [];
            })
        ;

        $personRepository = $this->createMock(PersonRepository::class);
        $publicLoginService = $this->createPublicLoginService($personRepository, null, $mailer);
        $sessionResult = [];
        $session = $this->createMock(Session::class);
        $session
            ->method('set')
            ->willReturnCallback(function (string $name, $value) use (&$sessionResult): void {
                if ('public_login_code' !== $name) {
                    $sessionResult[] = [$name, $value];
                }
            })
        ;

        $publicLoginService->createAndSendCode($person, false, $session);
        $this->assertSame([
            ['public_login_person', $person->getId()],
            ['public_login_method', 'email'],
            ['public_logged', false],
        ], $sessionResult);
    }

    public function testCreateAndSendCodeRandom(): void
    {
        $title = new Title();
        $title->setSex('M')->setName('Monsieur');
        $person = $this->createPerson($title);
        $this->entityManager->persist($title);
        $this->entityManager->persist($person);
        $this->entityManager->flush();

        $personRepository = $this->createMock(PersonRepository::class);
        $publicLoginService = $this->createPublicLoginService($personRepository);
        $codes = [];
        $session = $this->createMock(Session::class);
        $session
            ->method('set')
            ->willReturnCallback(function (string $name, $value) use (&$codes): void {
                if ('public_login_code' === $name) {
                    $codes[] = [$name, $value];
                }
            })
        ;

        $publicLoginService->createAndSendCode($person, false, $session);
        $publicLoginService->createAndSendCode($person, false, $session);
        $this->assertSame(true, $codes[0] && $codes[1] && $codes[0][1] !== $codes[1][1]);
    }

    public function testValidateCodeGood(): void
    {
        $personRepository = $this->createMock(PersonRepository::class);
        $publicLoginService = $this->createPublicLoginService($personRepository);
        $sessionResult = [];
        $logged = false;
        $session = $this->createMock(Session::class);
        $session
            ->method('get')
            ->willReturn(123)
        ;
        $session
            ->method('remove')
            ->willReturnCallback(function (string $name) use (&$sessionResult): void {
                $sessionResult[] = $name;
            })
        ;
        $session
            ->method('set')
            ->willReturnCallback(function (string $name, $value) use (&$logged): void {
                if ('public_logged' === $name) {
                    $logged = $value;
                }
            })
        ;

        $this->assertSame(true, $publicLoginService->validateCode(123, $session));
        $this->assertSame(false, $publicLoginService->validateCode(456, $session));
        $this->assertSame([
            'public_login_code',
            'public_login_method',
        ], $sessionResult);
        $this->assertSame(true, $logged);
    }

    public function testReset(): void
    {
        $personRepository = $this->createMock(PersonRepository::class);
        $publicLoginService = $this->createPublicLoginService($personRepository);
        $sessionResult = [];
        $logged = false;
        $session = $this->createMock(Session::class);
        $session
            ->method('remove')
            ->willReturnCallback(function (string $name) use (&$sessionResult): void {
                $sessionResult[] = $name;
            })
        ;

        $publicLoginService->reset($session);
        $this->assertSame([
            'public_login_person',
            'public_login_code',
            'public_login_method',
            'public_logged',
        ], $sessionResult);
    }

    protected function createPublicLoginService(
        PersonRepository $personRepository,
        ?OvhService $ovhService = null,
        ?UserMailer $userMailer = null,
    ): PublicLoginService {
        if (!$ovhService) {
            $ovhService = $this->createMock(OvhService::class);
        }
        if (!$userMailer) {
            $userMailer = $this->createMock(UserMailer::class);
        }

        return new PublicLoginService($personRepository, $ovhService, $userMailer);
    }

    protected function createRequest($person = 0, $logged = false, ?Session $session = null): Request
    {
        if (!$session) {
            $session = $this->createSession($person, $logged);
        }
        $request = new Request();
        $request->setSession($session);

        return $request;
    }

    protected function createSession($person = 0, $logged = false): Session
    {
        $session = $this->createMock(Session::class);
        $session
            ->method('has')
            ->willReturn(0 !== $person)
        ;
        $session
            ->method('get')
            ->willReturnMap([
                ['public_login_person', $person],
                ['public_logged', false, $logged],
            ])
        ;

        return $session;
    }

    protected function createPerson(?Title $title = null): Person
    {
        if (!$title) {
            $title = new Title();
            $title->setSex('M')->setName('Monsieur');
        }
        $phoneUtil = PhoneNumberUtil::getInstance();

        $person = new Person();
        $person
            ->setSex('M')
            ->setTitle($title)
            ->setFirstName('John')
            ->setLastName('Doe')
            ->setPhone($phoneUtil->parse('+480000000000'))
        ;

        return $person;
    }
}
