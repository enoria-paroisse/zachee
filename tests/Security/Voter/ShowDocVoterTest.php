<?php

namespace App\Tests\Security\Voter;

use App\Entity\Doc;
use App\Entity\Folder;
use App\Entity\User;
use App\Security\ShowDocVoter;
use App\Services\DocService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManager;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\RoleHierarchyVoter;
use Symfony\Component\Security\Core\Authorization\Voter\RoleVoter;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchy;

/**
 * @internal
 *
 * @coversNothing
 */
final class ShowDocVoterTest extends KernelTestCase
{
    /**
     * @dataProvider providePublicData
     */
    public function testPublicAccess(string $user): void
    {
        $token = $this->createToken($this->getUsers()[$user]);

        $folder = new Folder();
        $doc = new Doc();
        $doc->setFolder($folder)
            ->setTags(['public'])
        ;
        $doc2 = new Doc();
        $doc2->setFolder($folder)
            ->setTags(['membership_public_legal'])
        ;
        $doc3 = new Doc();
        $doc3->setFolder($folder)
            ->setTags(['membership_public_natural'])
        ;
        $doc4 = new Doc();
        $doc4->setFolder($folder)
            ->setTags(['organization_diocese'])
        ;
        $doc5 = new Doc();
        $doc5->setFolder($folder)
            ->setTags(['ag'])
        ;
        $doc6 = new Doc();
        $doc6->setFolder($folder)
            ->setTags(['ag-multi'])
        ;

        $result = $this->getVoter()->vote($token, $doc, ['show_doc']);
        $result2 = $this->getVoter()->vote($token, $doc2, ['show_doc']);
        $result3 = $this->getVoter()->vote($token, $doc3, ['show_doc']);
        $result4 = $this->getVoter()->vote($token, $doc4, ['show_doc']);
        $result5 = $this->getVoter()->vote($token, $doc5, ['show_doc']);
        $result6 = $this->getVoter()->vote($token, $doc6, ['show_doc']);

        $this->assertSame(VoterInterface::ACCESS_GRANTED, $result);
        $this->assertSame(VoterInterface::ACCESS_GRANTED, $result2);
        $this->assertSame(VoterInterface::ACCESS_GRANTED, $result3);
        $this->assertSame(VoterInterface::ACCESS_GRANTED, $result4);
        $this->assertSame(VoterInterface::ACCESS_GRANTED, $result5);
        $this->assertSame(VoterInterface::ACCESS_GRANTED, $result6);
    }

    /**
     * @dataProvider providePublicData
     */
    public function testNoTagAccess(string $user, int $expected): void
    {
        $token = $this->createToken($this->getUsers()[$user]);

        $folder = new Folder();
        $doc = new Doc();
        $doc->setFolder($folder)
            ->setTags([])
        ;

        $result = $this->getVoter()->vote($token, $doc, ['show_doc']);

        $this->assertSame($expected, $result);
    }

    protected static function providePublicData(): array
    {
        return [
            ['public', VoterInterface::ACCESS_DENIED],
            ['userSimple', VoterInterface::ACCESS_DENIED],
            ['userBasicShow', VoterInterface::ACCESS_DENIED],
            ['userAdmin', VoterInterface::ACCESS_GRANTED],
            ['userDocEdit', VoterInterface::ACCESS_GRANTED],
            ['userCaEdit', VoterInterface::ACCESS_DENIED],
        ];
    }

    /**
     * @dataProvider provideRoleData
     */
    public function testRoleAccess(string $user, array $expected): void
    {
        $token = $this->createToken($this->getUsers()[$user]);

        $folder = new Folder();
        $doc = new Doc();
        $doc->setFolder($folder)
            ->setTags(['role_basic_show'])
        ;
        $doc2 = new Doc();
        $doc2->setFolder($folder)
            ->setTags(['role_admin'])
        ;
        $doc3 = new Doc();
        $doc3->setFolder($folder)
            ->setTags(['role_doc_edit'])
        ;
        $doc4 = new Doc();
        $doc4->setFolder($folder)
            ->setTags(['role_ca_edit'])
        ;

        $result = $this->getVoter()->vote($token, $doc, ['show_doc']);
        $result2 = $this->getVoter()->vote($token, $doc2, ['show_doc']);
        $result3 = $this->getVoter()->vote($token, $doc3, ['show_doc']);
        $result4 = $this->getVoter()->vote($token, $doc4, ['show_doc']);

        $this->assertSame($expected[0], $result);
        $this->assertSame($expected[1], $result2);
        $this->assertSame($expected[2], $result3);
        $this->assertSame($expected[3], $result4);
    }

    protected static function provideRoleData(): array
    {
        return [
            ['public', [VoterInterface::ACCESS_DENIED, VoterInterface::ACCESS_DENIED, VoterInterface::ACCESS_DENIED, VoterInterface::ACCESS_DENIED]],
            ['userSimple', [VoterInterface::ACCESS_DENIED, VoterInterface::ACCESS_DENIED, VoterInterface::ACCESS_DENIED, VoterInterface::ACCESS_DENIED]],
            ['userBasicShow', [VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_DENIED, VoterInterface::ACCESS_DENIED, VoterInterface::ACCESS_DENIED]],
            ['userAdmin', [VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED]],
            ['userDocEdit', [VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED]],
            ['userCaEdit', [VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_DENIED, VoterInterface::ACCESS_DENIED, VoterInterface::ACCESS_GRANTED]],
        ];
    }

    /**
     * @dataProvider provideFolderInherit
     */
    public function testFolderInherit(string $user, array $expected): void
    {
        $token = $this->createToken($this->getUsers()[$user]);

        $folder = new Folder();
        $folder->setTags(['public']);
        $folder2 = new Folder();
        $folder2->setTags(['role_ca_edit']);

        $doc = new Doc();
        $doc->setFolder($folder)
            ->setTags(['role_ca_edit'])
        ;
        $doc2 = new Doc();
        $doc2->setFolder($folder2)
            ->setTags(['ag'])
        ;
        $doc3 = new Doc();
        $doc3->setFolder($folder2)
            ->setTags(['role_admin'])
        ;

        $result = $this->getVoter()->vote($token, $doc, ['show_doc']);
        $result2 = $this->getVoter()->vote($token, $doc2, ['show_doc']);
        $result3 = $this->getVoter()->vote($token, $doc3, ['show_doc']);

        $this->assertSame($expected[0], $result);
        $this->assertSame($expected[1], $result2);
        $this->assertSame($expected[2], $result3);
    }

    protected static function provideFolderInherit(): array
    {
        return [
            ['public', [VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_DENIED]],
            ['userSimple', [VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_DENIED]],
            ['userBasicShow', [VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_DENIED]],
            ['userAdmin', [VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED]],
            ['userDocEdit', [VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED]],
            ['userCaEdit', [VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED]],
        ];
    }

    /**
     * @dataProvider provideSubFolderInherit
     */
    public function testSubFolderInherit(string $user, array $expected): void
    {
        $token = $this->createToken($this->getUsers()[$user]);

        $folder = new Folder();
        $folder->setTags(['public']);
        $folder2 = new Folder();
        $folder2->setTags(['role_ca_edit']);
        $folder->addFolder($folder2);

        $folder3 = new Folder();
        $folder3->setTags(['role_basic_show']);
        $folder4 = new Folder();
        $folder4->setTags(['role_doc_edit']);
        $folder3->addFolder($folder4);

        $doc = new Doc();
        $doc->setFolder($folder2)->setTags([]);
        $doc2 = new Doc();
        $doc2->setFolder($folder4)->setTags([]);

        $result = $this->getVoter()->vote($token, $doc, ['show_doc']);
        $result2 = $this->getVoter()->vote($token, $doc2, ['show_doc']);

        $this->assertSame($expected[0], $result);
        $this->assertSame($expected[1], $result2);
    }

    protected static function provideSubFolderInherit(): array
    {
        return [
            ['public', [VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_DENIED]],
            ['userSimple', [VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_DENIED]],
            ['userBasicShow', [VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED]],
            ['userAdmin', [VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED]],
            ['userDocEdit', [VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED]],
            ['userCaEdit', [VoterInterface::ACCESS_GRANTED, VoterInterface::ACCESS_GRANTED]],
        ];
    }

    /**
     * @return array<string, User>
     */
    protected static function getUsers(): array
    {
        $userSimple = new User();
        $userBasicShow = (new User())->setRoles(['ROLE_BASIC_SHOW']);
        $userAdmin = (new User())->setRoles(['ROLE_ADMIN']);
        $userDocEdit = (new User())->setRoles(['ROLE_DOC_EDIT']);
        $userCaEdit = (new User())->setRoles(['ROLE_CA_EDIT']);

        return [
            'public' => null,
            'userSimple' => $userSimple,
            'userBasicShow' => $userBasicShow,
            'userAdmin' => $userAdmin,
            'userDocEdit' => $userDocEdit,
            'userCaEdit' => $userCaEdit,
        ];
    }

    protected function createToken(?User $user): TokenInterface
    {
        $token = $this->createMock(TokenInterface::class);
        $token->method('getUser')->willReturn($user);
        $token->method('getRoleNames')->willReturn($user ? $user->getRoles() : []);

        return $token;
    }

    protected static function getAccessDecisionManager(): AccessDecisionManagerInterface
    {
        $roleHierarchy = new RoleHierarchy([
            'ROLE_USER' => [],
            'ROLE_BASIC_SHOW' => [],
            'ROLE_DIOCESE_EDIT' => ['ROLE_BASIC_SHOW'],
            'ROLE_PERSON_EDIT' => ['ROLE_BASIC_SHOW'],
            'ROLE_ORGANIZATION_EDIT' => ['ROLE_BASIC_SHOW'],
            'ROLE_VOLUNTEER_EDIT' => ['ROLE_BASIC_SHOW'],
            'ROLE_CA_EDIT' => ['ROLE_BASIC_SHOW'],
            'ROLE_MEMBERSHIP_REQUEST_SHOW' => ['ROLE_BASIC_SHOW'],
            'ROLE_MEMBERSHIP_EDIT' => ['ROLE_MEMBERSHIP_REQUEST_SHOW'],
            'ROLE_AGGREGATE_EDIT' => ['ROLE_BASIC_SHOW'],
            'ROLE_DOC_EDIT' => [],
            'ROLE_USER_SHOW' => ['ROLE_BASIC_SHOW'],
            'ROLE_USER_EDIT' => ['ROLE_USER_SHOW', 'ROLE_ALLOWED_TO_SWITCH'],
            'ROLE_ACCOUNTING' => ['ROLE_BASIC_SHOW'],
            'ROLE_GENERAL_MEETING_SHOW' => ['ROLE_BASIC_SHOW'],
            'ROLE_GENERAL_MEETING_EDIT' => ['ROLE_GENERAL_MEETING_SHOW'],
            'ROLE_ADMIN' => [
                'ROLE_DIOCESE_EDIT',
                'ROLE_PERSON_EDIT',
                'ROLE_ORGANIZATION_EDIT',
                'ROLE_VOLUNTEER_EDIT',
                'ROLE_CA_EDIT',
                'ROLE_MEMBERSHIP_EDIT',
                'ROLE_AGGREGATE_EDIT',
                'ROLE_DOC_EDIT',
                'ROLE_USER_EDIT',
                'ROLE_ACCOUNTING',
                'ROLE_GENERAL_MEETING_EDIT',
                'ROLE_USER_ADMIN',
                'ROLE_STATS_SHOW',
            ],
        ]);

        return new AccessDecisionManager([
            new RoleVoter(),
            new RoleHierarchyVoter($roleHierarchy),
        ]);
    }

    protected function getVoter(): ShowDocVoter
    {
        return new ShowDocVoter($this->getAccessDecisionManager(), new DocService(new Security(new Container(), [])));
    }
}
