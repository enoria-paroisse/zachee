<?php

namespace App\Tests\Security\Voter;

use App\Entity\AssociatedPerson;
use App\Entity\Diocese;
use App\Entity\Organization;
use App\Entity\Person;
use App\Entity\User;
use App\Repository\AssociatedPersonRepository;
use App\Security\Traits\CanShowTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CanShow
{
    use CanShowTrait;

    public function __construct(protected AssociatedPersonRepository $associatedPersonRepository)
    {
    }

    public function canShowDiocesePublic(Diocese $diocese, User $user): bool
    {
        return $this->canShowDiocese($diocese, $user);
    }

    public function canShowOrganizationPublic(Organization $organization, User $user): bool
    {
        return $this->canShowOrganization($organization, $user);
    }

    public function canShowPersonPublic(Person $person, User $user): bool
    {
        return $this->canShowPerson($person, $user);
    }
}

/**
 * @internal
 *
 * @coversNothing
 */
final class CanShowTraitTest extends KernelTestCase
{
    private ?EntityManagerInterface $entityManager;
    private ?CanShow $canShow;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;

        $associatedPersonRepository = $this->entityManager->getRepository(AssociatedPerson::class);
        $this->canShow = new CanShow($associatedPersonRepository);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testCanShowDiocese(): void
    {
        $person = new Person();
        $person2 = new Person();
        $user = new User();
        $user->setPerson($person);
        $user2 = new User();
        $user2->setPerson($person2);

        $organization = new Organization();
        $diocese = new Diocese();
        $diocese->setAssociatedOrganization($organization);

        $organization2 = new Organization();
        $diocese2 = new Diocese();
        $diocese2->setAssociatedOrganization($organization2);

        $diocese3 = new Diocese();
        $diocese3->setAssociatedOrganization(null);

        $this->associatePersonOrganization($person, $organization);
        $this->associatePersonOrganization($person2, $organization2);

        $this->assertSame(true, $this->canShow->canShowDiocesePublic($diocese, $user));
        $this->assertSame(false, $this->canShow->canShowDiocesePublic($diocese2, $user));
        $this->assertSame(false, $this->canShow->canShowDiocesePublic($diocese, $user2));
        $this->assertSame(true, $this->canShow->canShowDiocesePublic($diocese2, $user2));
        $this->assertSame(false, $this->canShow->canShowDiocesePublic($diocese3, $user));
        $this->assertSame(false, $this->canShow->canShowDiocesePublic($diocese3, $user2));
    }

    public function testCanShowOrganizationAndDiocese(): void
    {
        $person = new Person();
        $person2 = new Person();
        $user = new User();
        $user->setPerson($person);
        $user2 = new User();
        $user2->setPerson($person2);

        $organizationDiocese = new Organization();
        $diocese = new Diocese();
        $diocese->setAssociatedOrganization($organizationDiocese);
        $this->associatePersonOrganization($person, $organizationDiocese);
        $organization = new Organization();
        $organization->setDiocese($diocese);

        $organizationDiocese2 = new Organization();
        $diocese2 = new Diocese();
        $diocese2->setAssociatedOrganization($organizationDiocese2);
        $organization2 = new Organization();
        $organization2->setDiocese($diocese2);

        $this->assertSame(true, $this->canShow->canShowOrganizationPublic($organization, $user));
        $this->assertSame(false, $this->canShow->canShowOrganizationPublic($organization2, $user));
        $this->assertSame(false, $this->canShow->canShowOrganizationPublic($organization2, $user));
    }

    public function testCanShowPersonSameUser(): void
    {
        $person = new Person();
        $person2 = new Person();
        $user = new User();
        $user->setPerson($person);
        $user2 = new User();
        $user2->setPerson($person2);

        $this->assertSame(true, $this->canShow->canShowPersonPublic($person, $user));
        $this->assertSame(false, $this->canShow->canShowPersonPublic($person, $user2));
        $this->assertSame(false, $this->canShow->canShowPersonPublic($person2, $user));
    }

    protected function associatePersonOrganization(Person $person, Organization $organization): AssociatedPerson
    {
        $associatedPerson = new AssociatedPerson();
        $associatedPerson->setPerson($person)->setOrganization($organization);
        $person->getAssociatedOrganizations()->add($associatedPerson);
        $organization->getAssociatedPeople()->add($associatedPerson);

        return $associatedPerson;
    }
}
