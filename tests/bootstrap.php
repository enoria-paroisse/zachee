<?php

use Symfony\Component\Dotenv\Dotenv;

require dirname(__DIR__).'/vendor/autoload.php';

if (method_exists(Dotenv::class, 'bootEnv')) {
    (new Dotenv())->bootEnv(dirname(__DIR__).'/.env');
}

// executes the "php bin/console cache:clear" command
passthru(sprintf(
    'APP_ENV=%s php "%s/../bin/console" cache:clear --no-warmup',
    $_ENV['APP_ENV'],
    __DIR__
));

// Clean database
passthru(sprintf(
    'php "%s/../bin/console" --env=test doctrine:database:drop --if-exists --force',
    __DIR__
));
passthru(sprintf(
    'php "%s/../bin/console" --env=test doctrine:database:create --if-not-exists',
    __DIR__
));
passthru(sprintf(
    'php "%s/../bin/console" --env=test doctrine:migrations:migrate -n',
    __DIR__
));
passthru(sprintf(
    'php "%s/../bin/console" --env=test doctrine:fixtures:load -n',
    __DIR__
));
if ($_SERVER['APP_DEBUG']) {
    umask(0o000);
}
