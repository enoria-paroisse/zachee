# Load a SQL dump

To load a SQL dump into your development environment, simply run the following command:

    docker-compose exec -T mysql mysql -u enoria --password=enoriapwd zachee --default-character-set=utf8mb4  < ./enoria.sql

Replace:

 - `./enoria.sql` with the path to your SQL file

The encoding, with the `--default-character-set` option is important.
