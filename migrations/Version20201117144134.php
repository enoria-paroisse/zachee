<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201117144134 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add membership';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE entity (id INT AUTO_INCREMENT NOT NULL, instance_id INT NOT NULL, name VARCHAR(255) NOT NULL, id_in_instance INT NOT NULL, INDEX IDX_E2844683A51721D (instance_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE instance (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE membership (id INT AUTO_INCREMENT NOT NULL, period_id INT NOT NULL, organization_id INT DEFAULT NULL, person_id INT DEFAULT NULL, entity_id INT DEFAULT NULL, status VARCHAR(20) NOT NULL, type VARCHAR(20) NOT NULL, hosting VARCHAR(20) DEFAULT NULL, membership_fee NUMERIC(10, 2) NOT NULL, membership_fee_paid NUMERIC(10, 2) NOT NULL, notes VARCHAR(255) DEFAULT NULL, sheet VARCHAR(255) DEFAULT NULL, information_submitted TINYINT(1) NOT NULL, information_validated TINYINT(1) NOT NULL, help_request TINYINT(1) NOT NULL, help_accepted TINYINT(1) NOT NULL, first TINYINT(1) NOT NULL, token VARCHAR(255) DEFAULT NULL, INDEX IDX_86FFD285EC8B7ADE (period_id), INDEX IDX_86FFD28532C8A3DE (organization_id), INDEX IDX_86FFD285217BBB47 (person_id), INDEX IDX_86FFD28581257D5D (entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE entity ADD CONSTRAINT FK_E2844683A51721D FOREIGN KEY (instance_id) REFERENCES instance (id)');
        $this->addSql('ALTER TABLE membership ADD CONSTRAINT FK_86FFD285EC8B7ADE FOREIGN KEY (period_id) REFERENCES period (id)');
        $this->addSql('ALTER TABLE membership ADD CONSTRAINT FK_86FFD28532C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id)');
        $this->addSql('ALTER TABLE membership ADD CONSTRAINT FK_86FFD285217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE membership ADD CONSTRAINT FK_86FFD28581257D5D FOREIGN KEY (entity_id) REFERENCES entity (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE membership DROP FOREIGN KEY FK_86FFD28581257D5D');
        $this->addSql('ALTER TABLE entity DROP FOREIGN KEY FK_E2844683A51721D');
        $this->addSql('DROP TABLE entity');
        $this->addSql('DROP TABLE instance');
        $this->addSql('DROP TABLE membership');
    }
}
