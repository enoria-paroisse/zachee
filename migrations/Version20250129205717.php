<?php

/**
 * @copyright Copyright (c) 2025 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250129205717 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add CGU, clean db and add 2FA';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user ADD cgu_validated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE entity CHANGE status status VARCHAR(5) NOT NULL');
        $this->addSql('ALTER TABLE `group` CHANGE viewable viewable TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE instance CHANGE sms_billing sms_billing TINYINT(1) NOT NULL, CHANGE sync_up_enabled sync_up_enabled TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE membership CHANGE reminder_enabled reminder_enabled TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
        $this->addSql('CREATE TABLE webauthn_credentials (id VARCHAR(255) NOT NULL, public_key_credential_id VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, transports JSON NOT NULL, attestation_type VARCHAR(255) NOT NULL, trust_path JSON NOT NULL, aaguid TINYTEXT NOT NULL, credential_public_key LONGTEXT NOT NULL, user_handle VARCHAR(255) NOT NULL, counter INT NOT NULL, other_ui JSON DEFAULT NULL, backup_eligible TINYINT(1) DEFAULT NULL, backup_status TINYINT(1) DEFAULT NULL, uv_initialized TINYINT(1) DEFAULT NULL, key_name VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_DFEA849072A8BD77 (public_key_credential_id), INDEX idx_webauthn_credentials_user_handle (user_handle), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user ADD email_auth_enabled TINYINT(1) NOT NULL, ADD auth_code VARCHAR(255) DEFAULT NULL, ADD trusted_version INT NOT NULL, ADD google_authenticator_secret VARCHAR(255) DEFAULT NULL, ADD backup_codes JSON NOT NULL');
        $this->addSql('UPDATE user set trusted_version = 0, backup_codes = \'[]\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE webauthn_credentials');
        $this->addSql('ALTER TABLE user DROP email_auth_enabled, DROP auth_code, DROP trusted_version, DROP google_authenticator_secret, DROP backup_codes');
        $this->addSql('ALTER TABLE user DROP cgu_validated_at');
        $this->addSql('ALTER TABLE entity CHANGE status status VARCHAR(5) DEFAULT \'A\' NOT NULL');
        $this->addSql('ALTER TABLE `group` CHANGE viewable viewable TINYINT(1) DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE instance CHANGE sms_billing sms_billing TINYINT(1) DEFAULT 0 NOT NULL, CHANGE sync_up_enabled sync_up_enabled TINYINT(1) DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE membership CHANGE reminder_enabled reminder_enabled TINYINT(1) DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }
}
