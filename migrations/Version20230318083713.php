<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230318083713 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'update Receipt';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE receipt ADD payment_id INT NOT NULL, ADD file VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE receipt CHANGE created_at created_at DATETIME DEFAULT NULL, CHANGE receipt_date receipt_date DATETIME DEFAULT NULL, CHANGE receipt_paid_date receipt_paid_date DATETIME DEFAULT NULL, CHANGE total_amount total_amount DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('UPDATE receipt INNER JOIN payment ON payment.membership = receipt.membership_id SET receipt.payment_id = payment.id');
        $this->addSql('ALTER TABLE receipt ADD CONSTRAINT FK_5399B6454C3A3BB FOREIGN KEY (payment_id) REFERENCES payment (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5399B6454C3A3BB ON receipt (payment_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE receipt CHANGE created_at created_at DATETIME NOT NULL, CHANGE receipt_date receipt_date DATETIME NOT NULL, CHANGE receipt_paid_date receipt_paid_date DATETIME NOT NULL, CHANGE total_amount total_amount DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE receipt DROP FOREIGN KEY FK_5399B6454C3A3BB');
        $this->addSql('DROP INDEX UNIQ_5399B6454C3A3BB ON receipt');
        $this->addSql('ALTER TABLE receipt DROP payment_id, DROP file');
    }
}
