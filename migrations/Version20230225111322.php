<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230225111322 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'update tables';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE address CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE address RENAME INDEX idx_d4e6f81ca83c286 TO IDX_D4E6F81896DBBDE');
        $this->addSql('ALTER TABLE diocese CHANGE country country VARCHAR(2) NOT NULL');
        $this->addSql('ALTER TABLE membership CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE organization CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE organization RENAME INDEX idx_c1ee637cca83c286 TO IDX_C1EE637C896DBBDE');
        $this->addSql('ALTER TABLE person CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE person RENAME INDEX idx_34dcd176ca83c286 TO IDX_34DCD176896DBBDE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE address CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
        $this->addSql('ALTER TABLE address RENAME INDEX idx_d4e6f81896dbbde TO IDX_D4E6F81CA83C286');
        $this->addSql('ALTER TABLE diocese CHANGE country country VARCHAR(2) DEFAULT \'FR\' NOT NULL');
        $this->addSql('ALTER TABLE membership CHANGE created_at created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
        $this->addSql('ALTER TABLE organization CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
        $this->addSql('ALTER TABLE organization RENAME INDEX idx_c1ee637c896dbbde TO IDX_C1EE637CCA83C286');
        $this->addSql('ALTER TABLE person CHANGE updated_at updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
        $this->addSql('ALTER TABLE person RENAME INDEX idx_34dcd176896dbbde TO IDX_34DCD176CA83C286');
    }
}
