<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230530132214 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add aggregate and newsletter';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE aggregate (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, person_or_organization VARCHAR(15) NOT NULL, membership JSON NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE aggregate_newsletter (aggregate_id INT NOT NULL, newsletter_id INT NOT NULL, INDEX IDX_E5696040D0BBCCBE (aggregate_id), INDEX IDX_E569604022DB1917 (newsletter_id), PRIMARY KEY(aggregate_id, newsletter_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE aggregate_organization_type (aggregate_id INT NOT NULL, organization_type_id INT NOT NULL, INDEX IDX_CFC20B6BD0BBCCBE (aggregate_id), INDEX IDX_CFC20B6B89E04D0 (organization_type_id), PRIMARY KEY(aggregate_id, organization_type_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE aggregate_type_associated_person (aggregate_id INT NOT NULL, type_associated_person_id INT NOT NULL, INDEX IDX_9500D55FD0BBCCBE (aggregate_id), INDEX IDX_9500D55FDE83CF48 (type_associated_person_id), PRIMARY KEY(aggregate_id, type_associated_person_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE aggregate_group (aggregate_id INT NOT NULL, group_id INT NOT NULL, INDEX IDX_80F93E2ED0BBCCBE (aggregate_id), INDEX IDX_80F93E2EFE54D947 (group_id), PRIMARY KEY(aggregate_id, group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE newsletter (id INT AUTO_INCREMENT NOT NULL, folder_id INT NOT NULL, brevo_id INT NOT NULL, name VARCHAR(255) NOT NULL, exist TINYINT(1) NOT NULL, last_sync DATETIME DEFAULT NULL, success TINYINT(1) NOT NULL, INDEX IDX_7E8585C8162CB942 (folder_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE newsletter_folder (id INT AUTO_INCREMENT NOT NULL, brevo_id INT NOT NULL, name VARCHAR(255) NOT NULL, exist TINYINT(1) NOT NULL, enabled TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE newsletter_subscription (id INT AUTO_INCREMENT NOT NULL, person_id INT NOT NULL, newsletter_id INT NOT NULL, status VARCHAR(20) NOT NULL, INDEX IDX_A82B55AD217BBB47 (person_id), INDEX IDX_A82B55AD22DB1917 (newsletter_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE aggregate_newsletter ADD CONSTRAINT FK_E5696040D0BBCCBE FOREIGN KEY (aggregate_id) REFERENCES aggregate (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE aggregate_newsletter ADD CONSTRAINT FK_E569604022DB1917 FOREIGN KEY (newsletter_id) REFERENCES newsletter (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE aggregate_organization_type ADD CONSTRAINT FK_CFC20B6BD0BBCCBE FOREIGN KEY (aggregate_id) REFERENCES aggregate (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE aggregate_organization_type ADD CONSTRAINT FK_CFC20B6B89E04D0 FOREIGN KEY (organization_type_id) REFERENCES organization_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE aggregate_type_associated_person ADD CONSTRAINT FK_9500D55FD0BBCCBE FOREIGN KEY (aggregate_id) REFERENCES aggregate (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE aggregate_type_associated_person ADD CONSTRAINT FK_9500D55FDE83CF48 FOREIGN KEY (type_associated_person_id) REFERENCES type_associated_person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE aggregate_group ADD CONSTRAINT FK_80F93E2ED0BBCCBE FOREIGN KEY (aggregate_id) REFERENCES aggregate (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE aggregate_group ADD CONSTRAINT FK_80F93E2EFE54D947 FOREIGN KEY (group_id) REFERENCES `group` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE newsletter ADD CONSTRAINT FK_7E8585C8162CB942 FOREIGN KEY (folder_id) REFERENCES newsletter_folder (id)');
        $this->addSql('ALTER TABLE newsletter_subscription ADD CONSTRAINT FK_A82B55AD217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE newsletter_subscription ADD CONSTRAINT FK_A82B55AD22DB1917 FOREIGN KEY (newsletter_id) REFERENCES newsletter (id)');
        $this->addSql('ALTER TABLE log CHANGE is_from_cli is_from_cli TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE aggregate_newsletter DROP FOREIGN KEY FK_E5696040D0BBCCBE');
        $this->addSql('ALTER TABLE aggregate_newsletter DROP FOREIGN KEY FK_E569604022DB1917');
        $this->addSql('ALTER TABLE aggregate_organization_type DROP FOREIGN KEY FK_CFC20B6BD0BBCCBE');
        $this->addSql('ALTER TABLE aggregate_organization_type DROP FOREIGN KEY FK_CFC20B6B89E04D0');
        $this->addSql('ALTER TABLE aggregate_type_associated_person DROP FOREIGN KEY FK_9500D55FD0BBCCBE');
        $this->addSql('ALTER TABLE aggregate_type_associated_person DROP FOREIGN KEY FK_9500D55FDE83CF48');
        $this->addSql('ALTER TABLE aggregate_group DROP FOREIGN KEY FK_80F93E2ED0BBCCBE');
        $this->addSql('ALTER TABLE aggregate_group DROP FOREIGN KEY FK_80F93E2EFE54D947');
        $this->addSql('ALTER TABLE newsletter DROP FOREIGN KEY FK_7E8585C8162CB942');
        $this->addSql('ALTER TABLE newsletter_subscription DROP FOREIGN KEY FK_A82B55AD217BBB47');
        $this->addSql('ALTER TABLE newsletter_subscription DROP FOREIGN KEY FK_A82B55AD22DB1917');
        $this->addSql('DROP TABLE aggregate');
        $this->addSql('DROP TABLE aggregate_newsletter');
        $this->addSql('DROP TABLE aggregate_organization_type');
        $this->addSql('DROP TABLE aggregate_type_associated_person');
        $this->addSql('DROP TABLE aggregate_group');
        $this->addSql('DROP TABLE newsletter');
        $this->addSql('DROP TABLE newsletter_folder');
        $this->addSql('DROP TABLE newsletter_subscription');
        $this->addSql('ALTER TABLE log CHANGE is_from_cli is_from_cli TINYINT(1) DEFAULT 0 NOT NULL');
    }
}
