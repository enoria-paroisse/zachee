<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230228200535 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add payment request';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE payment_request (id INT AUTO_INCREMENT NOT NULL, organization_id INT DEFAULT NULL, person_id INT DEFAULT NULL, membership_id INT DEFAULT NULL, datetime DATETIME NOT NULL, amount NUMERIC(10, 2) NOT NULL, status VARCHAR(20) NOT NULL, description VARCHAR(255) NOT NULL, INDEX IDX_22DE817532C8A3DE (organization_id), INDEX IDX_22DE8175217BBB47 (person_id), UNIQUE INDEX UNIQ_22DE81751FB354CD (membership_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE payment_request ADD CONSTRAINT FK_22DE817532C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id)');
        $this->addSql('ALTER TABLE payment_request ADD CONSTRAINT FK_22DE8175217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE payment_request ADD CONSTRAINT FK_22DE81751FB354CD FOREIGN KEY (membership_id) REFERENCES membership (id)');
        $this->addSql('INSERT INTO payment_request (membership_id, datetime, amount, status, description) SELECT membership.id, created_at, membership_fee, \'in_progress\', CONCAT(\'Cotisation annuelle \', period.name, \' \', organization.name) FROM membership  INNER JOIN period ON membership.period_id = period.id INNER JOIN organization ON organization_id = organization.id WHERE status != \'canceled\' AND organization_id IS NOT NULL');
        $this->addSql('INSERT INTO payment_request (membership_id, datetime, amount, status, description) SELECT membership.id, created_at, membership_fee, \'in_progress\', CONCAT(\'Cotisation annuelle \', period.name, \' \', person.firstname, \' \', person.lastname) FROM membership  INNER JOIN period ON membership.period_id = period.id INNER JOIN person ON person_id = person.id WHERE status != \'canceled\' AND person_id IS NOT NULL');
        $this->addSql('INSERT INTO payment_request (membership_id, datetime, amount, status, description) SELECT membership.id, created_at, membership_fee, \'canceled\', CONCAT(\'Cotisation annuelle \', period.name, \' \', organization.name) FROM membership  INNER JOIN period ON membership.period_id = period.id INNER JOIN organization ON organization_id = organization.id WHERE status = \'canceled\' AND organization_id IS NOT NULL');
        $this->addSql('INSERT INTO payment_request (membership_id, datetime, amount, status, description) SELECT membership.id, created_at, membership_fee, \'canceled\', CONCAT(\'Cotisation annuelle \', period.name, \' \', person.firstname, \' \', person.lastname) FROM membership  INNER JOIN period ON membership.period_id = period.id INNER JOIN person ON person_id = person.id WHERE status = \'canceled\' AND person_id IS NOT NULL');
        $this->addSql('ALTER TABLE membership DROP membership_fee');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE membership ADD membership_fee NUMERIC(10, 2) NOT NULL');
        $this->addSql('UPDATE membership INNER JOIN payment_request ON payment_request.membership_id = membership.id SET membership.membership_fee = payment_request.amount');
        $this->addSql('ALTER TABLE payment_request DROP FOREIGN KEY FK_22DE817532C8A3DE');
        $this->addSql('ALTER TABLE payment_request DROP FOREIGN KEY FK_22DE8175217BBB47');
        $this->addSql('ALTER TABLE payment_request DROP FOREIGN KEY FK_22DE81751FB354CD');
        $this->addSql('DROP TABLE payment_request');
    }
}
