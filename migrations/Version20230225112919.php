<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230225112919 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add sub organization membership fee';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE sub_organization_membership_fee (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sub_organization_membership_fee_period (id INT AUTO_INCREMENT NOT NULL, sub_organization_membership_fee_id INT NOT NULL, period_id INT NOT NULL, membership_fee NUMERIC(10, 2) DEFAULT \'100.00\' NOT NULL, INDEX IDX_67718C804DDB2CD (sub_organization_membership_fee_id), INDEX IDX_67718C80EC8B7ADE (period_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE diocese ADD sub_organization_membership_fee_id INT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE period DROP membershipfee_in_diocese');
        $this->addSql('CREATE INDEX IDX_8849E7424DDB2CD ON diocese (sub_organization_membership_fee_id)');
        $this->addSql('ALTER TABLE sub_organization_membership_fee_period ADD CONSTRAINT FK_67718C804DDB2CD FOREIGN KEY (sub_organization_membership_fee_id) REFERENCES sub_organization_membership_fee (id)');
        $this->addSql('ALTER TABLE sub_organization_membership_fee_period ADD CONSTRAINT FK_67718C80EC8B7ADE FOREIGN KEY (period_id) REFERENCES period (id)');
        $this->addSql('SET FOREIGN_KEY_CHECKS=0');
        $this->addSql('ALTER TABLE diocese ADD CONSTRAINT FK_8849E7424DDB2CD FOREIGN KEY (sub_organization_membership_fee_id) REFERENCES sub_organization_membership_fee (id)');
        $this->addSql('SET FOREIGN_KEY_CHECKS=1;');
        $this->addSql('INSERT INTO `sub_organization_membership_fee` (`name`) VALUES ("Base")');
        $this->addSql('INSERT INTO sub_organization_membership_fee_period (sub_organization_membership_fee_id, period_id, membership_fee) SELECT 1, id, \'100.00\' FROM period');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE diocese DROP FOREIGN KEY FK_8849E7424DDB2CD');
        $this->addSql('DROP INDEX IDX_8849E7424DDB2CD ON diocese');
        $this->addSql('ALTER TABLE sub_organization_membership_fee_period DROP FOREIGN KEY FK_67718C804DDB2CD');
        $this->addSql('ALTER TABLE sub_organization_membership_fee_period DROP FOREIGN KEY FK_67718C80EC8B7ADE');
        $this->addSql('ALTER TABLE diocese DROP sub_organization_membership_fee_id');
        $this->addSql('DROP TABLE sub_organization_membership_fee');
        $this->addSql('DROP TABLE sub_organization_membership_fee_period');
        $this->addSql('ALTER TABLE period ADD membershipfee_in_diocese NUMERIC(10, 2) DEFAULT \'100.00\' NOT NULL');
    }
}
