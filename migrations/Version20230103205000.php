<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230103205000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add numéro chèque';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE membership ADD membership_fee_payment_check_number VARCHAR(50) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE membership DROP membership_fee_payment_check_number');
    }
}
