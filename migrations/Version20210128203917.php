<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210128203917 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add logs';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE log (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, date DATETIME NOT NULL, type VARCHAR(20) NOT NULL, object VARCHAR(255) DEFAULT NULL, object_id INT DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, INDEX IDX_8F3F68C5A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE log ADD CONSTRAINT FK_8F3F68C5A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE address ADD updated_by_id INT DEFAULT NULL, ADD updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE address ADD CONSTRAINT FK_D4E6F81CA83C286 FOREIGN KEY (updated_by_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_D4E6F81CA83C286 ON address (updated_by_id)');
        $this->addSql('ALTER TABLE membership ADD created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, ADD updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE organization ADD updated_by_id INT DEFAULT NULL, ADD updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE organization ADD CONSTRAINT FK_C1EE637CCA83C286 FOREIGN KEY (updated_by_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_C1EE637CCA83C286 ON organization (updated_by_id)');
        $this->addSql('ALTER TABLE person ADD updated_by_id INT DEFAULT NULL, ADD updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD176CA83C286 FOREIGN KEY (updated_by_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_34DCD176CA83C286 ON person (updated_by_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE log');
        $this->addSql('ALTER TABLE address DROP FOREIGN KEY FK_D4E6F81CA83C286');
        $this->addSql('DROP INDEX IDX_D4E6F81CA83C286 ON address');
        $this->addSql('ALTER TABLE address DROP updated_by_id, DROP updated_at');
        $this->addSql('ALTER TABLE membership DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE organization DROP FOREIGN KEY FK_C1EE637CCA83C286');
        $this->addSql('DROP INDEX IDX_C1EE637CCA83C286 ON organization');
        $this->addSql('ALTER TABLE organization DROP updated_by_id, DROP updated_at');
        $this->addSql('ALTER TABLE person DROP FOREIGN KEY FK_34DCD176CA83C286');
        $this->addSql('DROP INDEX IDX_34DCD176CA83C286 ON person');
        $this->addSql('ALTER TABLE person DROP updated_by_id, DROP updated_at');
    }
}
