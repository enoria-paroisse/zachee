<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210125155538 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Set membershipfee and Wasselynck indice by period';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE wasselynck (id INT AUTO_INCREMENT NOT NULL, period_id INT NOT NULL, diocese_id INT NOT NULL, indice INT NOT NULL, INDEX IDX_876DB298EC8B7ADE (period_id), INDEX IDX_876DB298B600009 (diocese_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE wasselynck ADD CONSTRAINT FK_876DB298EC8B7ADE FOREIGN KEY (period_id) REFERENCES period (id)');
        $this->addSql('ALTER TABLE wasselynck ADD CONSTRAINT FK_876DB298B600009 FOREIGN KEY (diocese_id) REFERENCES diocese (id)');
        $this->addSql('INSERT INTO wasselynck (diocese_id, period_id, indice) SELECT diocese.id, period.id, diocese.indice FROM `diocese` CROSS JOIN period');
        $this->addSql('ALTER TABLE diocese DROP indice');
        $this->addSql('ALTER TABLE period ADD diocese_base_max_membershipfee NUMERIC(10, 2) DEFAULT \'14000.00\' NOT NULL, ADD diocese_base_wasselynck INT DEFAULT 37350 NOT NULL, ADD membershipfee_natural NUMERIC(10, 2) DEFAULT \'10.00\' NOT NULL, ADD membershipfee_legal NUMERIC(10, 2) DEFAULT \'600.00\' NOT NULL, ADD membershipfee_in_diocese NUMERIC(10, 2) DEFAULT \'100.00\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE diocese ADD indice INT NOT NULL');
        $this->addSql('DROP TABLE wasselynck');
        $this->addSql('ALTER TABLE period DROP diocese_base_max_membershipfee, DROP diocese_base_wasselynck, DROP membershipfee_natural, DROP membershipfee_legal, DROP membershipfee_in_diocese');
    }
}
