<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230303212752 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add payment allocation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE payment_allocation (id INT AUTO_INCREMENT NOT NULL, payment_id INT NOT NULL, payment_request_id INT NOT NULL, amount NUMERIC(10, 2) NOT NULL, INDEX IDX_69B2A2FF4C3A3BB (payment_id), INDEX IDX_69B2A2FF77883970 (payment_request_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE payment_allocation ADD CONSTRAINT FK_69B2A2FF4C3A3BB FOREIGN KEY (payment_id) REFERENCES payment (id)');
        $this->addSql('ALTER TABLE payment_allocation ADD CONSTRAINT FK_69B2A2FF77883970 FOREIGN KEY (payment_request_id) REFERENCES payment_request (id)');
        $this->addSql('INSERT INTO payment_allocation (payment_id, payment_request_id, amount) SELECT payment.id, payment_request.id, payment.amount FROM payment INNER JOIN payment_request ON payment_request.membership_id = payment.membership');
        $this->addSql('UPDATE payment INNER JOIN payment_allocation ON payment_allocation.payment_id = payment.id INNER JOIN payment_request ON payment_allocation.payment_request_id = payment_request.id SET payment.status = \'validated\' WHERE payment.amount = payment_request.amount');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE payment_allocation DROP FOREIGN KEY FK_69B2A2FF4C3A3BB');
        $this->addSql('ALTER TABLE payment_allocation DROP FOREIGN KEY FK_69B2A2FF77883970');
        $this->addSql('DROP TABLE payment_allocation');
    }
}
