<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240713071721 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add relation SmsBilling - PaymentRequest';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE sms_billing ADD payment_request_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sms_billing ADD CONSTRAINT FK_1C38877077883970 FOREIGN KEY (payment_request_id) REFERENCES payment_request (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1C38877077883970 ON sms_billing (payment_request_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE sms_billing DROP FOREIGN KEY FK_1C38877077883970');
        $this->addSql('DROP INDEX UNIQ_1C38877077883970 ON sms_billing');
        $this->addSql('ALTER TABLE sms_billing DROP payment_request_id');
    }
}
