<?php

/**
 * @copyright Copyright (c) 2022 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220520071555 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add receipts';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE receipt (id INT AUTO_INCREMENT NOT NULL, membership_id INT DEFAULT NULL, entity_id INT DEFAULT NULL, ref_prefix INT NOT NULL, ref_number INT NOT NULL, created_at DATETIME NOT NULL, receipt_date DATETIME NOT NULL, receipt_paid_date DATETIME NOT NULL, total_amount DOUBLE PRECISION NOT NULL, ref VARCHAR(25) NOT NULL, INDEX IDX_5399B6451FB354CD (membership_id), INDEX IDX_5399B64581257D5D (entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE receipt ADD CONSTRAINT FK_5399B6451FB354CD FOREIGN KEY (membership_id) REFERENCES membership (id)');
        $this->addSql('ALTER TABLE receipt ADD CONSTRAINT FK_5399B64581257D5D FOREIGN KEY (entity_id) REFERENCES entity (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5399B645146F3EA3 ON receipt (ref)');
        $this->addSql('ALTER TABLE membership ADD membership_fee_paid_at DATETIME DEFAULT NULL, ADD membership_fee_payment_mode VARCHAR(50) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX UNIQ_5399B645146F3EA3 ON receipt');
        $this->addSql('DROP TABLE receipt');
        $this->addSql('ALTER TABLE membership DROP membership_fee_paid_at, DROP membership_fee_payment_mode');
    }
}
