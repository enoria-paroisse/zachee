<?php

/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240213175429 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add general meeting';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE general_meeting (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(15) NOT NULL, datetime DATETIME NOT NULL, token VARCHAR(255) NOT NULL, meeting_link VARCHAR(255) DEFAULT NULL, meeting_id VARCHAR(255) DEFAULT NULL, meeting_passcode VARCHAR(255) DEFAULT NULL, locked TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE summons (id INT AUTO_INCREMENT NOT NULL, general_meeting_id INT NOT NULL, membership_id INT NOT NULL, mandate_id INT DEFAULT NULL, who_give_mandate_id INT DEFAULT NULL, date DATE DEFAULT NULL, date_video DATE DEFAULT NULL, date_report DATE DEFAULT NULL, file_mandate VARCHAR(255) DEFAULT NULL, INDEX IDX_C214FF2A147C1F0D (general_meeting_id), INDEX IDX_C214FF2A1FB354CD (membership_id), INDEX IDX_C214FF2A6C1129CD (mandate_id), INDEX IDX_C214FF2ADE3FA443 (who_give_mandate_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE summons ADD CONSTRAINT FK_C214FF2A147C1F0D FOREIGN KEY (general_meeting_id) REFERENCES general_meeting (id)');
        $this->addSql('ALTER TABLE summons ADD CONSTRAINT FK_C214FF2A1FB354CD FOREIGN KEY (membership_id) REFERENCES membership (id)');
        $this->addSql('ALTER TABLE summons ADD CONSTRAINT FK_C214FF2A6C1129CD FOREIGN KEY (mandate_id) REFERENCES summons (id)');
        $this->addSql('ALTER TABLE summons ADD CONSTRAINT FK_C214FF2ADE3FA443 FOREIGN KEY (who_give_mandate_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE doc ADD general_meeting_summons_id INT DEFAULT NULL, ADD general_meeting_report_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE doc ADD CONSTRAINT FK_8641FD6493A362C8 FOREIGN KEY (general_meeting_summons_id) REFERENCES general_meeting (id)');
        $this->addSql('ALTER TABLE doc ADD CONSTRAINT FK_8641FD64E4D8EA26 FOREIGN KEY (general_meeting_report_id) REFERENCES general_meeting (id)');
        $this->addSql('CREATE INDEX IDX_8641FD6493A362C8 ON doc (general_meeting_summons_id)');
        $this->addSql('CREATE INDEX IDX_8641FD64E4D8EA26 ON doc (general_meeting_report_id)');
        $this->addSql('CREATE TABLE general_meeting_person (general_meeting_id INT NOT NULL, person_id INT NOT NULL, INDEX IDX_9B53FEC1147C1F0D (general_meeting_id), INDEX IDX_9B53FEC1217BBB47 (person_id), PRIMARY KEY(general_meeting_id, person_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE general_meeting_person ADD CONSTRAINT FK_9B53FEC1147C1F0D FOREIGN KEY (general_meeting_id) REFERENCES general_meeting (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE general_meeting_person ADD CONSTRAINT FK_9B53FEC1217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE summons ADD is_present TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE general_meeting_person DROP FOREIGN KEY FK_9B53FEC1147C1F0D');
        $this->addSql('ALTER TABLE general_meeting_person DROP FOREIGN KEY FK_9B53FEC1217BBB47');
        $this->addSql('DROP TABLE general_meeting_person');
        $this->addSql('ALTER TABLE summons DROP is_present');
        $this->addSql('ALTER TABLE doc DROP FOREIGN KEY FK_8641FD6493A362C8');
        $this->addSql('ALTER TABLE doc DROP FOREIGN KEY FK_8641FD64E4D8EA26');
        $this->addSql('ALTER TABLE summons DROP FOREIGN KEY FK_C214FF2A147C1F0D');
        $this->addSql('ALTER TABLE summons DROP FOREIGN KEY FK_C214FF2A1FB354CD');
        $this->addSql('ALTER TABLE summons DROP FOREIGN KEY FK_C214FF2A6C1129CD');
        $this->addSql('ALTER TABLE summons DROP FOREIGN KEY FK_C214FF2ADE3FA443');
        $this->addSql('DROP TABLE general_meeting');
        $this->addSql('DROP TABLE summons');
        $this->addSql('DROP INDEX IDX_8641FD6493A362C8 ON doc');
        $this->addSql('DROP INDEX IDX_8641FD64E4D8EA26 ON doc');
        $this->addSql('ALTER TABLE doc DROP general_meeting_summons_id, DROP general_meeting_report_id');
    }
}
