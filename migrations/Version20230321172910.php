<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230321172910 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add diocese membership supplement';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE diocese_membership_supplement (id INT AUTO_INCREMENT NOT NULL, period_id INT NOT NULL, diocese_id INT NOT NULL, payment_request_id INT NOT NULL, INDEX IDX_192BCA58EC8B7ADE (period_id), INDEX IDX_192BCA58B600009 (diocese_id), UNIQUE INDEX UNIQ_192BCA5877883970 (payment_request_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE diocese_membership_supplement ADD CONSTRAINT FK_192BCA58EC8B7ADE FOREIGN KEY (period_id) REFERENCES period (id)');
        $this->addSql('ALTER TABLE diocese_membership_supplement ADD CONSTRAINT FK_192BCA58B600009 FOREIGN KEY (diocese_id) REFERENCES diocese (id)');
        $this->addSql('ALTER TABLE diocese_membership_supplement ADD CONSTRAINT FK_192BCA5877883970 FOREIGN KEY (payment_request_id) REFERENCES payment_request (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE diocese_membership_supplement DROP FOREIGN KEY FK_192BCA58EC8B7ADE');
        $this->addSql('ALTER TABLE diocese_membership_supplement DROP FOREIGN KEY FK_192BCA58B600009');
        $this->addSql('ALTER TABLE diocese_membership_supplement DROP FOREIGN KEY FK_192BCA5877883970');
        $this->addSql('DROP TABLE diocese_membership_supplement');
    }
}
