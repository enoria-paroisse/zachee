<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230628133947 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add entities and sms billing sync';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE entity ADD enable TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE entity ADD status VARCHAR(5) DEFAULT \'A\' NOT NULL');
        $this->addSql('ALTER TABLE instance CHANGE token token VARCHAR(128) DEFAULT NULL');
        $this->addSql('ALTER TABLE instance ADD sms_billing TINYINT(1) DEFAULT 0 NOT NULL');
        $this->addSql('CREATE TABLE sms_billing (id INT AUTO_INCREMENT NOT NULL, entity_id INT NOT NULL, id_in_instance INT NOT NULL, ref VARCHAR(255) DEFAULT NULL, paid TINYINT(1) NOT NULL, date DATE NOT NULL, total INT NOT NULL, exist TINYINT(1) NOT NULL, INDEX IDX_1C38877081257D5D (entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sms_billing ADD CONSTRAINT FK_1C38877081257D5D FOREIGN KEY (entity_id) REFERENCES entity (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE sms_billing DROP FOREIGN KEY FK_1C38877081257D5D');
        $this->addSql('DROP TABLE sms_billing');
        $this->addSql('ALTER TABLE entity DROP enable');
        $this->addSql('ALTER TABLE entity DROP status');
        $this->addSql('ALTER TABLE instance CHANGE token token VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE instance DROP sms_billing');
    }
}
