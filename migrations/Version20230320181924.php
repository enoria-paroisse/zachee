<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230320181924 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'delete old accounting system';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE membership DROP membership_fee_paid, DROP membership_fee_paid_at, DROP membership_fee_payment_mode, DROP membership_fee_payment_check_number');
        $this->addSql('ALTER TABLE payment DROP membership');
        $this->addSql('ALTER TABLE receipt DROP FOREIGN KEY FK_5399B6451FB354CD');
        $this->addSql('ALTER TABLE receipt DROP FOREIGN KEY FK_5399B64581257D5D');
        $this->addSql('DROP INDEX IDX_5399B6451FB354CD ON receipt');
        $this->addSql('DROP INDEX IDX_5399B64581257D5D ON receipt');
        $this->addSql('ALTER TABLE receipt DROP membership_id, DROP entity_id, DROP created_at, DROP receipt_date, DROP receipt_paid_date, DROP total_amount');
        $this->addSql('UPDATE payment_request INNER JOIN membership ON payment_request.membership_id = membership.id SET payment_request.status = \'canceled\' WHERE membership.status = \'canceled\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE membership ADD membership_fee_paid NUMERIC(10, 2) NOT NULL, ADD membership_fee_paid_at DATETIME DEFAULT NULL, ADD membership_fee_payment_mode VARCHAR(50) DEFAULT NULL, ADD membership_fee_payment_check_number VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE payment ADD membership INT DEFAULT NULL');
        $this->addSql('ALTER TABLE receipt ADD membership_id INT DEFAULT NULL, ADD entity_id INT DEFAULT NULL, ADD created_at DATETIME DEFAULT NULL, ADD receipt_date DATETIME DEFAULT NULL, ADD receipt_paid_date DATETIME DEFAULT NULL, ADD total_amount DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE receipt ADD CONSTRAINT FK_5399B6451FB354CD FOREIGN KEY (membership_id) REFERENCES membership (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE receipt ADD CONSTRAINT FK_5399B64581257D5D FOREIGN KEY (entity_id) REFERENCES entity (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_5399B6451FB354CD ON receipt (membership_id)');
        $this->addSql('CREATE INDEX IDX_5399B64581257D5D ON receipt (entity_id)');
        $this->addSql('UPDATE payment INNER JOIN payment_allocation ON payment_allocation.payment_id = payment.id INNER JOIN payment_request ON payment_allocation.payment_request_id = payment_request.id SET payment.membership = payment_request.membership_id');
        $this->addSql('UPDATE receipt INNER JOIN payment ON receipt.payment_id = payment.id INNER JOIN membership ON payment.membership = membership.id SET receipt.membership_id = payment.membership, receipt.entity_id = membership.entity_id, receipt.created_at = payment.datetime, receipt.receipt_date = payment.datetime, receipt.total_amount = payment.amount');
        $this->addSql('UPDATE membership INNER JOIN payment ON payment.membership = membership.id SET membership.membership_fee_paid = payment.amount, membership.membership_fee_paid_at = payment.datetime, membership.membership_fee_payment_mode = payment.type, membership.membership_fee_payment_check_number = payment.check_number');
    }
}
