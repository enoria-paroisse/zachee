<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230522202019 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add long notes and last reminder for memberships and cron logs';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE membership ADD last_reminder DATETIME DEFAULT NULL, CHANGE notes notes LONGTEXT DEFAULT NULL');
        $this->addSql('CREATE TABLE cron_log (id INT AUTO_INCREMENT NOT NULL, datetime DATETIME NOT NULL, result JSON NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cron_task_log (id INT AUTO_INCREMENT NOT NULL, cron_id INT NOT NULL, task VARCHAR(50) NOT NULL, description VARCHAR(255) NOT NULL, object VARCHAR(255) DEFAULT NULL, object_id INT DEFAULT NULL, INDEX IDX_504A78C838435942 (cron_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cron_task_log ADD CONSTRAINT FK_504A78C838435942 FOREIGN KEY (cron_id) REFERENCES cron_log (id)');
        $this->addSql('ALTER TABLE log ADD is_from_cli TINYINT(1) NOT NULL DEFAULT 0');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE log DROP is_from_cli');
        $this->addSql('ALTER TABLE cron_task_log DROP FOREIGN KEY FK_504A78C838435942');
        $this->addSql('DROP TABLE cron_log');
        $this->addSql('DROP TABLE cron_task_log');
        $this->addSql('ALTER TABLE membership DROP last_reminder, CHANGE notes notes VARCHAR(255) DEFAULT NULL');
    }
}
