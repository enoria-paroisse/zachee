<?php

/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201105201121 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add first migration';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, address_line_a VARCHAR(255) NOT NULL, address_line_b VARCHAR(255) DEFAULT NULL, city VARCHAR(255) NOT NULL, postal_code VARCHAR(20) NOT NULL, country VARCHAR(2) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE associated_person (id INT AUTO_INCREMENT NOT NULL, organization_id INT NOT NULL, person_id INT NOT NULL, type_id INT NOT NULL, INDEX IDX_7C86EE2332C8A3DE (organization_id), INDEX IDX_7C86EE23217BBB47 (person_id), INDEX IDX_7C86EE23C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE diocese (id INT AUTO_INCREMENT NOT NULL, associated_organization_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, indice INT NOT NULL, UNIQUE INDEX UNIQ_8849E742324ED8CE (associated_organization_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE organization (id INT AUTO_INCREMENT NOT NULL, diocese_id INT NOT NULL, address_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_C1EE637CB600009 (diocese_id), UNIQUE INDEX UNIQ_C1EE637CF5B7AF75 (address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person (id INT AUTO_INCREMENT NOT NULL, title_id INT NOT NULL, address_id INT DEFAULT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, sex VARCHAR(1) NOT NULL, email VARCHAR(255) DEFAULT NULL, phone VARCHAR(35) DEFAULT NULL COMMENT \'(DC2Type:phone_number)\', INDEX IDX_34DCD176A9F87BD (title_id), UNIQUE INDEX UNIQ_34DCD176F5B7AF75 (address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE title (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(20) NOT NULL, sex VARCHAR(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_associated_person (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, icon VARCHAR(20) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, person_id INT NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, token VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649217BBB47 (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `rememberme_token` (`series` char(88) UNIQUE PRIMARY KEY NOT NULL, `value` varchar(88) NOT NULL, `lastUsed` datetime NOT NULL, `class` varchar(100) NOT NULL, `username` varchar(200) NOT NULL)');
        $this->addSql('ALTER TABLE associated_person ADD CONSTRAINT FK_7C86EE2332C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id)');
        $this->addSql('ALTER TABLE associated_person ADD CONSTRAINT FK_7C86EE23217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE associated_person ADD CONSTRAINT FK_7C86EE23C54C8C93 FOREIGN KEY (type_id) REFERENCES type_associated_person (id)');
        $this->addSql('ALTER TABLE diocese ADD CONSTRAINT FK_8849E742324ED8CE FOREIGN KEY (associated_organization_id) REFERENCES organization (id)');
        $this->addSql('ALTER TABLE organization ADD CONSTRAINT FK_C1EE637CB600009 FOREIGN KEY (diocese_id) REFERENCES diocese (id)');
        $this->addSql('ALTER TABLE organization ADD CONSTRAINT FK_C1EE637CF5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD176A9F87BD FOREIGN KEY (title_id) REFERENCES title (id)');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD176F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE organization DROP FOREIGN KEY FK_C1EE637CF5B7AF75');
        $this->addSql('ALTER TABLE person DROP FOREIGN KEY FK_34DCD176F5B7AF75');
        $this->addSql('ALTER TABLE organization DROP FOREIGN KEY FK_C1EE637CB600009');
        $this->addSql('ALTER TABLE associated_person DROP FOREIGN KEY FK_7C86EE2332C8A3DE');
        $this->addSql('ALTER TABLE diocese DROP FOREIGN KEY FK_8849E742324ED8CE');
        $this->addSql('ALTER TABLE associated_person DROP FOREIGN KEY FK_7C86EE23217BBB47');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649217BBB47');
        $this->addSql('ALTER TABLE person DROP FOREIGN KEY FK_34DCD176A9F87BD');
        $this->addSql('ALTER TABLE associated_person DROP FOREIGN KEY FK_7C86EE23C54C8C93');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE associated_person');
        $this->addSql('DROP TABLE diocese');
        $this->addSql('DROP TABLE organization');
        $this->addSql('DROP TABLE person');
        $this->addSql('DROP TABLE title');
        $this->addSql('DROP TABLE type_associated_person');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE rememberme_token');
    }
}
