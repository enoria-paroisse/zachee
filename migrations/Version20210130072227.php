<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210130072227 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add membership help request status';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE membership CHANGE help_accepted help_status VARCHAR(20) NOT NULL');
        $this->addSql('UPDATE membership SET help_status = "untreated" WHERE help_status = "0"');
        $this->addSql('UPDATE membership SET help_status = "accepted", help_request = 1 WHERE help_status = "1"');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('UPDATE membership SET help_status = "1" WHERE help_status = "accepted"');
        $this->addSql('UPDATE membership SET help_status = "0" WHERE help_status != "1"');
        $this->addSql('ALTER TABLE membership CHANGE help_status help_accepted TINYINT(1) NOT NULL');
    }
}
