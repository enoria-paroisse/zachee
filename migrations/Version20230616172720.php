<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230616172720 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add aggregate link to group for auto subscription';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `group` ADD aggregate_id INT DEFAULT NULL, ADD viewable TINYINT(1) NOT NULL DEFAULT 0');
        $this->addSql('ALTER TABLE `group` ADD CONSTRAINT FK_6DC044C5D0BBCCBE FOREIGN KEY (aggregate_id) REFERENCES aggregate (id)');
        $this->addSql('CREATE INDEX IDX_6DC044C5D0BBCCBE ON `group` (aggregate_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `group` DROP FOREIGN KEY FK_6DC044C5D0BBCCBE');
        $this->addSql('DROP INDEX IDX_6DC044C5D0BBCCBE ON `group`');
        $this->addSql('ALTER TABLE `group` DROP aggregate_id, DROP viewable');
    }
}
