<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230302141441 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add paiement';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE payment_request SET status = \'completed\' WHERE amount = \'0.00\'');
        $this->addSql('CREATE TABLE payment (id INT AUTO_INCREMENT NOT NULL, person_id INT DEFAULT NULL, organization_id INT DEFAULT NULL, datetime DATETIME NOT NULL, type VARCHAR(10) NOT NULL, check_number VARCHAR(50) DEFAULT NULL, check_issuer VARCHAR(50) DEFAULT NULL, check_bank VARCHAR(50) DEFAULT NULL, amount NUMERIC(10, 2) NOT NULL, status VARCHAR(20) NOT NULL, membership INT DEFAULT NULL, INDEX IDX_6D28840D217BBB47 (person_id), INDEX IDX_6D28840D32C8A3DE (organization_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE payment ADD CONSTRAINT FK_6D28840D217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE payment ADD CONSTRAINT FK_6D28840D32C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id)');
        $this->addSql('UPDATE membership SET membership_fee_paid_at = updated_at WHERE membership_fee_paid_at IS NULL AND membership_fee_paid != 0.00');
        $this->addSql('UPDATE membership SET membership_fee_payment_mode = \'transfer\' WHERE membership_fee_payment_mode IS NULL OR membership_fee_payment_mode = \'tr\'');
        $this->addSql('UPDATE membership SET membership_fee_payment_mode = \'check\' WHERE membership_fee_payment_mode = \'ch\'');
        $this->addSql('INSERT INTO payment (organization_id, person_id, type, check_number, datetime, amount, status, membership) SELECT organization_id, person_id, membership_fee_payment_mode, membership_fee_payment_check_number, membership_fee_paid_at, membership_fee_paid, \'allocated\', membership.id FROM membership WHERE membership_fee_paid != \'0.00\'');
        $this->addSql('UPDATE payment_request INNER JOIN payment ON payment_request.membership_id = payment.membership SET payment_request.status = \'completed\' WHERE payment.membership IS NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('UPDATE membership SET membership_fee_payment_mode = \'ch\' WHERE membership_fee_payment_mode = \'check\'');
        $this->addSql('UPDATE membership SET membership_fee_payment_mode = \'tr\' WHERE membership_fee_payment_mode = \'transfer\'');
        $this->addSql('ALTER TABLE payment DROP FOREIGN KEY FK_6D28840D217BBB47');
        $this->addSql('ALTER TABLE payment DROP FOREIGN KEY FK_6D28840D32C8A3DE');
        $this->addSql('DROP TABLE payment');
    }
}
