<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230327114207 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'clean index names';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE diocese_membership_supplement RENAME INDEX idx_192bca58ec8b7ade TO IDX_37C1D101EC8B7ADE');
        $this->addSql('ALTER TABLE diocese_membership_supplement RENAME INDEX idx_192bca58b600009 TO IDX_37C1D101B600009');
        $this->addSql('ALTER TABLE diocese_membership_supplement RENAME INDEX uniq_192bca5877883970 TO UNIQ_37C1D10177883970');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE diocese_membership_supplement RENAME INDEX uniq_37c1d10177883970 TO UNIQ_192BCA5877883970');
        $this->addSql('ALTER TABLE diocese_membership_supplement RENAME INDEX idx_37c1d101ec8b7ade TO IDX_192BCA58EC8B7ADE');
        $this->addSql('ALTER TABLE diocese_membership_supplement RENAME INDEX idx_37c1d101b600009 TO IDX_192BCA58B600009');
    }
}
