<?php

/**
 * @copyright Copyright (c) 2025 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250225172511 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add application users';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE application (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, icon VARCHAR(255) DEFAULT NULL, url VARCHAR(255) NOT NULL, api VARCHAR(255) DEFAULT NULL, api_config JSON DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE application_user (id INT AUTO_INCREMENT NOT NULL, person_id INT NOT NULL, application_id INT NOT NULL, username VARCHAR(255) NOT NULL, infos LONGTEXT DEFAULT NULL, INDEX IDX_7A7FBEC1217BBB47 (person_id), INDEX IDX_7A7FBEC13E030ACD (application_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE application_user ADD CONSTRAINT FK_7A7FBEC1217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE application_user ADD CONSTRAINT FK_7A7FBEC13E030ACD FOREIGN KEY (application_id) REFERENCES application (id)');
        $this->addSql('ALTER TABLE general_meeting_summons_doc RENAME INDEX idx_93309555147c1f0d TO IDX_78B97C17147C1F0D');
        $this->addSql('ALTER TABLE general_meeting_summons_doc RENAME INDEX idx_93309555895648bc TO IDX_78B97C17895648BC');
        $this->addSql('DROP INDEX UNIQ_DFEA849072A8BD77 ON webauthn_credentials');
        $this->addSql('ALTER TABLE webauthn_credentials CHANGE public_key_credential_id public_key_credential_id LONGTEXT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE application_user DROP FOREIGN KEY FK_7A7FBEC1217BBB47');
        $this->addSql('ALTER TABLE application_user DROP FOREIGN KEY FK_7A7FBEC13E030ACD');
        $this->addSql('DROP TABLE application');
        $this->addSql('DROP TABLE application_user');
        $this->addSql('ALTER TABLE general_meeting_summons_doc RENAME INDEX idx_78b97c17147c1f0d TO IDX_93309555147C1F0D');
        $this->addSql('ALTER TABLE general_meeting_summons_doc RENAME INDEX idx_78b97c17895648bc TO IDX_93309555895648BC');
        $this->addSql('ALTER TABLE webauthn_credentials CHANGE public_key_credential_id public_key_credential_id VARCHAR(255) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DFEA849072A8BD77 ON webauthn_credentials (public_key_credential_id)');
    }
}
