<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230328211015 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add organization types';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE organization_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE organization ADD type_id INT NOT NULL');
        $this->addSql('INSERT INTO `organization_type` (`name`) VALUES (\'Paroisse\')');
        $this->addSql('INSERT INTO `organization_type` (`name`) VALUES (\'Diocèse\')');
        $this->addSql('INSERT INTO `organization_type` (`name`) VALUES (\'Maison diocésaine\')');
        $this->addSql('INSERT INTO `organization_type` (`name`) VALUES (\'Communauté religieuse\')');
        $this->addSql('INSERT INTO `organization_type` (`name`) VALUES (\'Établissement scolaire\')');
        $this->addSql('INSERT INTO `organization_type` (`name`) VALUES (\'Abbaye\')');
        $this->addSql('UPDATE organization SET organization.type_id = 1');
        $this->addSql('UPDATE organization INNER JOIN diocese ON organization.diocese_id = diocese.id SET organization.type_id = 2 WHERE diocese.associated_organization_id = organization.id');
        $this->addSql('ALTER TABLE organization ADD CONSTRAINT FK_C1EE637CC54C8C93 FOREIGN KEY (type_id) REFERENCES organization_type (id)');
        $this->addSql('CREATE INDEX IDX_C1EE637CC54C8C93 ON organization (type_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE organization DROP FOREIGN KEY FK_C1EE637CC54C8C93');
        $this->addSql('DROP TABLE organization_type');
        $this->addSql('DROP INDEX IDX_C1EE637CC54C8C93 ON organization');
        $this->addSql('ALTER TABLE organization DROP type_id');
    }
}
