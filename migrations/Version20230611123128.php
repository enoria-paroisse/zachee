<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230611123128 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add file for payment request';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE payment_request_file (id INT AUTO_INCREMENT NOT NULL, payment_request_id INT NOT NULL, ref VARCHAR(35) NOT NULL, ref_prefix VARCHAR(25) NOT NULL, ref_number INT NOT NULL, file VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_95C8A6F0146F3EA3 (ref), UNIQUE INDEX UNIQ_95C8A6F077883970 (payment_request_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE payment_request_file ADD CONSTRAINT FK_95C8A6F077883970 FOREIGN KEY (payment_request_id) REFERENCES payment_request (id)');
        $this->addSql('ALTER TABLE payment_request ADD last_reminder DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE payment_request ADD diocese_membership_supplement_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE payment_request ADD CONSTRAINT FK_22DE817513C3A948 FOREIGN KEY (diocese_membership_supplement_id) REFERENCES diocese_membership_supplement (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_22DE817513C3A948 ON payment_request (diocese_membership_supplement_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE payment_request DROP FOREIGN KEY FK_22DE817513C3A948');
        $this->addSql('DROP INDEX UNIQ_22DE817513C3A948 ON payment_request');
        $this->addSql('ALTER TABLE payment_request DROP diocese_membership_supplement_id');
        $this->addSql('ALTER TABLE payment_request_file DROP FOREIGN KEY FK_95C8A6F077883970');
        $this->addSql('DROP TABLE payment_request_file');
        $this->addSql('ALTER TABLE payment_request DROP last_reminder');
    }
}
