<?php

/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210131211529 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add docs';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE doc (id INT AUTO_INCREMENT NOT NULL, folder_id INT NOT NULL, ref VARCHAR(255) DEFAULT NULL, name VARCHAR(255) NOT NULL, tags LONGTEXT COMMENT \'(DC2Type:simple_array)\', file VARCHAR(255) NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_8641FD64162CB942 (folder_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE folder (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, ref VARCHAR(255) DEFAULT NULL, name VARCHAR(255) NOT NULL, tags LONGTEXT COMMENT \'(DC2Type:simple_array)\', INDEX IDX_ECA209CD727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE doc ADD CONSTRAINT FK_8641FD64162CB942 FOREIGN KEY (folder_id) REFERENCES folder (id)');
        $this->addSql('ALTER TABLE folder ADD CONSTRAINT FK_ECA209CD727ACA70 FOREIGN KEY (parent_id) REFERENCES folder (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE doc DROP FOREIGN KEY FK_8641FD64162CB942');
        $this->addSql('ALTER TABLE folder DROP FOREIGN KEY FK_ECA209CD727ACA70');
        $this->addSql('DROP TABLE doc');
        $this->addSql('DROP TABLE folder');
    }
}
