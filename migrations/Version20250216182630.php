<?php

/**
 * @copyright Copyright (c) 2025 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250216182630 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'a doc can be associated with many general meeting';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE general_meeting_summons_doc (general_meeting_id INT NOT NULL, doc_id INT NOT NULL, INDEX IDX_93309555147C1F0D (general_meeting_id), INDEX IDX_93309555895648BC (doc_id), PRIMARY KEY(general_meeting_id, doc_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE general_meeting_report_doc (general_meeting_id INT NOT NULL, doc_id INT NOT NULL, INDEX IDX_3F7CC8E0147C1F0D (general_meeting_id), INDEX IDX_3F7CC8E0895648BC (doc_id), PRIMARY KEY(general_meeting_id, doc_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE general_meeting_summons_doc ADD CONSTRAINT FK_93309555147C1F0D FOREIGN KEY (general_meeting_id) REFERENCES general_meeting (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE general_meeting_summons_doc ADD CONSTRAINT FK_93309555895648BC FOREIGN KEY (doc_id) REFERENCES doc (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE doc DROP FOREIGN KEY FK_8641FD6493A362C8');
        $this->addSql('ALTER TABLE doc DROP FOREIGN KEY FK_8641FD64E4D8EA26');
        $this->addSql('ALTER TABLE general_meeting_report_doc ADD CONSTRAINT FK_3F7CC8E0147C1F0D FOREIGN KEY (general_meeting_id) REFERENCES general_meeting (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE general_meeting_report_doc ADD CONSTRAINT FK_3F7CC8E0895648BC FOREIGN KEY (doc_id) REFERENCES doc (id) ON DELETE CASCADE');
        $this->addSql('INSERT INTO general_meeting_summons_doc (general_meeting_id, doc_id) SELECT general_meeting_summons_id, id FROM doc WHERE general_meeting_summons_id IS NOT NULL');
        $this->addSql('INSERT INTO general_meeting_report_doc (general_meeting_id, doc_id) SELECT general_meeting_report_id, id FROM doc WHERE general_meeting_report_id IS NOT NULL');
        $this->addSql('DROP INDEX IDX_8641FD6493A362C8 ON doc');
        $this->addSql('DROP INDEX IDX_8641FD64E4D8EA26 ON doc');
        $this->addSql('ALTER TABLE doc DROP general_meeting_summons_id, DROP general_meeting_report_id');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE general_meeting_summons_doc DROP FOREIGN KEY FK_93309555147C1F0D');
        $this->addSql('ALTER TABLE general_meeting_summons_doc DROP FOREIGN KEY FK_93309555895648BC');
        $this->addSql('ALTER TABLE general_meeting_report_doc DROP FOREIGN KEY FK_3F7CC8E0147C1F0D');
        $this->addSql('ALTER TABLE general_meeting_report_doc DROP FOREIGN KEY FK_3F7CC8E0895648BC');
        $this->addSql('ALTER TABLE doc ADD general_meeting_summons_id INT DEFAULT NULL, ADD general_meeting_report_id INT DEFAULT NULL');
        $this->addSql('UPDATE doc INNER JOIN general_meeting_summons_doc ON doc_id = doc.id SET general_meeting_summons_id = general_meeting_id');
        $this->addSql('UPDATE doc INNER JOIN general_meeting_report_doc ON doc_id = doc.id SET general_meeting_report_id = general_meeting_id');
        $this->addSql('ALTER TABLE doc ADD CONSTRAINT FK_8641FD6493A362C8 FOREIGN KEY (general_meeting_summons_id) REFERENCES general_meeting (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE doc ADD CONSTRAINT FK_8641FD64E4D8EA26 FOREIGN KEY (general_meeting_report_id) REFERENCES general_meeting (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_8641FD6493A362C8 ON doc (general_meeting_summons_id)');
        $this->addSql('CREATE INDEX IDX_8641FD64E4D8EA26 ON doc (general_meeting_report_id)');
        $this->addSql('DROP TABLE general_meeting_summons_doc');
        $this->addSql('DROP TABLE general_meeting_report_doc');
    }
}
