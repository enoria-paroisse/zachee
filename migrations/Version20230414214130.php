<?php

/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230414214130 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add history for ca member';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE ca_member DROP INDEX UNIQ_8B1DCB6F217BBB47, ADD INDEX IDX_8B1DCB6F217BBB47 (person_id)');
        $this->addSql('ALTER TABLE ca_member CHANGE active active TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE ca_member DROP INDEX IDX_8B1DCB6F217BBB47, ADD UNIQUE INDEX UNIQ_8B1DCB6F217BBB47 (person_id)');
        $this->addSql('ALTER TABLE ca_member CHANGE active active TINYINT(1) DEFAULT 1 NOT NULL');
    }
}
