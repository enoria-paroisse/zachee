/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import 'datatables.net-bs5';
import 'datatables.net-select-bs5';

// Accent neutralize for DataTable
function removeAccents(data) {
    if (data.normalize) {
        // Use I18n API if avaiable to split characters and accents, then remove
        // the accents wholesale. Note that we use the original data as well as
        // the new to allow for searching of either form.
        return data + ' ' + data.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    }
    return data;
}
const DataTable = $.fn.dataTable;
const searchType = DataTable.ext.type.search;
searchType.string = function (data) {
    return !data ? '' : typeof data === 'string' ? removeAccents(data) : data;
};
searchType.html = function (data) {
    return !data ? '' : typeof data === 'string' ? removeAccents(data.replace(/<.*?>/g, '')) : data;
};

const defaultConfig = { ...DataTable.defaults };
defaultConfig.layout.bottomEnd = {
    paging: {
        firstLast: false,
    },
};
defaultConfig.language = { url: '//cdn.datatables.net/plug-ins/2.1.8/i18n/fr-FR.json' };

Object.assign(DataTable.defaults, defaultConfig);

global.DataTable = DataTable;
