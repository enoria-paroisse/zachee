/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

// jQuery & Bootstrap
import $ from 'jquery';
global.$ = global.jQuery = $;
import * as bootstrap from 'bootstrap';
global.bootstrap = bootstrap;

const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]');
tooltipTriggerList.forEach((tooltipTriggerEl) => new bootstrap.Tooltip(tooltipTriggerEl));

// Font Awesome
require('@fortawesome/fontawesome-free/css/all.min.css');

// Datatables
import './datatable';

// sweetalert2
import Swal from 'sweetalert2';
global.Swal = Swal;

// Choices.js
import Choices from 'choices.js';
global.Choices = Choices;
global.newChoices = (element, options = false) => {
    return new Choices(element, {
        removeItemButton: true,
        loadingText: 'Chargement...',
        noResultsText: 'Pas de résultat trouvé',
        noChoicesText: 'Aucune valeur à choisir',
        itemSelectText: '',
        uniqueItemText: 'Seules les valeurs uniques peuvent être ajoutées',
        customAddItemText: 'Seules les valeurs remplissant les conditions peuvent être ajoutées',
        allowHTML: false,
        searchFields: ['label'],
        searchResultLimit: 10,
        fuseOptions: {
            includeScore: true,
            ignoreLocation: true,
        },
        ...options,
    });
};
const choicesElement = document.querySelectorAll('.js-choice');
choicesElement.forEach((choiceElement) => {
    global.newChoices(choiceElement);
});

// WebAuthn
import * as SimpleWebAuthnBrowser from '@simplewebauthn/browser';
global.SimpleWebAuthnBrowser = SimpleWebAuthnBrowser;

// jsonFetch
/**
 *
 * @param {string} url
 * @param {'GET'|'POST'} method
 * @param body
 * @return {Promise<any>}
 */
async function jsonFetch(url, method = 'GET', body = undefined) {
    const response = await fetch(url, {
        body: body ? JSON.stringify(body) : undefined,
        headers: {
            'Content-Type': 'application/json',
        },
        method: method,
    });

    return await response.json();
}
global.jsonFetch = jsonFetch;

// widgets
import './widgets/sex-switch';
import './widgets/copy-clipboard';

// Our scripts
import deleteAlert from './delete-alert';
global.deleteAlert = deleteAlert;
import './main-searchbar';

(() => {
    const sidebarCollapse = document.getElementById('sidebarCollapse');
    const body = document.getElementsByTagName('body')[0];
    const sidebarCollapseIcon = document.getElementById('sidebarCollapse');
    const sidebar = document.getElementById('sidebar');
    const sidebarMenuContent = document.querySelector('#sidebar .menu-content');

    // sidebar collapse (big screen)
    sidebarCollapse?.addEventListener('click', () => {
        body.classList.toggle('sidebar-large');
        body.classList.toggle('sidebar-active');
        sidebarCollapseIcon.classList.toggle('rotate');
    });

    // show lateral menu (little screen)
    document.getElementById('sidebarShow')?.addEventListener('click', () => {
        body.classList.toggle('sidebar-active');
        sidebarCollapseIcon.classList.toggle('rotate');
    });

    // lateral menu mouse over (big screen)
    if (null !== sidebarMenuContent) {
        sidebarMenuContent.addEventListener('mouseover', () => {
            sidebar.classList.add('sidebar-mini-mouse-hover');
        });
        sidebarMenuContent.addEventListener('mouseout', () => {
            sidebar.classList.remove('sidebar-mini-mouse-hover');
        });
    }

    // back button
    document.getElementById('backJs')?.addEventListener('click', () => {
        window.history.back();
    });
})();
