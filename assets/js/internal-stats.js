/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import Chart from 'chart.js/auto';
import 'chartjs-adapter-luxon';

function getRandomColors(nbElts) {
    const colors = [];

    const letters = '0123456789ABCDEF'.split('');

    for (let i = 0; i < nbElts; i++) {
        let color = '#';
        for (let j = 0; j < 6; j++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        colors[i] = color;
    }

    return colors;
}

(async function () {
    const g = document.getElementById('stat-membership-month');
    new Chart(g, {
        type: 'bar',
        options: {
            responsive: true,
            maintainAspectRatio: false,
            responsiveAnimationDuration: 500,
            plugins: {
                legend: { display: false },
            },
        },
        data: {
            labels: (JSON.parse(g.getAttribute('data-dataset')) ?? []).map((row) => row.x),
            datasets: [
                {
                    data: (JSON.parse(g.getAttribute('data-dataset')) ?? []).map((row) => row.y),
                    backgroundColor: '#105dbb',
                },
            ],
        },
    });

    const g2 = document.getElementById('stat-membership-year');
    const years = [];
    (JSON.parse(g2.getAttribute('data-dataset'))['organization'] ?? []).forEach((year) => {
        if (years.indexOf(year.x) === -1) years.push(year.x);
    });
    (JSON.parse(g2.getAttribute('data-dataset'))['person'] ?? []).forEach((year) => {
        if (years.indexOf(year.x) === -1) years.push(year.x);
    });
    new Chart(g2, {
        type: 'line',
        options: {
            responsive: true,
            maintainAspectRatio: false,
            responsiveAnimationDuration: 500,
            fill: true,
        },
        data: {
            labels: years,
            datasets: [
                {
                    label: 'Personnes morales',
                    data: (JSON.parse(g2.getAttribute('data-dataset'))['organization'] ?? []).map((row) => row.y),
                },
                {
                    label: 'Personnes physiques',
                    data: (JSON.parse(g2.getAttribute('data-dataset'))['person'] ?? []).map((row) => row.y),
                },
            ],
        },
    });

    const gOrganizationTypes = document.getElementById('stat-organization-types');
    new Chart(gOrganizationTypes, {
        type: 'pie',
        options: {
            responsive: true,
            maintainAspectRatio: false,
            responsiveAnimationDuration: 500,
        },
        data: {
            labels: (JSON.parse(gOrganizationTypes.getAttribute('data-dataset')) ?? []).map((row) => row.name),
            datasets: [
                {
                    data: (JSON.parse(gOrganizationTypes.getAttribute('data-dataset')) ?? []).map((row) => row.nb),
                    backgroundColor: getRandomColors((JSON.parse(gOrganizationTypes.getAttribute('data-dataset')) ?? []).length),
                },
            ],
        },
    });

    const gEntitiesByInstance = document.getElementById('stat-entities-by-instance');
    new Chart(gEntitiesByInstance, {
        type: 'pie',
        options: {
            responsive: true,
            maintainAspectRatio: false,
            responsiveAnimationDuration: 500,
        },
        data: {
            labels: JSON.parse(gEntitiesByInstance.getAttribute('data-labels')) ?? [],
            datasets: [
                {
                    data: JSON.parse(gEntitiesByInstance.getAttribute('data-dataset')) ?? [],
                    backgroundColor: getRandomColors((JSON.parse(gEntitiesByInstance.getAttribute('data-dataset')) ?? []).length),
                },
            ],
        },
    });

    const gEntitiesByDiocese = document.getElementById('stat-entities-by-diocese');
    new Chart(gEntitiesByDiocese, {
        type: 'pie',
        options: {
            responsive: true,
            maintainAspectRatio: false,
            responsiveAnimationDuration: 500,
        },
        data: {
            labels: JSON.parse(gEntitiesByDiocese.getAttribute('data-labels')) ?? [],
            datasets: [
                {
                    data: JSON.parse(gEntitiesByDiocese.getAttribute('data-dataset')) ?? [],
                    backgroundColor: getRandomColors((JSON.parse(gEntitiesByDiocese.getAttribute('data-dataset')) ?? []).length),
                },
            ],
        },
    });
})();
