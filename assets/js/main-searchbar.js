/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import { debounce } from './utils';

/**
 * @param {string} icon
 * @param {string} description
 * @returns {HTMLLIElement}
 */
function createTitleElem(icon, description) {
    const elem = document.createElement('li');
    elem.classList.add('main-searchbar-title');
    elem.innerHTML = `<i class="${icon}"></i> ${description}`;
    return elem;
}

/**
 * @param {string} url
 * @param {string} description
 * @param {boolean} blank
 * @returns {HTMLLIElement}
 */
function createResultElem(url, description, blank = false) {
    const elem = document.createElement('li');
    elem.innerHTML = `<a href="${url}"${blank ? ' target="_blank"' : ''}>${description}</a>`;
    return elem;
}

/**
 * Create component
 */
const errorElem = document.createElement('li');
errorElem.classList.add('main-searchbar-no-result', 'alert-danger');
errorElem.innerHTML = 'Une erreur est survenue&nbsp;!';

/**
 * @param {string} query
 * @param {HTMLElement} searchbarResult
 * @param {HTMLElement} searchbarSpinner
 * @returns {Promise<void>}
 */
async function loadResults(query, searchbarResult, searchbarSpinner) {
    if (query.length > 2) {
        searchbarResult.innerHTML = '';
        searchbarSpinner.classList.remove('d-none');
        searchbarResult.classList.add('d-none');

        try {
            const response = await fetch('/search/' + query);
            const result = await response.json();
            if (!response.ok) {
                searchbarResult.appendChild(errorElem);
                console.error(result.message ?? response.statusText);
            } else {
                let empty = true;

                const searchResult = result.result;

                // People
                if (searchResult['people'] && searchResult['people'].length) {
                    empty = false;
                    searchbarResult.appendChild(createTitleElem('fa-solid fa-address-book', 'Personnes'));

                    searchResult['people'].forEach((elem) => {
                        searchbarResult.appendChild(createResultElem('/person/show/' + elem.id, elem.firstname + ' <span class="smallcaps">' + elem.lastname + '</span>'));
                    });
                }

                // Organizations
                if (searchResult['organizations'] && searchResult['organizations'].length) {
                    empty = false;
                    searchbarResult.appendChild(createTitleElem('fa-solid fa-clone', 'Organisations'));

                    searchResult['organizations'].forEach((elem) => {
                        searchbarResult.appendChild(createResultElem('/organization/show/' + elem.id, elem.name));
                    });
                }

                // Dioceses
                if (searchResult['dioceses'] && searchResult['dioceses'].length) {
                    empty = false;
                    searchbarResult.appendChild(createTitleElem('fa-solid fa-church', 'Diocèses'));

                    searchResult['dioceses'].forEach((elem) => {
                        searchbarResult.appendChild(createResultElem('/diocese/show/' + elem.id, elem.name));
                    });
                }

                // VolunteerGroups
                if (searchResult['volunteerGroups'] && searchResult['volunteerGroups'].length) {
                    empty = false;
                    searchbarResult.appendChild(createTitleElem('fa-solid fa-handshake-angle', 'Groupes de bénévoles'));

                    searchResult['volunteerGroups'].forEach((elem) => {
                        searchbarResult.appendChild(createResultElem('/volunteer/group/' + elem.id, elem.name));
                    });
                }

                // Aggregates
                if (searchResult['aggregates'] && searchResult['aggregates'].length) {
                    empty = false;
                    searchbarResult.appendChild(createTitleElem('fa-solid fa-list-alt', 'Agrégats'));

                    searchResult['aggregates'].forEach((elem) => {
                        searchbarResult.appendChild(createResultElem('/aggregate/' + elem.id + '/show', elem.name));
                    });
                }

                // Docs
                if (searchResult['docs'] && searchResult['docs'].length) {
                    empty = false;
                    searchbarResult.appendChild(createTitleElem('fa-solid fa-book', 'Documents'));

                    searchResult['docs'].forEach((elem) => {
                        searchbarResult.appendChild(createResultElem('/uploads/doc/' + elem.file, elem.name, true));
                    });
                }

                // If no result
                if (empty) {
                    const emptyElem = document.createElement('li');
                    emptyElem.classList.add('main-searchbar-no-result');
                    emptyElem.innerHTML = `Aucun résultat pour <strong>«&nbsp;${query}&nbsp;»`;
                    searchbarResult.appendChild(emptyElem);
                    searchbarResult.classList.add('d-none');
                }

                searchbarResult.classList.remove('d-none');
            }
        } catch (e) {
            searchbarResult.appendChild(errorElem);
            console.error(e);
        }

        searchbarSpinner.classList.add('d-none');
    }
}

/**
 * Body
 */
(() => {
    const headerbar = document.getElementById('headerbar');
    const searchbarLink = document.getElementById('searchbar-link');
    const searchbarForm = document.getElementById('searchbar-form');
    const searchbarFormInput = document.querySelector('#searchbar-form input');
    const searchbarSpinner = document.getElementById('searchbar-spinner');
    const searchbarResult = document.querySelector('#searchbar-form > ul');

    if (headerbar && searchbarLink && searchbarForm && searchbarFormInput && searchbarSpinner && searchbarResult) {
        // Show form
        window.addEventListener('keydown', (e) => {
            if (e.key === '/' && e.ctrlKey === true) {
                e.preventDefault();
                headerbar.classList.add('searchbar-active');
                searchbarFormInput.focus();
            }
        });
        searchbarLink.addEventListener('click', (e) => {
            e.preventDefault();
            headerbar.classList.add('searchbar-active');
            searchbarFormInput.focus();
        });

        // Hide searchbar
        document.addEventListener('click', (e) => {
            let targetElem = e.target;
            do {
                if (targetElem === searchbarForm || targetElem === searchbarLink) {
                    return;
                }
                targetElem = targetElem.parentNode;
            } while (targetElem);
            searchbarResult.classList.add('d-none');
            headerbar.classList.remove('searchbar-active');
        });

        // Launch search
        const debouncedLoadResults = debounce((query) => {
            return loadResults(query, searchbarResult, searchbarSpinner);
        }, 300);
        searchbarFormInput.addEventListener('input', async (e) => {
            e.preventDefault();
            const query = searchbarFormInput.value;
            debouncedLoadResults(query);
        });
    }
})();
