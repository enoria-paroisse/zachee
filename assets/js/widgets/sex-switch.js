/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

function update(sexInput, container) {
    if (sexInput.value === 'M') container.className = 'sex-switch-element male-selected';
    else if (sexInput.value === 'F') container.className = 'sex-switch-element female-selected';
}

function toogle(sexInput) {
    if (sexInput.value === 'M') sexInput.value = 'F';
    else sexInput.value = 'M';
}

(() => {
    [...document.getElementsByClassName('sex-switch')].forEach((element) => {
        const container = document.createElement('div');
        container.className = 'sex-switch-element';
        const male = document.createElement('span');
        male.className = 'sex-switch-male';
        male.textContent = 'Homme';
        const label = document.createElement('span');
        label.className = 'sex-switch-label';
        label.textContent = 'Sexe';
        const female = document.createElement('span');
        female.className = 'sex-switch-female';
        female.textContent = 'Femme';
        container.append(male, label, female);

        update(element, container);

        container.addEventListener('click', () => {
            toogle(element);
            update(element, container);
        });
        element.addEventListener('change', () => {
            update(element, container);
        });

        element.after(container);
    });
})();
