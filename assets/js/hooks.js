/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import { useState, useCallback } from 'react';

async function jsonLdFetch(url, method = 'GET', data = null) {
    const params = {
        method: method,
        headers: {
            Accept: 'application/ld+json',
            'Content-Type': 'application/json',
        },
    };
    if (data) params.body = JSON.stringify(data);

    const response = await fetch(url, params);
    if (response.status === 204) return null;
    const responseData = await response.json();
    if (response.ok) {
        return responseData;
    } else {
        throw responseData;
    }
}

export function usePaginatedFetch(url, callback = null) {
    const [loading, setLoading] = useState(false);
    const [items, setItems] = useState([]);
    const [count, setCount] = useState(0);
    const [next, setNext] = useState(null);
    const [error, setError] = useState(null);

    const load = useCallback(async () => {
        try {
            setLoading(true);
            const response = await jsonLdFetch(next || url);
            setItems((items) => [...items, ...response['hydra:member']]);
            setCount(response['hydra:totalItems']);

            if (response['hydra:view'] && response['hydra:view']['hydra:next']) setNext(response['hydra:view']['hydra:next']);
            else setNext(null);

            if (callback) callback(response);
        } catch (error) {
            console.error(error);
            setError(error);
        }
        setLoading(false);
    }, [next, url, callback]);

    return {
        items,
        setItems,
        load,
        count,
        loading,
        hasMore: next !== null,
        error,
    };
}

export function useFetch(url, method = 'POST', callback = null) {
    const [errors, setErrors] = useState({});
    const [loading, setLoading] = useState(false);
    const load = useCallback(
        async (data = null) => {
            setLoading(true);
            try {
                const response = await jsonLdFetch(url, method, data);
                setLoading(false);
                if (callback) callback(response);
            } catch (error) {
                setLoading(false);
                if (errors.violations) {
                    setErrors(
                        error.violations.reduce((acc, violation) => {
                            acc[violation.propertyPath] = violation.message;
                            return acc;
                        }, {}),
                    );
                } else throw error;
            }
        },
        [url, method, callback],
    );
    const clearError = useCallback(
        (name) => {
            if (errors[name]) setErrors((errors) => ({ ...errors, [name]: null }));
        },
        [errors],
    );

    return {
        loading,
        errors,
        load,
        clearError,
    };
}
