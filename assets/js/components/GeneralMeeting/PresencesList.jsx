/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';

import PresencesRow from "./PresencesRow";

function PresencesList({ generalMeetingId, people, canEdit = false }) {
    const peopleElem = [];
    if (people.length > 0) {
        people.forEach(person => {
            peopleElem.push(<PresencesRow
                key={ person.id }
                generalMeetingId={ generalMeetingId }
                person={ person }
                canEdit={ canEdit }
            />);
        });
    }

    return (<table className="table table-striped table-bordered table-middle text-center dataTable">
        <thead>
        <tr>
            <th>Nom</th>
            <th>Organisation(s)</th>
            {canEdit && <th></th>}
        </tr>
        </thead>
        <tbody>
        { peopleElem }
        </tbody>
    </table>);
}

export default PresencesList;
