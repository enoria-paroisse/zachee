/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, {useEffect, useState} from 'react';
import {createRoot} from "react-dom/client";
import slug from 'slug';

import Spinner from "../Toolbar/Spinner";
import PresencesList from "./PresencesList";

function Presences({ generalMeetingId, canEdit = false }) {
    const [people, setPeople] = useState([]);
    const [peopleFiltered, setPeopleFiltered] = useState([]);
    const [peopleLoaded, setPeopleLoaded] = useState(false);
    const [peopleError, setPeopleError] = useState(false);
    const [peopleLoading, setPeopleLoading] = useState(false);

    function loadPeople(id) {
        setPeopleError(false);
        setPeopleLoading(true);
        fetch('/general-meeting/' + id + '/presence.json')
            .then(response => {
                setPeopleLoading(false);
                if (!response.ok) {
                    console.error(response.status);
                    setPeopleError(true);

                    return Promise.reject();
                }
                return response.text();
            })
            .then(response => {
                response = JSON.parse(response);
                setPeople(response.people.sort((a, b) => {
                    if (a.lastname.toLowerCase() > b.lastname.toLowerCase())
                        return 1;
                    if (a.lastname.toLowerCase() < b.lastname.toLowerCase())
                        return -1;
                    if (a.firstname.toLowerCase() > b.firstname.toLowerCase())
                        return 1;
                    if (a.firstname.toLowerCase() < b.firstname.toLowerCase())
                        return -1;
                    return 0;
                }));
                setPeopleFiltered(response.people);
                setPeopleLoaded(true);
            });
    }

    function filterResult(query) {
        query = slug(query.trim());
        setPeopleFiltered(people.filter(person => {
            if (slug(person.firstname).includes(query) || slug(person.lastname).includes(query))
                return true;

            let result = false;
            person.organizations.forEach(organization => {
                if (slug(organization.name).includes(query))
                    result = true;
            })
            return result;
        }));
    }

    useEffect(() => {
        if (people.length === 0 && !peopleLoaded && !peopleLoading && !peopleError) loadPeople(generalMeetingId);
    }, [people]);

    return (<div>
        <div className="input-group mb-1">
            <span className="input-group-text"><i className="fa-solid fa-magnifying-glass"></i></span>
            <input
                type="text"
                className="form-control"
                placeholder="Rechercher"
                onChange={(e) => filterResult(e.target.value)}
            />
        </div>
        {peopleLoaded && <PresencesList generalMeetingId={ generalMeetingId } people={ peopleFiltered } canEdit={ canEdit }/>}
        {peopleLoading && <Spinner/>}
        {peopleError && <p className="mb-1 alert alert-danger">
            Une erreur est survenue. Impossible de charger la liste des personnes.
        </p>}
    </div>);y
}

const presences = document.querySelectorAll('.presences');
presences.forEach((presencesList) => {
    const root = createRoot(presencesList);
    root.render(
        <Presences
            generalMeetingId={ presencesList.getAttribute('data-enoria-general-meetting') }
            canEdit={ presencesList.getAttribute('data-enoria-can-edit') }
        />
    );
});


export default Presences;
