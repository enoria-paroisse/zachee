/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, {useState} from 'react';

import PresencesOrganization from "./PresencesOrganization";
import Spinner from "../Toolbar/Spinner";

function PresencesRow({ generalMeetingId, person, canEdit = false }) {
    const [present, setPresent] = useState(person.present);
    const [toggleError, setToggleError] = useState(false);
    const [toggleLoading, setToggleLoading] = useState(false);

    const organizations = [];
    if (person.organizations.length > 0) {
        person.organizations.forEach(organization => {
            organizations.push(<PresencesOrganization
                key={ organization.id }
                organization={ organization }
            />);
        });
    }

    function togglePresence() {
        if (!canEdit) return alert('Vous ne pouvez pas modifier la présence');

        setToggleLoading(true);
        fetch('/general-meeting/' + generalMeetingId + '/presence/toggle/' + person.id)
            .then(response => {
                setToggleLoading(false);
                if (!response.ok) {
                    console.error(response.status);
                    setToggleError(true);

                    return Promise.reject();
                }
                return response.text();
            })
            .then(response => {
                response = JSON.parse(response);
                person.present = !!response.present;
                setPresent(person.present);
            });
    }

    return (<tr className={ present ? ' bg-success' : '' }>
        <td className="text-start">
            <a href={ '/person/show/' + person.id }>
                { person.title } { person.firstname } <span className="smallcaps">{ person.lastname }</span>
            </a>
        </td>
        <td>
            { organizations }
        </td>
        {canEdit && <td onClick={togglePresence}>
            {toggleLoading && <Spinner/>}
            {!toggleLoading && <i className={'fa-regular fa-square' + (present ? '-check' : '')}></i>}
            {toggleError && <span className="text-danger">
                <br/>Une erreur est survenue. Impossible de mettre à jour la présence.
            </span>}
        </td>}
    </tr>);
}

export default PresencesRow;
