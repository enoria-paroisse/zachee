/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, {useState} from 'react';

function PresencesSearchBar() {
    const [query, setQuery] = useState('');

    return (<div className="input-group mb-1">
        <span className="input-group-text"><i className="fa-solid fa-magnifying-glass"></i></span>
        <input
            type="text"
            className="form-control"
            placeholder="Rechercher"
            value={ query }
            onChange={ (e) => setQuery(e.target.value) }
        />
        <span className="input-group-text"><i className="fa-solid fa-spinner fa-spin text-primary"></i></span>
    </div>);
}

export default PresencesSearchBar;
