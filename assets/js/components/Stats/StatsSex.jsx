/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import {Doughnut} from "react-chartjs-2";

function StatsSex({ stats }) {
    return <div className="card">
        <div className="card-header">
            <h4 className="card-title">Statistiques Hommes/Femmes</h4>
        </div>
        <div className="card-body">
            <Doughnut data={{
                labels: ['Hommes', 'Garçons', 'Femmes', 'Filles'],
                datasets: [
                    {
                        data: [stats.personnes.details.M.A, stats.personnes.details.M.J, stats.personnes.details.F.A, stats.personnes.details.F.J],
                        backgroundColor: ['#1e5895', '#1e9ff2', '#951e50', '#d72570'],
                    },
                ],
            }} options={{
                responsive: true,
                maintainAspectRatio: false,
                circumference: 180,
                rotation: -90,
            }} style={{ display: 'block', height: '250px', width: '400px' }} />
        </div>
    </div>;
}

export default StatsSex;
