/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';

function General({ stats }) {
    return <div>
            <div className="row">
                <div className="col-xl-3 col-md-6 col-12">
                    <div className="card bg-success">
                        <div className="card-content">
                            <div className="card-body">
                                <div className="media d-flex">
                                    <div className="align-self-center">
                                        <i className="fa fa-sitemap white font-large-2 float-left"></i>
                                    </div>
                                    <div className="media-body white text-end ms-2">
                                        <h3 className="white">{ stats.entites.paroisses + stats.entites.dioceses }</h3>
                                        <span>Entités ({ stats.entites.paroisses } paroisses)</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-xl-3 col-md-6 col-12">
                    <div className="card bg-warning">
                        <div className="card-content">
                            <div className="card-body">
                                <div className="media d-flex">
                                    <div className="align-self-center">
                                        <i className="fa fa-address-book white font-large-2 float-left"></i>
                                    </div>
                                    <div className="media-body white text-end ms-2">
                                        <h3 className="white">{ stats.personnes.total }</h3>
                                        <span>Personnes</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-xl-3 col-md-6 col-12">
                    <div className="card bg-info">
                        <div className="card-content">
                            <div className="card-body">
                                <div className="media d-flex">
                                    <div className="align-self-center">
                                        <i className="fa fa-users-cog white font-large-2 float-left"></i>
                                    </div>
                                    <div className="media-body white text-end ms-2">
                                        <h3 className="white">{ stats.users }</h3>
                                        <span>Utilisateurs</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-xl-3 col-md-6 col-12">
                    <div className="card bg-danger">
                        <div className="card-content">
                            <div className="card-body">
                                <div className="media d-flex">
                                    <div className="align-self-center bg-danger">
                                        <i className="fa fa-database white font-large-2 float-left"></i>
                                    </div>
                                    <div className="media-body white text-end ms-2">
                                        <h3 className="white">{ stats.logs }</h3>
                                        <span>actions enregistrées</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-xl-2 col-md-4 col-12">
                    <div className="card bg-light">
                        <div className="card-content">
                            <div className="card-body">
                                <div className="media d-flex">
                                    <div className="align-self-center">
                                        <i className="fa fa-building black font-large-2 float-left"></i>
                                    </div>
                                    <div className="media-body text-end ms-2">
                                        <h3 className="black">{ stats.salles }</h3>
                                        <span>salles</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-xl-2 col-md-4 col-12">
                    <div className="card bg-light black">
                        <div className="card-content">
                            <div className="card-body">
                                <div className="media d-flex">
                                    <div className="align-self-center">
                                        <i className="fa fa-calendar-plus black font-large-2 float-left"></i>
                                    </div>
                                    <div className="media-body text-end ms-2">
                                        <h3 className="black">{ stats.reservations }</h3>
                                        <span>réserva.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-xl-2 col-md-4 col-12">
                    <div className="card bg-light black">
                        <div className="card-content">
                            <div className="card-body">
                                <div className="media d-flex">
                                    <div className="align-self-center">
                                        <i className="fa fa-users black font-large-2 float-left"></i>
                                    </div>
                                    <div className="media-body text-end ms-2">
                                        <h3 className="black">{ stats.groupes }</h3>
                                        <span>groupes</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-xl-2 col-md-4 col-12">
                    <div className="card bg-light black">
                        <div className="card-content">
                            <div className="card-body">
                                <div className="media d-flex">
                                    <div className="align-self-center">
                                        <i className="fa fa-cross black font-large-2 float-left"></i>
                                    </div>
                                    <div className="media-body text-end ms-2">
                                        <h3 className="black">{ stats.sacrements }</h3>
                                        <span>céleb sacr.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-xl-2 col-md-4 col-12">
                    <div className="card bg-light black">
                        <div className="card-content">
                            <div className="card-body">
                                <div className="media d-flex">
                                    <div className="align-self-center">
                                        <i className="fa fa-book-dead black font-large-2 float-left"></i>
                                    </div>
                                    <div className="media-body text-end ms-2">
                                        <h3 className="black">{ stats.funerailles }</h3>
                                        <span>funérailles</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-xl-2 col-md-4 col-12">
                    <div className="card bg-light black">
                        <div className="card-content">
                            <div className="card-body">
                                <div className="media d-flex">
                                    <div className="align-self-center">
                                        <i className="fa fa-pray black font-large-2 float-left"></i>
                                    </div>
                                    <div className="media-body text-end ms-2">
                                        <h3 className="black">{ stats.messes }</h3>
                                        <span>messes</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>;
}

export default General;
