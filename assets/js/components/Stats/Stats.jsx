/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, {useEffect, useState} from 'react';
import {createRoot} from "react-dom/client";
import { Chart as ChartJS, ArcElement, CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Filler, Legend } from 'chart.js';
import 'chartjs-adapter-luxon';

import Spinner from "../Toolbar/Spinner";
import General from "./General";
import StatsSex from "./StatsSex";
import StatsConnections from "./StatsConnections";
import StatsActivity from "./StatsActivity";
import StatsMaps from "./StatsMaps";
import InstancesButtons from "./InstancesButtons";

ChartJS.register(ArcElement, CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Filler, Legend);

const emptyStats = {
    ConnexionsParMois: [],
    EntreeLogsParMois: [],
    entites: {
        paroisses: 0,
        dioceses: 0,
    },
    personnes: {
        total: 0,
        details: {
            M: {
                A: 0,
                J: 0,
            },
            F: {
                A: 0,
                J: 0,
            },
        },
    },
    adresses: [],
    users: 0,
    groupes: 0,
    logs: 0,
    salles: 0,
    reservations: 0,
    sacrements: 0,
    funerailles: 0,
    messes: 0,
};

function Stats({ instance, instances = [] }) {
    if (!Array.isArray(instances)) instances = [];

    const [stats, setStats] = useState(emptyStats);
    const [instancesButtons, setInstancesButtons] = useState(instances);

    const [statsLoaded, setStatsLoaded] = useState(false);
    const [statsError, setStatsError] = useState(false);
    const [loadingStats, setLoadingStats] = useState(false);



    function addStats(rawStats) {
        setStats(prevStats => {
            const finalConnexionsParMois = [];
            const tmpConnexionsParMois = {};
            prevStats.ConnexionsParMois.forEach(mois => {
                tmpConnexionsParMois[mois.tri] = {
                    value: mois.y,
                    label: mois.x,
                };
            });
            rawStats.ConnexionsParMois.forEach(mois => {
                if (tmpConnexionsParMois[mois.tri] === undefined) tmpConnexionsParMois[mois.tri] = {
                    label: mois.x,
                    value: 0,
                };
                tmpConnexionsParMois[mois.tri].value += mois.y;
            });
            Object.getOwnPropertyNames(tmpConnexionsParMois).sort().forEach(tri => {
                finalConnexionsParMois.push({
                    x: tmpConnexionsParMois[tri].label,
                    y: tmpConnexionsParMois[tri].value,
                    tri,
                });
            });

            const finalEntreeLogsParMois = [];
            const tmpEntreeLogsParMois = {};
            prevStats.EntreeLogsParMois.forEach(mois => {
                tmpEntreeLogsParMois[mois.tri] = {
                    value: mois.y,
                    label: mois.x,
                };
            });
            rawStats.EntreeLogsParMois.forEach(mois => {
                if (tmpEntreeLogsParMois[mois.tri] === undefined) tmpEntreeLogsParMois[mois.tri] = {
                    label: mois.x,
                    value: 0,
                };
                tmpEntreeLogsParMois[mois.tri].value += mois.y;
            });
            Object.getOwnPropertyNames(tmpEntreeLogsParMois).sort().forEach(tri => {
                finalEntreeLogsParMois.push({
                    x: tmpEntreeLogsParMois[tri].label,
                    y: tmpEntreeLogsParMois[tri].value,
                    tri,
                });
            });

            return {
                ConnexionsParMois: finalConnexionsParMois,
                EntreeLogsParMois: finalEntreeLogsParMois,
                entites: {
                    paroisses: prevStats.entites.paroisses + Number(rawStats.entites.paroisses),
                    dioceses: prevStats.entites.dioceses + Number(rawStats.entites.dioceses),
                },
                personnes: {
                    total: prevStats.personnes.total + Number(rawStats.personnes.total),
                        details: {
                        M: {
                            A: prevStats.personnes.details.M.A + Number(rawStats.personnes.details.M.A),
                                J: prevStats.personnes.details.M.J + Number(rawStats.personnes.details.M.J),
                        },
                        F: {
                            A: prevStats.personnes.details.F.A + Number(rawStats.personnes.details.F.A),
                                J: prevStats.personnes.details.F.J + Number(rawStats.personnes.details.F.J),
                        },
                    },
                },
                adresses: [...prevStats.adresses, ...rawStats.adresses],
                    users: prevStats.users + Number(rawStats.users),
                groupes: prevStats.groupes + Number(rawStats.groupes),
                logs: prevStats.logs + Number(rawStats.logs),
                salles: prevStats.salles + Number(rawStats.salles),
                reservations: prevStats.reservations + Number(rawStats.reservations),
                sacrements: prevStats.sacrements + Number(rawStats.sacrements),
                funerailles: prevStats.funerailles + Number(rawStats.funerailles),
                messes: prevStats.messes + Number(rawStats.messes),
            }
        });
    }

    function loadStatsInstance(id, setLoadingStats, setStatsError, setStatsLoaded) {
        setStatsError(false);
        setLoadingStats(true);
        return fetch('/stats/' + id + '.json')
            .then(response => {
                setLoadingStats(false);
                if (!response.ok) {
                    console.error(response.status);
                    setStatsError(true);

                    return Promise.reject();
                }
                return response.text();
            })
            .then(response => {
                addStats(JSON.parse(response));
                setStatsLoaded(true);
                return true;
            })
            .catch(reason => {
                console.error(reason);
                setStatsError(true);
                setStatsLoaded(true);
                setLoadingStats(false);
                return false;
            });
    }

    function loadStats() {
        instances.forEach(instance => {
            loadStatsInstance(
                Number(instance.id),
                (status) => {
                    instance.loading = status;
                },
                () => {},
                () => {}
            ).then((success) => {
                if (success) instance.status = 'success';
                else instance.status = 'warning';
                setInstancesButtons([...instances]);
            });
        });
    }

    useEffect(() => {
        if (!statsLoaded && !statsError && !loadingStats) {
            if (instance === '0') loadStats();
            else loadStatsInstance(Number(instance), setLoadingStats, setStatsError, setStatsLoaded).finally();
        }
    }, [statsLoaded]);

    return <div>
        { loadingStats && <div className="row mb-1">
            <Spinner />
        </div> }
        { statsError && <div className="row">
            <div className="col">
                <p className="alert alert-warning">Impossible de récupérer les statistiques</p>
            </div>
        </div> }

        { instances.length > 0 && <InstancesButtons instances={ instancesButtons } /> }

        <General stats={ stats } />

        <div className="row">
            <div className="col-6">
                <StatsSex stats={ stats } />
            </div>
            <div className="col-6">
                <StatsConnections stats={ stats } />
            </div>
        </div>
        <div className="row">
            <div className="col-12">
                <StatsActivity stats={ stats } />
            </div>
        </div>

        <div className="row">
            <div className="col-12">
                <StatsMaps stats={ stats } />
            </div>
        </div>
    </div>;
}

const stats = document.querySelectorAll('.stats');
stats.forEach((statsBlock) => {
    const root = createRoot(statsBlock);
    root.render(
        <Stats
            instance={ statsBlock.getAttribute('data-enoria-instance') ?? 0 }
            instances={ JSON.parse(statsBlock.getAttribute('data-enoria-instances')) ?? [] }
        />
    );
});

export default Stats;
