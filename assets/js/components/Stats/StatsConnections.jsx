/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import {Line} from "react-chartjs-2";

function StatsConnections({ stats }) {
    return <div className="card">
        <div className="card-header">
            <h4 className="card-title">Connexions mensuelles</h4>
        </div>
        <div className="card-body">
            <Line data={{
                labels: stats.ConnexionsParMois.map((row) => row.x),
                datasets: [
                    {
                        data: stats.ConnexionsParMois.map((row) => row.y),
                        backgroundColor: '#bb8210',
                    },
                ],
            }} options={{
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: { display: false },
                },
                fill: true,
                tension: 0.3,
            }} style={{ display: 'block', height: '350px', width: '400px' }} />
        </div>
    </div>;
}

export default StatsConnections;
