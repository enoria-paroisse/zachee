/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';

import Spinner from "../Toolbar/Spinner";

function InstancesButtons({ instances = [] }) {
    const buttons = [];

    instances.forEach(instance => {
        buttons.push(
            <a key={ instance.id } className={ 'me-1 btn btn-outline-' + instance.status } href={ instance.url }>
                {
                    instance.loading && <span className="spinner-border spinner-border-sm text-primary" role="status">
                        <span className="visually-hidden">Chargement...</span>
                    </span>
                } { instance.name }
            </a>
        );
    });

    return <div className="row mb-1">
        <div className="col">
            { buttons }
        </div>
    </div>;
}

export default InstancesButtons;
