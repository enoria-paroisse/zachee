/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, {useEffect, useState} from 'react';
import L from 'leaflet';
import {MapContainer, Marker, Popup, TileLayer} from 'react-leaflet'

import 'leaflet/dist/leaflet.css';

L.Icon.Default.prototype.options.iconUrl = '/assets/images/map-icon.png';
L.Icon.Default.prototype.options.shadowUrl = '/assets/images/map-shadow.png';
L.Icon.Default.prototype.options.iconRetinaUrl = '/assets/images/map-icon-2x.png';
L.Icon.Default.prototype.options.iconSize = [12, 20];
L.Icon.Default.prototype.options.iconAnchor = [6, 10];
L.Icon.Default.prototype.options.shadowAnchor = [12, 30];

function StatsMaps({ stats }) {

    const [markers, setMarkers] = useState([]);

    useEffect(() => {
        const tmpMarkers = [];
        stats.adresses.forEach((adresse, index) => {
            tmpMarkers.push(
                <Marker position={ [Number(adresse.lat), Number(adresse.lgt)] } key={ index }>
                <Popup>
                    { adresse.entite }
                </Popup>
                </Marker>
            );
        });
        setMarkers(tmpMarkers);
    }, [stats]);

    return <div className="card">
        <div className="card-header">
            <h4 className="card-title">Emplacement des entités</h4>
        </div>
        <div className="card-body">
            <MapContainer center={ [48.866667, 2.333333] } zoom={ 6 } scrollWheelZoom={ true } style={ { width: '100%', height: '800px' } }>
                <TileLayer
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                { markers }
            </MapContainer>
        </div>
    </div>;
}

export default StatsMaps;
