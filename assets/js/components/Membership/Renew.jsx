/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import {createRoot} from "react-dom/client";
import React, {useEffect, useState} from "react";

import Spinner from "../Toolbar/Spinner";
import RenewImportable from "./RenewImportable";

function Renew() {
    const [period, setPeriod] = useState({id: 0, name: ''});
    const [periods, setPeriods] = useState({fake: true, periods: []})
    const [periodsLoaded, setPeriodsLoaded] = useState(false);
    const [periodsError, setPeriodsError] = useState(false);
    const [periodsLoading, setPeriodsLoading] = useState(false);

    const [memberships, setMemberships] = useState({
        dioceses: [],
        organizations: [],
        people: [],
        diocesesAlready: [],
        organizationsAlready: [],
        peopleAlready: [],
        importable: {
            dioceses: [],
            organizations: [],
            people: [],
        },
    });
    const [membershipsLoaded, setMembershipsLoaded] = useState(false);
    const [membershipsError, setMembershipsError] = useState(false);
    const [membershipsErrorMessage, setMembershipsErrorMessage] = useState('');
    const [membershipsLoading, setMembershipsLoading] = useState(false);

    function loadPeriods() {
        setPeriodsError(false);
        setPeriodsLoading(true);
        fetch('/api/periods?pagination=false')
            .then(response => {
                setPeriodsLoading(false);
                if (!response.ok) {
                    console.error(response.status);
                    setPeriodsError(true);

                    return Promise.reject();
                }
                return response.text();
            })
            .then(response => {
                response = JSON.parse(response);
                if (response['hydra:member']) {
                    const result = [];
                    response['hydra:member'].forEach(item => {
                        result.push({id: item.id, name: item.name})
                    });
                    setPeriods({fake: false, periods: result});
                    setPeriodsLoaded(true);
                } else {
                    setPeriodsError(true);
                    console.error('error parsing periods')

                    return Promise.reject();
                }
            })
        ;
    }

    function loadMemberships() {
        setMembershipsError(false);
        setMembershipsErrorMessage('Impossible de charger les adhésions depuis la période sélectionnée.');
        setMembershipsLoading(true);
        fetch('/request/renew/importable/'+period.id)
            .then(response => {
                setMembershipsLoading(false);
                if (!response.ok) {
                    setMembershipsError(true);
                    console.error(response.status);
                    response.text().then(response => {
                        response = JSON.parse(response);
                        if (response.error) {
                            setMembershipsErrorMessage(response.error);
                        }
                    })

                    return Promise.reject();
                }
                return response.text();
            })
            .then(response => {
                response = JSON.parse(response);
                const result = {
                    dioceses: response.memberships.dioceses,
                    organizations: response.memberships.organizations,
                    people: response.memberships.people,
                    diocesesAlready: response.memberships.diocesesAlready,
                    organizationsAlready: response.memberships.organizationsAlready,
                    peopleAlready: response.memberships.peopleAlready,
                    importable: {
                        dioceses: response.memberships.importable.dioceses,
                        organizations: response.memberships.importable.organizations,
                        people: response.memberships.importable.people,
                    },
                };
                setMemberships(result);
                setMembershipsLoaded(true);
            })
        ;
    }

    function selectPeriod() {
        const selectPeriod = document.getElementById('period');
        if (selectPeriod && selectPeriod.value) {
            periods.periods.forEach(item => {
                if (item.id === Number(selectPeriod.value)) {
                    setPeriod({id: item.id, name: item.name})
                }
            })
        }
    }

    useEffect(() => {
        if ((!periods || periods.fake) && !periodsLoaded && !periodsLoading && !periodsError) loadPeriods();
    }, [periods]);

    useEffect(() => {
        if (period.id !== 0) {
            loadMemberships();
        }
    }, [period]);

    return (
        <div>
            <div className="row">
                <div className="col-md-6">
                    <div className="alert alert-info">
                        Vous pouvez importer toutes les adhésions validées d’une autre période pour demander un
                        renouvellement des cotisations.
                    </div>
                </div>
                <div className="col-md-6">
                    <div className="card">
                        <div className="card-body">
                            <h2 className="card-title"><i className="fa fa-history"></i> Sélection de la période</h2>
                            {periodsLoaded && <div>
                                <div>
                                    <select className="form-select" id="period">
                                        {periods.periods.map(item => <option key={item.id}
                                                                             value={item.id}>{item.name}</option>)}
                                    </select>
                                </div>
                                <p className="text-end mt-1">
                                    <button className="btn btn-success" onClick={ selectPeriod }>
                                        <i className="fa fa-check"></i> Sélectionner
                                    </button>
                                </p>
                            </div>}
                            {periodsLoading && <Spinner/>}
                            {periodsError && <p className="mb-1 alert alert-danger">
                                Une erreur est survenue. Impossible de charger les périodes.
                            </p>}
                        </div>
                    </div>
                </div>
            </div>

            <div className="row">
                {membershipsLoading && <Spinner />}
                {membershipsError && <div className="col"><p className="mb-1 alert alert-danger">
                    Une erreur est survenue. { membershipsErrorMessage }
                </p></div>}
            </div>
            {membershipsLoaded && <RenewImportable period={period} memberships={memberships} />}
        </div>
    );
}

const renews = document.querySelectorAll('.renew-membership');
renews.forEach((renew) => {
    const root = createRoot(renew);
    root.render(
        <Renew/>
    );
});

export default Renew;
