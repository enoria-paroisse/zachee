/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from "react";

function RenewElem({type, idMember, name, status, imported = false, error = ''}) {
    let href = '';
    if (type === 'person') {
        href = '/person/show/' + idMember;
    } else {
        href = '/organization/show/' + idMember;
    }

    if (imported) {
        let icon = 'fa-solid ';
        if (type === 'person') {
            icon += 'fa-address-book';
        } else if (type === 'diocese') {
            icon += 'fa-church';
        } else {
            icon += 'fa-clone';
        }

        return (
            <li>
                <a href={href} className={error ? 'text-decoration-line-through' : ''}>
                    <i className={icon}></i> {name}
                </a>
                {error !== '' ? <span className="text-danger"> {error}</span> : ''}
            </li>
        );
    } else {
        let statusElem = <span className="badge bg-dark">Statut inconnu</span>
        if (status === 'already') {
            statusElem = <span className="badge bg-light text-secondary">Déjà présente</span>;
        } else if (status === 'in_progress') {
            statusElem = <span className="badge bg-primary">Adhésion en cours</span>;
        } else if (status === 'canceled') {
            statusElem = <span className="badge bg-danger">Adhésion annulée</span>;
        } else if (status === 'validated') {
            statusElem = <span className="badge bg-success">Adhésion validée</span>;
        }

        return (
            <li><a href={href}>{name}</a> {statusElem}</li>
        );
    }
}

export default RenewElem;
