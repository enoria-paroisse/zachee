/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, {useEffect, useState} from "react";

import RenewElem from "./RenewElem";
import RenewImportation from "./RenewImportation";

function RenewImportable({period, memberships}) {
    const [importationStarted, setImportationStarted] = useState(false);
    const [progress, setProgress] = useState(true);
    const [total, setTotal] = useState((memberships.importable.dioceses.length ?? 0) + (memberships.importable.organizations.length ?? 0) + (memberships.importable.people.length ?? 0));
    const [now, setNow] = useState(0);
    const [imported, setImported] = useState([]);
    const [importationErrors, setImportationErrors] = useState([]);

    useEffect(() => {
        setProgress(true);
        setImported([]);
        setImportationErrors([]);
        setTotal((memberships.importable.dioceses.length ?? 0) + (memberships.importable.organizations.length ?? 0) + (memberships.importable.people.length ?? 0));
        setNow(0);
        setImportationStarted(false);
    }, [period, memberships]);

    function startImport() {
        setImportationStarted(true);
        setProgress(true);
        Promise
            .all(memberships.importable.dioceses.map(elem => importMembership(elem)))
            .then(() => Promise.all(memberships.importable.organizations.map(elem => importMembership(elem))))
            .then(() => Promise.all(memberships.importable.people.map(elem => importMembership(elem))))
            .finally(() => {
                setNow(total);
                setProgress(false);
            });
    }

    function importMembership(id) {
        return fetch('/request/renew/exec/'+id)
            .then(response => {
                return response.text().then(text => {
                    setNow(prev => prev + 1);

                    let parsed = {};
                    try {
                        parsed = {...JSON.parse(text)}
                    } catch (error) {}

                    const result = {
                        code: response.status,
                        ...parsed,
                    };

                    if (result.code !== 200) {
                        if (!result.error) {
                            result.error = response.error;
                        }
                        setImportationErrors(prev => ([...prev, result]))
                    } else {
                        setImported(prev => ([...prev, result]))
                    }

                    return Promise.resolve();
                });
            });
    }

    return (
        <div className="row">
            <div className="col-md-8">
                {importationStarted && <div className="card card-body">
                    <h2 className='card-title'>
                        Importation de la période <em>{period.name}</em> dans la période courante
                    </h2>
                    <RenewImportation progress={progress} total={total} now={now} imported={imported} importationErrors={importationErrors} />
                </div>}

                <div className="card card-body">
                    <h2 className="card-title">
                        Adhésions de la période <em>{period.name}</em> importables dans la période
                        courante
                    </h2>

                    <div className="ms-2">
                        <h3><i className="fa fa-church"></i> Diocèses</h3>
                        <ul>
                            {!memberships.dioceses.length && !memberships.diocesesAlready.length &&
                                <li><em>Aucun diocèse adhérent</em></li>}
                            {memberships.dioceses.length && memberships.dioceses.map(
                                organization => <RenewElem key={organization.id} type='organization' idMember={organization.idMember} name={organization.name} status={organization.status} />
                            ) || ''}
                            {memberships.diocesesAlready.length && memberships.diocesesAlready.map(
                                organization => <RenewElem key={organization.id} type='organization' idMember={organization.idMember} name={organization.name} status='already' />
                            ) || ''}
                        </ul>

                        <h3><i className="fa fa-clone"></i> Organisations</h3>
                        <ul>
                            {!memberships.organizations.length && !memberships.organizationsAlready.length &&
                                <li><em>Aucune organisation adhérente</em></li>}
                            {memberships.organizations.length && memberships.organizations.map(
                                organization => <RenewElem key={organization.id} type='organization' idMember={organization.idMember} name={organization.name} status={organization.status} />
                            ) || ''}
                            {memberships.organizationsAlready.length && memberships.organizationsAlready.map(
                                organization => <RenewElem key={organization.id} type='organization' idMember={organization.idMember} name={organization.name} status='already' />
                            ) || ''}
                        </ul>

                        <h3><i className="fa fa-address-book"></i> Personnes</h3>
                        <ul>
                            {!memberships.people.length && !memberships.peopleAlready.length &&
                                <li><em>Aucune personne adhérente</em></li>}
                            {memberships.people.length && memberships.people.map(
                                person => <RenewElem key={person.id} type='person' idMember={person.idMember} name={person.name} status={person.status} />
                            ) || ''}
                            {memberships.peopleAlready.length && memberships.peopleAlready.map(
                                person => <RenewElem key={person.id} type='person' idMember={person.idMember} name={person.name} status='already' />
                            ) || ''}
                        </ul>
                    </div>
                </div>
            </div>
            <div className="col-md-4">
                <div className="alert alert-success">
                    <h4><i className="fa fa-question"></i> Aide</h4>
                    <p className="ms-2">
                        Une <strong>adhésion annulée</strong> dans la période sélectionnée n’est pas
                        importable dans la période en cours.
                    </p>
                    <p className="ms-2">
                        Une <strong>adhésion en cours</strong> dans la période sélectionnée n’est pas
                        importable. Vous pouvez la modifier et changer sa période.
                    </p>
                    <p className="ms-2">
                        Une <strong>adhésion validée</strong> est importable.
                    </p>
                    <p className="ms-2">
                        <strong>Déjà présente</strong>&nbsp;: une adhésion correspondante est déjà présente dans la
                        période en cours.
                    </p>
                </div>
                {progress && <div className="text-center">
                    <button className='btn btn-warning' onClick={startImport}>
                        <i className="fa-solid fa-triangle-exclamation"></i> Lancer l’importation
                    </button>
                </div>}
            </div>
        </div>
    );
}

export default RenewImportable;
