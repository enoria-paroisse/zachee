/**
 * @copyright Copyright (c) 2024 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from "react";
import ProgressBar from 'react-bootstrap/ProgressBar';

import RenewElem from "./RenewElem";

function RenewImportation({progress, total, now, imported, importationErrors}) {
    return (
        <div>
            {progress &&
                <p className="alert alert-danger">Ne pas changer de période tant que l’importation n’est pas terminée&nbsp;!</p>}
            {progress &&
                <p className="text-center">Importation de {total} adhésion{total > 1 ? 's' : ''} en cours...</p>}
            <div className="mb-2">
                <ProgressBar animated={progress} now={100 * now / total} label={now + '/' + total}/>
            </div>

            <h3>Adhésions importées</h3>
            <ul>
                {!imported.length && <li>Aucune erreur</li>}
                {imported.length && imported.map(
                    (elem, k) => <RenewElem key={k} imported={true} type={elem.type} name={elem.name} idMember={elem.idMember} />
                ) || ''}
            </ul>

            <h3>Erreurs d’importation</h3>
            <ul>
                {!importationErrors.length && <li>Aucune erreur</li>}
                {importationErrors.length && importationErrors.map(
                    (elem, k) => <RenewElem key={k} imported={true} type={elem.type} name={elem.name} idMember={elem.idMember} error={elem.error} />
                ) || ''}
            </ul>

        </div>
    );
}

export default RenewImportation;
