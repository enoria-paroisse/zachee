/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, {useEffect, useState} from 'react';

import Spinner from "../Toolbar/Spinner";

function PaymentRequestFileForm({ paymentRequest, file, setFormShow, isUpdate }) {
    const [errorUpdate, setErrorUpdate] = useState(false);
    const [fileUpload, setFileUpload] = useState(!!file.file);

    function hideForm() {
        setFormShow(false);
        setErrorUpdate(false);
    }

    function toggleFileUpload() {
        setFileUpload(!fileUpload);
    }

    function getDefaultPrefix() {
        if (file.refPrefix) return file.refPrefix;
        else {
            let base = 'apayer';
            if (paymentRequest.membership) base = 'cotisation';
            else if (paymentRequest.dioceseMembershipSupplement && paymentRequest.amount > 0) base = 'diocese-cplmt';
            else if (paymentRequest.dioceseMembershipSupplement) base = 'diocese-refund';
            else if (paymentRequest.smsBilling) base = 'sms-'+paymentRequest.smsBilling.entity.instance.name.toLowerCase();
            return base + '-' + new Date(paymentRequest.datetime).getUTCFullYear();
        }
    }

    return (
        <div className={ 'rounded-2 border border-' + (isUpdate ? 'warning' : 'success')+ ' m-2 p-2' }>
            <header className="mb-2">
                <h2 id="addAllocationModalLabel" className="d-inline">{ isUpdate ? 'Regénérer' : 'Générer' } le fichier associé à la demande de paiement</h2>
                <button type="button" className="btn-close float-end" aria-label="Fermer" onClick={ hideForm }></button>
            </header>
            <form action={ isUpdate ? '/accounting/payment_request/file/edit/' + file.id : '/accounting/payment_request/file/new/' + paymentRequest.id } method="POST" encType="multipart/form-data">
                <section>
                    <div className="row">
                        <div className="col-sm-6 mb-1">
                            <label htmlFor="prefix" className="form-label">
                                Préfixe
                            </label>
                            <input type="text" id="prefix" name="prefix" className="form-control" defaultValue={ getDefaultPrefix() } />
                        </div>
                        <div className="col-sm-6 mb-1">
                            <label htmlFor="number" className="form-label">
                                Numéro
                            </label>
                            <input type="text" id="number"  name="number" className="form-control" defaultValue={ file.refNumber ?? 0 } />
                            <div className="form-text">Mettre 0 pour générer automatiquement ce numéro</div>
                        </div>
                    </div>
                    <div className="form-check form-switch mb-1">
                        <input type="checkbox" id="upload" name="upload" className="form-check-input" checked={ fileUpload } onChange={ toggleFileUpload } />
                        <label htmlFor="upload" className="form-check-label">
                            Enregistrer ou conserver un fichier plutôt que { isUpdate ? 'regénérer' : 'générer' } de le fichier
                        </label>
                    </div>
                    { fileUpload &&
                        <div className="mb-1">
                            <label htmlFor="file" className="form-label">Reçu</label>
                            <input type="file" id="file" name="file" placeholder="Choisir un PDF" className="form-control" />
                            <div className="form-text">Laisser vide pour conserver le fichier</div>
                        </div>
                    }
                    <div className="form-check form-switch mb-1">
                        <input type="checkbox" id="sendMail" name="sendMail" className="form-check-input" defaultChecked={ false } />
                        <label htmlFor="sendMail" className="form-check-label">
                            Envoyer la demande par mail
                        </label>
                        <div className="form-text">Cela activera la relance automatique (tous les 15 jours).</div>
                    </div>
                    { errorUpdate && <p className="alert alert-danger">{ errorUpdate }</p> }
                </section>

                <footer className="text-center">
                    <button className="btn btn-warning me-2" onClick={ hideForm } type="reset">
                        <i className="fa fa-undo"></i> Annuler
                    </button>
                    <button className="btn btn-success" type="submit">
                        <i className="fa fa-arrows-rotate"></i> { isUpdate ? 'Regénérer' : 'Générer' } le fichier
                    </button>
                </footer>
            </form>
        </div>
    );
}

export default PaymentRequestFileForm;
