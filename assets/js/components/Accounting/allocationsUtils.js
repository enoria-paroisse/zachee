/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

export function checkAmount(payment, paymentRequest, allocatedAmount, finalMaximum, setErrorAllocate, update = false) {
    if ((!update && payment.status !== 'registered') || (update && ['registered', 'allocated'].indexOf(payment.status) === -1)) {
        setErrorAllocate('Vous ne pouvez pas affecter paiement déjà ' + (!update ? 'affecté,' : '') + ' validé ou annulé');
        return false;
    } else if (paymentRequest === 0) {
        setErrorAllocate('Vous devez choisir une demande de paiement');
        return false;
    } else if (allocatedAmount <= 0) {
        setErrorAllocate('Vous devez affecter plus de 0,00 €');
        return false;
    } else if (allocatedAmount > finalMaximum) {
        setErrorAllocate(
            'Vous ne pouvez pas affecter plus de ' +
                new Intl.NumberFormat('fr-FR', {
                    currency: 'EUR',
                    style: 'currency',
                }).format(finalMaximum),
        );
        return false;
    }

    return true;
}
