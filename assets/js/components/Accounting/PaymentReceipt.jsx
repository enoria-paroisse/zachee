/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, {useEffect, useState} from 'react';
import {createRoot} from "react-dom/client";

import Spinner from "../Toolbar/Spinner";
import ReceiptShow from "./ReceiptShow";
import ReceiptAdd from "./ReceiptAdd";

function PaymentReceipt({ paymentId, canEdit }) {
    canEdit = canEdit === '1';

    const [flash, setFlash] = useState(false);

    const [payment, setPayment] = useState(null);
    const [paymentLoaded, setPaymentLoaded] = useState(false);
    const [paymentError, setPaymentError] = useState(false);
    const [loadingPayment, setLoadingPayment] = useState(false);

    const [hasReceipt, setHasReceipt] = useState(false);
    const [receipt, setReceipt] = useState('');
    const [paymentStatus, setPaymentStatus] = useState('registered');

    function addFlash(type, message) {
        setFlash({ type, message });
    }

    function removeFlash() {
        setFlash(false);
    }

    function loadPayment(id) {
        setPaymentError(false);
        setLoadingPayment(true);
        fetch('/api/payments/' + id)
            .then(response => {
                setLoadingPayment(false);
                if (!response.ok) {
                    console.error(response.status);
                    setPaymentError(true);

                    return Promise.reject();
                }
                return response.text();
            })
            .then(response => {
                response = JSON.parse(response);
                setPayment(response);

                if (response.receipt !== undefined) {
                    setHasReceipt(true);
                    setReceipt(response.receipt);
                } else
                    setHasReceipt(false);
                setPaymentStatus(response.status);

                setPaymentLoaded(true);
            });
    }

    useEffect(() => {
        if (!payment && !paymentLoaded && !loadingPayment && !paymentError) loadPayment(paymentId);
    });

    return (
        <div className="row">
            <div className="col">
                <div className="card">
                    <div className="card-body">
                        <div className="row align-items-center">
                            <h2 className="col-6"><i className="fa fa-receipt"></i> Reçu</h2>
                        </div>
                        { flash && <p className={ 'm-2 alert alert-' + flash.type + ' alert-dismissible' }>
                            { flash.message }
                            <button type="button" className="btn-close" aria-label="Fermer" onClick={ removeFlash }></button>
                        </p> }

                        { paymentLoaded &&
                            (hasReceipt ?
                                <ReceiptShow canEdit={ canEdit } payment={ payment } receiptUri={ receipt } addFlash={ addFlash } /> :
                                (['allocated', 'validated'].indexOf(paymentStatus) > -1 ?
                                    (canEdit ? <ReceiptAdd payment={ payment } addFlash={ addFlash } /> : <div>
                                        <p className="alert alert-info m-2">Aucun reçu à afficher.</p>
                                    </div>) :
                                    ( canEdit ? <div>
                                        <p className="alert alert-info m-2">Vous ne pouvez pas ajouter un reçu à un paiement non affecté totalement ou annulé.</p>
                                        <p className="text-center m-2"><button className="btn btn-primary" onClick={ () => loadPayment(paymentId) }>Actualiser</button></p>
                                    </div> : <div>
                                        <p className="alert alert-info m-2">Aucun reçu à afficher.</p>
                                    </div>)
                                )
                            )
                        }
                        { loadingPayment && <Spinner /> }
                        { paymentError && <p className="mb-1 alert alert-danger">
                            Une erreur est survenue. Impossible de charger le reçu.
                        </p> }
                    </div>
                </div>
            </div>
        </div>
    );
}

const allocations = document.querySelectorAll('.payment-receipt');
allocations.forEach((allocationList) => {
    const root = createRoot(allocationList);
    root.render(
        <PaymentReceipt
            paymentId={ allocationList.getAttribute('data-enoria-payment') }
            canEdit={ allocationList.getAttribute('data-enoria-can-edit') }
        />
    );
});


export default PaymentReceipt;
