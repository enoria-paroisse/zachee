/**
* @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
* @license AGPL-3.0
*
* This file is part of Zachée Association Enoria.
*
* Zachée Association Enoria is free software: you can redistribute it and/or
* modify it under the terms of the GNU Affero Public License as
* published by the Free Software Foundation, either version 3 of the
* License, or any later version.
*
* Zachée Association Enoria is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero Public License for more details.
*
* You should have received a copy of the GNU Affero Public License
* along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
*/

import React, {useEffect, useState} from 'react';

import Spinner from "../Toolbar/Spinner";
import {checkAmount} from "./allocationsUtils";
import AllocationDonation from "./AllocationDonation";

function AllocationAddModal({ payment, organization, person, organizations, people, loadPayment }) {
    const [paymentMaximum, setPaymentMaximum] = useState(Math.round((payment.amount - payment.amountAllocated ?? 0) * 100) / 100);

    const [formShow, setFormShow] = useState(false);

    const [finalMaximum, setFinalMaximum] = useState(paymentMaximum);
    const [organizationSelected, setOrganizationSelected] = useState(organization);
    const [personSelected, setPersonSelected] = useState(person);

    const [loadingPaymentRequests, setLoadingPaymentRequests] = useState(false);
    const [errorGetPaymentRequests, setErrorGetPaymentRequests] = useState(false);

    const organizationsOptions = [];
    organizations.forEach(item => {
        organizationsOptions.push(
            { value: item.id, label: (item.diocese ?
                    (item.diocese.deptCode ? item.diocese.deptCode + ' ' : '') + item.diocese.name + ' : '  :
                    '' ) + item.name }
        );
    });
    const peopleOptions = [];
    people.forEach(item => {
        peopleOptions.push(
            { value: item.id, label: item.firstname + ' ' + item.lastname });
    });

    const [errorAllocate, setErrorAllocate] = useState(false);

    useEffect(() => {
        setPaymentMaximum(Math.round((payment.amount - payment.amountAllocated ?? 0) * 100) / 100);

        const organizationSelect = document.getElementById('organization');
        const personSelect = document.getElementById('person');
        const paymentRequestSelect = document.getElementById('paymentRequest');
        if (organizationSelect instanceof HTMLSelectElement && personSelect instanceof HTMLSelectElement && paymentRequestSelect instanceof HTMLSelectElement
            && organizationSelect.dataset.choice === undefined && personSelect.dataset.choice === undefined && paymentRequestSelect.dataset.choice === undefined) {
            const organizationChoices = newChoices(organizationSelect);
            organizationChoices.setChoices(organizationsOptions);
            const personChoices = newChoices(personSelect);
            personChoices.setChoices(peopleOptions);
            const paymentRequestChoices = newChoices(paymentRequestSelect);

            organizationChoices.setChoiceByValue(organizationSelected ?? '');
            personChoices.setChoiceByValue(personSelected ?? '');

            if (organizationSelected) {
                console.log(organizationChoices)
                updatePaymentRequests('organization', organizationSelected).then((paymentRequestOptions => {
                    console.log('update')
                    console.log(paymentRequestOptions);
                    paymentRequestChoices.clearStore();
                    paymentRequestChoices.setChoices(paymentRequestOptions);
                }));
            }
            else if (personSelected) {
                updatePaymentRequests('person', personSelected).then((paymentRequestOptions => {
                    paymentRequestChoices.clearStore();
                    paymentRequestChoices.setChoices(paymentRequestOptions);
                }));
            }

            organizationSelect.addEventListener('change', (e) => {
                personChoices.clearStore();
                personChoices.setChoices(peopleOptions);
                if (organizationSelect.options[organizationSelect.selectedIndex]) {
                    updatePaymentRequests('organization', organizationSelect.options[organizationSelect.selectedIndex].value)
                        .then((paymentRequestOptions => {
                            paymentRequestChoices.clearStore();
                            paymentRequestChoices.setChoices(paymentRequestOptions);
                        }));
                    setOrganizationSelected(organizationSelect.options[organizationSelect.selectedIndex].value);
                }
            });
            personSelect.addEventListener('change', (e) => {
                organizationChoices.clearStore();
                organizationChoices.setChoices(organizationsOptions);
                if (personSelect.options[personSelect.selectedIndex]) {
                    setPersonSelected(personSelect.options[personSelect.selectedIndex].value);
                    updatePaymentRequests('person', personSelect.options[personSelect.selectedIndex].value)
                        .then((paymentRequestOptions => {
                            paymentRequestChoices.clearStore();
                            paymentRequestChoices.setChoices(paymentRequestOptions);
                        }));
                }
            });

            paymentRequestSelect.addEventListener('change', (e) => {
                if (paymentRequestSelect.options[paymentRequestSelect.selectedIndex].value) {
                    updateMaxAmount(paymentRequestSelect.options[paymentRequestSelect.selectedIndex].value);
                }
            });
        }
    });

    function updatePaymentRequests(type, id) {
        if (!loadingPaymentRequests) {
            setErrorGetPaymentRequests(false);
            setLoadingPaymentRequests(true);

            const result = [
                {
                    id: 0,
                    description: "Sélectionner une demande paiement",
                }
            ];

            return fetch('/api/payment_requests?pagination=0&' + type + '=' + id + '&status=in_progress')
                .then(response => {
                    if (!response.ok) {
                        console.error(response.status);
                        setErrorGetPaymentRequests(true);

                        return Promise.reject();
                    }
                    return response.text();
                })
                .then(response => {
                    response = JSON.parse(response);
                    response['hydra:member'].forEach(item => {
                        result.push({
                            id: item.id,
                            description: item.description,
                        });
                    });

                    return fetch('/api/payment_requests?pagination=0&membership.' + type + '=' + id + '&status=in_progress');
                })
                .then(response => {
                    setLoadingPaymentRequests(false);
                    if (!response.ok) {
                        console.error(response.status);
                        setErrorGetPaymentRequests(true);
                        return Promise.reject();
                    }
                    return response.text();
                })
                .then(response => {
                    response = JSON.parse(response);
                    response['hydra:member'].forEach(item => {
                        result.push({
                            id: item.id,
                            description: item.description,
                        });
                    });

                    return fetch('/api/payment_requests?pagination=0&dioceseMembershipSupplement.diocese.associatedOrganization=' + id + '&status=in_progress');
                })
                .then(response => {
                    setLoadingPaymentRequests(false);
                    if (!response.ok) {
                        console.error(response.status);
                        setErrorGetPaymentRequests(true);
                        return Promise.reject();
                    }
                    return response.text();
                })
                .then(response => {
                    response = JSON.parse(response);
                    response['hydra:member'].forEach(item => {
                        result.push({
                            id: item.id,
                            description: item.description,
                        });
                    });

                    const paymentRequestOptions = [];
                    result.forEach(item => {
                        paymentRequestOptions.push({value: item.id, label: item.description});
                    });

                    return paymentRequestOptions;
                });
        } else return Promise.reject();
    }

    function updateMaxAmount(id) {
        fetch('/api/payment_requests/' + id)
            .then(response => {
                if (!response.ok) {
                    console.error(response.status);
                    throw Error(response.status + ': Impossible de charger la demande de paiement #' + id)
                }
                return response.text();
            })
            .then(response => {
                const result = JSON.parse(response);
                const amount = (Math.abs(Number(result.amount)) + 0.00 ) - (Number(result.amountPaid) + 0.00);
                setPaymentMaximum(Math.round((payment.amount - payment.amountAllocated ?? 0) * 100) / 100);
                if (amount < paymentMaximum) setFinalMaximum(amount)
                else setFinalMaximum(paymentMaximum);
            });
    }

    function showForm() {
        setFormShow(true);
    }

    function hideForm() {
        setFormShow(false);
    }

    function allocate() {
        setErrorAllocate(false);
        const paymentRequest = Number(document.getElementById('paymentRequest').value);
        const allocatedAmount = Number(document.getElementById('allocatedAmount').value.replace(',', '.'));

        if (checkAmount(payment, paymentRequest, allocatedAmount, finalMaximum, setErrorAllocate))
            fetch('/api/payment_allocations', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    payment: '/api/payments/' + payment.id,
                    paymentRequest: '/api/payment_requests/' + paymentRequest,
                    amount: allocatedAmount + '',
                }),
            }).then(response => {
                if (!response.ok) {
                    console.error(response.status);
                    console.error(response);
                    setErrorAllocate('Impossible d’affecter le paiement');
                    return;
                }
                loadPayment(payment.id).then(() => {
                    setFormShow(false);
                });
            });
    }

    return (
        <div className="col-md-12 col-xl-6">
            { formShow ?
                <div className="rounded-2 border border-success m-2 p-2">
                    <header className="mb-2">
                        <h2 id="addAllocationModalLabel" className="d-inline">Affecter à un paiement</h2>
                        <button type="button" className="btn-close float-end" aria-label="Fermer" onClick={ hideForm }></button>
                    </header>
                    <section>
                        <div className="mb-1">
                            <label htmlFor="organization" className="form-label">Organisation</label>
                            <select className="form-select" id="organization"></select>
                        </div>
                        <div className="mb-1">
                            <label htmlFor="person" className="form-label">Personne</label>
                            <select className="form-select" id="person"></select>
                        </div>
                        <div className="mb-1">
                            <label htmlFor="paymentRequest" className="form-label">Demande de paiement</label>
                            <select className="form-select" id="paymentRequest"></select>
                        </div>
                        { loadingPaymentRequests && <Spinner /> }
                        { errorGetPaymentRequests && <p className="mb-1 alert alert-danger">
                        Une erreur est survenue. Impossible d’afficher les demandes de paiements.
                        </p> }
                        <div className="mb-1">
                            <label htmlFor="allocatedAmount" className="form-label">
                                Montant à affecter (maximum { new Intl.NumberFormat('fr-FR', {
                                    currency: 'EUR',
                                    style: 'currency',
                                }).format(finalMaximum) })
                            </label>
                            <div className="input-group">
                                <input type="text" id="allocatedAmount" className="form-control" defaultValue="0,00" />
                                <span className="input-group-text"> €</span>
                            </div>
                        </div>

                        { errorAllocate && <p className="alert alert-danger">{ errorAllocate }</p> }
                    </section>

                    <footer className="text-center">
                        <button type="button" className="btn btn-warning me-2" onClick={ hideForm }>
                            <i className="fa fa-undo"></i> Annuler
                        </button>
                        <button type="button" className="btn btn-success" onClick={ allocate }>
                            <i className="fa fa-link"></i> Affecter
                        </button>
                    </footer>
                </div> :
                <div className="m-0 row">
                    <div className="col text-center">
                        <button type="button" className="btn btn-success" onClick={showForm}>
                            <i className="fa-solid fa-link"></i> Affecter
                        </button>
                    </div>
                    <div className="col">
                        <AllocationDonation payment={payment} />
                    </div>
                </div>
            }
        </div>
    );
}

export default AllocationAddModal;
