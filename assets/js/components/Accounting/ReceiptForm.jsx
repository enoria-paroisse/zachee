/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, {useEffect, useState} from 'react';

import Spinner from "../Toolbar/Spinner";

function ReceiptForm({ payment, receipt, setFormShow, isUpdate }) {
    const [errorUpdate, setErrorUpdate] = useState(false);
    const [fileUpload, setFileUpload] = useState(!!receipt.file);

    function hideForm() {
        setFormShow(false);
        setErrorUpdate(false);
    }

    function toggleFileUpload() {
        setFileUpload(!fileUpload);
    }

    return (
        <div className={ 'rounded-2 border border-' + (isUpdate ? 'warning' : 'success')+ ' m-2 p-2' }>
            <header className="mb-2">
                <h2 id="addAllocationModalLabel" className="d-inline">{ isUpdate ? 'Regénérer' : 'Générer' } le reçu</h2>
                <button type="button" className="btn-close float-end" aria-label="Fermer" onClick={ hideForm }></button>
            </header>
            <form action={ isUpdate ? '/accounting/receipt/edit/' + receipt.id : '/accounting/receipt/new/' + payment.id } method="POST" encType="multipart/form-data">
                <section>
                    { !isUpdate && <p className="alert alert-success">Générer un reçu validera le paiement.</p> }
                    <div className="row">
                        <div className="col-sm-6 mb-1">
                            <label htmlFor="prefix" className="form-label">
                                Préfixe
                            </label>
                            <input type="text" id="prefix" name="prefix" className="form-control" defaultValue={ receipt.refPrefix ?? new Date(payment.datetime).getUTCFullYear() } />
                        </div>
                        <div className="col-sm-6 mb-1">
                            <label htmlFor="number" className="form-label">
                                Numéro
                            </label>
                            <input type="text" id="number"  name="number" className="form-control" defaultValue={ receipt.refNumber ?? 0 } />
                            <div className="form-text">Mettre 0 pour générer automatiquement ce numéro</div>
                        </div>
                    </div>
                    <div className="form-check form-switch mb-1">
                        <input type="checkbox" id="upload" name="upload" className="form-check-input" checked={ fileUpload } onChange={ toggleFileUpload } />
                        <label htmlFor="upload" className="form-check-label">
                            Enregistrer ou conserver un fichier plutôt que { isUpdate ? 'regénérer' : 'générer' } le reçu
                        </label>
                    </div>
                    { fileUpload &&
                        <div className="mb-1">
                            <label htmlFor="receiptFile" className="form-label">Reçu</label>
                            <input type="file" id="receiptFile" name="receiptFile" placeholder="Choisir un PDF" className="form-control" />
                            <div className="form-text">Laisser vide pour conserver le fichier</div>
                        </div>
                    }
                    <div className="form-check form-switch mb-1">
                        <input type="checkbox" id="sendMail" name="sendMail" className="form-check-input" defaultChecked={ true } />
                        <label htmlFor="sendMail" className="form-check-label">
                            Envoyer le reçu par mail
                        </label>
                    </div>
                    { errorUpdate && <p className="alert alert-danger">{ errorUpdate }</p> }
                </section>

                <footer className="text-center">
                    <button className="btn btn-warning me-2" onClick={ hideForm } type="reset">
                        <i className="fa fa-undo"></i> Annuler
                    </button>
                    <button className="btn btn-success" type="submit">
                        <i className="fa fa-arrows-rotate"></i> { isUpdate ? 'Regénérer' : 'Générer' } le reçu
                    </button>
                </footer>
            </form>
        </div>
    );
}

export default ReceiptForm;