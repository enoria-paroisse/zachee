/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, {useEffect, useState} from 'react';
import {createRoot} from "react-dom/client";

import Spinner from "../Toolbar/Spinner";
import AllocationsList from "./AllocationsList";
import AllocationsStatus from "./AllocationsStatus";

function Allocations({ paymentId, canEdit }) {
    canEdit = canEdit === '1';

    const [payment, setPayment] = useState( {
        fake: true,
        amount: 0,
        amountAllocated: 0,
        paymentAllocations: [],
        status: undefined,
    });
    const [paymentLoaded, setPaymentLoaded] = useState(false);
    const [paymentError, setPaymentError] = useState(false);
    const [loadingPayment, setLoadingPayment] = useState(false);

    function loadPayment(id) {
        const oldStatus = payment.status;
        setPaymentError(false);
        setLoadingPayment(true);
        return fetch('/api/payments/' + id)
            .then(response => {
                setLoadingPayment(false);
                if (!response.ok) {
                    console.error(response.status);
                    setPaymentError(true);

                    return Promise.reject();
                }
                return response.text();
            })
            .then(response => {
                response = JSON.parse(response);
                response.amountAllocated = Number(response.amountAllocated);
                response.amount = Number(response.amount);
                setPayment(response);

                setPaymentLoaded(true);

                return response;
            })
            .then(payment => {
                const badgePaymentStatus = document.getElementById('badgePaymentStatus');
                const headerPayment = document.getElementById('headerPayment');

                if (payment.status === 'validated') {
                    badgePaymentStatus.classList.remove('bg-success', 'bg-warning', 'bg-danger');
                    badgePaymentStatus.classList.add('bg-success');
                    badgePaymentStatus.innerText = 'Validée';
                    headerPayment.classList.remove('bg-gradient-directional-success', 'bg-gradient-directional-warning', 'bg-gradient-directional-danger', 'bg-gradient-directional-primary');
                    headerPayment.classList.add('bg-gradient-directional-success')
                } else if (payment.status === 'canceled') {
                    badgePaymentStatus.classList.remove('bg-success', 'bg-warning', 'bg-danger', 'bg-primary');
                    badgePaymentStatus.classList.add('bg-danger');
                    badgePaymentStatus.innerText = 'Annulée';
                    headerPayment.classList.remove('bg-gradient-directional-success', 'bg-gradient-directional-warning', 'bg-gradient-directional-danger', 'bg-gradient-directional-primary');
                    headerPayment.classList.add('bg-gradient-directional-danger')
                } else if (payment.status === 'allocated') {
                    badgePaymentStatus.classList.remove('bg-success', 'bg-warning', 'bg-danger', 'bg-primary');
                    badgePaymentStatus.classList.add('bg-primary');
                    badgePaymentStatus.innerText = 'Affectée';
                    headerPayment.classList.remove('bg-gradient-directional-success', 'bg-gradient-directional-warning', 'bg-gradient-directional-danger', 'bg-gradient-directional-primary');
                    headerPayment.classList.add('bg-gradient-directional-primary')
                } else if (payment.status === 'registered') {
                    badgePaymentStatus.classList.remove('bg-success', 'bg-warning', 'bg-danger', 'bg-primary');
                    badgePaymentStatus.classList.add('bg-warning');
                    badgePaymentStatus.innerText = 'Enregistrée';
                    headerPayment.classList.remove('bg-gradient-directional-success', 'bg-gradient-directional-warning', 'bg-gradient-directional-danger', 'bg-gradient-directional-primary');
                    headerPayment.classList.add('bg-gradient-directional-warning')
                }

                if (oldStatus !== undefined && oldStatus !== payment.status) {
                    window.location.reload();
                }

                return payment;
            });
    }

    useEffect(() => {
        if ((!payment || payment.fake) && !paymentLoaded && !loadingPayment && !paymentError) loadPayment(paymentId);
    }, [payment]);

    return (
        <div className="row">
            <div className="col">
                <div className="card">
                    <div className="card-body">
                        <div className="row align-items-center">
                            <h2 className="col-6"><i className="fa fa-file-invoice"></i> Affectation</h2>
                            <p className="col-6 text-end">
                                { paymentLoaded && <AllocationsStatus payment={ payment } /> }
                            </p>
                        </div>

                        { paymentLoaded && <AllocationsList canEdit={ canEdit } payment={ payment } loadPayment={ loadPayment } /> }
                        { loadingPayment && <Spinner /> }
                        { paymentError && <p className="mb-1 alert alert-danger">
                            Une erreur est survenue. Impossible de charger les affections.
                        </p> }
                    </div>
                </div>
            </div>
        </div>
    );
}

const allocations = document.querySelectorAll('.allocations');
allocations.forEach((allocationList) => {
    const root = createRoot(allocationList);
    root.render(
        <Allocations
            paymentId={ allocationList.getAttribute('data-enoria-payment') }
            canEdit={ allocationList.getAttribute('data-enoria-can-edit') }
        />
    );
});


export default Allocations;
