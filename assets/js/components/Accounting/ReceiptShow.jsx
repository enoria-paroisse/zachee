/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, {useEffect, useState} from 'react';

import Spinner from "../Toolbar/Spinner";
import ReceiptDisplayFile from "./ReceiptDisplayFile";
import ReceiptForm from "./ReceiptForm";

function ReceiptShow({ canEdit, payment, receiptUri }) {
    const [formShow, setFormShow] = useState(false);

    const [receipt, setReceipt] = useState(null);
    const [receiptLoaded, setReceiptLoaded] = useState(false);
    const [receiptError, setReceiptError] = useState(false);
    const [loadingReceipt, setLoadingReceipt] = useState(false);

    function showForm() {
        setFormShow(true);
    }

    function loadReceipt() {
        setReceiptError(false);
        setLoadingReceipt(true);
        fetch(receiptUri)
            .then(response => {
                setLoadingReceipt(false);
                if (!response.ok) {
                    console.error(response.status);
                    setReceiptError(true);

                    return Promise.reject();
                }
                return response.text();
            })
            .then(response => {
                response = JSON.parse(response);
                setReceipt(response);
                setReceiptLoaded(true);
            });
    }

    useEffect(() => {
        if (!receipt && !receiptLoaded && !loadingReceipt && !receiptError) loadReceipt();
    });

    function update() {}

    return (
        <div>
            { receiptLoaded && <div className="row">
                <div className="col-xxl-6">
                {
                    formShow ?
                        <ReceiptForm payment={ payment } receipt={ receipt } setFormShow={ setFormShow } isUpdate={ true } /> :
                        (canEdit ? <div>
                            <p className="text-center h1 fw-bold">N<sup>o</sup> { receipt.ref }</p>
                            <p className="text-center mt-4">
                                <button className="btn btn-warning" onClick={ showForm }><i className="fa-solid fa-arrows-rotate"></i> Regénérer</button>
                            </p>
                        </div> : <div></div>)
                }
                </div>
                <div className="col-xxl-6 p-2"><ReceiptDisplayFile file={ receipt.file } /></div>
            </div> }
            { loadingReceipt && <Spinner /> }
            { receiptError && <p className="mb-1 alert alert-danger">
                Une erreur est survenue. Impossible de charger le reçu.
            </p> }
        </div>
    );
}

export default ReceiptShow;
