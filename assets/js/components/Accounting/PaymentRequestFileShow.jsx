/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, {useEffect, useState} from 'react';

import Spinner from "../Toolbar/Spinner";
import PaymentRequestFileForm from "./PaymentRequestFileForm";
import PaymentRequestDisplayFile from "./PaymentRequestDisplayFile";

function PaymentRequestFileShow({ paymentRequest, fileUri, canEdit }) {
    const [formShow, setFormShow] = useState(false);

    const [file, setFile] = useState(null);
    const [fileLoaded, setFileLoaded] = useState(false);
    const [fileError, setFileError] = useState(false);
    const [loadingFile, setLoadingFile] = useState(false);

    function showForm() {
        setFormShow(true);
    }

    function loadFile() {
        setFileError(false);
        setLoadingFile(true);
        fetch(fileUri)
            .then(response => {
                setLoadingFile(false);
                if (!response.ok) {
                    console.error(response.status);
                    setFileError(true);

                    return Promise.reject();
                }
                return response.text();
            })
            .then(response => {
                response = JSON.parse(response);
                setFile(response);
                setFileLoaded(true);
            });
    }

    useEffect(() => {
        if (!file && !fileLoaded && !loadingFile && !fileError) loadFile();
    });

    function update() {}

    return (
        <div>
            { fileLoaded && <div className="row">
                <div className="col-xxl-6">
                {
                    formShow ?
                        <PaymentRequestFileForm paymentRequest={ paymentRequest } file={ file } setFormShow={ setFormShow } isUpdate={ true } /> :
                        (canEdit ? <div>
                            <p className="text-center h1 fw-bold">N<sup>o</sup> { file.ref }</p>
                            <p className="text-center mt-4">
                                <button className="btn btn-warning" onClick={ showForm }><i className="fa-solid fa-arrows-rotate"></i> Regénérer</button>
                            </p>
                        </div> : <div><p className="text-center h1 fw-bold">N<sup>o</sup> { file.ref }</p></div>)
                }
                </div>
                <div className="col-xxl-6 p-2"><PaymentRequestDisplayFile file={ file.file } /></div>
            </div> }
            { loadingFile && <Spinner /> }
            { fileError && <p className="mb-1 alert alert-danger">
                Une erreur est survenue. Impossible de charger le fichier.
            </p> }
        </div>
    );
}

export default PaymentRequestFileShow;
