/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, {useState} from 'react';

import {checkAmount} from "./allocationsUtils";

function AllocationShow({ allocation, amount, payment, paymentRequest, paymentRequestAmount, loadPayment }) {
    const [formShow, setFormShow] = useState(false);
    const finalMaximum = Math.round(Math.min(paymentRequestAmount, payment.amount - payment.amountAllocated + amount ?? 0) * 100) / 100;
    const [errorAllocate, setErrorAllocate] = useState(false);

    function showForm() {
        setFormShow(true);
    }

    function hideForm() {
        setFormShow(false);
        setErrorAllocate(false);
    }

    function allocate() {
        setErrorAllocate(false);
        const allocatedAmount = Number(document.getElementById('allocatedAmount').value.replace(',', '.'));

        if (allocatedAmount === 0)
            fetch('/api/payment_allocations/' + allocation.id, {
                method: 'DELETE',
            }).then(response => {
                if (!response.ok) {
                    console.error(response.status);
                    setErrorAllocate('Impossible de supprimer la demande de paiement');
                    return;
                }
                loadPayment(payment.id).then(() => {
                    setFormShow(false);
                });
            });
        else if (checkAmount(payment, paymentRequest, allocatedAmount, finalMaximum, setErrorAllocate, true))
            fetch('/api/payment_allocations/' + allocation.id, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    amount: allocatedAmount + '',
                }),
            }).then(response => {
                if (!response.ok) {
                    console.error(response.status);
                    setErrorAllocate('Impossible d’ajouter la demande de paiement');
                    return;
                }
                loadPayment(payment.id).then(() => {
                    setFormShow(false);
                });
            });
    }

    return (
        <div className="col-md-12 col-xl-6">{
            formShow ?
                <div className="rounded-2 border border-warning m-2 p-2">
                    <header className="mb-2">
                        <h2 id="addAllocationModalLabel" className="d-inline">Modifier une affectation</h2>
                        <button type="button" className="btn-close float-end" aria-label="Fermer" onClick={ hideForm }></button>
                    </header>
                    <section>
                        <p className="mb-1 alert alert-info">
                            En mettant le montant à 0,00&nbsp;€, vous supprimerez l’affectation.
                        </p>
                        <div className="mb-1">
                            <label htmlFor="allocatedAmount" className="form-label">
                                Montant à affecter (maximum { new Intl.NumberFormat('fr-FR', {
                                    currency: 'EUR',
                                    style: 'currency',
                                }).format(finalMaximum) })
                            </label>
                            <div className="input-group">
                                <input type="text" id="allocatedAmount" className="form-control" defaultValue={ Number(amount).toLocaleString('fr-FR') } />
                                <span className="input-group-text"> €</span>
                            </div>
                        </div>

                        { errorAllocate && <p className="alert alert-danger">{ errorAllocate }</p> }
                    </section>

                    <footer className="text-center">
                        <button type="button" className="btn btn-warning me-2" onClick={ hideForm }>
                            <i className="fa fa-undo"></i> Annuler
                        </button>
                        <button type="button" className="btn btn-success" onClick={ allocate }>
                            <i className="fa fa-link"></i> Affecter
                        </button>
                    </footer>
                </div> :
                <div className="rounded-2 border border-dark m-2">
                    <p className="h3 mt-1 fw-bold text-center">
                        { new Intl.NumberFormat('fr-FR', {
                                currency: 'EUR',
                                style: 'currency',
                            }).format(Number(amount)) }
                    </p>
                    <p className="text-center">
                        <a href={ '/accounting/payment_request/' + paymentRequest.id }>{ paymentRequest.description }</a>
                    </p>
                    {
                        payment.status !== 'validated' &&
                        <p className="text-center">
                            <button className="btn btn-warning text-white" onClick={ showForm }>
                                <i className="fas fa-unlink"></i> Désaffecter
                            </button>
                        </p>
                    }
                </div>
            }</div>
        );
}

export default AllocationShow;
