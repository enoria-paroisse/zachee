/**
 * @copyright Copyright (c) 2025 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, {useState} from 'react';

function AllocationDonation({ payment }) {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);

    function allocateDonation() {
        setLoading(true);
        setError(false);

        fetch('/accounting/payment/allocate-donation/'+payment.id, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(response => {
            console.log(response);
            setLoading(false);
            if (!response.ok) {
                setError(true);
                console.error(response.status);
            }
            return response.text();
        }).then(response => {
            const result = JSON.parse(response);
            if (result.error) {
                setError(result.error);
            }
            if (result.success) {
                window.location.reload();
            }
        });
    }

    return (
        <div>
            <p className="text-center">
                {loading ? <button type="button" className="ms-1 btn btn-primary">
                    <i className="spinner-border" role="status"></i>
                </button> : <button type="button" className="ms-1 btn btn-primary" onClick={allocateDonation}>
                    <i className="fa-solid fa-hand-holding-dollar" /> Affecter le solde en don
                </button>}
            </p>

            {error === true && <p className="mb-1 alert alert-danger">
                Une erreur est survenue. Impossible d’affecter le solde en don.
            </p>}
            {(error !== true && error !== false) && <p className="mb-1 alert alert-danger">
                {error}
            </p>}
        </div>
    );
}

export default AllocationDonation;
