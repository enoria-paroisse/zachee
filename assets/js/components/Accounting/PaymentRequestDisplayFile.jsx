/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, {useEffect, useState} from 'react';

import Spinner from "../Toolbar/Spinner";

function PaymentRequestDisplayFile({file}) {
    return (
        <div>
            {
                (file !== null && file !== undefined) ?
                    <iframe src={ '/uploads/paymentRequests/' + file } width="100%" height="500px"></iframe> :
                    <p className="alert alert-warning">Le fichier n’a pas encore été généré ou envoyé sur Zachée.</p>
            }
        </div>
    );
}

export default PaymentRequestDisplayFile;
