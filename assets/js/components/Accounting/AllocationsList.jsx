/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, {useEffect, useState} from 'react';

import AllocationAddModal from "./AllocationAddModal";
import {usePaginatedFetch} from "../../hooks";
import Spinner from "../Toolbar/Spinner";
import AllocationShow from "./AllocationShow";

function AllocationsList({ payment, loadPayment, canEdit }) {
    const [organizationsLoaded, setOrganizationsLoaded] = useState(false);
    const {
        items: organizations,
        load: getOrganizations,
        loading: loadingOrganizations,
        error: errorGetOrganization
    } = usePaginatedFetch('/api/organizations?pagination=0', () => {
        setOrganizationsLoaded(true)
    });
    const [peopleLoaded, setPeopleLoaded] = useState(false);
    const {
        items: people,
        load: getPeople,
        loading: loadingPeople,
        error: errorGetPeople
    } = usePaginatedFetch('/api/people?pagination=0', () => {
        setPeopleLoaded(true)
    });

    const allocations = [];
    if (payment && payment.paymentAllocations) {
        payment.paymentAllocations.forEach(allocation => {
            allocations.push(<AllocationShow
                key={ allocation.id }
                allocation={ allocation }
                amount={ Number(allocation.amount) }
                payment={ payment }
                paymentRequest={ allocation.paymentRequest }
                paymentRequestAmount={ Number(allocation.paymentRequest.amount) }
                loadPayment={ loadPayment }
            />);
        });
    }

    useEffect(() => {
        if (canEdit && !organizationsLoaded && !loadingOrganizations && !errorGetOrganization) {
            getOrganizations();
        }
        if (canEdit && !peopleLoaded && !loadingPeople && !errorGetPeople) {
            getPeople();
        }
    });

    return (
        <div className="row align-items-center">
            { allocations }

            {canEdit && !errorGetOrganization && !errorGetPeople && organizationsLoaded && peopleLoaded && (payment.amountAllocated < payment.amount) && payment.status === 'registered' && <AllocationAddModal
                payment={ payment }
                organization={ payment.organization ? payment.organization.id : null }
                person={ payment.person ? payment.person.id : null }
                organizations={organizationsLoaded ? organizations : []}
                people={peopleLoaded ? people : []}
                loadPayment={ loadPayment }
            />}
            {(payment.amountAllocated >= payment.amount) && ['registered', 'allocated'].indexOf(payment.status) > -1 && <div className="col-md-12 col-xl-6">
                <p className="m-2 alert alert-info">Votre paiement est totalement affecté</p>
            </div>}
            {(loadingOrganizations || loadingPeople) && !errorGetPeople && !errorGetOrganization && <Spinner />}
            {(errorGetPeople || errorGetOrganization) && <div className="col-md-12 col-xl-6">
            <p className="m-2 alert alert-danger">
                Une erreur est survenue. Impossible d’afficher le formulaire d’affectation.
            </p>
            </div>}
        </div>
    );
}

export default AllocationsList;
