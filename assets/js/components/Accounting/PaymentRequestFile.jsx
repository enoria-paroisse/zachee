/**
 * @copyright Copyright (c) 2023 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, {useEffect, useState} from 'react';
import {createRoot} from "react-dom/client";

import Spinner from "../Toolbar/Spinner";
import PaymentRequestFileShow from "./PaymentRequestFileShow";
import PaymentRequestFileAdd from "./PaymentRequestFileAdd";

function PaymentRequestFile({ paymentRequestId, canEdit }) {
    canEdit = canEdit === '1';

    const [flash, setFlash] = useState(false);

    const [paymentRequest, setPaymentRequest] = useState(null);
    const [paymentRequestLoaded, setPaymentRequestLoaded] = useState(false);
    const [paymentRequestError, setPaymentRequestError] = useState(false);
    const [loadingPaymentRequest, setLoadingPaymentRequest] = useState(false);

    const [hasFile, setHasFile] = useState(false);
    const [file, setFile] = useState('');
    const [paymentRequestStatus, setPaymentRequestStatus] = useState('in_progress');

    function addFlash(type, message) {
        setFlash({ type, message });
    }

    function removeFlash() {
        setFlash(false);
    }

    function loadPayment(id) {
        setPaymentRequestError(false);
        setLoadingPaymentRequest(true);
        fetch('/api/payment_requests/' + id)
            .then(response => {
                setLoadingPaymentRequest(false);
                if (!response.ok) {
                    console.error(response.status);
                    setPaymentRequestError(true);

                    return Promise.reject();
                }
                return response.text();
            })
            .then(response => {
                response = JSON.parse(response);
                setPaymentRequest(response);

                if (response.file !== undefined) {
                    setHasFile(true);
                    setFile(response.file);
                } else
                    setHasFile(false);
                setPaymentRequestStatus(response.status);

                setPaymentRequestLoaded(true);
            });
    }

    useEffect(() => {
        if (!paymentRequest && !paymentRequestLoaded && !loadingPaymentRequest && !paymentRequestError)
            loadPayment(paymentRequestId);
    });

    return (
        <div className="row">
            <div className="col">
                <div className="card">
                    <div className="card-body">
                        <div className="row align-items-center">
                            <h2 className="col-6"><i className="fa fa-file-lines"></i> Fichier associé</h2>
                        </div>
                        { flash && <p className={ 'm-2 alert alert-' + flash.type + ' alert-dismissible' }>
                            { flash.message }
                            <button type="button" className="btn-close" aria-label="Fermer" onClick={ removeFlash }></button>
                        </p> }

                        { paymentRequestLoaded &&
                            (hasFile ?
                                <PaymentRequestFileShow canEdit={ canEdit } paymentRequest={ paymentRequest } fileUri={ file } addFlash={ addFlash } /> :
                                (['in_progress', 'completed'].indexOf(paymentRequestStatus) > -1 && canEdit ?
                                    <PaymentRequestFileAdd paymentRequest={ paymentRequest } addFlash={ addFlash } /> :
                                    (canEdit ? <div>
                                        <p className="alert alert-info m-2">Vous ne pouvez pas ajouter de fichier à une demande de paiement annulée.</p>
                                        <p className="text-center m-2"><button className="btn btn-primary" onClick={ () => loadPayment(paymentRequestId) }>Actualiser</button></p>
                                    </div> : <div>
                                        <p className="alert alert-info m-2">Aucun fichier à afficher.</p>
                                    </div>)
                                )
                            )
                        }
                        { loadingPaymentRequest && <Spinner /> }
                        { paymentRequestError && <p className="mb-1 alert alert-danger">
                            Une erreur est survenue. Impossible de charger le fichier.
                        </p> }
                    </div>
                </div>
            </div>
        </div>
    );
}

const allocations = document.querySelectorAll('.payment-request-file');
allocations.forEach((allocationList) => {
    const root = createRoot(allocationList);
    root.render(
        <PaymentRequestFile
            paymentRequestId={ allocationList.getAttribute('data-enoria-payment-request') }
            canEdit={ allocationList.getAttribute('data-enoria-can-edit') }
        />
    );
});


export default PaymentRequestFile;
