/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';

class DataTablesSelectButton extends React.Component {
  constructor(props) {
    super(props);

    if (props.type === 'select-all') {
      this.icon = 'check-square';
      this.label = 'Sélectionner tout';
    } else if (props.type === 'unselect-all') {
      this.icon = 'square';
      this.label = 'Déselectionner tout';
    } else if (props.type === 'select-search') {
      this.icon = 'check-square';
      this.label = 'Sélectionner les résultats filtrés';
    } else {
      throw new Error('The type need to be select-all');
    }

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    const table = new DataTable('#' + this.props.dataTables);

    if (this.props.type === 'select-all') {
      table.rows().select();
    } else if (this.props.type === 'unselect-all') {
      table.rows().deselect();
    } else if (this.props.type === 'select-search') {
      table.rows({ search: 'applied' }).select();
    }
  }

  render() {
    return (
      <button className="dropdown-item" href="#" onClick={this.handleClick}>
        <span className="badge rounded-pill bg-light text-white me-1">
          <i className={'fa fa-' + this.icon}></i>
        </span>
        {this.label}
      </button>
    );
  }
}

export default DataTablesSelectButton;
