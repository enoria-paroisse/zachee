/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import { createRoot } from "react-dom/client";

import DataTablesSelectButton from './DataTablesSelectButton';
import MailModal from './MailModal';

class Toolbar extends React.Component {
  constructor(props) {
    super(props);

    if (!props.type) {
      throw new Error('Attribute type is missing');
    } else if (!props.dataTables) {
      throw new Error('Attribute dataTables is missing');
    }
  }

  render() {
    return (
      <div style={{textAlign: 'left'}}>
        <div className="btn-group float-end" role="group">
          <div className="btn-group" role="group">
            <button id={'btnSelectDatatables-' + this.props.dataTables} type="button" className="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
              <i className="fa fa-check-square" />
            </button>
            <ul className="dropdown-menu" aria-labelledby={'btnSelectDatatables-' + this.props.dataTables}>
              <li>
                <DataTablesSelectButton dataTables={this.props.dataTables} type="select-all" />
              </li>
              <li>
                <DataTablesSelectButton dataTables={this.props.dataTables} type="unselect-all" />
              </li>
              <li>
                <DataTablesSelectButton dataTables={this.props.dataTables} type="select-search" />
              </li>
            </ul>
          </div>
          <button type="button" className="btn btn-light text-dark" data-bs-toggle="modal" data-bs-target={'#mail-modal-' + this.props.dataTables}>
            <i className="fa fa-envelope" />
            &nbsp;Mail
          </button>
        </div>
        <MailModal dataTables={this.props.dataTables} type={this.props.type} />
      </div>
    );
  }
}

const toolbars = document.querySelectorAll('.toolbar');
toolbars.forEach((toolbar) => {
  const root = createRoot(toolbar);
  root.render(
      <Toolbar type={toolbar.getAttribute('data-enoria-type')} dataTables={toolbar.getAttribute('data-enoria-for')} />
  );
});

export default Toolbar;
