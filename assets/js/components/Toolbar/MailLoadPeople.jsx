/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, { useState, useEffect } from 'react';

import { usePaginatedFetch } from '../../hooks';

import MailList from './MailList';
import Spinner from './Spinner';

function MailLoad({ ids }) {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    const [people, setPeople] = useState([]);

    useEffect(() => {
        setLoading(true);
        setError(false);

        const requests = [];
        let requestTmp = [];
        let i = 0;
        ids.forEach((id) => {
            i++;
            if (i > 50) {
                requests.push(requestTmp);
                requestTmp = [];
                i = 0;
            }
            requestTmp.push(id);
        });
        if (requestTmp.length > 0) requests.push(requestTmp);

        const params = {
            method: 'GET',
            headers: {
                Accept: 'application/ld+json',
                'Content-Type': 'application/json',
            },
        };

        const promises = [];
        requests.forEach((request, id) => {
            let peopleQuery = '';
            request.forEach((id) => {
                peopleQuery += 'id[]=' + id + '&';
            });
            promises.push(fetch('/api/people?pagination=false&' + peopleQuery, params)
                .then(function(response) {
                    if (response.status === 204) return null;
                    if (response.ok) return response.json();
                    else return Promise.reject();
                })
                .then(function(response) {
                    setPeople(prevState => [...prevState, ...response['hydra:member']]);
                })
                .catch(() => {
                    setError(true)
                }));
        });
        Promise.all(promises).then(() => {
            setLoading(false);
        });
    }, [ids]);

    return (
        <div>
            { !loading && <MailList people={ people } /> }
            { loading && <Spinner /> }
            { error && <p className="alert alert-danger">Une erreur est survenue</p> }
        </div>
    );
}

export default MailLoad;
