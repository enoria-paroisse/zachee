/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, { useEffect } from 'react';

import { usePaginatedFetch } from '../../hooks';

import AssociatedPeopleTypes from './AssociatedPeopleTypes';

function MailLoadOrganizations({ ids }) {
  const { items: types, load: getTypesAssociatedPeople, loading: loadingGetTypesAssociatedPeople, error: errorGetTypesAssociatedPeople } = usePaginatedFetch('/api/type_associated_people?pagination=false');

  useEffect(() => {
    getTypesAssociatedPeople();
  }, []);

  return (
    <div>
      <AssociatedPeopleTypes types={types} organizations={ids} loading={loadingGetTypesAssociatedPeople} />
      {errorGetTypesAssociatedPeople && <p className="mt-1 alert alert-danger">Une erreur est survenue</p>}
    </div>
  );
}

export default MailLoadOrganizations;
