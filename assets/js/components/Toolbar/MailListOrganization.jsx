/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, { useEffect, useState } from 'react';

import MailList from './MailList';
import Spinner from './Spinner';

function MailListOrganization({ organizations, associatedPeople }) {
  const [people, setPeople] = useState([]);
  const [emptyOrganizations, setEmptyOrganizations] = useState([]);
  const [finished, setFinished] = useState(false);

  useEffect(() => {
    const peopleTmp = [];
    const emptyOrganizationsTmp = [];
    const checkEmpy = {};
    organizations.forEach((organization) => {
      checkEmpy[organization.id] = 0;
    });

    associatedPeople.forEach((associatedPerson) => {
      peopleTmp.push(associatedPerson.person);
      checkEmpy[associatedPerson.organization.id]++;
    });
    organizations.forEach((organization) => {
      if (checkEmpy[organization.id] === 0) emptyOrganizationsTmp.push(organization);
    });
    setPeople(peopleTmp);
    setEmptyOrganizations(emptyOrganizationsTmp);
    setFinished(true);
  }, []);

  return (
    <div>
      {finished && emptyOrganizations.length > 0 && (
        <div className="mt-1 alert alert-warning">
          <p>Aucun contact, appartenant aux types sélectionnés, n’a été trouvé pour les organisations suivantes :</p>
          <ul>
            {emptyOrganizations.map((organization) => (
              <li key={organization.id}>{organization.name}</li>
            ))}
          </ul>
        </div>
      )}
      {finished ? <MailList people={people} /> : <Spinner />}
    </div>
  );
}

export default MailListOrganization;
