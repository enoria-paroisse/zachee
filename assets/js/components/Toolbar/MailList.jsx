/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';

class MailList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      copied: false,
      mails: [],
      empty: [{ id: 1, title: { name: 'M.'}, firstname: 'Grégoire', lastname: 'Le Beau Gosse'}],
    };
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    let mails = [];
    const empty = [];
    this.props.people.forEach((person) => {
      if (!person.email) empty.push(person);
      else mails.push(person.email);
    });
    mails = [...new Set(mails)];
    this.setState({
      mails,
      empty,
    });
  }

  handleClick() {
    if (window.isSecureContext) {
      navigator.clipboard.writeText(this.state.mails.join('; ')).then(() => {
        this.setState({
          copied: true,
        });
      });
    }
  }

  render() {
    return (
      <div>
        {this.state.empty.length > 0 && (
          <div className="alert alert-warning">
            <p>Les personnes suivantes n’ont pas d’adresse mail :</p>
            <ul>
              {this.state.empty.map((person) => (
                <li key={person.id}>
                  {person.title.name} {person.firstname} <span className="smallcaps">{person.lastname}</span>
                </li>
              ))}
            </ul>
          </div>
        )}
        <p>Liste des adresses mail à copier&nbsp;:</p>
        {this.state.mails.length ? (
          <p className="alert alert-light">
            {this.state.mails.map((mail) => {
              return mail + '; ';
            })}
          </p>
        ) : (
          <p className="alert alert-light text-center">
            <em>Aucune adresse mail trouvée</em>
          </p>
        )}
        {window.isSecureContext && this.state.mails.length ? (
          <div className="d-flex justify-content-end">
            {this.state.copied ? (
              <div className="flex-grow-1 me-1">
                <p className="alert alert-success">La liste a été copiée dans le presse-papiers</p>
              </div>
            ) : (
              ''
            )}
            <p className="text-end">
              <button className="btn btn-primary" onClick={this.handleClick}>
                <i className="fa fa-clone"></i> Copier
              </button>
            </p>
          </div>
        ) : (
          ''
        )}
      </div>
    );
  }
}

export default MailList;
