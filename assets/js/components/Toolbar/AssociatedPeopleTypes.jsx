/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React, { useEffect, useState } from 'react';

import AssociatedPeople from './AssociatedPeople';
import Spinner from './Spinner';

function TypeCheckbox({ type, isSelected, onCheckboxChange }) {
  return (
    <div className="form-check form-switch">
      <input className="form-check-input" type="checkbox" name={type.id} id={'type-' + type.id} checked={isSelected} onChange={onCheckboxChange} />
      <label className="form-check-label" htmlFor={'type-' + type.id}>
        <i className={'me-1 fa fa-' + type.icon} />
        {type.name}
      </label>
    </div>
  );
}

function AssociatedPeopleTypes({ types, organizations, loading }) {
  const [checkboxes, setCheckboxes] = useState({});
  const [typesSelected, setTypesSelected] = useState([]);
  const [empty, setEmpty] = useState(false);
  const [finished, setFinished] = useState(false);
  const [loaded, setLoaded] = useState(false);

  const checkTypeAll = { id:'all', name : "Tous", icon: 'check'}
  useEffect(() => {
    const checkboxesTmp = {};
    types.forEach((type) => {
      checkboxesTmp[type.id] = false;
    });
    setCheckboxes(checkboxesTmp);
  }, [loading]);

  useEffect(() => {
    if (Object.keys(checkboxes).length) setLoaded(true);
  }, [checkboxes]);

  const handleCheckboxChange = (changeEvent) => {
    let { name } = changeEvent.target;
    name = Number(name);

    setCheckboxes((prevState) => ({
      ...prevState,
      [name]: !prevState[name],
    }));
    if (empty) setEmpty(false);
    if (finished) setFinished(false);
  };

  const toggleAllCheboxes = event => {
    const checkboxesTmp = {};
    types.forEach((type) => {
      checkboxesTmp[type.id] = event.target.checked;
    });
    setCheckboxes(checkboxesTmp);
  }

  const handleValidate = () => {
    const typeSelectedTmp = [];
    types.forEach((type) => {
      if (checkboxes[type.id]) typeSelectedTmp.push(type.id);
    });
    setTypesSelected(typeSelectedTmp);
    if (typeSelectedTmp.length) setFinished(true);
    else setEmpty(true);
  };

  return (
    <div className="row">
      <div className="col-12 col-lg-8">
        <p className="alert alert-info">À quel(s) type(s) de contacts souhaitez-vous envoyer un mail ?</p>
      </div>
      <div className="col-12 col-lg-4">
        {loaded && types.map((type) => (
          <TypeCheckbox key={type.id} type={type} isSelected={checkboxes[type.id]} onCheckboxChange={handleCheckboxChange} />
        ))}
        <TypeCheckbox key='all' type={checkTypeAll} onCheckboxChange={toggleAllCheboxes} />
        {loading && <Spinner />}
        <div className="text-center">
          <button className="mt-1 btn btn-outline-primary" onClick={handleValidate}>
            Valider
          </button>
        </div>
      </div>
      {empty && (
        <div className="col-12">
          <p className="mt-1 alert alert-warning">Vous devez sélectionner au moins un type</p>
        </div>
      )}
      {finished && <AssociatedPeople organizations={organizations} types={typesSelected} />}
    </div>
  );
}

export default AssociatedPeopleTypes;
