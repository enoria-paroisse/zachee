/**
 * @copyright Copyright (c) 2020 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Zachée Association Enoria.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

import React from 'react';
import MailLoadPeople from './MailLoadPeople';
import MailLoadOrganizations from './MailLoadOrganizations';

class MailModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      ids: [],
    };

    if (this.props.type !== 'person' && this.props.type !== 'organization') {
      throw new Error('The type need to be person or organization, "' + this.props.type + '" gived');
    }
  }
  componentDidMount() {
    const modal = document.getElementById('mail-modal-' + this.props.dataTables);

    modal.addEventListener('show.bs.modal', () => {
      const table = new DataTable('#' + this.props.dataTables);
      const rowsSelected = table.rows({ selected: true }).nodes();
      this.setState({
        ids: $.map(rowsSelected, row => {
          return $(row).data(this.props.type + '-id');
        }),
      });
    });
    modal.addEventListener('hide.bs.modal', () => {
      this.setState({
        ids: [],
      });
    });
  }

  render() {
    return (
      <div className="modal fade" id={'mail-modal-' + this.props.dataTables} tabIndex="-1" aria-hidden="true">
        <div className="modal-dialog modal-lg">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Exporter les adresses mail</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              {this.state.ids.length > 0 ? (
                this.props.type === 'person' ? (
                  <MailLoadPeople ids={this.state.ids} />
                ) : (
                  <MailLoadOrganizations ids={this.state.ids} />
                )
              ) : (
                <p className="alert alert-warning text-center">
                  <em>Aucune ligne sélectionnée</em>
                </p>
              )}
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">
                Fermer
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MailModal;
